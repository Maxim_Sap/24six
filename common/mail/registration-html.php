<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */
Yii::$app->urlManager->setHostInfo('https://'.$_SERVER['HTTP_HOST']);
$registerLink = Yii::$app->urlManager->createAbsoluteUrl(['login/check-email-link', 'token' => $user->password_reset_token]);

?>
<div class="password-reset">
    <p>Hello, <?= Html::encode($user->username) ?>,</p>
    <p>Thanks for registering on our site</p>
    <p>To complete the registration, please click on the following link:</p>
    <p><?= Html::a(Html::encode($registerLink), $registerLink) ?></p>
</div>
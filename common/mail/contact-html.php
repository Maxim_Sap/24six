<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

?>
<div>
    <p>Hi, <?= Html::encode($name) ?>,</p>
    <p>Thank you for your message.</p>
    <p>We will answer you nearest time.</p>
    <p>Best regards, <?= Html::a(Html::encode('Mitzvot'), 'https://'.$_SERVER['HTTP_HOST']) ?></p>
    <p>Copy:</p>
    <p>Name: <?=$name?></p>
    <p>E-mail: <?=$email?></p>
    <p>Message text: <?=$text?></p>
</div>
<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

Yii::$app->urlManager->setHostInfo('https://'.$_SERVER['HTTP_HOST']);
$registerLink = Yii::$app->urlManager->createAbsoluteUrl(['login/check-email-link', 'token' => $user->password_reset_token]);
?>
    Hello, <?= $user->username?>,
    Thanks for registering on our site
    To complete the registration, please click on the following link:

<?= $registerLink ?>
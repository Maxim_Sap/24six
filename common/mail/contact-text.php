<?php
    use yii\helpers\Html;
?>
Hi, <?= Html::encode($name) ?>,
Thank you for your message.
We will answer you nearest time.
Best regards, <?= Html::a(Html::encode('Mitzvot'), 'https://'.$_SERVER['HTTP_HOST'])?>
Copy:
Name: <?=$name?>
E-mail: <?=$email?>
Message text: <?=$text?>

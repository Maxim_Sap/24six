<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "countryes".
 *
 * @property int $id
 * @property string $country
 *
 * @property PartnerEvents[] $partnerEvents
 * @property SpaceWork[] $spaceWorks
 */
class Countryes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countryes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country'], 'required'],
            [['country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'country' => Yii::t('frontend', 'City'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerEvents()
    {
        return $this->hasMany(PartnerEvents::className(), ['id_country' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpaceWorks()
    {
        return $this->hasMany(SpaceWork::className(), ['id_country' => 'id']);
    }
}

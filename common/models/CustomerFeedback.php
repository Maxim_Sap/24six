<?php

namespace common\models;

use foo\bar;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "customer_feedback".
 *
 * @property int $id
 * @property int $user_id
 * @property string $review
 * @property string $social
 */
class CustomerFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */


    const TYPE_EVENT_FEEDBACK = 1;
    const TYPE_PLACE_FEEDBACK = 2;

    public static function tableName()
    {
        return 'customer_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'review', 'social'], 'required'],
            [['user_id'], 'integer'],
            [['review', 'social'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'review' => Yii::t('frontend', 'Review'),
            'social' => Yii::t('frontend', 'Social'),
        ];
    }

    public static function getFeedbackCustomers($type = null)
    {
        $reviewQuery = (new Query())
            ->select('
				feedback.review,
				user.username,
				user_prof.last_name,
				feedback.type_feedback
			')
            ->from(CustomerFeedback::tableName()." feedback")
            ->leftJoin(User::tableName()." user",'user.id = feedback.user_id ')
            ->leftJoin(UserProfile::tableName()." user_prof",'user_prof.user_id = feedback.user_id');
            if(!empty($type)) {
                switch ($type) {
                    case 1 :
                        $reviewQuery->where(['type_feedback' => CustomerFeedback::TYPE_EVENT_FEEDBACK]);
                        break;
                    case 2 :
                        $reviewQuery->where(['type_feedback' => CustomerFeedback::TYPE_PLACE_FEEDBACK]);
                        break;
                }
            }

        return $review = $reviewQuery->all();
    }
}

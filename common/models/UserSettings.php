<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_settings".
 *
 * @property int $id
 * @property int $user_id
 * @property int $share_social
 * @property int $settings_posts
 * @property int $settings_comments
 * @property int $settings_add_friends
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id', 'share_social', 'settings_posts', 'settings_comments', 'settings_add_friends'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'share_social' => Yii::t('frontend', 'Share Social'),
            'settings_posts' => Yii::t('frontend', 'Settings Posts'),
            'settings_comments' => Yii::t('frontend', 'Settings Comments'),
            'settings_add_friends' => Yii::t('frontend', 'Settings Add Friends'),
        ];
    }
}

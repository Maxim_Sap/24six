<?php

	namespace common\models;

	use Yii;

	/**
	 * This is the model class for table "user_bonus".
	 *
	 * @property int        $id
	 * @property int        $id_user
	 * @property int        $id_bonus
	 *
	 * @property User       $user
	 * @property BonusPlans $bonus
	 */
	class UserBonus extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		const SCENARIO_UPDATE_BONUS_TYPE = 'update_bonus_type';

		public static function tableName()
		{
			return 'user_bonus';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'id_user', 'id_bonus' ], 'required' ],
				[ [ 'id_bonus' ], 'required', 'on' => self::SCENARIO_UPDATE_BONUS_TYPE ],
				[ [ 'id_user', 'id_bonus' ], 'integer' ],
				[ [ 'id_user' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'id_user' => 'id' ] ],
				[ [ 'id_bonus' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => BonusPlans ::className(), 'targetAttribute' => [ 'id_bonus' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'       => Yii ::t( 'frontend', 'ID' ),
				'id_user'  => Yii ::t( 'frontend', 'User' ),
				'id_bonus' => Yii ::t( 'frontend', 'Bonus' ),
			];
		}

		public function scenarios()
		{
			$scenarios = parent::scenarios();
			$scenarios[self::SCENARIO_UPDATE_BONUS_TYPE] = ['id_bonus'];

			return $scenarios;
		}
		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'id_user' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getBonus()
		{
			return $this -> hasOne( BonusPlans ::className(), [ 'id' => 'id_bonus' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getPlan()
		{
			return $this -> hasOne( Plans ::className(), [ 'id' => 'id_bonus' ] );
		}

		public function getPersonalBonus()
		{
			return $this -> hasOne( UserMainPersonalBonus ::className(), [ 'user_id' => 'id_user' ] );
		}
		public static function addBonusForUser( $user_id, $bonus_id )
		{
			$row = self ::find()
						-> where( [ 'id_user' => $user_id ] )
						-> andWhere( [ 'id_bonus' => $bonus_id ] )
						-> one();

			if( $row == NULL && !empty( $row ) )
			{
				$model             = new self();
				$model -> id_user  = $user_id;
				$model -> id_bonus = $bonus_id;
				if( !$model -> save() )
				{
					dd( $model -> errors );
				}
			}
			else
			{
				UserBonus ::deleteAll( [ 'id_user' => $user_id ] );
				$model 				= new self();
				$model -> id_user 	= $user_id;
				$model -> id_bonus 	= $bonus_id;
				if( !$model->save() )
				{
					dd( $model -> errors );
				}
			}
		}
	}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories_type_event".
 *
 * @property int $id
 * @property string $category
 *
 * @property CategoriesToPlase[] $categoriesToPlases
 * @property Events[] $events
 */
class CategoriesTypeEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_type_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'category' => Yii::t('frontend', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesToPlases()
    {
        return $this->hasMany(CategoriesToPlase::className(), ['category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['id_cate_event' => 'id']);
    }
}

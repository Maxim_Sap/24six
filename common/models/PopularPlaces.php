<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "popular_places".
 *
 * @property int $id
 * @property int $user_id
 * @property int $id_place
 *
 * @property SpaceWork $place
 * @property UserProfile $user
 */
class PopularPlaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'popular_places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'id_place'], 'required'],
            [['user_id', 'id_place'], 'integer'],
            [['id_place'], 'exist', 'skipOnError' => true, 'targetClass' => SpaceWork::className(), 'targetAttribute' => ['id_place' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User'),
            'id_place' => Yii::t('frontend', 'Place'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(SpaceWork::className(), ['id' => 'id_place']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }
}

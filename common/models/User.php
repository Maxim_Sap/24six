<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $last_activity
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NO_ACTIVE = 5;
    const STATUS_ACTIVE = 10;

    const USER_DEFAULT     = 1;
    const USER_ORGANIZATION      = 2;
    const USER_ORGANIZATION_ADMIN = 3;
    const USER_CONTENT_MANAGER  = 6;
    const USER_ADMIN       = 7;
    const USER_SUPER_ADMIN  = 8;


    const ROLE_NAME_USER = 'user';
    const ROLE_NAME_ORGANIZATION = 'organization';
    const ROLE_NAME_ORGANIZATION_ADMIN = 'organizationAdmin';
    const ROLE_NAME_CONTENT_MANAGER = 'contentManager';
    const ROLE_NAME_ADMIN = 'admin';
    const ROLE_NAME_SUPER_ADMIN = 'superAdmin';

    public $password;
    public $new_password;
    public $confirm_new_password;

    const SCENARIO_EDIT_PASSW = 'edit_profile_password';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NO_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED,self::STATUS_NO_ACTIVE]],

            /* update user in account */
            [['username', 'email'], 'required'],
            [['username'], 'string', 'max' => 255],

            [['email'], 'email','on' => 'update'],
            [['email'], 'unique' , 'on' => 'update'],

            [['password','confirm_new_password','new_password'], 'string'],
            [['password','confirm_new_password','new_password'], 'required', 'on' => self::SCENARIO_EDIT_PASSW],
            ['confirm_new_password', 'compare', 'compareAttribute' => 'new_password','message' => 'Passwords do not match'],

            [['password_hash'], 'string', 'min' => 4, 'on' => 'update']
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_EDIT_PASSW] = ['password','confirm_new_password','new_password'];

        return $scenarios;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return \Yii::$app->user->identity->username;
    }
  

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    


    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(),['user_id' => 'id'] );
    }

    public function getUserSettings()
    {
        return $this->hasOne(UserSettings::className(),['user_id' => 'id'] );
    }

}

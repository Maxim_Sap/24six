<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "set_main_places".
 *
 * @property int $id
 * @property int $id_place
 *
 * @property SpaceWork $place
 */
class SetMainPlaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'set_main_places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_place'], 'required'],
            [['id_place'], 'integer'],
            [['id_place'], 'exist', 'skipOnError' => true, 'targetClass' => SpaceWork::className(), 'targetAttribute' => ['id_place' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'id_place' => Yii::t('frontend', 'Place'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(SpaceWork::className(), ['id' => 'id_place']);
    }
}

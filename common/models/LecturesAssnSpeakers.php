<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "lectures_assn_speakers".
 *
 * @property int $id
 * @property int $lecture_id
 * @property int $speaker_id
 *
 * @property Lectures $lecture
 * @property Speakers $speaker
 */
class LecturesAssnSpeakers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lectures_assn_speakers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lecture_id', 'speaker_id'], 'required'],
            [['lecture_id', 'speaker_id'], 'integer'],
            [['lecture_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lectures::className(), 'targetAttribute' => ['lecture_id' => 'id']],
            [['speaker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Speakers::className(), 'targetAttribute' => ['speaker_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'lecture_id' => Yii::t('frontend', 'Lecture ID'),
            'speaker_id' => Yii::t('frontend', 'Speaker ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLecture()
    {
        return $this->hasOne(Lectures::className(), ['id' => 'lecture_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeaker()
    {
        return $this->hasOne(Speakers::className(), ['id' => 'speaker_id']);
    }

    public static function addLectures($lecture_id,$speaker_ids){
        if(!empty($speaker_ids)){
            foreach($speaker_ids as $one){
                $model = new LecturesAssnSpeakers();
                $model->lecture_id = $lecture_id;
                $model->speaker_id = (int)$one;
                $model->save();
            }
        }
    }

    public static function UpdateLectures($lecture_id,$speaker_ids){
        if(!empty($speaker_ids)){
            LecturesAssnSpeakers::deleteAll(['lecture_id'=>$lecture_id]);
            return self::addLectures($lecture_id,$speaker_ids);
        }
    }
    public static function getLecturesAndSpeakers( $id_event )
    {
        $query = ( new Query() )
            -> from( LecturesAssnSpeakers::tableName() . " lecture_assn_speaker" )
            -> leftJoin( Lectures::tableName() . " lecture",'lecture.id = lecture_assn_speaker.lecture_id' )
            -> leftJoin( Speakers::tableName() . " speaker", 'speaker.id = lecture_assn_speaker.speaker_id' )
            -> where( [ 'lecture.id_event' => $id_event] )
            -> all();
        return $query;
    }
}

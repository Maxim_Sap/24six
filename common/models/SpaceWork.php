<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;
	use common\models\traits\SpaceForm;

	/**
	 * This is the model class for table "space_work".
	 *
	 * @property int               $id
	 * @property int               $id_cate_plase
	 * @property string            $title
	 * @property string            $description
	 * @property string            $text
	 * @property string            $email
	 * @property string            $image
	 * @property int               $id_country
	 * @property int               $unfixiable_price
	 * @property int               $separate_price
	 * @property int               $one_day_price
	 * @property string            $address
	 * @property string            $phone
	 * @property int               $square_meter
	 * @property int               $working_places
	 * @property int               $kitchen
	 * @property int               $max_spaces
	 * @property int               $max_office
	 * @property string            $created_at
	 *
	 * @property SetMainPlaces[]   $setMainPlaces
	 * @property CategoriesToPlase $catePlase
	 * @property Countryes         $country
	 */
	class SpaceWork extends \yii\db\ActiveRecord
	{
		use SpaceForm;

		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'space_work';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'id_cate_plase', 'title', 'text', 'address', 'description', 'email', 'unfixiable_price', 'separate_price', 'one_day_price', 'id_country', 'phone' ], 'required' ],
				[ [ 'id_cate_plase', 'id_country', 'square_meter', 'working_places', 'kitchen', 'max_office', 'max_spaces' ], 'integer' ],
				[ [ 'text' ], 'string' ],
				[ [ 'unfixiable_price', 'separate_price', 'one_day_price'],'double'],
				[ [ 'created_at' ], 'safe' ],
				[ [ 'title', 'description', 'email', 'image' ], 'string', 'max' => 255 ],
				[ [ 'address' ], 'string', 'max' => 100 ],
				[ [ 'phone' ], 'string', 'max' => 50 ],
				[ [ 'id_cate_plase' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => CategoriesToPlase ::className(), 'targetAttribute' => [ 'id_cate_plase' => 'id' ] ],
				[ [ 'id_country' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => Countryes ::className(), 'targetAttribute' => [ 'id_country' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'               => Yii ::t( 'frontend', 'ID' ),
				'id_cate_plase'    => Yii ::t( 'frontend', 'Category Place' ),
				'title'            => Yii ::t( 'frontend', 'Title' ),
				'description'      => Yii ::t( 'frontend', 'Short Description' ),
				'text'             => Yii ::t( 'frontend', 'Full Description' ),
				'email'            => Yii ::t( 'frontend', 'Email' ),
				'image'            => Yii ::t( 'frontend', 'Image' ),
				'id_country'       => Yii ::t( 'frontend', 'Select Country' ),
				'unfixiable_price' => Yii ::t( 'frontend', 'Unfixiable Price' ),
				'separate_price'   => Yii ::t( 'frontend', 'Separate Price' ),
				'one_day_price'    => Yii ::t( 'frontend', 'One Day Price' ),
				'address'          => Yii ::t( 'frontend', 'Address' ),
				'phone'            => Yii ::t( 'frontend', 'Phone' ),
				'square_meter'     => Yii ::t( 'frontend', 'Square Meter' ),
				'working_places'   => Yii ::t( 'frontend', 'Working Places' ),
				'kitchen'          => Yii ::t( 'frontend', 'Kitchen' ),
				'max_spaces'       => Yii ::t( 'frontend', 'Max Place' ),
				'max_office'       => Yii ::t( 'frontend', 'Max Office' ),
				'created_at'       => Yii ::t( 'frontend', 'Date Created' ),
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getCatePlase()
		{
			return $this -> hasOne( CategoriesToPlase ::className(), [ 'id' => 'id_cate_plase' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getCountry()
		{
			return $this -> hasOne( Countryes ::className(), [ 'id' => 'id_country' ] );
		}

		public function getConfRooms()
		{
			return $this -> hasMany( ConfRooms ::className(), [ 'space_id' => 'id' ] );
		}

		public static function getLastPlaces()
		{
			return SpaceWork ::find()
							 -> select( 'title,image,address,unfixiable_price,separate_price,id' )
							 -> orderBy( [ 'created_at' => SORT_DESC ] )
							 -> limit( 3 )
							 -> all();
		}

		public static function getPlaceInCategory( $tableName )
		{
			$query = new Query();
			$query -> from( $tableName . " place_post" )
				   -> select(
					   '
				  count(place_post.id_cate_plase) as count_place_in_category, 
				  place_category.category, 
				  place_category.id,
				  place_category.background_image,
			  '
				   )
				   -> leftJoin( CategoriesToPlase ::tableName() . " place_category", 'place_category.id = place_post.id_cate_plase' )
				   -> groupBy( 'place_post.id_cate_plase' )
				   -> limit( 3 );
			return $query;
		}

		public static function getSetsPlaces()
		{
			$query = ( new Query() )
				-> from( SetMainPlaces ::tableName() . " sets_place" )
				-> select(
					'
                place.title as title_place, 
                place.image as image_place, 
                place.id as place_id,
                place.created_at as date,
            '
				)
				-> leftJoin( SpaceWork ::tableName() . " place", 'place.id = sets_place.id_place' )
				-> limit( 4 );
			return $query -> all();
		}

		public static function getPlaces( $user_id = NULL, $categoryTypePlaces_id = NULL, $keySearchTypePlaces = NULL, $get_date = NULL, $sort_places = NULL )
		{
			// Определяем наличие place в моих фаворитах
			$myFavorite = ( new Query() )
				-> from( PopularPlaces ::tableName() )
				-> select( 'user_id, id_place as favorite_place' )
				-> where( [ 'user_id' => $user_id ] );
			$places     = ( new Query() )
				-> from( SpaceWork ::tableName() . " places" )
				-> leftJoin( [ 'my_favorite' => $myFavorite ], 'my_favorite.favorite_place = places.id' );

			//Выбираем place по категории
			if( !empty( $categoryTypePlaces_id ) )
			{
				$places -> where( [ 'places.id_cate_plase' => $categoryTypePlaces_id ] );
			}
			//Выбираем евенты по ключевым словам - поиск евента
			if( !empty( $keySearchTypePlaces ) )
			{
				$places -> andWhere(
					[
						'or',
						[ 'like', 'places.title', $keySearchTypePlaces ],
						[ 'like', 'places.description', $keySearchTypePlaces ],
					]
				);
			}
			//Выбираем евент по конкретной дате
			if( !empty( $get_date ) )
			{
				$places -> where( [ 'places.created_at' => $get_date ] );
			}

			// Сортируем про рейтингу
			if( $sort_places == 'popular-places' )
			{
				$popularPlace = ( new Query() )
					-> from( PopularPlaces ::tableName() )
					-> select( 'count(*) as popular, id_place as id_favorites_place, user_id' )
					-> groupBy( 'id_place' )
					-> where( [ 'user_id' => $user_id ] );

				$places -> leftJoin( [ 'popular' => $popularPlace ], 'popular.id_favorites_place = places.id' )
						-> orderBy( [ 'popular.popular' => SORT_DESC ] );
			}

			//Выбираем собитя из мои фаворитов
			if( $sort_places == 'my-favorite' )
			{
				$places -> leftJoin( Favorites ::tableName() . " my_favorites", 'my_favorites.id_favorites = places.id' )
						-> select(
							'
                    my_favorites.id_favorites, 
                    places.id,
                    places.title,
                    places.image,
                    places.address,
                    places.created_at,
                    places.description,
                    my_favorite.favorite_place
                '
						)
						-> where( [ 'my_favorites.user_id' => $user_id ] )
						-> andWhere( [ 'my_favorites.type_favorite' => Favorites::TYPE_PLACES ] )
						-> groupBy( 'places.id' )
						-> groupBy( 'my_favorites.id_favorites' );
				if( !empty( $get_date ) )
				{
					$places -> andWhere( [ 'places.created_at' => $get_date ] );
				}
			}

			//Выбираем самые новые события
			if( $sort_places == 'new-place' )
			{
				$places -> orderBy( [ 'created_at' => SORT_DESC ] );
			}

			return $places;
		}

		public static function getNextOrPrevSpacePost( $id_space, $operator )
		{
			$data = [];
			switch( $operator )
			{
				case '<' :
					$data = SpaceWork ::find()
									  -> select( 'id,title,image' )
									  -> where( [ '<', 'id', $id_space ] )
									  -> orderBy( 'id DESC' );
					break;
				case '>' :
					$data = SpaceWork ::find()
									  -> select( 'id,title,image' )
									  -> where( [ '>', 'id', $id_space ] )
									  -> orderBy( 'id ASC' );
					break;
			}
			return $data -> one();
		}

		public static function getLastOrFirstEventPost( $id_space = NULL, $what_post )
		{
			$dataPost = [];
			switch( $what_post )
			{
				case 'first' :
					$dataPost = SpaceWork ::find()
										  -> select( 'id,title,image' )
										  -> orderBy( [ 'id' => SORT_ASC ] );
					break;
				case 'last' :
					$dataPost = SpaceWork ::find()
										  -> select( 'id,title,image' )
										  -> orderBy( [ 'id' => SORT_DESC ] );
					break;
			}
			return $dataPost -> one();
		}

		public static function checkTickets( $id_res )
		{
			$type_place_us = ( new Query() )
				-> from( ResourcesEventsPlaces ::tableName() )
				-> select( 'count(id_res) as saleTicketsUs, id_res' )
				-> groupBy( 'id_res' )
				-> where( [ 'id_res' => $id_res ] )
				-> andWhere( [ 'type_place' => UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ] );

			$type_place_od = ( new Query() )
				-> from( ResourcesEventsPlaces ::tableName() )
				-> select( 'count(id_res) as saleTicketsOd, id_res' )
				-> groupBy( 'id_res' )
				-> where( [ 'id_res' => $id_res ] )
				-> andWhere( [ 'type_place' => UserProfile::TYPE_PLACE_ONE_DAY ] );

			$type_place_ss = ( new Query() )
				-> from( ResourcesEventsPlaces ::tableName() )
				-> select( 'count(id_res) as saleTicketsSs, id_res' )
				-> groupBy( 'id_res' )
				-> where( [ 'id_res' => $id_res ] )
				-> andWhere( [ 'id_res_type' => Favorites::TYPE_PLACES ] )
				-> andWhere( [ 'type_place' => UserProfile::TYPE_PLACE_SEPARATE_SPACE ] );

			$data = [
				'type_place_us' => $type_place_us -> one(),
				'type_place_od' => $type_place_od -> one(),
				'type_place_ss' => $type_place_ss -> one(),
			];
			return $data;
		}

		public static function deleteImages( $img_name, $place )
		{
			$webroot = Yii ::getAlias( '@frontend/web' );
			switch( $place )
			{

				case 'background_image':
					SpaceWork ::updateAll( [ 'image' => '' ], [ 'image' => $img_name ] );
					$path = \Yii ::getAlias( '@background_image' );
					break;
			}
			@unlink( $webroot . $path . "/origin/" . $img_name );
			@unlink( $webroot . $path . "/thumb/" . $img_name );
			@unlink( $webroot . $path . "/icon/" . $img_name );
		}
	}

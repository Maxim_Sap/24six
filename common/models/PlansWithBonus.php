<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plans_with_bonus".
 *
 * @property int $id
 * @property int $id_plan
 * @property int $id_bonus
 *
 * @property BonusPlans $bonus
 * @property Plans $plan
 */
class PlansWithBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans_with_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plan', 'id_bonus'], 'required'],
            [['id_plan', 'id_bonus'], 'integer'],
            [['id_bonus'], 'exist', 'skipOnError' => true, 'targetClass' => BonusPlans::className(), 'targetAttribute' => ['id_bonus' => 'id']],
            [['id_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['id_plan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'id_plan' => Yii::t('frontend', 'Id Plan'),
            'id_bonus' => Yii::t('frontend', 'Id Bonus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(BonusPlans::className(), ['id' => 'id_bonus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plans::className(), ['id' => 'id_plan']);
    }
}

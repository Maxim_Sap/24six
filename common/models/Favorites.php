<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "favorites".
 *
 * @property int $id
 * @property int $user_id
 * @property int $id_favorites
 *
 * @property Events $favorites
 * @property UserProfile $user
 */
class Favorites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const TYPE_PLACES = 1;
    const TYPE_EVENTS = 2;
    const TYPE_REPEAT_EVENT = 3;

    public static function tableName()
    {
        return 'favorites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'id_favorites'], 'required'],
            [['user_id', 'id_favorites'], 'integer'],
            [['id_favorites'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['id_favorites' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t( 'frontend', 'ID' ),
            'user_id' => Yii::t( 'frontend', 'User ID' ),
            'id_favorites' => Yii::t( 'frontend', 'Id Favorites' ),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasOne( Events::className(), ['id' => 'id_favorites'] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne( UserProfile::className(), ['user_id' => 'user_id'] );
    }



	public static function setFavorite($user_id, $post_id, $type )
	{
	    $attribute = [];
	    switch ($type) {
            case 1 :
                $attribute = self::TYPE_PLACES;
                break;
            case 2 :
                $attribute = self::TYPE_EVENTS;
                break;
        }
        $current = $attribute;

		$setPopular = (new Query())
			->from( Favorites::tableName() )
			->createCommand()
			->insert( Favorites::tableName(), [
				'user_id' => $user_id,
				'id_favorites' => $post_id,
                'type_favorite' => $current,
			] )->execute();

		return $setPopular;
	}
	public static function removeFavorite($user_id, $post_id, $type)
	{
        $attribute = [];
        switch ($type) {
            case 1 :
                $attribute = self::TYPE_PLACES;
                break;
            case 2 :
                $attribute = self::TYPE_EVENTS;
                break;
        }
        $current = $attribute;
		$removePopular = (new Query())
			->from( Favorites::tableName() )
			->createCommand()
			->delete( Favorites::tableName(), [
				'user_id' => $user_id,
				'id_favorites' => $post_id,
                'type_favorite' => $current,
			] )->execute();

		return $removePopular;
	}

    public static function checkMyFavorite($user_id, $type)
    {
		$myFavorite = [];
    	switch($type){
			case Events::tableName() :
				$myFavorite = (new Query())
					->from( Favorites::tableName() . " my_favorit" )
					->select('
							events.id,
							events.title,
							events.description,
							events.address,
							events.price_ticket_event,
							events.image,
							events.time_start_event,
							events.time_end_event,
							events.date_event,
							my_favorit.id_favorites as id_favorites,
						')
					->leftJoin( Events::tableName() . " events", 'events.id = my_favorit.id_favorites' )
					->where( ['user_id' => $user_id] )
					->andWhere(['my_favorit.type_favorite' => self::TYPE_EVENTS]);
				break;
			case SpaceWork::tableName() :
				$myFavorite = (new Query())
					->from( Favorites::tableName() . " my_favorit" )
					->select('
							places.id,
							places.title,
							places.description,
							places.address,
							places.image,
							places.created_at,
							my_favorit.id_favorites as id_favorites,
						')
					->leftJoin( SpaceWork::tableName() . " places", 'places.id = my_favorit.id_favorites' )
					->where( ['user_id' => $user_id] )
					->andWhere(['my_favorit.type_favorite' => self::TYPE_PLACES]);
		}
        return $myFavorite;
    }

}

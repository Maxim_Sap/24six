<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $title_partner
 * @property string $logo_partner
 * @property string $link_partner
 *
 * @property PartnerEvents[] $partnerEvents
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_partner', 'logo_partner', 'link_partner'], 'required'],
            [['title_partner', 'logo_partner', 'link_partner'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'title_partner' => Yii::t('frontend', 'Title Partner'),
            'logo_partner' => Yii::t('frontend', 'Logo Partner'),
            'link_partner' => Yii::t('frontend', 'Link Partner'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerEvents()
    {
        return $this->hasMany(PartnerEvents::className(), ['id_partner' => 'id']);
    }
}

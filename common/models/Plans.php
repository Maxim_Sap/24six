<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property int $id
 * @property string $title
 * @property int $price
 *
 * @property PlansWithBonus[] $plansWithBonuses
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'title' => Yii::t('frontend', 'Title'),
            'price' => Yii::t('frontend', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlansWithBonuses()
    {
        return $this->hasMany(PlansWithBonus::className(), ['id_plan' => 'id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "answer_for_question".
 *
 * @property int $id
 * @property int $id_question
 * @property string $question
 * @property string $answer
 * @property int $user_id
 *
 * @property Questions $question0
 * @property User $user
 */
class AnswerForQuestion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer_for_question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_question', 'question', 'answer', 'user_id'], 'required'],
            [['id_question', 'user_id'], 'integer'],
            [['answer'], 'string'],
            [['question'], 'string', 'max' => 255],
            [['id_question'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['id_question' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'id_question' => Yii::t('frontend', 'Id Question'),
            'question' => Yii::t('frontend', 'Question'),
            'answer' => Yii::t('frontend', 'Answer'),
            'user_id' => Yii::t('frontend', 'Username'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion0()
    {
        return $this->hasOne(Questions::className(), ['id' => 'id_question']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

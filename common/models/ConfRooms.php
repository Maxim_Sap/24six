<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "conf_rooms".
 *
 * @property int $id
 * @property int $space_id
 * @property string $name
 * @property string $desc
 * @property string $price_for_hour
 * @property string $working_days days mass in integer
 * @property string $working_time_start
 * @property string $working_time_end
 * @property int $status
 *
 * @property SpaceWork $space
 */
class ConfRooms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conf_rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['space_id', 'name', 'desc', 'price_for_hour', 'working_days', 'working_time_start', 'working_time_end'], 'required'],
            [['space_id', 'status'], 'integer'],
            [['desc'], 'string'],
            [['price_for_hour'], 'double'],
            [['working_time_start', 'working_time_end'], 'safe'],
            [['name', 'working_days'], 'string', 'max' => 255],
            [['space_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpaceWork::className(), 'targetAttribute' => ['space_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'space_id' => Yii::t('frontend', 'Space ID'),
            'name' => Yii::t('frontend', 'Name'),
            'desc' => Yii::t('frontend', 'Description'),
            'price_for_hour' => Yii::t('frontend', 'Price For Hour'),
            'working_days' => Yii::t('frontend', 'Working Days'),
            'working_time_start' => Yii::t('frontend', 'Working Time Start'),
            'working_time_end' => Yii::t('frontend', 'Working Time End'),
            'status' => Yii::t('frontend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpace()
    {
        return $this->hasOne(SpaceWork::className(), ['id' => 'space_id']);
    }
}

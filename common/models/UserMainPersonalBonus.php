<?php

	namespace common\models;

	use Yii;

	/**
	 * This is the model class for table "user_main_personal_bonus".
	 *
	 * @property int    $id
	 * @property int    $user_id
	 * @property int    $count_events_week
	 * @property int    $count_time_for_massage
	 * @property int    $count_time_for_conf_room
	 * @property string $date_end$
	 */
	class UserMainPersonalBonus extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'user_main_personal_bonus';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				//				[ [ 'user_id', 'count_events_week', 'count_time_for_massage', 'count_time_for_conf_room' ], 'required' ],
				[ [ 'date_end' ], 'safe' ],
				[ [ 'user_id', 'count_events_week', 'count_time_for_massage', 'count_time_for_conf_room' ], 'integer' ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'                       => Yii ::t( 'frontend', 'ID' ),
				'user_id'                  => Yii ::t( 'frontend', 'User' ),
				'count_events_week'        => Yii ::t( 'frontend', 'Count Events Week' ),
				'count_time_for_massage'   => Yii ::t( 'frontend', 'Count Time For Massage' ),
				'count_time_for_conf_room' => Yii ::t( 'frontend', 'Count Time For Conf Room' ),
				'date_end'                 => Yii ::t( 'frontend', 'Date End' ),
			];
		}

		public static function addBonus( $user_id, $type_payment, $date = NULL )
		{
			$row       = new self();
			$dataBonus = BonusPlans ::find();
			if( $type_payment == UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP )
			{
				$dataBonus -> where( [ 'id' => $type_payment ] );
			}
			if( $type_payment == UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP )
			{
				$dataBonus -> where( [ 'id' => $type_payment ] );
			}
			$bonus = $dataBonus -> one();
			if( $row == NULL )
			{
				self ::addBonusForUser( $user_id, $bonus );
			}
			else
			{
				$row -> user_id                  = $user_id;
				$row -> count_events_week        = $bonus -> count_events_week;
				$row -> count_time_for_massage   = $bonus -> count_time_for_massage;
				$row -> count_time_for_conf_room = $bonus -> count_time_for_conf_room;
				if( !empty( $date ) )
				{
					$row -> date_end = date( "Y-m-d H:i:s", strtotime( $date ) + 60 * 60 * 24 * 30 );
				}
				else
				{
					$row -> date_end = date( "Y-m-d H:i:s", time() + 60 * 60 * 24 * 30 );
				}
				if( !$row -> save() )
				{
					dd( $row -> errors );
				}
			}
		}

		protected static function addBonusForUser( $user_id, $data )
		{
			$model                             = new UserMainPersonalBonus();
			$model -> user_id                  = $user_id;
			$model -> count_events_week        = $data -> count_events_week;
			$model -> count_time_for_massage   = $data -> count_time_for_massage;
			$model -> count_time_for_conf_room = $data -> count_time_for_conf_room;
			if( !$model -> save() )
			{
				dd( $model -> errors );
			}
		}

		public static function getMyBonus( $user_id )
		{
			return self ::find()
						-> where( [ 'user_id' => $user_id ] )
						-> select(
							'
							date_end,
							SUM(count_events_week) as count_events_week,
							SUM(count_time_for_conf_room) as count_time_for_conf_room,
							SUM(count_time_for_massage) as count_time_for_massage,
			'
						)
						-> groupBy( 'user_id' )
						-> all();
		}

		public static function checkMyBonus( $user_id )
		{
			return UserMainPersonalBonus ::find()
										 -> where( [ 'user_id' => $user_id ] )
										 -> select(
											 '
											SUM(count_events_week) as count_events_week,
											SUM(count_time_for_conf_room) as count_time_for_conf_room,
											SUM(count_time_for_massage) as count_time_for_massage,
				'
										 )
										 -> groupBy( 'user_id' )
										 -> all();
		}

		public static function updateMyBonus( $user_id, $count_events_week = NULL )
		{
			$model = self ::updateBonus( $user_id );
			if( $count_events_week < $model -> count_events_week )
			{
				$old_data_value             = $model -> count_events_week - $count_events_week;
				$model -> count_events_week = $old_data_value;
				$model -> save();
			}
			else if( $count_events_week == $model -> count_events_week )
			{
				$model -> count_events_week = 0;
				$model -> save();
			}
			else
			{
				$remainder                  = $count_events_week - $model -> count_events_week;
				$model -> count_events_week = 0;
				if( $model -> save() )
				{
					$next_model                      = self ::updateBonus( $user_id );
					$old_data_value                  = $next_model -> count_events_week - $remainder;
					$next_model -> count_events_week = $old_data_value;
					$next_model -> save();
				}
			}
		}

		public static function updateMyBonusConferenceRoom( $user_id, $count_time_for_conf_room = NULL )
		{
			$model = self ::updateBonusConferenceRoom( $user_id );
			if( $count_time_for_conf_room < $model -> count_time_for_conf_room )
			{
				$old_data_value                    = $model -> count_time_for_conf_room - $count_time_for_conf_room;
				$model -> count_time_for_conf_room = $old_data_value;
				$model -> save();
			}
			else if( $count_time_for_conf_room == $model -> count_time_for_conf_room )
			{
				$model -> count_time_for_conf_room = 0;
				$model -> save();
			}
			else
			{
				$remainder                         = $count_time_for_conf_room - $model -> count_time_for_conf_room;
				$model -> count_time_for_conf_room = 0;
				if( $model -> save() )
				{
					$next_model                             = self ::updateBonusConferenceRoom( $user_id );
					$old_data_value                         = $next_model -> count_time_for_conf_room - $remainder;
					$next_model -> count_time_for_conf_room = $old_data_value;
					$next_model -> save();
				}
			}
		}

		protected static function updateBonusConferenceRoom( $user_id )
		{
			$row = self ::find() -> where( [ 'user_id' => $user_id ] )
						-> andWhere( [ '>', 'count_time_for_conf_room', 0 ] )
						-> orderBy( [ 'date_end' => SORT_ASC ] )
						-> one();
			return $row;
		}

		protected static function updateBonus( $user_id )
		{
			$row = self ::find() -> where( [ 'user_id' => $user_id ] )
						-> andWhere( [ '>', 'count_events_week', 0 ] )
						-> orderBy( [ 'date_end' => SORT_ASC ] )
						-> one();
			return $row;
		}
	}


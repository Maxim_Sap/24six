<?php

	namespace common\models;

	use Stripe\Event;
	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "resources_events_places".
	 *
	 * @property int    $id
	 * @property int    $user_id
	 * @property int    $id_res
	 * @property int    $id_pay_type
	 * @property int    $status
	 * @property int    $type_place
	 * @property int    $id_res_type
	 * @property string $date
	 * @property string $code_res
	 * @property string $created_at
	 * @property string $date_end
	 *
	 * @property User   $user
	 * @property Events $res
	 */
	class ResourcesEventsPlaces extends \yii\db\ActiveRecord
	{
		const ACTIVE_TICKETS    = 1;
		const NO_ACTIVE_TICKETS = 0;

		const SCENARIO_UPDATE_STATUS   = 'update_status_tickets';
		const SCENARIO_SET_DECK_TICKET = 'set_deck_for_ticket';

		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'resources_events_places';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'user_id', 'id_res', 'id_res_type', 'code_res' ],
				  'required',
				  'when' => function()
				  {
					  return !$this -> scenario == self::SCENARIO_SET_DECK_TICKET;
				  },
				],
				[ [ 'user_id', 'id_res', 'id_res_type', 'status' ], 'integer' ],
				[ [ 'status' ], 'integer', 'on' => self::SCENARIO_UPDATE_STATUS ],
				[ [ 'created_at', 'date' ], 'safe' ],
				[ [ 'code_res' ], 'string', 'max' => 20 ],
				[ [ 'user_id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'user_id' => 'id' ] ],
				[ [ 'id_res' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => Events ::className(), 'targetAttribute' => [ 'id_res' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */

		public function attributeLabels()
		{
			return [
				'id'          => Yii ::t( 'frontend', 'ID' ),
				'user_id'     => Yii ::t( 'frontend', 'User' ),
				'id_res'      => Yii ::t( 'frontend', 'Resource' ),
				'id_res_type' => Yii ::t( 'frontend', 'Payment Type' ),
				'status'      => Yii ::t( 'frontend', 'Status' ),
				'code_res'    => Yii ::t( 'frontend', 'Code Resource' ),
				'created_at'  => Yii ::t( 'frontend', 'Created At' ),
				'deck_ticket' => Yii ::t( 'frontend', 'Deck Ticket' ),
				'date'        => Yii ::t( 'frontend', 'Date' ),
				'type_place'  => Yii ::t( 'frontend', 'Type Place' ),
				'date_end'    => Yii ::t( 'frontend', 'Date End' ),
			];
		}

		public function scenarios()
		{
			$scenarios                                   = parent ::scenarios();
			$scenarios[ self::SCENARIO_UPDATE_STATUS ]   = [ 'status' ];
			$scenarios[ self::SCENARIO_SET_DECK_TICKET ] = [ 'deck_ticket' ];

			return $scenarios;
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'user_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRes()
		{
			return $this -> hasOne( Events ::className(), [ 'id' => 'id_res' ] );
		}

		public static function getTickets( $user_id = NULL, $type_res )
		{
			$queryTickets = ( new Query() )
				-> from( self ::tableName() . " my_ticket" )
				-> select(
					'
						 my_ticket.date,
						 event.id as id_event,
						 event.title, 
						 event.time_start_event, 
						 event.time_end_event,
						 event.price_ticket_event,
						 my_ticket.id as id_ticket,
						 my_ticket.status
					 '
				) -> leftJoin( Events ::tableName() . " event", 'event.id = my_ticket.id_res' )
				-> andWhere( [ 'my_ticket.user_id' => $user_id ] )
				-> andWhere( [ 'my_ticket.status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
				-> where( [ 'my_ticket.id_res_type' => $type_res ] )
				-> orderBy( [ 'my_ticket.created_at' => SORT_DESC ] );

			return $queryTickets -> all();
		}

		public static function getBookingTickets( $user_id, $type_res )
		{
			$queryTickets = ( new Query() )
				-> from( self ::tableName() . " my_ticket" );
			$queryTickets -> select(
				'
						 space.id as id_space,
						 space.title, 
						 space.unfixiable_price,
						 space.separate_price,
						 space.one_day_price,
						 my_ticket.date as date_order,
						 my_ticket.id as id_ticket,
						 my_ticket.type_place,
						 my_ticket.status,
						 my_ticket.date,
						 my_ticket.date_end,
						 my_ticket.id as id_ticket, 
					 '
			)
						  -> leftJoin( SpaceWork ::tableName() . " space", 'space.id = my_ticket.id_res' );
			if( !empty( $user_id ) )
			{
				$queryTickets -> andWhere( [ 'booking_room.user_id' => $user_id ] );
			}
			else
			{
				$queryTickets -> andWhere( [ 'booking_room.status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] );
			}
			$queryTickets -> where( [ 'my_ticket.id_res_type' => $type_res ] )
						  -> orderBy( [ 'my_ticket.created_at' => SORT_DESC ] );
			return $queryTickets -> all();
		}

		public static function addResource( $user_id, $id_res, $type_res, $count_res, $type_place = NULL, $order_date = NULL )
		{
			$date_end = [];
			if( !empty( $type_place ) )
			{
				switch( $type_place )
				{
					case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
						$date_end = strtotime( $order_date ) + 60 * 60 * 24 * 30;
						break;
					case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
						$date_end = strtotime( $order_date ) + 60 * 60 * 24 * 30;
						break;
					case UserProfile::TYPE_PLACE_ONE_DAY :
						$date_end = strtotime( $order_date ) + 60 * 60 * 24;
						break;
				}
			}
			for( $x = 1; $x <= $count_res; $x++ )
			{
				$setTickets                = new self();
				$setTickets -> user_id     = $user_id;
				$setTickets -> id_res      = $id_res;
				$setTickets -> id_res_type = $type_res;
				$setTickets -> status      = self::ACTIVE_TICKETS;
				$setTickets -> date        = $order_date;

				if
				(
					$type_place == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
					$type_place == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
					$type_place == UserProfile::TYPE_PLACE_ONE_DAY
				)
				{
					$setTickets -> date       = $order_date;
					$setTickets -> type_place = $type_place;
					$setTickets -> date_end   = date( 'Y-m-d H:i:s', $date_end );
				}

				$setTickets -> code_res   = (string)self ::randomNumber();
				$setTickets -> created_at = date( 'Y-m-d H:i:s' );
				$setTickets -> save();
			}
		}

		static function randomNumber()
		{
			$length = 12;
			$str    = '';
			for( $i = 0; $i < $length; ++$i )
			{
				$first = $i ? 0 : 1;
				$n     = mt_rand( $first, 9 );
				$str   .= $n;
			}
			return $str;
		}

		public static function getDataTicketEvent( $id_ticket )
		{
			$row = ( new Query() )
				-> from( ResourcesEventsPlaces ::tableName() . " ticket_event" )
				-> select(
					'
					event.date_event,
					event.title,
					event.time_start_event,
					event.time_end_event,
					event.price_ticket_event,
					event.address,
					event.id as id_event,
					
					ticket_event.code_res,
					ticket_event.deck_ticket,
					ticket_event.id as id_ticket,
				'
				)
				-> leftJoin( Events ::tableName() . " event", 'event.id = ticket_event.id_res' )
				-> where( [ 'ticket_event.id' => $id_ticket ] )
				-> andWhere( [ 'id_res_type' => Favorites::TYPE_EVENTS ] )
				-> one();

			return $row;
		}

		public static function getDataTicketPlace( $id_ticket )
		{
			$row = ( new Query() )
				-> from( ResourcesEventsPlaces ::tableName() . " ticket_place" )
				-> select(
					'
					space.title,
					space.email,
					space.unfixiable_price,
					space.separate_price,
					space.one_day_price,
					space.address,
					space.phone,
					space.id as id_place,
					
					ticket_place.code_res,
					ticket_place.deck_ticket,
					ticket_place.id as id_ticket,
					ticket_place.date,
					ticket_place.type_place,
					ticket_place.date_end,
				'
				)
				-> leftJoin( SpaceWork ::tableName() . " space", 'space.id = ticket_place.id_res' )
				-> where( [ 'ticket_place.id' => $id_ticket ] )
				-> one();
			return $row;
		}
	}

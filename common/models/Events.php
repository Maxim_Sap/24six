<?php

namespace common\models;

use Yii;
use yii\base\Event;
use yii\db\Query;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property int $id_cate_event
 * @property string $title
 * @property string $description
 * @property string $address
 * @property string $data_event
 * @property int $type_event
 * @property string $created_at
 * @property int $price_ticket_event
 *
 * @property CategoriesTypeEvent $cateEvent
 * @property PopularEvents[] $popularEvents
 */
class Events extends \yii\db\ActiveRecord
{

	const SCENARIO_UPDATE_DATE = 'repeat_data_event';
	const SCENARIO_UPDATE_TYPE_EVENT = 'repeat_event';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cate_event', 'title', 'text', 'description', 'address', 'price_ticket_event','max_spaces','id_country'], 'required'],
			[['id_cate_event', 'type_event', 'id_country', 'max_spaces'], 'integer'],
			[['price_ticket_event'], 'number'],
            [['created_at','time_start_event','time_end_event'], 'safe'],
            [['date_event','text','description'], 'string'],
            [['email'], 'email'],
			[['type_event'],'required','on' => self::SCENARIO_UPDATE_TYPE_EVENT],
			[['date_event'],'required', 'on' => self::SCENARIO_UPDATE_DATE],
            [['image','title', 'address','phone'], 'string', 'max' => 255],
            [['id_cate_event'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesTypeEvent::className(), 'targetAttribute' => ['id_cate_event' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t( 'frontend', 'ID' ),
            'id_cate_event' => Yii::t( 'frontend', 'Event Category' ),
            'title' => Yii::t( 'frontend', 'Title' ),
            'description' => Yii::t( 'frontend', 'Short Description' ),
            'text' => Yii::t( 'frontend', 'Full Description' ),
            'address' => Yii::t( 'frontend', 'Address' ),
            'data_event' => Yii::t( 'frontend', 'Data Event' ),
            'created_at' => Yii::t( 'frontend', 'Created At' ),
            'time_start_event' => Yii::t( 'frontend', 'Time Start Event' ),
            'time_end_event' => Yii::t( 'frontend', 'Time End Event' ),
            'image' => Yii::t( 'frontend', 'Image' ),
            'phone' => Yii::t( 'frontend', 'Phone' ),
            'Email' => Yii::t( 'frontend', 'Email' ),
            'price_ticket_event' => Yii::t( 'frontend', 'Price Ticket Event' ),
            'max_spaces' => Yii::t( 'frontend', 'Max Spaces' ),
            'id_country' => Yii::t( 'frontend', 'Country' ),
            'type_event' => Yii::t( 'frontend', 'Weekly / single-time' ),
        ];
    }

	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_UPDATE_DATE] = ['data_event'];
		$scenarios[self::SCENARIO_UPDATE_TYPE_EVENT] = ['type_event'];
		return $scenarios;
	}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCateEvent()
    {
        return $this->hasOne( CategoriesTypeEvent::className(), ['id' => 'id_cate_event'] );
    }
	public function getCustomFields()
	{
		return $this->hasOne( CustomFields::className(), ['source_id' => 'id'] );
	}
	public function getLectures()
	{
		return $this->hasOne( Lectures::className(), ['id_event' => 'id'] );
	}
	public function getPartners()
	{
		return $this->hasOne( PartnerEvents::className(), ['id_event' => 'id'] );
	}
    public static function getLastEvents()
	{
		return Events::find()
            ->select('title,address,image,date_event,time_start_event,time_end_event,id')
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)
            ->all();
	}

    public static function getSetsEvents()
    {
        $query = (new Query())
            ->from(SetMainEvent::tableName()." sets_event")
            ->select('
                event.title as title_event,
                event.image as image_event, 
                event.id as event_id,
                event.date_event as date
            ')
            ->leftJoin(Events::tableName()." event",'event.id = sets_event.id_event')
            ->limit(4);
        return $query->all();
    }

    public static function getEvents(
    	$user_id = null,
		$categoryTypeEvent_id = null,
		$keySearchTypeEvent = null,
		$get_date = null,
		$sort_event = null,
		$count_id = null
	)
    {
        // Определяем наличие евента в моих фаворитах
        $myFavorite = (new Query())
            ->from( PopularEvents::tableName() )
            ->select( 'user_id, id_event as favorite_event' )
            ->where( ['user_id' => $user_id] );

        $events = (new Query())
            ->from( Events::tableName() . " events" )
            ->leftJoin( ['my_favorite' => $myFavorite], 'my_favorite.favorite_event = events.id' )
			->orderBy(['events.date_event' => SORT_DESC]);
        //Выбираем евенты по категории
        if (!empty( $categoryTypeEvent_id )) {
            $events->where( ['events.id_cate_event' => $categoryTypeEvent_id] );
        }
         if (!empty( $count_id )) {
			 $events->where( ['events.id_country' => $count_id] );
		 }
        //Выбираем евенты по ключевым словам - поиск евента
        if (!empty( $keySearchTypeEvent )) {
            $events->andWhere( [
                'or',
                ['like', 'events.title', $keySearchTypeEvent],
                ['like', 'events.description', $keySearchTypeEvent]
            ] );
        }
        //Выбираем евент по конкретной дате
        if(!empty( $get_date )) {
            $events->where(['events.date_event' => $get_date]);
        }

        // Сортируем про рейтингу
        if($sort_event == 'popular-events') {
			$popularEvent = (new Query())
                ->from( PopularEvents::tableName() )
                ->select( 'count(*) as popular, id_event as id_favorites_event, user_id' )
                ->groupBy( 'id_event' )
				->where(['user_id' => $user_id]);

                $events->leftJoin( ['popular' => $popularEvent], 'popular.id_favorites_event = events.id' )
                    ->orderBy(['popular.popular' => SORT_DESC]);
        }
        //Выбираем события которые должны быть в скором времени.
        if($sort_event == 'upcoming-events') {
            $events->where([">",'date_event',date('Y-m-d')])
                ->orderBy(['date_event' => SORT_ASC]);
        }

        //Выбираем собитя из мои фаворитов
        if($sort_event == 'my-favorite') {
            $events->leftJoin(Favorites::tableName()." my_favorites",'my_favorites.id_favorites = events.id')
                ->select('
                    my_favorites.id_favorites,
                    events.id,
                    events.title,
                    events.image,
                    events.date_event,
                    events.address,
                    events.price_ticket_event,
                    events.time_start_event,
                    events.time_end_event,
                    events.description,
                    my_favorite.favorite_event
                ')
                ->where(['my_favorites.user_id' => $user_id])
                ->andWhere(['my_favorites.type_favorite' => Favorites::TYPE_EVENTS])
                ->groupBy('events.id')
                ->groupBy('my_favorites.id_favorites');
            if(!empty($get_date)) {
                $events->andWhere(['events.date_event' => $get_date]);
            }
        }

        //Выбираем самые новые события
        if($sort_event == 'new-event') {
            $events->orderBy(['created_at' => SORT_ASC]);
        }

        //Проверяем скольбко осталось билетов на событие
        return $events;
    }

    //Подзапрос для подсчета количества билетов на событие
    public static function checkTickets($id_res)
	{
		$checkTickets = (new Query())
			->from(ResourcesEventsPlaces::tableName())
			->select('count(id_res) as saleTickets, id_res')
			->groupBy('id_res')
			->where(['id_res' => $id_res ])
			->all();
		return $checkTickets;
	}
	public static function getNextOrPrevEventPost($id_event,$operator)
	{
		$data = [];
		switch($operator) {
			case '<' :
				$data = Events::find()
					->select('id,title,image')
					->where(['<','id',$id_event])
                    ->orderBy('id DESC');
				break;
			case '>' :
				$data = Events::find()
					->select('id,title,image')
					->where(['>','id',$id_event])
                    ->orderBy('id ASC');
				break;
		}
		return $data->one();
	}
	public static function getLastOrFirstEventPost( $id_event = null , $what_post)
	{
		$dataPost = [];
		switch( $what_post ) {
			case 'first' :
				$dataPost = Events ::find()
							   -> select( 'id,title,image')
							   -> orderBy( [ 'id' => SORT_ASC ] );
				break;
			case 'last' :
				$dataPost = Events ::find()
							   -> select( 'id,title,image')
							   -> orderBy( [ 'id' => SORT_DESC ] );
				break;
		}
		return $dataPost -> one();

	}

    public static function deleteImages($img_name,$place){
        $webroot = Yii::getAlias('@frontend/web');
        switch ($place){
            /*case 'avatar':
                UserProfile::updateAll(['avatar'=>''],['user_id'=>$my_id]);
                $path = \Yii::getAlias('@avatar');
                break;*/
            case 'background_image':
                Events::updateAll(['image'=>''],['image'=>$img_name]);
                $path = \Yii::getAlias('@background_image');
                break;
        }
        @unlink($webroot.$path ."/origin/". $img_name);
        @unlink($webroot.$path ."/thumb/". $img_name);
        @unlink($webroot.$path ."/icon/". $img_name);

    }
}

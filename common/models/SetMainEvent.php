<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "set_main_event".
 *
 * @property int $id
 * @property int $id_event
 *
 * @property Events $event
 */
class SetMainEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'set_main_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_event'], 'required'],
            [['id_event'], 'integer'],
            [['id_event'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['id_event' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'id_event' => Yii::t('frontend', 'Event'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'id_event']);
    }
}

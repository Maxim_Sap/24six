<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "payment_history".
	 *
	 * @property int    $id
	 * @property int    $user_id
	 * @property string $created_at
	 * @property string $paym_type
	 * @property int    $amount
	 * @property int    count_tickets
	 * @property int    id_res
	 *
	 * @property User   $user
	 */
	class PaymentHistory extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'payment_history';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'user_id', 'paym_type', 'amount' ], 'required' ],
				[ [ 'user_id', 'count_tickets', 'id_res' ], 'integer' ],
				[['amount'],'double'],
				[ [ 'paym_type' ], 'string' ],
				[ [ 'created_at' ], 'safe' ],
				[ [ 'user_id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'user_id' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'            => Yii ::t( 'frontend', 'ID' ),
				'user_id'       => Yii ::t( 'frontend', 'User' ),
				'created_at'    => Yii ::t( 'frontend', 'Date Created' ),
				'paym_type'     => Yii ::t( 'frontend', 'Payment Type' ),
				'amount'        => Yii ::t( 'frontend', 'Amount' ),
				'count_tickets' => Yii ::t( 'frontend', 'Count Tickets' ),
				'id_res'        => Yii ::t( 'frontend', 'Resource' ),
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'user_id' ] );
		}

		public function getEvents()
		{
			return $this -> hasOne( Events ::className(), [ 'id' => 'id_res' ] );
		}

		public function getPlaces()
		{
			return $this -> hasOne( SpaceWork ::className(), [ 'id' => 'id_res' ] );
		}

		public static function addFunds( $user_id, $payment_type, $price, $count_tickets = NULL, $id_res = NULL )
		{
			$payments_model              = new self();
			$payments_model -> user_id   = $user_id;
			$payments_model -> paym_type = (string)$payment_type;
			if
			(
				$payment_type == UserProfile::TYPE_PAYMENT_TICKET or
				$payment_type == UserProfile::TYPE_PAYMENT_FREE_TICKET or
				$payment_type == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
				$payment_type == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
				$payment_type == UserProfile::TYPE_PLACE_CONF_ROM or
				$payment_type == UserProfile::TYPE_PLACE_ONE_DAY
			)
			{
				if( $payment_type == UserProfile::TYPE_PLACE_CONF_ROM )
				{
					$payments_model -> amount = $price;
				}
				else
				{
					$payments_model -> amount = $price * $count_tickets;
				}
				$payments_model -> count_tickets = $count_tickets;
				$payments_model -> id_res        = $id_res;
			}
			else
			{
				$payments_model -> amount = $price;
			}
			if( !$payments_model -> save() )
			{
				dd( $payments_model -> errors );
			}

			switch( $payment_type )
			{
				case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
					$date_end = time() + 60 * 60 * 24;
					break;
				case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
					$date_end = time() + 60 * 60 * 24 * 30;
					break;
				case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
					$date_end = time() + 60 * 60 * 24 * 30;
					break;
			}
			if( $payment_type == UserProfile::TYPE_PAYMENT_TICKET or UserProfile::TYPE_PAYMENT_FREE_TICKET )
			{

			}
			else
			{
				UserAssnPayments ::deleteAll( [ 'user_Id' => $user_id ] );
				$user_assn_paymm                 = new UserAssnPayments();
				$user_assn_paymm -> user_Id      = $user_id;
				$user_assn_paymm -> payment_type = $payment_type;
				$user_assn_paymm -> date_end     = date( "Y-m-d H:i:s", $date_end );
				if( !$user_assn_paymm -> save() )
				{
					var_dump( $user_assn_paymm -> errors );
					die;
				}
			}
		}

		public static function getHistoryPayment( $user_id )
		{

			$row = ( new Query() )
				-> from( PaymentHistory ::tableName() . " history_pay" )
				-> select(
					'
                    events.id as id_event,
                    events.title as event_title, 
                    events.date_event as date_event,
                    
                    spaces.title as spaces_title,
                    spaces.id as id_spaces,
                    
                    history_pay.paym_type,
                    history_pay.amount,
                    history_pay.count_tickets,
                    history_pay.created_at
                '
				)
				-> leftJoin( Events ::tableName() . " events", 'events.id = history_pay.id_res' )
				-> leftJoin( SpaceWork ::tableName() . " spaces", 'spaces.id = history_pay.id_res' )
				-> where( [ 'history_pay.user_id' => $user_id ] )
				-> orderBy( [ 'history_pay.created_at' => SORT_DESC ] )
				-> all();
			return $row;
		}
	}

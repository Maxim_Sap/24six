<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "partner_events".
	 *
	 * @property int    id_partner
	 * @property int    id_event
	 */
	class PartnerEvents extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'partner_events';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'id_partner', 'id_event' ], 'required' ],
				[ [ 'id_event','id_partner' ], 'integer' ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id_event' => Yii ::t( 'frontend', 'Event' ),
				'id_partner' => Yii ::t( 'frontend', 'Partner' ),
			];
		}

		public function getPartners()
		{
			return $this->hasOne(Partners::className(), ['id' => 'id_partner']);
		}
		public static function getPartnerEvents( $id_res )
		{
			return ( new Query() )
				-> from( PartnerEvents ::tableName() . " part_event" )
				-> select('
					part.title_partner,
					part.logo_partner,
					part.link_partner,
					
					part_event.id_event,
					part_event.id_partner as id_partner_in_event,
					part_event.id,
				')
				-> leftJoin( Partners ::tableName() . " part", 'part.id = part_event.id_partner' )
				-> where( [ 'id_event' => $id_res ] )
				-> all();
		}
		public static function getLastAddPartner($id_event)
		{
			return PartnerEvents::find() -> with( 'partners' )
				-> where( [ 'id_event' => $id_event] )
				-> orderBy( [ 'id' => SORT_DESC ] )
				-> one();
		}
	}

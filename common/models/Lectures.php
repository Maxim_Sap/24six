<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "lectures".
	 *
	 * @property int      $id
	 * @property int      $id_event
	 * @property string   $time_from
	 * @property string   $time_to
	 * @property string   $subject
	 *
	 * @property Speakers $speaker
	 * @property Events   $event
	 */
	class Lectures extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */
		public $speaker_ids = [];

		public static function tableName()
		{
			return 'lectures';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'id_event', 'time_from', 'time_to', 'subject'], 'required' ],
				[ [ 'id_event'], 'integer' ],
				[ [ 'time_from', 'time_to' ], 'safe' ],
				[ [ 'subject' ], 'string', 'max' => 255 ],
				[ [ 'id_event' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => Events ::className(), 'targetAttribute' => [ 'id_event' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'                    => Yii ::t( 'frontend', 'ID' ),
				'id_event'              => Yii ::t( 'frontend', 'Id Event' ),
				'time_from'             => Yii ::t( 'frontend', 'Time From' ),
				'time_to'               => Yii ::t( 'frontend', 'Time To' ),
				'subject'               => Yii ::t( 'frontend', 'Subject' ),
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */

		public function getEvent()
		{
			return $this -> hasOne( Events ::className(), [ 'id' => 'id_event' ] );
		}

        public function getSpeakersIds()
        {
            return $this -> hasMany( LecturesAssnSpeakers::className(), [ 'lecture_id' => 'id' ] );
        }

        public function getSpeakersData()
        {
            return $this -> hasMany( Speakers::className(), ['id' => 'speaker_id'] )
                //viaTable('{{%suburbs}}', ['id' => 'suburb_id'])->where('{{%states}}.default = 1');
                ->viaTable(LecturesAssnSpeakers::tableName(), [ 'lecture_id' => 'id' ])->asArray()->all();
        }

		public static function getLastLecture($number)
		{
			$dataLecture = ( new Query() )
				-> from( self::tableName() . " lecture" );
				if($number == 1) {
					$dataLecture-> select('
						lecture.id as id_lecture,
						lecture.time_from,
						lecture.time_to,
						lecture.subject,
						lecture.id_event,
						lecture.additional_speaker_id,
						
						speaker.name as speaker_name,
						speaker.avatar as speaker_avatar,
						speaker.position as speaker_positional,
						
						additional_speaker.name as additional_speaker_name,
						additional_speaker.avatar as additional_speaker_avatar,
						additional_speaker.position as additional_speaker_position,
					');
				}
				if($number == 0) {
					$dataLecture-> select('
						lecture.id as id_lecture,
						lecture.time_from,
						lecture.time_to,
						lecture.subject,
						lecture.id_event,
						lecture.additional_speaker_id,
						
						speaker.name as speaker_name,
						speaker.avatar as speaker_avatar,
						speaker.position as speaker_positional,
					');
				}
			$dataLecture -> leftJoin( Speakers ::tableName() . " speaker", 'speaker.id = lecture.speaker_id' )
				-> leftJoin( Speakers ::tableName() . " additional_speaker", 'additional_speaker.id = lecture.additional_speaker_id' );
			return $dataLecture -> one();
		}
		public static function getLectureEvent($id_event)
		{
			return ( new Query() )
				-> from( self::tableName() . " lecture" )
				-> select('
					lecture.id as id_lecture,
					lecture.time_from,
					lecture.time_to,
					lecture.subject,
					lecture.id_event,
					lecture.additional_speaker_id,
					
					speaker.name as speaker_name,
					speaker.avatar as speaker_avatar,
					speaker.position as speaker_positional,
					
					additional_speaker.name as additional_speaker_name,
					additional_speaker.avatar as additional_speaker_avatar,
					additional_speaker.position as additional_speaker_position,
				')
				-> leftJoin( Speakers ::tableName() . " speaker", 'speaker.id = lecture.speaker_id' )
				-> leftJoin( Speakers ::tableName() . " additional_speaker", 'additional_speaker.id = lecture.additional_speaker_id' )
				-> groupBy( 'lecture.id' )
				-> where( [ 'id_event' => $id_event ] )
				-> orderBy( [ 'lecture.id' => SORT_DESC ] )
				-> all();
		}
	}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories_to_plase".
 *
 * @property int $id
 * @property string $category
 *
 * @property SpaceWork[] $spaceWorks
 */
class CategoriesToPlase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_to_plase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'category' => Yii::t('frontend', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpaceWorks()
    {
        return $this->hasMany(SpaceWork::className(), ['id_cate_plase' => 'id']);
    }
}

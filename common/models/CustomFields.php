<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_fields".
 *
 * @property int $id
 * @property string $title_field
 * @property string $description_field
 * @property int $source_id
 * @property int $type_source
 */
class CustomFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_field', 'description_field', 'source_id','type_source'], 'required'],
            [['source_id','type_source'], 'integer'],
            [['title_field', 'description_field'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'title_field' => Yii::t('frontend', 'Title Field'),
            'description_field' => Yii::t('frontend', 'Description Field'),
            'source_id' => Yii::t('frontend', 'Source ID'),
            'type_source' => Yii::t('frontend', 'Type Source'),
        ];
    }
}

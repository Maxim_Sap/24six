<?php

namespace common\models\traits;

use common\models\BookingConferenceRoom;
use common\models\{
	ConfRooms, SpaceWork, UserProfile
};
use common\models\UserMainPersonalBonus;
use Yii;

trait SpaceForm
{

	public $form_id; // incoming data, type id or conference room id
	public $place_id; // incoming data, working places || conference rooms - model id
	private $model; //working places || conference rooms, model
	private $countTheTickets; // count sale tickets

	public function getForm(){
		if(!$this->model = $this->getPlaceModel())
			return false;

		switch($this->form_id){
			case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
				return $this->monthUnfixed(); // us
				break;
			case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
				return $this->monthSeparate(); //ss
				break;
			case UserProfile::TYPE_PLACE_ONE_DAY :
				return $this->oneDay(); //od
				break;
			default :
				return $this->hourly();
		}
	}

	public function getPlaceModel(){
		$model = SpaceWork::findOne((int)$this->place_id);
		if($model instanceof SpaceWork){
			return $model;
		}
		return false;
	}


	/**
	 * @return bool|string
	 * working place
	 */
	private function monthSeparate(){
		if(!$this->model = $this->getPlaceModel())
			return false;
		$rent = $this->model->separate_price;

		$tax = $rent / 100;

		$tickets = $this -> getTotalSaleTickets( $this -> place_id );
		$totalCountSpaces = $this->getPlaceModel() -> max_office - $tickets[ 'type_place_ss' ][ 'saleTicketsSs' ];

		if( $this->getPlaceModel() -> max_office == $tickets[ 'type_place_ss' ][ 'saleTicketsSs' ] )
		{
			$html = "
				<h3>All offices are occupied</h3>
			";
		}
		else
		{
			$html = "
			<div class='custom_input_number'>
					<div class='control-label'>Number of people</div>
					<div class='custom_input_number'>
						<input type='number' class='cin_input' step='1' value='0' min='0' max='{$totalCountSpaces}'>
						<div class='cin_btn cin_increment pos_abs'>+</div>
						<div class='cin_btn cin_decrement pos_abs'>-</div>
					</div>
				</div>

				<div class='custom_input_number'>
					<div class='control-label'>Date</div>
					<input type='text' name='mount-date' autocomplete='off' placeholder = 'Place select' id='select-place'>
					<input type='text' name='date' id='select-place-day' style='display: none'>
				</div>

				<div class='price_count'>
				
					<div class='label left'>Count space</div>
					<div class='value right' id='countSpaces' data-id='{$totalCountSpaces}'>{$totalCountSpaces}</div>
					<div class='divider'></div>
				
					<div class='label left'>30 days rent</div>
					<div class='value right' id='mainPrice' data-id='{$rent}'>"."$".$rent."</div>
					<div class='divider'></div>
					
					<div class='label left'>Number of tickets</div>
					<div class='value right' id='countTickets'>1</div>
					<div class='divider'></div>
					
					<div class='label left'>Tax 1%</div>
					<div class='value right' id='mon_tax'>$<span>{$tax}</span></div>
					<div class='divider'></div>

					<div class='label left'>Total</div>
					<div class='value right'>$<span id='comboPrice'>0</span></div>
					<div class='divider'></div> 
					
				</div>
				<input type='hidden' id='type_place' name='type' value='".UserProfile::TYPE_PLACE_SEPARATE_SPACE."'>
				<input type='hidden' id='type_form' value='2'>
				<input type='hidden' id='main_tax_price' value='' name='main_tax_price'>
				
		";
		}
		return $html;
	}

	/**
	 * @return bool|string
	 * working place
	 */
	private function monthUnfixed(){
		if(!$this->model = $this->getPlaceModel())
			return false;
		$rent = $this->model->unfixiable_price;

		$tax = $rent / 100;

		$tickets = $this -> getTotalSaleTickets( $this -> place_id );
		$sum = $tickets[ 'type_place_us' ][ 'saleTicketsUs' ] + $tickets[ 'type_place_od' ][ 'saleTicketsOd' ];
		$totalCountSpaces = $this->getPlaceModel() -> working_places - $sum;

			$html = "
			<div class='custom_input_number'>
					<div class='control-label'>Number of people</div>
					<div class='custom_input_number'>
						<input type='number' class='cin_input' step='1' value='0' min='0' max='".$totalCountSpaces."'>
						<div class='cin_btn cin_increment pos_abs'>+</div>
						<div class='cin_btn cin_decrement pos_abs'>-</div>
					</div>
				</div>

				<div class='custom_input_number'>
					<div class='control-label'>Date</div>
					<input type='text' name='mount-date' autocomplete='off' placeholder = 'Place select' id='select-place'>
					<input type='text' name='date' id='select-place-day' style='display: none'>
				</div>

				<div class='price_count'>
				
					<div class='label left'>Count space</div>
					<div class='value right' id='countSpaces' data-id='{$totalCountSpaces}'>{$totalCountSpaces}</div>
					<div class='divider'></div>
				
					<div class='label left'>30 days rent</div>
					<div class='value right' id='mainPrice' data-id='{$rent}'>"."$".$rent."</div>
					<div class='divider'></div>
					
					<div class='label left'>Number of tickets</div>
					<div class='value right' id='countTickets'>1</div>
					<div class='divider'></div>
					
					<div class='label left'>Tax 1%</div>
					<div class='value right' id='mon_tax'>$<span>{$tax}</span></div>
					<div class='divider'></div>

					<div class='label left'>Total</div>
					<div class='value right' id='mon_total'>$<span id='comboPrice'>0</span></div>
					<div class='divider'></div>
					
				</div>
				<input type='hidden' id='type_place' name='type' value='".UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE."'>
				<input type='hidden' id='type_form' value='2'>
				<input type='hidden' id='main_tax_price' value='' name='main_tax_price'>
			";

		return $html;
	}

	/**
	 * @return bool|string
	 * working place
	 */
	private function oneDay(){
		if(!$this->model = $this->getPlaceModel())
			return false;

		$rent = $this->model->one_day_price;
		$tax = $rent / 100;

//		$tickets = $this -> getTotalSaleTickets( $this -> place_id );
//		$sum = $tickets[ 'type_place_us' ][ 'saleTicketsUs' ] + $tickets[ 'type_place_od' ][ 'saleTicketsOd' ];
		$totalCountSpaces = $this->getPlaceModel() -> working_places ;

		$html = "
			<div class='custom_input_number'>
					<div class='control-label'>Number of people</div>
					<div class='custom_input_number'>
						<input type='number' class='cin_input' step='1' value='0' min='0' max='".$totalCountSpaces."'>
						<div class='cin_btn cin_increment pos_abs'>+</div>
						<div class='cin_btn cin_decrement pos_abs'>-</div>
					</div>
				</div>

				<div class='custom_input_number'>
					<div class='control-label'>Date</div>
					<input type='text' name='date' autocomplete='off' placeholder = 'Place select' id='select-day'>
				</div>

				<div class='price_count'>
					
					<div id='box-count' style='display: none;'>
						<div class='label left'>Count space</div>
						<div class='value right' id='countSpaces' data-id='{$totalCountSpaces}'>{$totalCountSpaces}</div>
						<div class='divider'></div>
					</div>
				
					<div class='label left'>30 days rent</div>
					<div class='value right' id='mainPrice' data-id='{$rent}'>"."$".$rent."</div>
					<div class='divider'></div>
					
					<div class='label left'>Number of tickets</div>
					<div class='value right' id='countTickets'>1</div>
					<div class='divider'></div>
					
					<div class='label left'>Tax 1%</div>
					<div class='value right' id='mon_tax'>$<span>{$tax}</span></div>
					<div class='divider'></div>

					<div class='label left'>Total</div>
					<div class='value right' id='mon_total'>$<span id='comboPrice'>0</span></div>
					<div class='divider'></div>
				</div>
				<input type='hidden' id='type_place' name='type' value='".UserProfile::TYPE_PLACE_ONE_DAY."'>
				<input type='hidden' id='type_form' value='2'>
				<input type='hidden' id='main_tax_price' value='' name='main_tax_price'>
		";

		return $html;
	}

	/**
	 * @return bool|string
	 * conference rooms
	 */
	private function hourly(){
		$formatter = Yii ::$app -> formatter;

		if(!$this->model = $this->getConferenceRoomsModel())
			return false;

		$my_id = [];
		try {
			if (!Yii::$app->user->isGuest) {
				$my_id = (int)Yii::$app->user->identity->getId();
			}
		} catch (\Exception $e) {
			$my_id = NULL;
		}

		$price 		= $this->model->price_for_hour;
		$timeStart 	= $formatter -> asTime( $this->model->working_time_start, 'php:G');
		$timeEnd 	= $formatter -> asTime( $this->model->working_time_end, 'php:G');

		$checkTimeStart = $this -> getAllTimeMyOrderInConferenceRoom();

		$myBounusTime = $this -> getBonusTimeConfRoom( $my_id );


		$options = '';
		for( $i  = $timeStart; $i <= $timeEnd; $i++ )
		{
			$disabled = ( in_array( $i ,$checkTimeStart)) ? 'disabled' : '';
			$options .= "<option ". $disabled ." value='".$i."'>".$i.":00</option>";
		}
		$bonus = '';
		if( !empty( $myBounusTime ) && isset( $myBounusTime ) )
		{

			if( !Yii ::$app -> user -> isGuest )
			{
				if( $myBounusTime[ 0 ] -> count_time_for_conf_room != 0 or $myBounusTime[ 0 ] -> count_time_for_conf_room != NULL )
				{
					$bonus = "
					<div class='label left'>Free time</div>
					<div 
						class='value right' 
						id='free_tickets'
						data-id='".$myBounusTime[ 0 ] -> count_time_for_conf_room."'
					>".$myBounusTime[ 0 ] -> count_time_for_conf_room."</div>
					<input type='hidden' value='' id='additional_area'>
					<div id='elseMainAreaPrice' data-id='0'></div>
					<div class='divider'></div>
				";
				}
				else
				{
					$bonus = "
					<div class='label left'>Free time</div>
					<div class='value right' id='hour_bonuses'>no bonuses</div>
					<div id='elseMainAreaPrice' data-id='1'></div>
					<div class='divider'></div>
				";
				}
			}
			else {
				$bonus = null;
			}
		}



		$html = "
			<div class='custom_input_number'>
				<div class='control-label'>Choose Day</div>
				<input required type='text' autocomplete='off' name='date' placeholder = 'Place select' id='select-conference-room'>
			</div>

			<div class='custom_input_number' id='show-time' style='display: none;'>
				<div class='control-label'>Choose Time</div>
				<div style='display: flex' id='time-select'>
					<select required name='from' id='time_set_one' class='time_from'>
						<option>from</option>
						{$options}
					</select>
					>
					<select required name='to' id='time_set_two' class='time_to'>
						<option>to</option>
						{$options}
					</select>
				</div>
				<div id='errors'></div>
			</div>
		    <div class='price_count'>
				<div class='label left'>Type</div>
				<div class='value right'>Conference Room</div>
				<div class='divider'></div>
				
				{$bonus}
				
				<div class='label left'>Duration</div>
				<div class='value right' id='hour_duration'>0 hour</div>
				<div class='divider'></div>
			
				<div class='label left'>Price</div>
				<div class='value right' id='hour_price' data-id='{$price}'>$<span>{$price}</span></div>
				<div class='divider'></div>
			
				<div class='label left'>Total</div>
				<div class='value right' id='hour_total'>$<span>{$price}</span></div>
				<div class='divider'></div>
			</div>
			<input type='hidden' id='type_place' name='type' value='".UserProfile::TYPE_PLACE_CONF_ROM."'>
			<input type='hidden' id='price_hours_conf_rom' name='price_hours_conf_rom' value='".$price."'>		
			<input type='hidden' id='conf_room_id' name='conf_room_id' value='".$this->getConferenceRoomsModel() -> id."'>		
		";
		return $html;
	}

	public function getConferenceRoomsModel(){
		$model = ConfRooms::findOne((int)$this->form_id);

		if($model instanceof ConfRooms){
			return $model;
		}
		return false;
	}

	public function getAllTimeMyOrderInConferenceRoom( )
    {
        $model = ( new BookingConferenceRoom() ) -> getAllTimeMyOrderInConferenceRoom( $this -> form_id );
        return $model;
    }
    public function getTotalSaleTickets( $id_res )
	{
		return SpaceWork::checkTickets( $id_res );
	}
	public function getBonusTimeConfRoom( $user_id )
	{
		return UserMainPersonalBonus::checkMyBonus( $user_id );
	}

}
<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "user_profile".
	 *
	 * @property int             $id
	 * @property int             $user_id
	 * @property string          $logo
	 * @property string          $last_name
	 * @property string          $birthday
	 * @property string          $phone
	 * @property string          $address
	 * @property string          $created_at
	 * @property string          $my_space
	 *
	 * @property PopularEvents[] $popularEvents
	 * @property Questions[]     $questions
	 * @property User            $user
	 */
	class UserProfile extends \yii\db\ActiveRecord
	{
		/**
		 * @inheritdoc
		 */

		const TYPE_PAYMENT_DAILY_GUEST_PASS  = 1;
		const TYPE_PAYMENT_SILVER_MEMBERSHIP = 2;
		const TYPE_PAYMENT_GOLD_MEMBERSHIP   = 3;

		const TYPE_CONTINUE_PACKAGE_PLAN = 6;

		const TYPE_PAYMENT_TICKET      = 4;
		const TYPE_PAYMENT_FREE_TICKET = 5;

		// For space pay
		const TYPE_PLACE_ONE_DAY          = 'od';
		const TYPE_PLACE_UNFIXIABLE_SPACE = 'us';
		const TYPE_PLACE_SEPARATE_SPACE   = "ss";
		const TYPE_PLACE_CONF_ROM         = 'cr';

		const CONF_ROM   = "Conference room";
		const WORK_PLACE = "Working places";

		// ?
		const PRIVATE_OFFICE = "Private Office";

		const DELETE_ALL = 'delete';

		public static function payment_tickets( $id_res = NULL, $type_pay = NULL )
		{
			$queryTickets = [];
			switch( $type_pay )
			{
				case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
					$queryTickets = ( new Query() )
						-> from( SpaceWork ::tableName() ) -> where( [ 'id' => $id_res ] ) -> one();
					break;
				case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
					$queryTickets = ( new Query() )
						-> from( SpaceWork ::tableName() ) -> where( [ 'id' => $id_res ] ) -> one();
					break;
				case UserProfile::TYPE_PLACE_ONE_DAY :
					$queryTickets = ( new Query() )
						-> from( SpaceWork ::tableName() ) -> where( [ 'id' => $id_res ] ) -> one();
					break;
				case UserProfile::TYPE_PLACE_CONF_ROM :
					$queryTickets = ( new Query() )
						-> from( SpaceWork ::tableName() . " spaces" )
						-> where( [ 'spaces.id' => $id_res ] )
						-> leftJoin( ConfRooms ::tableName() . " conf_rom", "conf_rom.space_id = spaces.id" )
						-> one();
					break;
				case UserProfile::TYPE_PAYMENT_TICKET :
					$queryTickets = ( new Query() )
						-> from( Events ::tableName() ) -> where( [ 'id' => $id_res ] ) -> one();
					break;
			}
			return $queryTickets;
		}

		public static function tableName()
		{
			return 'user_profile';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'user_id' ], 'required' ],
				[ [ 'user_id' ], 'integer' ],
				[ [ 'birthday', 'created_at' ], 'safe' ],
				[ [ 'last_name' ], 'string', 'max' => 50 ],
				[ [ 'phone' ], 'string', 'max' => 30 ],
				[ [ 'user_id' ], 'unique' ],
				[ [ 'user_id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'user_id' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'         => Yii ::t( 'frontend', 'ID' ),
				'user_id'    => Yii ::t( 'frontend', 'User' ),
				'avatar'     => Yii ::t( 'frontend', 'Avatar' ),
				'logo'       => Yii ::t( 'frontend', 'Logo' ),
				'last_name'  => Yii ::t( 'frontend', 'Last Name' ),
				'birthday'   => Yii ::t( 'frontend', 'Birthday' ),
				'phone'      => Yii ::t( 'frontend', 'Phone' ),
				'address'    => Yii ::t( 'frontend', 'Address' ),
				'created_at' => Yii ::t( 'frontend', 'Created At' ),
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getPopularEvents()
		{
			return $this -> hasMany( PopularEvents ::className(), [ 'user_id' => 'user_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getQuestions()
		{
			return $this -> hasMany( Questions ::className(), [ 'user_id' => 'id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'user_id' ] );
		}

		public static function getPlansForFront( $type_plan = NULL )
		{
			$data = PlansWithBonus ::find() -> with( 'plan' ) -> with( 'bonus' );
			if( !empty( $type_plan ) )
			{
				$data -> where( [ 'id_plan' => $type_plan ] );
			}
			return $data -> all();
		}
	}

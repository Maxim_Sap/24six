<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "speakers".
 *
 * @property int $id
 * @property string $name
 * @property string $avatar
 * @property string $position
 *
 * @property Lectures[] $lectures
 */
class Speakers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'speakers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['name', 'avatar', 'position'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'Name'),
            'avatar' => Yii::t('frontend', 'Avatar'),
            'position' => Yii::t('frontend', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectures()
    {
        return $this->hasMany(Lectures::className(), ['speacer_id' => 'id']);
    }

    public static function deleteImages($img_name,$place){
        $webroot = Yii::getAlias('@frontend/web');
        switch ($place){
            case 'avatar':
                Speakers::updateAll(['avatar'=>''],['avatar'=>$img_name]);
                $path = \Yii::getAlias('@avatar');
                break;
        }
        @unlink($webroot.$path ."/origin/". $img_name);
        @unlink($webroot.$path ."/thumb/". $img_name);
        @unlink($webroot.$path ."/icon/". $img_name);

    }
}

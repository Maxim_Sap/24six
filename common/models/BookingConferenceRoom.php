<?php

	namespace common\models;

	use Yii;
	use yii\db\Query;

	/**
	 * This is the model class for table "booking_conference_room".
	 *
	 * @property int       $id
	 * @property int       $conf_room_id
	 * @property int       $user_id
	 * @property string    $date
	 * @property string    $time_start
	 * @property int       $status
	 * @property string    $code_res_room
	 *
	 * @property ConfRooms $confRoom
	 * @property User      $user
	 */
	class BookingConferenceRoom extends \yii\db\ActiveRecord
	{
		const SCENARIO_UPDATE_STATUS_PLACE_FOR_TICKETS = 'update_status_place_ticket';

		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'booking_conference_room';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'conf_room_id', 'user_id', 'date', 'time_start', 'status', 'code_res_room' ], 'required' ],
				[ [ 'conf_room_id', 'user_id', 'status' ], 'integer' ],
				[ [ 'date', 'time_start' ], 'safe' ],
				[ [ 'code_res_room' ], 'string', 'max' => 13 ],
				[ [ 'status' ], 'required', 'on' => self::SCENARIO_UPDATE_STATUS_PLACE_FOR_TICKETS ],
				[ [ 'conf_room_id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => ConfRooms ::className(), 'targetAttribute' => [ 'conf_room_id' => 'id' ] ],
				[ [ 'user_id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'user_id' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'            => Yii ::t( 'frontend', 'ID' ),
				'conf_room_id'  => Yii ::t( 'frontend', 'Conf Room ID' ),
				'user_id'       => Yii ::t( 'frontend', 'User ID' ),
				'date'          => Yii ::t( 'frontend', 'Date' ),
				'time_start'    => Yii ::t( 'frontend', 'Time Start' ),
				'status'        => Yii ::t( 'frontend', 'Status' ),
				'code_res_room' => Yii ::t( 'frontend', 'Code Resource' ),
			];
		}

		public function scenarios()
		{
			$scenarios                                                   = parent ::scenarios();
			$scenarios[ self::SCENARIO_UPDATE_STATUS_PLACE_FOR_TICKETS ] = [ 'status' ];

			return $scenarios;
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getConfRoom()
		{
			return $this -> hasOne( ConfRooms ::className(), [ 'id' => 'conf_room_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'user_id' ] );
		}

		public static function getTicketsForConfRoom( $user_id, $id_ticket = NULL )
		{

			$rows = ( new Query() )
				-> from( ConfRooms ::tableName() . " conf_room" )
				-> leftJoin( BookingConferenceRoom ::tableName() . " book_conf_room", 'book_conf_room.conf_room_id = conf_room.id' )
				-> select(
					'
					conf_room.id as conf_room_id,
					conf_room.name,
					conf_room.price_for_hour,
					conf_room.working_days,
					
					book_conf_room.date,
					book_conf_room.status,
				 	book_conf_room.code_res_room,
				 	book_conf_room.id as id_ticket,
				 	book_conf_room.created_at,
				'
				)
				-> groupBy( 'book_conf_room.code_res_room' )
				-> where( [ 'book_conf_room.user_id' => $user_id ] )
				-> orderBy( [ 'book_conf_room.created_at' => SORT_DESC, 'book_conf_room.date' => SORT_DESC ] )
				-> all();

			$data_mass = [];
			foreach( $rows as $row )
			{
				$data_mass[ $row[ 'code_res_room' ] ][ 'name' ]           = $row[ 'name' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'price_for_hour' ] = $row[ 'price_for_hour' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'working_days' ]   = $row[ 'working_days' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'date' ]           = $row[ 'date' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'status' ]         = $row[ 'status' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'id_ticket' ]      = $row[ 'id_ticket' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'created_at' ]     = $row[ 'created_at' ];
				$data_mass[ $row[ 'code_res_room' ] ][ 'type_place' ]     = UserProfile::TYPE_PLACE_CONF_ROM;

				$data_mass[ $row[ 'code_res_room' ] ][ 'time_start' ] = ( new self() ) -> getTimeStart( $user_id, $row[ 'date' ], $row[ 'code_res_room' ] );
				$data_mass[ $row[ 'code_res_room' ] ][ 'time_end' ]   = ( new self() ) -> getTimeEnd( $user_id, $row[ 'date' ], $row[ 'code_res_room' ] );
			}
			return $data_mass;
		}

		public static function getOneTicketConfRoom( $id_tickets, $user_id )
		{
			$row       = ( new Query() )
				-> from( ConfRooms ::tableName() . " conf_room" )
				-> leftJoin( BookingConferenceRoom ::tableName() . " book_conf_room", 'book_conf_room.conf_room_id = conf_room.id' )
				-> leftJoin( SpaceWork ::tableName() . " spaces", 'spaces.id = conf_room.space_id' )
				-> select(
					'
					conf_room.id as conf_room_id,
					conf_room.name,
					conf_room.price_for_hour,
					conf_room.working_days,
					
					book_conf_room.date,
					book_conf_room.status,
				 	book_conf_room.code_res_room,
				 	book_conf_room.id as id_ticket,
				 	
				 	spaces.title,
				 	spaces.id as id_place,
				 	spaces.address,
				'
				)
				-> groupBy( 'book_conf_room.code_res_room' )
				-> where( [ 'book_conf_room.user_id' => $user_id ] )
				-> andWhere( [ 'book_conf_room.id' => $id_tickets ] )
				-> one();
			$data_mass = [];
			foreach( $data = [ $row ] as $item )
			{
				$data_mass[ $item[ 'id_ticket' ] ][ 'conf_room_id' ]   = $item[ 'conf_room_id' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'name' ]           = $item[ 'name' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'price_for_hour' ] = $item[ 'price_for_hour' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'working_days' ]   = $item[ 'working_days' ];

				$data_mass[ $item[ 'id_ticket' ] ][ 'date' ]          = $item[ 'date' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'status' ]        = $item[ 'status' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'code_res_room' ] = $item[ 'code_res_room' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'id_ticket' ]     = $item[ 'id_ticket' ];

				$data_mass[ $item[ 'id_ticket' ] ][ 'title' ]    = $item[ 'title' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'id_place' ] = $item[ 'id_place' ];
				$data_mass[ $item[ 'id_ticket' ] ][ 'address' ]  = $item[ 'address' ];

				$data_mass[ $item[ 'id_ticket' ] ][ 'time_start' ] = ( new self() ) -> getTimeStart( $user_id, $item[ 'date' ], $item[ 'code_res_room' ] );
				$data_mass[ $item[ 'id_ticket' ] ][ 'time_end' ]   = ( new self() ) -> getTimeEnd( $user_id, $item[ 'date' ], $item[ 'code_res_room' ] );
			}
			return $data_mass;
		}

		public function getTimeStart( $user_id, $date, $code_res )
		{
			$time_start = ( new Query() )
				-> from( self ::tableName() . " booking_room" )
				-> select(
					'
					booking_room.time_start as time_start,
				'
				)
				-> leftJoin( ConfRooms ::tableName() . " conf_room", 'conf_room.id = booking_room.conf_room_id' )
				-> where( [ 'booking_room.user_id' => $user_id ] )
				-> andWhere( [ 'date' => $date ] )
				-> andWhere( [ 'code_res_room' => $code_res ] )
				-> orderBy( [ 'booking_room.time_start' => SORT_ASC ] )
				-> one();

			return $time_start;
		}

		public function getTimeEnd( $user_id, $date, $code_res )
		{
			$time_end = ( new Query() )
				-> from( self ::tableName() . " booking_room" )
				-> select(
					'
					booking_room.time_start as time_end,
				'
				)
				-> leftJoin( ConfRooms ::tableName() . " conf_room", 'conf_room.id = booking_room.conf_room_id' )
				-> where( [ 'booking_room.user_id' => $user_id ] )
				-> andWhere( [ 'date' => $date ] )
				-> andWhere( [ 'code_res_room' => $code_res ] )
				-> orderBy( [ 'booking_room.time_start' => SORT_DESC ] )
				-> one();

			return $time_end;
		}

		public function getOrderMyConferenceRoom( $id_conf_rom, $date = NULL, $data_time )
		{
			$rows = BookingConferenceRoom ::find()

										  -> select( 'time_start' )
										  -> where( [ 'conf_room_id' => $id_conf_rom ] )
										  -> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
										  -> andWhere( [ 'date' => $date ] )
										  -> orderBy( [ 'time_start' => SORT_ASC ] )
										  -> andWhere( [ 'in', 'time_start', $data_time ] )
										  -> column();
			if( $rows )
			{
				$dates = [];
				foreach( $rows as $row )
				{
					$dates[] = Yii ::$app -> formatter -> asTime( $row, 'php:G' ) . ':00';
				}
				return $dates;
			}
			return [];
		}

		public function getAllTimeMyOrderInConferenceRoom( $id_conf_rom, $date = NULL )
		{
			$rows = BookingConferenceRoom ::find()
										  -> select( 'time_start' )
										  -> where( [ 'conf_room_id' => $id_conf_rom ] )
										  -> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
										  -> andWhere( [ 'date' => $date ] )
										  -> orderBy( [ 'time_start' => SORT_ASC ] )
										  -> column();
			if( $rows )
			{
				$dates = [];
				foreach( $rows as $row )
				{
					$dates[] = Yii ::$app -> formatter -> asTime( $row, 'php:G' );
				}
				return $dates;
			}
			return [];
		}

		public static function addFundsConfRoom( $conf_room_id, $user_id, $order_date, $time )
		{
			$randomString = (string)self ::randomNumber();
			for( $i = $time[ 'from' ]; $i < $time[ 'to' ]; $i++ )
			{
				$model                  = new self();
				$model -> conf_room_id  = $conf_room_id;
				$model -> user_id       = $user_id;
				$model -> date          = $order_date;
				$model -> time_start    = self ::convertToTime( $i );
				$model -> code_res_room = $randomString;
				$model -> status        = ResourcesEventsPlaces::ACTIVE_TICKETS;
				if( $model -> validate() )
				{
					if( !$model -> save() )
					{
						dd( $model -> errors );
					}
				}
			}
		}

		static function randomNumber()
		{
			$length = 13;
			$str    = '';
			for( $i = 0; $i < $length; ++$i )
			{
				$first = $i ? 0 : 1;
				$n     = mt_rand( $first, 9 );
				$str   .= $n;
			}
			return $str;
		}

		public static function convertToTime( $from )
		{
			if( $from < 10 )
			{
				$timeFrom = '0' . $from . ':00:00';
			}
			else
			{
				$timeFrom = $from . ':00:00';
			}
			return $timeFrom;
		}
	}

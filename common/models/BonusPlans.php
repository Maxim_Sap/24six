<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bonus_plans".
 *
 * @property int $id
 * @property int $count_workspaces
 * @property int $count_events_week
 * @property int $count_daily_guest passes
 * @property int $count_time_for_massage
 * @property int $count_time_for_conf_room
 * @property int $priority_registration_for_all_event
 * @property int $complimentary_beverages
 * @property int $mailbox_with_las_olas
 * @property int $fax_and_printer_service
 *
 * @property PlansWithBonus[] $plansWithBonuses
 */
class BonusPlans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count_workspaces', 'count_time_for_massage' , 'count_time_for_conf_room' ,'count_events_week', 'count_daily_guest passes', 'priority_registration_for_all_event', 'complimentary_beverages', 'mailbox_with_las_olas', 'fax_and_printer_service'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'count_workspaces' => Yii::t('frontend', 'Count Workspaces'),
            'count_events_week' => Yii::t('frontend', 'Count Events Week'),
            'count_daily_guest passes' => Yii::t('frontend', 'Count Daily Guest Passes'),
            'count_time_for_massage' => Yii::t('frontend', 'Count Time For Massage'),
            'count_time_for_conf_room' => Yii::t('frontend', 'Count Time For Conf Room'),
            'priority_registration_for_all_event' => Yii::t('frontend', 'Priority Registration For All Event'),
            'complimentary_beverages' => Yii::t('frontend', 'Complimentary beverages'),
            'mailbox_with_las_olas' => Yii::t('frontend', 'Mailbox With Las Olas'),
            'fax_and_printer_service' => Yii::t('frontend', 'Fax And Printer Service'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlansWithBonuses()
    {
        return $this->hasMany(PlansWithBonus::className(), ['id_bonus' => 'id']);
    }
}

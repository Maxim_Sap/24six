<?php

	namespace common\models;

	use backend\models\search\SearchSetMainPlaces;
	use Stripe\Plan;
	use Yii;
	use yii\db\Query;
	use yii\helpers\Url;

	/**
	 * This is the model class for table "user_assn_payments".
	 *
	 * @property int    $id
	 * @property int    $user_Id
	 * @property int    $payment_type
	 * @property int    $status
	 * @property string $date_end
	 *
	 * @property User   $user
	 */
	class UserAssnPayments extends \yii\db\ActiveRecord
	{

		const SCENARIO_UPDATE_STATUS_ASSN_PLANS = 'update_status_plan';

		/**
		 * @inheritdoc
		 */
		public static function tableName()
		{
			return 'user_assn_payments';
		}

		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			return [
				[ [ 'user_Id', 'payment_type', 'date_end' ], 'required' ],
				[ [ 'user_Id', 'payment_type', 'status' ], 'integer' ],
				[ [ 'status' ], 'required', 'on' => self::SCENARIO_UPDATE_STATUS_ASSN_PLANS ],
				[ [ 'date_end' ], 'safe' ],
				[ [ 'user_Id' ], 'exist', 'skipOnError' => TRUE, 'targetClass' => User ::className(), 'targetAttribute' => [ 'user_Id' => 'id' ] ],
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'id'           => Yii ::t( 'frontend', 'ID' ),
				'user_Id'      => Yii ::t( 'frontend', 'User  ID' ),
				'payment_type' => Yii ::t( 'frontend', 'Payment Type' ),
				'date_end'     => Yii ::t( 'frontend', 'Date End' ),
				'status'       => Yii ::t( 'frontend', 'Status' ),
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */

		public function scenarios()
		{
			$scenarios                                            = parent ::scenarios();
			$scenarios[ self::SCENARIO_UPDATE_STATUS_ASSN_PLANS ] = [ 'status', 'confirm_new_password', 'new_password' ];

			return $scenarios;
		}

		public function getUser()
		{
			return $this -> hasOne( User ::className(), [ 'id' => 'user_Id' ] );
		}

		public static function checkCurrentPlans()
		{

			return self ::deleteAll( [ '<', 'date_end', date( 'Y-m-d: H:i:s', time() ) ] );
		}

		public static function addPayPlan( $user_id, $type_plan, $date = NULL, $my_assn = NULL )
		{
			switch( $type_plan )
			{
				case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
					$date_end = time() + 60 * 60 * 24;
					break;
				case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
					$date_end = time() + 60 * 60 * 24 * 30;
					break;
				case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
					$date_end = time() + 60 * 60 * 24 * 30;
					break;
			}
			$data = [
				'user_id'   => $user_id,
				'type_plan' => $type_plan,
				'date_end'  => $date_end,
			];
			if( $my_assn == 2 )
			{
				( UserAssnPayments ::find()
								-> where( [ 'user_Id' => $user_id ] )
								-> andWhere( [ 'date_end' => $date ] )
								-> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
								-> orderBy( 'date_end ASC' )
								-> one() )
				-> delete();
				( new self ) -> setAssnUserPlan( $data );
			}
			else
			{
				( new self ) -> setAssnUserPlan( $data );
			}
		}

		function setAssnUserPlan( $data )
		{
			$model                 = new self();
			$model -> user_Id      = $data[ 'user_id' ];
			$model -> payment_type = $data[ 'type_plan' ];
			$model -> date_end     = date( 'Y-m-d: H:i:s', $data[ 'date_end' ] );
			$model -> status       = ResourcesEventsPlaces::ACTIVE_TICKETS;
			if( !$model -> save() )
			{
				dd( $model -> errors );
			}
			return $model;
		}

		public static function addContinuePlan( $user_id, $plan_type )
		{
			$findMyAssnPlans = UserAssnPayments ::find()
												-> where( [ 'user_Id' => $user_id, 'payment_type' => $plan_type ] )
												-> orderBy( [ 'date_end' => SORT_DESC ] )
												-> one();

			$payment_type    = isset( $plan_type ) && in_array(
				$plan_type,
				[
					(int)UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS,
					(int)UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP,
					(int)UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP,
				]
			) ? $plan_type : NULL;

			$date_end = [];
			switch( $plan_type )
			{
				case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS :
					$date_end = strtotime( $findMyAssnPlans -> date_end ) + 60 * 60 * 24;
					break;
				case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP :
					$date_end = strtotime( $findMyAssnPlans -> date_end ) + 60 * 60 * 24 * 30;
					break;
				case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP :
					$date_end = strtotime( $findMyAssnPlans -> date_end ) + 60 * 60 * 24 * 30;
					break;
			}

			$plan = Plans ::findOne( $payment_type );

			if( $payment_type == NULL )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Plan' );
				return Url ::toRoute( '/' );
			}
			$model                 = new UserAssnPayments();
			$model -> user_Id      = $user_id;
			$model -> payment_type = $payment_type;
			$model -> date_end     = date( 'Y-m-d H:i:s', $date_end );
			$model -> status       = ResourcesEventsPlaces::ACTIVE_TICKETS;
			if( !$model -> save() )
			{
				var_dump( $model -> errors );
				die();
			}
			else
			{
				Yii ::$app -> session -> setFlash( 'message', 'You have extended the plan:' . $plan -> title );
				return Url ::toRoute( [ '/account/memberships' ] );
			}
		}

		public static function checkMyPlans( $user_id )
		{
			 $row = UserAssnPayments ::find()
									-> select( 'payment_type' )
									-> where( [ 'user_id' => $user_id ] )
									-> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
									-> orderBy( [ 'date_end' => SORT_DESC ] )
									-> one();
			 if( !empty( $row ) or !isset( $row ) )
			 {
				 return $data = [
				 	'type' => UserProfile::DELETE_ALL,
					'row' => $row,
				 ];
			 }
			 else
			 {
				 return $row;
			 }
		}

		public static function getMyAssnPlans( $user_id )
		{
			$row = ( new Query() )
				-> from( UserAssnPayments ::tableName() . " user_assn" )
				-> select(
					'
						user_assn.date_end, 
						user_assn.status, 
						user_assn.payment_type,
						
						plan.title,
					'
				)
				-> leftJoin( Plans ::tableName() . " plan", 'plan.id = user_assn.payment_type' )
				-> orderBy( 'user_assn.date_end ASC' )
				-> where( [ 'user_id' => $user_id ] )
				-> all();
			return $row;
		}
	}

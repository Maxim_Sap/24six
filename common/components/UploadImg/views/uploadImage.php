<style>
	#<?= $form_image_id ?>{
		display: none;
	}
    #<?= $form_image_id ?>_box .blog_img{
        height: <?=$block_height;?>px;
	}
    #<?= $form_image_id ?>_box .default{
        height: <?=$block_height;?>px;
        line-height: <?=$block_height;?>px;
    }
    #<?= $form_image_id ?>_box .img_db{
        height: <?=$block_height;?>px;
    }
</style>

<div id="<?=$form_image_id?>_box" data-type="<?=$form_image_id?>" class="widget_image">
	<?php //dd(\Yii::getAlias('@webroot') . $pathToImg . $imageName);?>
	<?php
        $thumb_img = $pathToImg ."thumb/". $imageName;
		$origin_img = $pathToImg ."origin/". $imageName;
        $front_url = Yii::$app->params['front-url'];
        $width = '';
        $height = '';
        $no_photo = false;
		//var_dump(\Yii::getAlias('@frontend/web') . $thumb_img);die;
        if($imageName and is_file(\Yii::getAlias('@frontend/web') . $thumb_img)) {
            $img_size = getimagesize(\Yii::getAlias('@frontend/web') . $thumb_img);
            $width = $img_size[0];
            $height = $img_size[1];
            $no_photo = true;
        }
		?>
		<div id="<?=$form_image_id?>_preview" class="img_db"
             data-width="<?=$width?>"
             data-height="<?=$height?>"
             data-original-src="<?= $front_url.$origin_img ?>"
			 style="
                     background-image: url('<?= $front_url.$thumb_img?>');
                     <?=(!$no_photo)?'display:none;':'';?>
                     ">
			<?php if(in_array('delete',$buttons)){?>
                <a class="del_db" data-image-name="<?=$imageName?>" href="javascript:void(0);" data-palce="<?=$form_image_id?>"><i class="fa fa-trash" aria-hidden="true" title="delete"></i></a>
            <?php } ?>
            <?php if(in_array('crop',$buttons)){?>
			    <a class="crop-image" data-image-name="<?=$imageName?>" href="javascript:void(0);" onclick="window['CropImageLibrary']['cropPhoto'](<?=$form_image_id?>_box);" title="Crop image"><i class="fa fa-crop" aria-hidden="true"></i></a>
            <?php } ?>
		</div>
            <div class="crop-img-block">
                <img class='crop-img-source' src='<?= $front_url.$origin_img ?>'/>
                <a class="crop-apply" href="javascript:void(0);" onclick="window['CropImageLibrary']['applyCrop'](<?=$form_image_id?>_box);" title="Apply Crop"><i class="fa fa-check-square-o" aria-hidden="true"></i></a>
                <a class="close-btn" href="javascript:void(0);" onclick="window['CropImageLibrary']['closeCrop'](<?=$form_image_id?>_box);" title="Close"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>

	<?php //endif;?>
	<div class="img_db" hidden></div>
	<div class="blog_img">
		<div class="del glyphicon glyphicon-remove-circle"></div>
	</div>
	<label class="label-for-input-file">
		<div class="default" style="<?=(!$no_photo)?'display:block;':'';?>"> <?php //if not images ?>
			<div class="add_image"><i class="fa fa-plus-circle" aria-hidden="true"></i></div>
			<input type="file" name="file" class="file-input" style="display: none" disabled>
			<input class="x1" type="hidden" value="0">
			<input class="x2" type="hidden" value="0">
			<input class="y1" type="hidden" value="0">
			<input class="y2" type="hidden" value="0">
			<input class="width" type="hidden" value="0">
			<input class="height" type="hidden" value="0">
			<input class="photo_field_name" name="<?=$field_name?>" type="hidden" value="<?=$imageName?>">
		</div>
	</label>
</div>


<?php ##### JS ##### ?>
<?php
$js = <<<JS
var front_url = '$front_url'; 
$(document).ready(function () {
	if (!$('#{$form_image_id}_box .img_db').is(':visible')) {
		$('#{$form_image_id}_box .default').show();
		$('#{$form_image_id}_box .file-input').attr('disabled', false);
	}
});

$('#{$form_image_id}_box .file-input').change(function () {
	var fd = new FormData;
	var resource_id = $('#resource_id').val();  
	var resource_type = $('#resource_type').val();
	fd.append('image', $('#{$form_image_id}_box .file-input')[0].files[0])
	fd.append('img_type', '$form_image_id')
	fd.append('resource_id', resource_id)
	fd.append('resource_type', resource_type);
	$.ajax({
		url: '{$action}',
		data: fd,
		type: 'POST',
		processData: false,
		contentType: false,
		dataType: "json",
		error: function () {
			alert('Error')
		},
		success: function (data) {
			if (data) {
				if (data.result) {
					$('#{$form_image_id}_preview').css({'background-image': 'url("'+front_url+ data.thumb_path + '")'});
					$('#{$form_image_id}_preview').attr('data-original-src',front_url+data.thumb_path);
					$('#{$form_image_id}_preview .crop-image').attr('data-image-name',data.img_name);
					$('#{$form_image_id}_box .crop-img-block .crop-img-source').attr('src',front_url+data.origin_path);
					$('#{$form_image_id}_box .photo_field_name').val(data.img_name);
					$('#{$form_image_id}_preview').show();
                    $('#{$form_image_id}_box .default').hide();
                    $('#{$form_image_id}_box .file-input').attr('disabled', true);
                    $('#{$form_image_id}').val("");
				} else {
					alert(data.message);
				}
			} else {
				alert('Error');
			}
		}
	});
});

/*$('#{$form_image_id}_box .blog_img .del').on('click', function () {
	$('#{$form_image_id}_box .blog_img').hide();
	$('#{$form_image_id}_box .default').show();
	$('#{$form_image_id}_box .file-input').attr('disabled', false);
	$('#{$form_image_id}').val("");
});*/

$('#{$form_image_id}_box .img_db .del_db').on('click', function () {
	if(confirm('Do you want delete photo?')){
	    $('#{$form_image_id}_preview').css({'background-image': 'none'});
        $('#{$form_image_id}_box .img_db').hide();
        $('#{$form_image_id}_box .default').show();
        $('#{$form_image_id}_box .file-input').attr('disabled', false);
        var photo_name = $('#{$form_image_id}_preview a.del_db').attr('data-image-name'); 
        delete_one_photo(photo_name,'{$form_image_id}');
	}
    
});
if(typeof delete_one_photo === 'undefined'){
    function delete_one_photo(photo_name,place) {
      var resource_type = $('#resource_type').val();
        $.ajax({
            url: '/index.php?r=upload/del-image/&name='+photo_name+'&place='+place+'&resource_type='+resource_type,
            type: 'GET',
            processData: false,
            contentType: false,
            dataType: "json",
            error: function () {
                alert('Error')
            },
            success: function (data) {
                if (data) {
                
                }
            }
        });
    }
}
JS;
$this->registerJs($js);
?>










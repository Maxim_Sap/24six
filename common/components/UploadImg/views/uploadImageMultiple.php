<span class="input-file-block">
    <input id="<?=$form_image_id?>_file" type="file" name="file" class="file-input" style="display: none" >
    <label for="<?=$form_image_id?>_file">
        <i class="fa fa-plus-circle" aria-hidden="true"></i>
    </label>
</span>
<?php if (is_array($imageName)) {
    $i = 1;
    $safe_image_name = $form_image_id;
    foreach ($imageName as $one) {
        $form_image_id = $safe_image_name.'_' . $i; ?>
        <style>
            #<?= $form_image_id ?>{
                    display: none;
                }
            #<?= $form_image_id ?>_box .blog_img {
                height: <?=$block_height;?>px;
            }

            #<?= $form_image_id?>_box .default {
                height: <?=$block_height;?>px;
                line-height: <?=$block_height;?>px;
            }

            #<?= $form_image_id?>_box .img_db {
                height: <?=$block_height;?>px;
            }
        </style>
        <div id="<?= $form_image_id ?>_box" data-type="<?= $safe_image_name ?>" class="widget_image">
            <?php //dd(\Yii::getAlias('@webroot') . $pathToImg . $imageName);
            ?>
            <?php
            $thumb_img = $pathToImg . "thumb/" . $one->name;
            $origin_img = $pathToImg . "origin/" . $one->name;
            $width = '';
            $height = '';
            $no_photo = false;
            if ($one->name and is_file(\Yii::getAlias('@webroot') . $thumb_img)) {
                $img_size = getimagesize(\Yii::getAlias('@webroot') . $thumb_img);
                $width = $img_size[0];
                $height = $img_size[1];
                $no_photo = true;
            }
            ?>
            <div id="<?= $form_image_id ?>_preview" class="img_db"
                 data-width="<?= $width ?>"
                 data-height="<?= $height ?>"
                 data-original-src="<?= 'https://' . $_SERVER['HTTP_HOST'] . $origin_img ?>"
                 style="
                         background-image: url('<?= 'https://' . $_SERVER['HTTP_HOST'] . $thumb_img ?>');
                 <?= (!$no_photo) ? 'display:none;' : ''; ?>
                         ">
                <a class="del_db" href="javascript:void(0);" data-palce="<?= $place ?>">
                    <i class="fa fa-trash" aria-hidden="true" title="delete"></i>
                </a>
                <a class="crop-image" data-image-name="<?= $one->name ?>" href="javascript:void(0);"
                   onclick="window['CropImageLibrary']['cropPhoto'](<?= $form_image_id ?>_box);" title="Crop image">
                    <i class="fa fa-crop" aria-hidden="true"></i>
                </a>
            </div>
            <div class="crop-img-block">
                <img class='crop-img-source' src='<?= 'https://' . $_SERVER['HTTP_HOST'] . $origin_img ?>'/>
                <a class="crop-apply" href="javascript:void(0);"
                   onclick="window['CropImageLibrary']['applyCrop'](<?= $form_image_id ?>_box);" title="Apply Crop">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                </a>
                <a class="close-btn" href="javascript:void(0);"
                   onclick="window['CropImageLibrary']['closeCrop'](<?= $form_image_id ?>_box);" title="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>

            <?php //endif;
            ?>
            <div class="img_db" hidden></div>
<!--            <div class="blog_img">-->
<!--                <div class="del glyphicon glyphicon-remove-circle"></div>-->
<!--            </div>-->
            <input class="x1" type="hidden" value="0">
            <input class="x2" type="hidden" value="0">
            <input class="y1" type="hidden" value="0">
            <input class="y2" type="hidden" value="0">
            <input class="width" type="hidden" value="0">
            <input class="height" type="hidden" value="0">
        </div>
        <?php
        $js = <<<JS
$('#{$form_image_id}_box .img_db .del_db').on('click', function () {
	if(confirm('Вы уверены что хотите удалить фото?')){
        var photo_name = $('#{$form_image_id}_preview .crop-image').attr('data-image-name');
        var img_type = $('#{$form_image_id}_box').attr('data-type');
        delete_one_photo_in_gallery(photo_name,img_type,$('#{$form_image_id}_box'));
	}
});
JS;
        $this->registerJs($js);
        ?>
        <?php $i++;
    }
} ?>
<?php
$js2 = <<<JS2
$('#{$safe_image_name} .input-file-block .file-input').change(function () {
	var fd = new FormData;
	fd.append('image', $('#{$safe_image_name} .input-file-block .file-input')[0].files[0])
	fd.append('img_type', '$safe_image_name')
	$.ajax({
		url: '{$action}',
		data: fd,
		type: 'POST',
		processData: false,
		contentType: false,
		dataType: "json",
		error: function () {
			alert('Error')
		},
		success: function (data) {
            if (data.result) {
                location.reload();
            } else {
                alert(data.message);
            }
		}
	});
});

if(typeof delete_one_photo_in_gallery === 'undefined'){
    function delete_one_photo_in_gallery(photo_name,place,element) {
      $.ajax({
            url: '/account/profile/del-image/?name='+photo_name+'&place='+place,
            type: 'GET',
            processData: false,
            contentType: false,
            dataType: "json",
            error: function () {
                alert('Error')
            },
            success: function (data) {
                if (data.success) {
                    element.remove();
                }
            }
        });
    }
}
JS2;
$this->registerJs($js2);
?>
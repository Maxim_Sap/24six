<?php
namespace common\components\UploadImg;

use yii\base\Widget;

class UploadImg extends Widget
{
	public $imageName; // имя картинки из базы
	public $pathToImg; //где хранится картинка, от web/
	public $action; // Путь к action, который принемает фаил
	public $form_image_id; // id поля формы, где прикрепляем имя фаила
	public $place; // id поля формы, где прикрепляем имя фаила
	public $block_height; // id поля формы, где прикрепляем имя фаила
	public $type; //single or multiple
	public $field_name; //filed name for new records
	public $buttons; //filed name for new records

	public function run(){
        $template_name = 'uploadImage';
	    if($this->type == 'multiple'){
		    $template_name = 'uploadImageMultiple';
        }

	    return $this->render($template_name,
			[
				'imageName'=>$this->imageName,
				'pathToImg'=>$this->pathToImg,
				'action'=>$this->action,
				'form_image_id'=>$this->form_image_id,
				'place'=>$this->place,
				'block_height'=>$this->block_height,
				'type'=>$this->type,
				'field_name'=>$this->field_name,
				'buttons'=>(!empty($this->buttons)) ? $this->buttons : ['delete','crop'],
			]);
	}
}
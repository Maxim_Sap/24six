<?php


namespace common\components\UploadImg;


use yii\base\Model;
use Yii;

class ModelUploadImg extends Model
{
	private $path_temp_dir;
	public $image;
	public function rules(){
		return [
			[['image'], 'file', 'skipOnEmpty' => FALSE, 'extensions' => 'png, jpg, jpeg, gif', 'maxSize' => 1024 * 1024 * 6],
		];
	}
	// the path to the temporary folder
	public function setPath($path){
		$this->path_temp_dir = $path;
	}

	public function upload($img_type,$resource_id){
		if($this->validate()){
            $webroot = Yii::getAlias('@frontend/web');
		    switch ($img_type){
                case 'avatar':
                    $dir        = Yii::getAlias('@avatar');
                    break;
                case 'logo':
                    $dir        = Yii::getAlias('@logo');
                    break;
                case 'background_image':
                    $dir        = Yii::getAlias('@background_image');
                    break;
                /*case 'gallery':
                    $dir        = Yii::getAlias('@gallery')."/".$user_id;
                    break;
                case 'certificates':
                    $dir        = Yii::getAlias('@certificates')."/".$user_id;
                    break;*/
            }

            $origin_path = $webroot."/".$dir."/origin/";
            $thumb_path = $webroot."/".$dir."/thumb/";
            $icon_path = $webroot."/".$dir."/icon/";
            if (!file_exists($origin_path)) {
                mkdir($origin_path, 0777, TRUE);
                mkdir($thumb_path, 0777, TRUE);
                mkdir($icon_path, 0777, TRUE);
            }
		    $new_img_name = \Yii::$app->security->generateRandomString(20) . '.' . $this->image->extension;
//			if (!is_dir($this->path_temp_dir)) {
//				mkdir($this->path_temp_dir, 0777, TRUE);
//			}
			$this->image->saveAs($origin_path.$new_img_name);

            $file = Yii::getAlias($origin_path . $new_img_name);
            $image = Yii::$app->image->load($file);
			switch ($img_type){
                case 'avatar':
                    $image->resize(250, 300, \yii\image\drivers\Image::WIDTH);
                    $image->save($thumb_path . $new_img_name);
                    $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                    $image->save($icon_path . $new_img_name);
                    break;
                case 'logo':
                    $image->resize(260, 160, \yii\image\drivers\Image::WIDTH);
                    $image->save($thumb_path . $new_img_name);
                    break;
                case 'background_image':
                    $image->resize(1467, 400, \yii\image\drivers\Image::HEIGHT);
                    $image->save($thumb_path . $new_img_name);
                    $image->resize(500, 500, \yii\image\drivers\Image::CROP);
                    $image->save($icon_path . $new_img_name);
                    break;
                /*case 'gallery':
                    $image->resize(300, 300, \yii\image\drivers\Image::HEIGHT);
                    $image->save($thumb_path . $new_img_name);
                    break;
                case 'certificates':
                    $image->resize(300, 300, \yii\image\drivers\Image::HEIGHT);
                    $image->save($thumb_path . $new_img_name);
                    break;*/
            }

			return [
			    'result' => true,
                'img_name' => $new_img_name,
                'origin_path' => $dir."/origin/".$new_img_name,
                'thumb_path' => $dir."/thumb/".$new_img_name,
                'icon_path' => $dir."/icon/".$new_img_name,
            ];
		}else{
			return ['result' => FALSE, 'message' => 'Invalid File Format'];
		}
	}

}
<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@background_image' => '/source/img/background_image/',
        '@avatar' => '/source/img/avatar/',
        '@logo' => '/source/img/logo/',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'image'        => array(  //image resizer
            'class'  => 'yii\image\ImageDriver',
            'driver' => 'Imagick',  //GD or Imagick
        ),
        'authManager'  => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n'         => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@app/common/messages',
                ],
                '*' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/common/messages',
                    'fileMap'  => [
                        //'common'=>'common.php',
                        //'backend'=>'backend.php',
                        'frontend' => 'frontend.php',
                    ],
                ],
                /*'*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en-US', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                ],*/
            ],
        ],
    ],
];

<?php
return [
    'siteName' => '24six',
    'adminEmail' => '24six@test-server.shn-host.ru',
//    'adminEmail' => 'maksym.sapozhnikov@gmail.com',
    'supportEmail' => '24six@test-server.shn-host.ru',
    'superAdminLogin' => 'admin@localhost.com',
    'replyEmail' => '24six@test-server.shn-host.ru',
    'noreplyEmail' => 'noreply@test-server.shn-host.ru',
    'user.passwordResetTokenExpire'  => 3 * 60 * 60,
    'token_time'            => 7 * 24 * 60 * 60, // 7 days
    'stripe'                         => [
        'api_key'      => 'sk_test_BQokikJOvBiI2HlWgH4olfQ2',
        'publish_key'  => 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
        'url-payment-event' => '/account/payments/add-funds-by-stripe-event',
		'url-payment-place' => '/account/payments/add-funds-by-stripe-place',
		'url-payment-plan'  => '/account/payments/add-funds-by-stripe',
    ],
];

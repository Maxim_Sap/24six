<?php

	namespace backend\controllers;

	use common\models\UserMainPersonalBonus;
	use common\models\UserProfile;
	use Yii;
	use common\models\User;
	use common\models\GeoCountries;
	use common\models\GeoCities;
	use backend\models\search\UsersSearch;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use yii\web\Controller;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;

	/**
	 * UsersController implements the CRUD actions for User model.
	 */
	class UsersController extends BackendController
	{

		/**
		 * Lists all User models.
		 * @return mixed
		 */
		public function actionIndex()
		{
			$searchModel  = new UsersSearch();
			$dataProvider = $searchModel -> search( Yii ::$app -> request -> queryParams );

			return $this -> render(
				'index',
				[
					'searchModel'  => $searchModel,
					'dataProvider' => $dataProvider,
				]
			);
		}

		/**
		 * Displays a single User model.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionView( $id )
		{
			return $this -> render(
				'view',
				[
					'model' => $this -> findModel( $id ),
				]
			);
		}

		/**
		 * Creates a new User model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 * @return mixed
		 */
		public function actionCreate()
		{
			$model       = new User();
			$userProfile = new UserProfile();

			if( $post = Yii ::$app -> request -> post() )
			{
				$model -> scenario = 'update';
				if( !empty( $post[ 'User' ][ 'password' ] ) )
				{
					$model -> setPassword( $post[ 'User' ][ 'password' ] );
				}
				if( !empty( $post[ 'UserProfile' ][ 'birthday' ] ) )
				{
					$post[ 'UserProfile' ][ 'birthday' ] = date( 'Y-m-d', strtotime( $post[ 'UserProfile' ][ 'birthday' ] ) );
				}

				$model -> load( $post );
				if( $model -> save() )
				{
					$userProfile -> user_id = $model -> id;
					$userProfile -> load( $post );
					if( !$userProfile -> save() )
					{
						dd( $userProfile -> errors );
					}
					return $this -> redirect( [ 'view', 'id' => $model -> id ] );
				}
			}

			return $this -> render(
				'create',
				[
					'model'       => $model,
					'userProfile' => $userProfile,
				]
			);
		}

		public function actionBonusesList( $user_id )
		{
			$query = ( new Query() )
				-> from( UserMainPersonalBonus ::tableName() . " bonuses_user" )
				-> select( 'bonuses_user.*' )
				-> leftJoin( User ::tableName() . " user", 'user.id = bonuses_user.user_id' )
				-> where( [ 'bonuses_user.user_id' => $user_id ] );

			$dataProvider = new ActiveDataProvider(
				[
					'query' => $query,
				]
			);

			$user_model = $this -> findModel( $user_id );

			return $this -> render(
				'bonuses-list',
				[
					'dataProvider' => $dataProvider,
					'user_model'   => $user_model,
				]
			);
		}

		public function actionFindUserBonuses( $id, $user_id )
		{
			$model = UserMainPersonalBonus ::find()
										   -> where( [ 'user_id' => $user_id ] )
										   -> andWhere( [ 'id' => $id ] )
										   -> one();

			$user_model = User ::find()
							   -> where( [ 'id' => $user_id ] )
							   -> with( 'userProfile' )
							   -> one();

			return $this -> render(
				'user-bonuses',
				[
					'user_model' => $user_model,
					'model'      => $model,
				]
			);
		}

		public function actionUpdateUserBonuses()
		{
			$bonusData  = Yii ::$app -> request -> post();
			$user_id    = $bonusData[ 'UserMainPersonalBonus' ][ 'user_id' ];
			$model      = UserMainPersonalBonus ::find() -> where( [ 'user_id' => $user_id ] ) -> one();
			$user_model = User ::find() -> where( [ 'id' => $user_id ] ) -> with( 'userProfile' ) -> one();
			if( $bonusData )
			{
				$model -> count_events_week        = $bonusData[ 'UserMainPersonalBonus' ][ 'count_events_week' ];
				$model -> count_time_for_massage   = $bonusData[ 'UserMainPersonalBonus' ][ 'count_time_for_massage' ];
				$model -> count_time_for_conf_room = $bonusData[ 'UserMainPersonalBonus' ][ 'count_time_for_conf_room' ];
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'bonuses-list', 'user_id' => $user_id ] );
				}
				else
				{
					dd( $model -> errors );
				}
			}
			return $this -> render(
				'user-bonuses',
				[
					'user_model' => $user_model,
					'model'      => $model,
				]
			);
		}

		public function actionDeleteBonusesUser( $id, $user_id )
		{
			$partn_event_model = UserMainPersonalBonus ::findOne( [ 'id' => $id, 'user_id' => $user_id ] );
			if( $partn_event_model )
			{
				$partn_event_model -> delete();
			}

			return $this -> redirect( [ 'bonuses-list', 'user_id' => $user_id ] );
		}

		/**
		 * Updates an existing User model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionUpdate( $id )
		{
			$model       = $this -> findModel( $id );
			$userProfile = UserProfile ::findOne( [ 'user_id' => $id ] );
			//dd($userProfile);
			if( $post = Yii ::$app -> request -> post() )
			{
				$model -> scenario = 'update';
				if( !empty( $post[ 'User' ][ 'password' ] ) )
				{
					$model -> setPassword( $post[ 'User' ][ 'password' ] );
				}
				if( !empty( $post[ 'UserProfile' ][ 'birthday' ] ) )
				{
					$post[ 'UserProfile' ][ 'birthday' ] = date( 'Y-m-d', strtotime( $post[ 'UserProfile' ][ 'birthday' ] ) );
				}

				$model -> load( $post );
				$userProfile -> load( $post );
				if( $model -> save() && $userProfile -> save() )
				{
					return $this -> redirect( [ 'view', 'id' => $model -> id ] );
				}
				else
				{
					dd( $model -> errors, TRUE );
					dd( $userProfile -> errors );
				}
			}

			//dd($userProfile);
			return $this -> render(
				'update',
				[
					'model'       => $model,
					'userProfile' => $userProfile,
				]
			);
		}

		/**
		 * Deletes an existing User model.
		 * If deletion is successful, the browser will be redirected to the 'index' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionDelete( $id )
		{
			//$this->findModel($id)->delete();
			$model           = $this -> findModel( $id );
			$model -> status = User::STATUS_DELETED;
			$model -> save();

			return $this -> redirect( [ 'index' ] );
		}

		/**
		 * Finds the User model based on its primary key value.
		 * If the model is not found, a 404 HTTP exception will be thrown.
		 *
		 * @param integer $id
		 *
		 * @return User the loaded model
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		protected function findModel( $id )
		{
			if( ( $model = User ::findOne( $id ) ) !== NULL )
			{
				return $model;
			}

			throw new NotFoundHttpException( Yii ::t( 'backend', 'The requested page does not exist.' ) );
		}
	}

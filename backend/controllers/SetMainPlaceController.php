<?php

	namespace backend\controllers;

	use common\models\SpaceWork;
	use Yii;
	use common\models\SetMainPlaces;
	use backend\models\search\SearchSetMainPlaces;
	use yii\helpers\ArrayHelper;
	use yii\web\Controller;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;

	/**
	 * SetMainPlacesController implements the CRUD actions for SetMainPlaces model.
	 */
	class SetMainPlaceController extends BackendController
	{
		/**
		 * @inheritdoc
		 */


		/**
		 * Lists all SetMainPlaces models.
		 * @return mixed
		 */
		public function actionIndex()
		{
			$searchModel        = new SearchSetMainPlaces();
			$dataProvider       = $searchModel -> search( Yii ::$app -> request -> queryParams );
			$checkCountSetSpace = SetMainPlaces ::find() -> count();
			$getAllSpaceTitle   = ArrayHelper ::map( SpaceWork ::find() -> orderBy( 'title' ) -> asArray() -> all(), 'id', 'title' );

			return $this -> render(
				'index',
				[
					'searchModel'        => $searchModel,
					'dataProvider'       => $dataProvider,
					'checkCountSetSpace' => $checkCountSetSpace,
					'getAllSpaceTitle'   => $getAllSpaceTitle,
					'spaceModel'         => new SpaceWork(),
				]
			);
		}

		public function actionSetPlace()
		{
			$model = new SetMainPlaces();
			$data  = Yii ::$app -> request -> post();
			if( !empty( $data[ 'SpaceWork' ] ) )
			{
				$model -> id_place = $data[ 'SpaceWork' ][ 'title' ];
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'index' ] );
				}
				else
				{
					dd( $model -> errors );
				}
			}
		}

		/**
		 * Displays a single SetMainPlaces model.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionView( $id )
		{
			return $this -> render(
				'view',
				[
					'model' => $this -> findModel( $id ),
				]
			);
		}

		/**
		 * Creates a new SetMainPlaces model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 * @return mixed
		 */
		public function actionCreate()
		{
			$model = new SetMainPlaces();

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'create',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Updates an existing SetMainPlaces model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionUpdate( $id )
		{
			$model = $this -> findModel( $id );

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'update',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Deletes an existing SetMainPlaces model.
		 * If deletion is successful, the browser will be redirected to the 'index' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionDelete( $id )
		{
			$this -> findModel( $id ) -> delete();

			return $this -> redirect( [ 'index' ] );
		}

		/**
		 * Finds the SetMainPlaces model based on its primary key value.
		 * If the model is not found, a 404 HTTP exception will be thrown.
		 *
		 * @param integer $id
		 *
		 * @return SetMainPlaces the loaded model
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		protected function findModel( $id )
		{
			if( ( $model = SetMainPlaces ::findOne( $id ) ) !== NULL )
			{
				return $model;
			}

			throw new NotFoundHttpException( Yii ::t( 'backend', 'The requested page does not exist.' ) );
		}
	}

<?php

namespace backend\controllers;

use app\common\models\UserImgAssn;
use common\models\Events;
use common\models\SpaceWork;
use common\models\Speakers;
use Yii;
use app\common\models\UserProfile;
use yii\web\Response;
use yii\web\BadRequestHttpException;

//use app\common\helpers\Translator;
//use yii\validators\InlineValidator;

class UploadController extends BackendController
{

    public function actionIndex()
    {

        throw new BadRequestHttpException('404 Error');
    }

    /*public function actionUploadPhoto()
    {

        $model = new \app\components\UploadImg\ModelUploadImg();
        if (Yii::$app->request->isAjax) {
            $model->setPath('source/temp-image/');
            $model->image = \yii\web\UploadedFile::getInstanceByName('image');

            return $model->upload();
        }
    }*/

    function actionUploadImages(){
        $img_type = \Yii::$app->request->post('img_type');
        $model = new \common\components\UploadImg\ModelUploadImg();
        if (Yii::$app->request->isAjax) {
            //$model->setPath('source/temp-image/');
            $resource_id = Yii::$app->request->post('resource_id');
            $resource_type = Yii::$app->request->post('resource_type');
            $model->image = \yii\web\UploadedFile::getInstanceByName('image');
            $upload_result = $model->upload($img_type,$resource_id);

            if($upload_result['result']){
                switch ($img_type){
                    case 'avatar':
                        //
                        break;
                    case 'logo':
                        //
                        break;
                    case 'background_image':
                        if($resource_type == 'event'){
                            Events::updateAll(['image'=>$upload_result['img_name']],['id'=>$resource_id]);
                        }elseif($resource_type == 'space'){
                            SpaceWork::updateAll(['image'=>$upload_result['img_name']],['id'=>$resource_id]);
                        }
                        break;
                    /*case 'gallery':
                        $attributes['UserImgAssn'] = [
                            'user_id'=> $user_id,
                            'type'=> UserImgAssn::TYPE_GALLERY,
                            'name'=> $upload_result['img_name'],
                        ];
                        $imgAssnModel = new UserImgAssn();
                        $imgAssnModel->load($attributes);
                        if(!$imgAssnModel->save()){
                            return json_encode($imgAssnModel->errors);
                        }
                        break;
                    case 'certificates':
                        $attributes['UserImgAssn'] = [
                            'user_id'=> $user_id,
                            'type'=> UserImgAssn::TYPE_CERTIFICATES,
                            'name'=> $upload_result['img_name'],
                        ];
                        $imgAssnModel = new UserImgAssn();
                        $imgAssnModel->load($attributes);
                        $result = $imgAssnModel->save();
                        break;*/
                }
            }


            return json_encode($upload_result);

        }
        return json_encode(['result' => FALSE, 'message' => 'Invalid request']);

    }

    public function actionCropPhoto()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $photoName                  = Yii::$app->request->post('photoName');
        $resource_id = Yii::$app->request->post('resource_id');
        $resource_type = Yii::$app->request->post('resource_type');
        if (!isset($photoName) || trim($photoName) == '') {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $imageCoordinates = Yii::$app->request->post('imageCoordinates');

        if (!isset($imageCoordinates) || !is_array($imageCoordinates) || !array_filter($imageCoordinates, 'is_numeric') || count($imageCoordinates) != 4) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $imageSize = Yii::$app->request->post('imageSize');
        if (!isset($imageSize) || !is_array($imageSize) || !array_filter($imageSize, 'is_numeric') || count($imageSize) != 2) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $cropType = Yii::$app->request->post('type');

        $cropTypes = ['avatar','background_image','gallery','certificates'];
        if (!in_array($cropType, $cropTypes)) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $owner = FALSE;
        $img_query = false;
        switch ($cropType) {
            case 'avatar':
                //доп проверка
                $owner = TRUE;
                $img_query = TRUE;
                break;
            case 'background_image':
                //доп проверка
                $img_query = null;
                if($resource_type == 'event'){
                    $img_query = Events::findOne(['id'=>$resource_id]);
                }elseif($resource_type == 'space'){
                    $img_query = SpaceWork::findOne(['id'=>$resource_id]);
                }

                if(!empty($img_query)){
                    $resource_img_name = $img_query->image;
                }
                if ($photoName == $resource_img_name) {
                    $owner = TRUE;
                };
                break;
            /*case 'gallery':
                //доп проверка
                $img_query = UserImgAssn::findOne(['user_id'=>$profileId,'type'=>UserImgAssn::TYPE_GALLERY,'name'=>$photoName]);
                if(!empty($img_query)){
                    $owner = TRUE;
                }
                break;
            case 'certificates':
                //доп проверка
                $img_query = UserImgAssn::findOne(['user_id'=>$profileId,'type'=>UserImgAssn::TYPE_CERTIFICATES,'name'=>$photoName]);
                if(!empty($img_query)){
                    $owner = TRUE;
                }
                break;*/
        }

        if (!$owner) {
            return [
                'success' => FALSE,
                'message' => 'Access Denied',
            ];
        }

        $result = $this->cropImage($cropType, $photoName, $imageCoordinates, $imageSize, $resource_id);
        if ($result && $img_query) {
            switch ($cropType) {
                case 'avatar':
                    $dir        = Yii::getAlias('@avatar');
                    return [
                        'success' => TRUE,
                        'action' => 'no-reload',
                        'message' => 'image was crop',
                        'new_img' => $result,
                        'thumb_path' => Yii::$app->params['front-url'].$dir."/thumb/".$result,
                    ];
                    break;
                case 'background_image':
                    $img_query->image = $result;
                    break;
                /*case 'gallery':
                    $img_query->name = $result;
                    break;
                case 'certificates':
                    $img_query->name = $result;
                    break;*/
            }

            if (!$img_query->save()) {
                return [
                    'success' => FALSE,
                    'message' => $img_query->errors,
                ];
            }

            return [
                'success' => TRUE,
                'action' => 'reload',
                'message' => 'image was crop',
            ];

        }

        return [
            'success' => FALSE,
            'message' => 'Error crop',
        ];
    }

    private function cropImage($cropType, $photoName, $imageCoordinates, $imageSize, $resource_id)
    {
        list($x1, $y1, $x2, $y2) = $imageCoordinates;

        if(in_array($cropType,['gallery','certificates'])){
            //$originalPath = Yii::getAlias("@app/web/source/img/$cropType/$profileId/origin/$photoName");
        }else{
            $originalPath = Yii::getAlias("@frontend/web/source/img/$cropType/origin/$photoName");
        }

        $width        = $imageSize[0]; //image width on front end
        $height       = $imageSize[1]; //image width on front end

        $newWidth  = $x2 - $x1;
        $newHeight = $y2 - $y1;

        $widthRatio  = $newWidth / $width;
        $heightRatio = $newHeight / $height;

        $result = $this->cropOneImage($cropType, $resource_id, $photoName, $originalPath, $width, $height, $widthRatio, $heightRatio, $x1, $y1);

        return $result;

    }

    private function cropOneImage($cropType, $resource_id, $photoName, $file, $width, $height, $widthRatio, $heightRatio, $x1, $y1)
    {
        $originalSize   = getimagesize($file);
        $originalWidth  = $originalSize[0];
        $originalHeight = $originalSize[1];
        $xRatio         = $width / $originalWidth;
        $yRatio         = $height / $originalHeight;
        $newWidth       = round($originalWidth * $widthRatio);
        $newHeight      = round($originalHeight * $heightRatio);
        $newX           = round($x1 / $xRatio);
        $newY           = round($y1 / $yRatio);
        $image          = Yii::$app->image->load($file);

        $extension = pathinfo($file);
        $extension = $extension['extension'];

        $image->crop($newWidth, $newHeight, $newX, $newY);

        // delete old photos
        $thumb = str_replace('/origin/', '/thumb/', $file);
        @unlink($thumb);

        $icon = str_replace('/origin/', '/icon/', $file);
        @unlink($icon);

        $newName    = md5($file . time());
        if(in_array($cropType,['gallery','certificates'])){
            //$originPath = "source/img/$cropType/$profileId/origin/";
            //$thumbPath  = "source/img/$cropType/$profileId/thumb/";
            //$iconPath   = "source/img/$cropType/$profileId/icon/";
        }else{
            $dir = Yii::getAlias("@frontend/web/source/img/$cropType");
            $originPath = $dir. '/origin/';
            $thumbPath  = $dir. '/thumb/';
            $iconPath   = $dir. '/icon/';
        }

        $originName = $originPath . $newName . '.' . $extension;
        $thumbName  = $thumbPath . $newName . '.' . $extension;
        $iconName   = $iconPath . $newName . '.' . $extension;

        switch ($cropType) {
            case 'avatar':
                $image->resize(250, 300, \yii\image\drivers\Image::WIDTH);
                $image->save($thumbName);
                $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                $image->save($iconName);
                break;
            case 'background_image':
                $image->resize(1467, 400, \yii\image\drivers\Image::HEIGHT);
                $image->save($thumbName);
                $image->resize(500, 500, \yii\image\drivers\Image::CROP);
                $image->save($iconName);
                break;
            /*case 'gallery':
                $image->resize(300, 300, \yii\image\drivers\Image::WIDTH);
                $image->save($thumbName);
                break;
            case 'certificates':
                $image->resize(300, 300, \yii\image\drivers\Image::WIDTH);
                $image->save($thumbName);
                break;*/
        }
        @rename($file, $originName);

        return $newName . '.' . $extension;
    }

    public function actionDelImage($name,$place,$resource_type = null){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if(!$name) {
            return ['success' => false, 'message' => 'Wrong ID'];
        }

        $my_id = \Yii::$app->user->id;

        switch ($place){
            case 'avatar':
                    Speakers::deleteImages($name,$place);
                break;
            case 'background_image':
                if($resource_type == 'event'){
                    Events::deleteImages($name,$place);
                }elseif($resource_type == 'space'){
                    SpaceWork::deleteImages($name,$place);
                }

                break;
            /*case 'gallery':
                $imgModel = UserImgAssn::findOne(['name'=>$name,'user_id'=>$my_id]);
                if(!$imgModel){
                    return ['success' => false, 'message' => 'Image not found'];
                }

                $ress = $imgModel->delOne($name,$place,$my_id);
                break;
            case 'certificates':
                $imgModel = UserImgAssn::findOne(['name'=>$name,'user_id'=>$my_id]);
                if(!$imgModel){
                    return ['success' => false, 'message' => 'Image not found'];
                }

                $ress = $imgModel->delOne($name,$place,$my_id);
                break;*/
        }




        return ['success' => true, 'message' => 'Image deleted'];
    }


}

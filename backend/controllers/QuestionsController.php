<?php

	namespace backend\controllers;

	use common\models\AnswerForQuestion;
	use common\models\User;
	use Yii;
	use common\models\Questions;
	use backend\models\search\QuestionSearch;
	use yii\data\ActiveDataProvider;
	use yii\helpers\ArrayHelper;
	use yii\web\Controller;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;

	/**
	 * QuestionsController implements the CRUD actions for Questions model.
	 */
	class QuestionsController extends Controller
	{
		/**
		 * {@inheritdoc}
		 */
		public function behaviors()
		{
			return [
				'verbs' => [
					'class'   => VerbFilter ::className(),
					'actions' => [
						'delete' => [ 'POST' ],
					],
				],
			];
		}

		/**
		 * Lists all Questions models.
		 * @return mixed
		 */
		public function actionIndex()
		{
			$searchModel  = new QuestionSearch();
			$dataProvider = $searchModel -> search( Yii ::$app -> request -> queryParams );

			return $this -> render(
				'index',
				[
					'searchModel'  => $searchModel,
					'dataProvider' => $dataProvider,
				]
			);
		}

		/**
		 * Displays a single Questions model.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionView( $id )
		{
			return $this -> render(
				'view',
				[
					'model' => $this -> findModel( $id ),
				]
			);
		}

		public function actionListQuestion( $id )
		{

			$answers = AnswerForQuestion ::find() -> where( [ 'id_question' => $id ] );

			$question_model = Questions ::findOne( $id );
			$dataProvider   = new ActiveDataProvider(
				[
					'query' => $answers,
				]
			);
			return $this -> render(
				'list-question',
				[
					'question_model' => $question_model,
					'dataProvider'   => $dataProvider,
				]
			);
		}

		public function actionUpdateAnswer( $id, $id_question )
		{
			$model = AnswerForQuestion ::findOne( $id );

			$questionData   = Yii ::$app -> request -> post( 'AnswerForQuestion' );
			$username       = ArrayHelper ::map( User ::find() -> orderBy( 'username' ) -> asArray() -> all(), 'id', 'username' );
			$question_model = Questions ::find() -> where( [ 'id' => $id_question ] ) -> one();
			if( !empty( $questionData ) )
			{
				$model -> attributes = $questionData;
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'list-question', 'id' => $question_model -> id ] );
				}
			}

			return $this -> render(
				'update-answer',
				[
					'model'          => $model,
					'username'       => $username,
					'question_model' => $question_model,
				]
			);
		}

		public function actionDeleteAnswer( $id, $id_question )
		{

			$question_model = AnswerForQuestion ::findOne( $id );
			if( $question_model )
			{
				$question_model -> delete();
			}

			return $this -> redirect( [ 'list-question', 'id' => $id_question ] );
		}

		/**
		 * Creates a new Questions model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 * @return mixed
		 */
		public function actionCreate()
		{
			$model = new Questions();

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'create',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Updates an existing Questions model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionUpdate( $id )
		{
			$model = $this -> findModel( $id );

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'update',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Deletes an existing Questions model.
		 * If deletion is successful, the browser will be redirected to the 'index' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionDelete( $id )
		{
			$this -> findModel( $id ) -> delete();

			return $this -> redirect( [ 'index' ] );
		}

		/**
		 * Finds the Questions model based on its primary key value.
		 * If the model is not found, a 404 HTTP exception will be thrown.
		 *
		 * @param integer $id
		 *
		 * @return Questions the loaded model
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		protected function findModel( $id )
		{
			if( ( $model = Questions ::findOne( $id ) ) !== NULL )
			{
				return $model;
			}

			throw new NotFoundHttpException( Yii ::t( 'backend', 'The requested page does not exist.' ) );
		}
	}

<?php

	namespace backend\controllers;

	use common\models\Events;
	use Yii;
	use common\models\SetMainEvent;
	use backend\models\search\SearchSetMainEvent;
	use yii\helpers\ArrayHelper;
	use yii\web\Controller;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;

	/**
	 * SetMainEventController implements the CRUD actions for SetMainEvent model.
	 */
	class SetMainEventController extends BackendController
	{
		/**
		 * @inheritdoc
		 */


		/**
		 * Lists all SetMainEvent models.
		 * @return mixed
		 */
		public function actionIndex()
		{
			$searchModel        = new SearchSetMainEvent();
			$dataProvider       = $searchModel -> search( Yii ::$app -> request -> queryParams );
			$checkCountSetEvent = SetMainEvent ::find() -> count();
			$getAllEventTitle   = ArrayHelper ::map( Events ::find() -> orderBy( 'title' ) -> asArray() -> all(), 'id', 'title' );
			return $this -> render(
				'index',
				[
					'searchModel'        => $searchModel,
					'checkCountSetEvent' => $checkCountSetEvent,
					'getAllEventTitle'   => $getAllEventTitle,
					'dataProvider'       => $dataProvider,
					'eventModel'         => new Events(),
				]
			);
		}

		public function actionSetEvent()
		{
			$model = new SetMainEvent();
			$data  = Yii ::$app -> request -> post();
			if( !empty( $data[ 'Events' ] ) )
			{
				$model -> id_event = $data[ 'Events' ][ 'title' ];
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'index' ] );
				}
				else
				{
					dd( $model -> errors );
				}
			}
		}

		/**
		 * Displays a single SetMainEvent model.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionView( $id )
		{
			return $this -> render(
				'view',
				[
					'model' => $this -> findModel( $id ),
				]
			);
		}

		/**
		 * Creates a new SetMainEvent model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 * @return mixed
		 */
		public function actionCreate()
		{
			$model = new SetMainEvent();

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'create',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Updates an existing SetMainEvent model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionUpdate( $id )
		{
			$model = $this -> findModel( $id );

			if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
			{
				return $this -> redirect( [ 'view', 'id' => $model -> id ] );
			}

			return $this -> render(
				'update',
				[
					'model' => $model,
				]
			);
		}

		/**
		 * Deletes an existing SetMainEvent model.
		 * If deletion is successful, the browser will be redirected to the 'index' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionDelete( $id )
		{
			$this -> findModel( $id ) -> delete();

			return $this -> redirect( [ 'index' ] );
		}

		/**
		 * Finds the SetMainEvent model based on its primary key value.
		 * If the model is not found, a 404 HTTP exception will be thrown.
		 *
		 * @param integer $id
		 *
		 * @return SetMainEvent the loaded model
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		protected function findModel( $id )
		{
			if( ( $model = SetMainEvent ::findOne( $id ) ) !== NULL )
			{
				return $model;
			}

			throw new NotFoundHttpException( Yii ::t( 'backend', 'The requested page does not exist.' ) );
		}
	}

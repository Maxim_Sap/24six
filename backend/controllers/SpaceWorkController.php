<?php

namespace backend\controllers;

//use Codeception\Events;
use common\models\CategoriesToPlase;
use common\models\ConfRooms;
use common\models\Countryes;
use common\models\CustomFields;
use common\models\Favorites;
use Yii;
use common\models\SpaceWork;
use backend\models\search\SpaceWorkSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Events;

/**
 * SpaceWorkController implements the CRUD actions for SpaceWork model.
 */
class SpaceWorkController extends BackendController
{
    /**
     * @inheritdoc
     */


    /**
     * Lists all SpaceWork models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SpaceWorkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SpaceWork model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SpaceWork model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SpaceWork();

        $categories = ArrayHelper::map(CategoriesToPlase::find()->orderBy('category')->asArray()->all(), 'id', 'category');
        $countries = ArrayHelper::map(Countryes::find()->orderBy('country')->asArray()->all(), 'id', 'country');

        $SpacesData = Yii::$app->request->post('SpaceWork');
        if (!empty($SpacesData)) {
            //$SpacesData['created_at'] = date( 'Y-m-d', strtotime( $SpacesData['created_at'] ) );
            $model->attributes = $SpacesData;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }
        return $this->render('create', [
            'model' => $model,
            'categories' => $categories,
            'countries' => $countries
        ]);
    }

    /**
     * Updates an existing SpaceWork model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = ArrayHelper::map(CategoriesToPlase::find()->orderBy('category')->asArray()->all(), 'id', 'category');
        $countries = ArrayHelper::map(Countryes::find()->orderBy('country')->asArray()->all(), 'id', 'country');

        $SpacesData = Yii::$app->request->post('SpaceWork');
        if (!empty($SpacesData)) {
            //$SpacesData['created_at'] = date( 'Y-m-d', strtotime( $SpacesData['created_at'] ) );
            $model->attributes = $SpacesData;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('update', [
            'categories' => $categories,
            'countries' => $countries,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SpaceWork model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpaceWork model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpaceWork the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpaceWork::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }


    public function actionConferenceView($space_id)
    {

        $query = ConfRooms::find()
            ->where(['space_id' => $space_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $space_model = $this->findModel($space_id);

        return $this->render('conference-list', [
            'dataProvider' => $dataProvider,
            'space_model' => $space_model,
        ]);
    }

    public function actionUpdateConference($id, $space_id)
    {

        $model = ConfRooms::findOne(['id' => $id, 'space_id' => $space_id]);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }

        $post = Yii::$app->request->post('ConfRooms');
        if ($post) {
            $post['space_id'] = $space_id;
            $model->attributes = $post;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['conference-view', 'space_id' => $space_id]);
            } else {
                dd($model->errors);
            }
        }

        $space_model = $this->findModel($space_id);

        return $this->render('add-conf-room', [
            'model' => $model,
            'space_model' => $space_model,
        ]);
    }

    public function actionDeleteConference($id, $space_id)
    {
        $model = ConfRooms::findOne(['id' => $id, 'space_id' => $space_id]);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }

        $model->delete();

        return $this->redirect(['conference-view', 'space_id' => $space_id]);

    }

    public function actionAddConfRoom($space_id)
    {
        $model = new ConfRooms();
        $space_model = $this->findModel($space_id);
        $post = Yii::$app->request->post('ConfRooms');
        if ($post) {
            $post['space_id'] = $space_id;
            $model->attributes = $post;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['conference-view', 'space_id' => $space_id]);
            } else {
                dd($model->errors);
            }


        }

        return $this->render('add-conf-room', [
            'model' => $model,
            'space_model' => $space_model,
        ]);
    }

    public function actionCustomFieldsList($space_id)
    {
        $query = CustomFields::find()
            ->where(['source_id' => $space_id])
            ->andWhere(['type_source' => Favorites::TYPE_PLACES]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $space_model = $this->findModel($space_id);

        return $this->render('custom-fields-list', [
            'dataProvider' => $dataProvider,
            'space_model' => $space_model,
        ]);
    }

    public function actionUpdateCustomField($id, $space_id)
    {
        $model = CustomFields::findOne(['id' => $id, 'source_id' => $space_id]);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }

        $post = Yii::$app->request->post('CustomFields');
        if ($post) {
            $post['source_id'] = $space_id;
            $model->attributes = $post;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['custom-fields-list', 'space_id' => $space_id]);
            } else {
                dd($model->errors);
            }
        }

        $space_model = $this->findModel($space_id);

        return $this->render('add-custom-field', [
            'model' => $model,
            'space_model' => $space_model,
        ]);
    }

    public function actionAddCustomField($space_id)
    {
        $model = new CustomFields();

        $post = Yii::$app->request->post('CustomFields');
        if ($post) {
            $post['source_id'] = $space_id;
            $model->attributes = $post;
            $model->type_source = Favorites::TYPE_PLACES;
            if ($model->validate() && $model->save()) {
                return $this->redirect(['custom-fields-list', 'space_id' => $space_id]);
            } else {
                dd($model->errors);
            }
        }

        $space_model = $this->findModel($space_id);

        return $this->render('add-custom-field', [
            'model' => $model,
            'space_model' => $space_model,
        ]);

    }

    public function actionDeleteCustomField($id, $space_id)
    {
        CustomFields::findOne($id)->delete();
        return $this->redirect(['custom-fields-list', 'space_id' => $space_id]);
    }
}

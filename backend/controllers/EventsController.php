<?php

	namespace backend\controllers;

	use common\models\CategoriesTypeEvent;
    use common\models\LecturesAssnSpeakers;
    use common\models\UserProfile;
	use yii\data\ActiveDataProvider;
	use common\models\Countryes;
	use common\models\CustomFields;
	use common\models\Favorites;
	use common\models\Lectures;
	use common\models\PartnerEvents;
	use common\models\Partners;
	use common\models\Speakers;
	use Yii;
	use common\models\Events;
	use backend\models\search\EventsSearch;
	use yii\db\Query;
	use yii\helpers\Url;
	use yii\web\Controller;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;
	use yii\helpers\ArrayHelper;

	/**
	 * EventsController implements the CRUD actions for Events model.
	 */
	class EventsController extends BackendController
	{
		/**
		 * @inheritdoc
		 */


		/**
		 * Lists all Events models.
		 * @return mixed
		 */
		public function actionIndex()
		{
			$searchModel  = new EventsSearch();
			$dataProvider = $searchModel -> search( Yii ::$app -> request -> queryParams );

			return $this -> render(
				'index',
				[
					'searchModel'  => $searchModel,
					'dataProvider' => $dataProvider,
				]
			);
		}

		/**
		 * Displays a single Events model.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionView( $id )
		{
			return $this -> render(
				'view',
				[
					'model'             => $this -> findModel( $id ),
				]
			);
		}

//		public function actionAddField()
//		{
//			$model = new CustomFields();
//			if( Yii ::$app -> request -> isAjax )
//			{
//				$status             = TRUE;
//				$getOptionsPostData = Yii ::$app -> request -> post();
//				if( !empty( $getOptionsPostData[ 'titleField' ] ) && !empty( $getOptionsPostData[ 'descField' ] ) )
//				{
//					$model -> title_field       = $getOptionsPostData[ 'titleField' ];
//					$model -> description_field = $getOptionsPostData[ 'descField' ];
//					$model -> source_id         = $getOptionsPostData[ 'source_id' ];
//					$model -> type_source       = $getOptionsPostData[ 'type_source' ];
//					if( $model -> validate() )
//					{
//						$model -> save();
//					}
//				}
//				else
//				{
//					Yii ::$app -> session -> setFlash( 'message', 'No fields are filled' );
//					return $this -> redirect( Url ::toRoute( [ 'view', 'add_option' => 'add_field', 'id' => $getOptionsPostData[ 'source_id' ] ] ) );
//				}
//				$getLastAddField = CustomFields ::find() -> orderBy( [ 'id' => SORT_DESC ] ) -> all();
//				$title           = $getLastAddField[ 0 ] -> title_field;
//				$deck            = $getLastAddField[ 0 ] -> description_field;
//				$id_field        = $getLastAddField[ 0 ] -> id;
//				return json_encode(
//					[
//						'id_field' => $id_field,
//						'status'   => $status,
//						'title'    => $title,
//						'deck'     => $deck,
//					]
//				);
//			}
//			return $status = FALSE;
//		}

		/*public function actionAddPartner()
		{
			$model = new PartnerEvents();
			if( Yii ::$app -> request -> isAjax )
			{
				$status             = TRUE;
				$getOptionsPostData = Yii ::$app -> request -> post();
				if( !empty( $getOptionsPostData ) )
				{
					$model -> id_event   = $getOptionsPostData[ 'id_event' ];
					$model -> id_partner = $getOptionsPostData[ 'select_partner' ];
					if( !$model -> save() )
					{
						dd( $model -> errors );
					}
				}
				$getLastAddPartner = PartnerEvents ::getLastAddPartner( $getOptionsPostData[ 'id_event' ] );
				$logo              = $getLastAddPartner[ 'partners' ] -> logo_partner;
				$title             = $getLastAddPartner[ 'partners' ] -> title_partner;
				$id_partner        = $getLastAddPartner -> id_partner;
				return json_encode(
					[
						'status'     => $status,
						'title'      => $title,
						'logo'       => $logo,
						'id_partner' => $id_partner,
					]
				);
			}
			return $status = FALSE;
		}*/

//		public function actionSetTypeEvent()
//		{
//			$getData = Yii ::$app -> request -> post();
//			$id      = $getData[ 'id_event_in_select' ];
//			if( Yii ::$app -> request -> isAjax )
//			{
//				$model               = Events ::findOne( $id );
//				$model -> scenario   = Events::SCENARIO_UPDATE_TYPE_EVENT;
//				$model -> type_event = $getData[ 'type_event' ];
//				if( $model -> save() )
//				{
//					return $this -> redirect( [ 'view', 'id' => $id ] );
//				}
//			}
//		}

//		public function actionAddLectures()
//		{
//			$model = new Lectures();
//			if( Yii ::$app -> request -> isAjax )
//			{
//				$status             = TRUE;
//				$getOptionsPostData = Yii ::$app -> request -> post();
//				if( !empty( $getOptionsPostData[ 'subject' ] ) && !empty( $getOptionsPostData[ 'time_from' ] ) && !empty( $getOptionsPostData[ 'time_to' ] ) && !empty( $getOptionsPostData[ 'speaker_id' ] ) )
//				{
//					$model -> id_event   = $getOptionsPostData[ 'id_event' ];
//					$model -> time_from  = $getOptionsPostData[ 'time_from' ];
//					$model -> time_to    = $getOptionsPostData[ 'time_to' ];
//					$model -> subject    = $getOptionsPostData[ 'subject' ];
//					$model -> speaker_id = $getOptionsPostData[ 'speaker_id' ];
//					if( $getOptionsPostData[ 'numberForAdditionSpeaker' ] == 1 && !empty( $getOptionsPostData[ 'additional_speaker_id' ] ) )
//					{
//						$model -> additional_speaker_id = $getOptionsPostData[ 'additional_speaker_id' ];
//					}
//					if( $model -> validate() )
//					{
//						$model -> save();
//					}
//					else
//					{
//						dd( $model -> errors );
//					}
//				}
//				else
//				{
//					Yii ::$app -> session -> setFlash( 'message', 'No fields are filled' );
//					return $this -> redirect( Url ::toRoute( [ 'view', 'add_option' => 'add_lectures', 'id' => $getOptionsPostData[ 'id_event' ] ] ) );
//				}
//				$getLastAddLecture = Lectures ::getLastLecture( $getOptionsPostData[ 'numberForAdditionSpeaker' ] );
//				$time_from         = $getLastAddLecture[ 'time_from' ];
//				$time_to           = $getLastAddLecture[ 'time_to' ];
//				$subject           = $getLastAddLecture[ 'subject' ];
//
//				$speaker_name       = $getLastAddLecture[ 'speaker_name' ];
//				$speaker_avatar     = $getLastAddLecture[ 'speaker_avatar' ];
//				$speaker_positional = $getLastAddLecture[ 'speaker_positional' ];
//
//				$id_lecture = $getLastAddLecture[ 'id_lecture' ];
//				if( $getOptionsPostData[ 'numberForAdditionSpeaker' ] == 0 && empty( $getOptionsPostData[ 'additional_speaker_id' ] ) )
//				{
//					return json_encode(
//						[
//							'status'    => $status,
//							'time_from' => $time_from,
//							'time_to'   => $time_to,
//							'subject'   => $subject,
//
//							'speaker_name'       => $speaker_name,
//							'speaker_avatar'     => $speaker_avatar,
//							'speaker_positional' => $speaker_positional,
//							'getOptionsPostData' => $getOptionsPostData[ 'numberForAdditionSpeaker' ],
//
//							'id_lecture' => $id_lecture,
//						]
//					);
//				}
//				else
//				{
//					$additional_speaker_name     = $getLastAddLecture[ 'additional_speaker_name' ];
//					$additional_speaker_avatar   = $getLastAddLecture[ 'additional_speaker_avatar' ];
//					$additional_speaker_position = $getLastAddLecture[ 'additional_speaker_position' ];
//					$additional_speaker_id       = $getLastAddLecture[ 'additional_speaker_id' ];
//					return json_encode(
//						[
//							'status'    => $status,
//							'time_from' => $time_from,
//							'time_to'   => $time_to,
//							'subject'   => $subject,
//
//							'speaker_name'       => $speaker_name,
//							'speaker_avatar'     => $speaker_avatar,
//							'speaker_positional' => $speaker_positional,
//
//							'additional_speaker_name'     => $additional_speaker_name,
//							'additional_speaker_avatar'   => $additional_speaker_avatar,
//							'additional_speaker_position' => $additional_speaker_position,
//							'getOptionsPostData'          => $getOptionsPostData[ 'numberForAdditionSpeaker' ],
//
//							'id_lecture'            => $id_lecture,
//							'additional_speaker_id' => $additional_speaker_id,
//						]
//					);
//				}
//			}
//			return $status = FALSE;
//		}

		/**
		 * Creates a new Events model.
		 * If creation is successful, the browser will be redirected to the 'view' page.
		 * @return mixed
		 */
		public function actionCreate()
		{
			$model = new Events();

			$categories = ArrayHelper ::map( CategoriesTypeEvent ::find() -> orderBy( 'category' ) -> asArray() -> all(), 'id', 'category' );
			$country    = ArrayHelper ::map( Countryes ::find() -> orderBy( 'country' ) -> asArray() -> all(), 'id', 'country' );
			$EventsData = Yii ::$app -> request -> post( 'Events' );
			if( !empty( $EventsData ) )
			{
				$EventsData[ 'date_event' ] = date( 'Y-m-d', strtotime( $EventsData[ 'date_event' ] ) );
				$model -> attributes        = $EventsData;
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'view', 'id' => $model -> id ] );
				}
			}
			return $this -> render(
				'create',
				[
					'model'      => $model,
					'categories' => $categories,
					'country'    => $country,
				]
			);
		}

		/**
		 * Updates an existing Events model.
		 * If update is successful, the browser will be redirected to the 'view' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionUpdate( $id )
		{
			$model = $this -> findModel( $id );

			$EventsData = Yii ::$app -> request -> post( 'Events' );
			$categories = ArrayHelper ::map( CategoriesTypeEvent ::find() -> orderBy( 'category' ) -> asArray() -> all(), 'id', 'category' );
			$country    = ArrayHelper ::map( Countryes ::find() -> orderBy( 'country' ) -> asArray() -> all(), 'id', 'country' );
			if( !empty( $EventsData ) )
			{
				$EventsData[ 'date_event' ] = date( 'Y-m-d', strtotime( $EventsData[ 'date_event' ] ) );
				$model -> attributes        = $EventsData;
				if( $model -> validate() && $model -> save() )
				{
					return $this -> redirect( [ 'view', 'id' => $model -> id ] );
				}
			}

			return $this -> render(
				'update',
				[
					'model'      => $model,
					'categories' => $categories,
					'country'    => $country,
				]
			);
		}

		/**
		 * Deletes an existing Events model.
		 * If deletion is successful, the browser will be redirected to the 'index' page.
		 *
		 * @param integer $id
		 *
		 * @return mixed
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		public function actionDelete( $id )
		{
			$this -> findModel( $id ) -> delete();

			return $this -> redirect( [ 'index' ] );
		}

		/**
		 * Finds the Events model based on its primary key value.
		 * If the model is not found, a 404 HTTP exception will be thrown.
		 *
		 * @param integer $id
		 *
		 * @return Events the loaded model
		 * @throws NotFoundHttpException if the model cannot be found
		 */
		protected function findModel( $id )
		{
			if( ( $model = Events ::findOne( $id ) ) !== NULL )
			{
				return $model;
			}

			throw new NotFoundHttpException( Yii ::t( 'backend', 'The requested page does not exist.' ) );
		}

        public function actionPartnersList($event_id)
        {
            $query = (new Query())
                -> from( PartnerEvents ::tableName() . " part_event" )
                -> select('partners.*')
                -> leftJoin( Partners ::tableName() . " partners", 'partners.id = part_event.id_partner' )
                ->where(['part_event.id_event' => $event_id]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $event_model = $this->findModel($event_id);

            return $this->render('partners-list', [
                'dataProvider' => $dataProvider,
                'event_model' => $event_model,
            ]);
        }

        public function actionUpdatePartner($id, $event_id)
        {
            $model = PartnerEvents ::findOne(['id_partner' => $id,'id_event' => $event_id]);

            if (!$model) {
                throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
            }

            $post = Yii::$app->request->post('PartnerEvents');
            if ($post) {
                $model->attributes = $post;
                if ($model->validate() && $model->save()) {
                    return $this->redirect(['partners-list', 'event_id' => $event_id]);
                } else {
                    dd($model->errors);
                }
            }

            $event_model = $this->findModel($event_id);
            $getAllPartner = ArrayHelper ::map( Partners ::find() -> orderBy( 'title_partner' ) -> asArray() -> all(), 'id', 'title_partner' );

            return $this->render('add-partner', [
                'model' => $model,
                'event_model' => $event_model,
                'getAllPartner' => $getAllPartner,
            ]);
        }



        public function actionAddPartner($event_id)
        {
			$model = new PartnerEvents();
			$getAllPartner = ArrayHelper ::map( Partners ::find() -> orderBy( 'title_partner' ) -> asArray() -> all(), 'id', 'title_partner' );

            $post = Yii::$app->request->post('PartnerEvents');
            if ($post) {
				$model->id_event = $event_id;
				$model->id_partner = $post[ 'id_partner' ];
				if ($model->save()) {
					return $this->redirect(['partners-list', 'event_id' => $event_id]);
				}
            }

            $event_model = $this->findModel($event_id);

            return $this->render('add-partner', [
                'model' => $model,
                'getAllPartner' => $getAllPartner,
                'event_model' => $event_model,
            ]);

        }


		public function actionDeletePartner($id, $event_id)
		{
			$partn_event_model = PartnerEvents::findOne(['id_partner'=>$id,'id_event'=>$event_id]);
			if($partn_event_model){
				$partn_event_model->delete();
			}

			return $this->redirect(['partners-list', 'event_id' => $event_id]);
		}




		public function actionFieldsList( $event_id )
		{
			$query = ( new Query() )
				-> from( CustomFields::tableName(). " fields")
				-> select('fields.*')
				-> leftJoin( Events::tableName(). " events",'events.id = fields.source_id')
				-> where( [ 'fields.source_id' => $event_id]);

			$dataProvider = new ActiveDataProvider([
													   'query' => $query
												   ]);

			$event_model = $this->findModel($event_id);

			return $this -> render('fields-list', [
				'dataProvider' => $dataProvider,
				'event_model' => $event_model,
			]);
		}

		public function actionUpdateFields( $id, $event_id )
		{
			$model = CustomFields ::findOne(['id' => $id]);

			if (!$model) {
				throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
			}

			$post = Yii::$app->request->post('CustomFields');
			if ($post) {
				$model->attributes = $post;
				if ($model->validate() && $model->save()) {
					return $this->redirect(['fields-list', 'event_id' => $event_id]);
				} else {
					dd($model->errors);
				}
			}

			$event_model = $this->findModel($event_id);

			return $this->render('add-fields', [
				'model' => $model,
				'event_model' => $event_model,
			]);
		}


		public function actionAddFields($event_id)
		{
			$model = new CustomFields();
			$post = Yii::$app->request->post('CustomFields');
			if ($post) {
				$model -> attributes = $post;
				$model -> source_id = $event_id;
				if (!$model->validate())
				{
					dd($model->errors);
				}
				else
				{
					if( $model->save() )
					{
						return $this -> redirect( [ 'fields-list', 'event_id' => $event_id ] );
					}
				}
			}

			$event_model = $this->findModel($event_id);

			return $this->render('add-fields', [
				'model' => $model,
				'event_model' => $event_model,
			]);

		}

		public function actionDeleteField( $id , $event_id )
		{
			$deleteField = CustomFields ::findOne( $id );
			if( $deleteField -> delete() )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Field was delete' );
				return $this -> redirect( Url ::toRoute( [ 'fields-list', 'event_id' => $event_id ] ) );
			}
		}


		public function actionLecturesList( $event_id )
		{
			$query = Lectures::find()->where([ 'id_event' => $event_id]);
			$dataProvider = new ActiveDataProvider([
				'query' => $query
			]);

			$event_model = $this->findModel($event_id);

			return $this -> render('lectures-list', [
				'dataProvider' => $dataProvider,
				'event_model' => $event_model,
			]);
		}


		public function actionAddLecture( $event_id )
		{
			$model = new Lectures();
			$post = Yii::$app->request->post('Lectures');
			if ($post) {
				$model -> attributes = $post;
				$model -> id_event = $event_id;
				if ($model->validate() && $model->save())
				{
				    LecturesAssnSpeakers::addLectures($model->id,$post['speaker_ids']);

                    return $this -> redirect( [ 'lectures-list', 'event_id' => $event_id ] );
				}
				else
				{
                    dd($model->errors);
				}
			}

			$getAndSetSpeakers = ArrayHelper ::map( Speakers ::find() -> orderBy( 'name' ) -> asArray() -> all(), 'id', 'name' );
			$event_model = $this->findModel($event_id);

			return $this->render('add-lecture', [
				'model' => $model,
				'event_model' => $event_model,
				'getAndSetSpeakers' => $getAndSetSpeakers
			]);

		}

		public function actionUpdateLecture( $id, $event_id )
		{
			$model = Lectures ::findOne( [ 'id' => $id ] );

			if (!$model) {
				throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
			}

			$post = Yii::$app->request->post('Lectures');
			if ($post) {
				$model->attributes = $post;
				if ($model->validate() && $model->save()) {
                    LecturesAssnSpeakers::UpdateLectures($model->id,$post['speaker_ids']);
					return $this->redirect(['lectures-list', 'event_id' => $event_id]);
				} else {
					dd($model->errors);
				}
			}

			$event_model = $this->findModel($event_id);
			$getAndSetSpeakers = ArrayHelper ::map( Speakers ::find() -> orderBy( 'name' ) -> asArray() -> all(), 'id', 'name' );
            $db_speakers = ArrayHelper ::getColumn($model->getSpeakersIds()->asArray()->all(), 'speaker_id');
            $model->speaker_ids = $db_speakers;

			return $this->render('add-lecture', [
				'model' => $model,
				'getAndSetSpeakers' => $getAndSetSpeakers,
				'event_model' => $event_model,
			]);
		}

		public function actionDeleteLecture( $id , $event_id )
		{
			$deleteLecture = Lectures ::findOne( $id );
			if( $deleteLecture -> delete() )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Lecture was delete' );
				return $this -> redirect( Url ::toRoute( [ 'lectures-list', 'event_id' => $event_id ] ) );
			}
		}

	}

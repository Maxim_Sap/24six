<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SpaceWork;

/**
 * SpaceWorkSearch represents the model behind the search form of `common\models\SpaceWork`.
 */
class SpaceWorkSearch extends SpaceWork
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_cate_plase', 'unfixiable_price', 'separate_price', 'square_meter', 'working_places', 'kitchen', 'conference_room', 'id_country'], 'integer'],
            [['title', 'description', 'text', 'email', 'address', 'phone', 'image', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SpaceWork::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cate_plase' => $this->id_cate_plase,
            'unfixiable_price' => $this->unfixiable_price,
            'separate_price' => $this->separate_price,
            'square_meter' => $this->square_meter,
            'working_places' => $this->working_places,
            'kitchen' => $this->kitchen,
            'conference_room' => $this->conference_room,
            'id_country' => $this->id_country,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=400, initial-scale=1.0">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php

?>
<body class="
<?= Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index' ? 'home_page_tpl' : '' ?>
">
<?php $this->beginBody() ?>
<?php echo $this->render('parts/header.php');?>
<div class="wrap">
    <?= $content ?>
</div>
<?php echo $this->render('parts/footer.php');?>
<?php if (!Yii::$app->user->isGuest) { ?>
    <?php echo $this->render('parts/admin_menu.php');?>
<?php } ?>
<div class="transparent_overlay"></div>
<?php $this->endBody() ?>
<?php if(Yii::$app->session->hasFlash('message')){ ?>
    <div id="alert-message">
        <?= Yii::$app->session->getFlash('message') ?>
    </div>
<?php } ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
use yii\helpers\Url;
?>
<header>
    <div class="wrap">
    <a href="/" class="logo"><img src="img/logo.png" alt=""></a>
    <?php if (!Yii::$app->user->isGuest) { ?>
        <ul>
            <li><a href="/" class="<?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Dashboard</a></li>
            <li><a href="<?=Url::toRoute(['/users'])?>" class="<?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'about') ? 'active' : ''?>">Users</a></li>
            <li><a href="<?=Url::toRoute(['/events'])?>" class="<?=(Yii::$app->controller->id == 'events') ? 'active' : ''?>">Events</a></li>
            <li><a href="<?=Url::toRoute(['/space-work'])?>" class="<?=(Yii::$app->controller->id == 'space-work') ? 'active' : ''?>">Spaces</a></li>
	        <li><a href="<?=Url::toRoute(['/partners'])?>" class="<?=(Yii::$app->controller->id == 'partners') ? 'active' : ''?>">Partners</a></li>
	        <li><a href="<?=Url::toRoute(['/set-main-event'])?>" class="<?=(Yii::$app->controller->id == 'set-main-event') ? 'active' : ''?>">Set main events</a></li>
	        <li><a href="<?=Url::toRoute(['/set-main-place'])?>" class="<?=(Yii::$app->controller->id == 'set-main-place') ? 'active' : ''?>">Set main places</a></li>
	        <li><a href="<?=Url::toRoute(['/questions'])?>" class="<?=(Yii::$app->controller->id == 'questions') ? 'active' : ''?>">Questions</a></li>
            <li><a href="<?=Url::toRoute(['/site/logout'])?>">Logout</a></li>
        </ul>
    <?php }else{?>
<!--        <ul>-->
<!--            <li><a href="/site/login">Login</a></li>-->
<!--        </ul>-->
    <?php }?>
    </div>

</header>
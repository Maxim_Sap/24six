<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Speakers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Speakers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$dir = Yii::getAlias("@avatar");
?>
<div class="speakers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('backend', 'Speakers list'), ['index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'avatar',
                'value' => ($model->avatar) ? Html::img(Yii::$app->params['front-url'] . $dir . "/thumb/" . $model->avatar, ['width' => 100]) : 'No image',
                'format' => 'html',
            ],
            'position',
        ],
    ]) ?>

</div>

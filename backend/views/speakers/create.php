<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Speakers */

$this->title = Yii::t('backend', 'Create Speakers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Speakers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speakers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

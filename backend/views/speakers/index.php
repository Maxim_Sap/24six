<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpeakersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Speakers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speakers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Speakers'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute' => 'avatar',
                'contentOptions' => function ($model, $key, $index, $column) {
                    return ['class' => 'id-img'];
                },
                'content' => function ($data) {
                    $dir = Yii::getAlias("@avatar");
                    return Html::img(Yii::$app->params['front-url'].$dir."/thumb/".$data->avatar,['width'=>100]);
                }
            ],
            'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

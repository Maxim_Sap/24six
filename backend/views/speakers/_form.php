<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\UploadImg\UploadImg;

/* @var $this yii\web\View */
/* @var $model common\models\Speakers */
/* @var $form yii\widgets\ActiveForm */
$avatar = isset($model->avatar) ? $model->avatar : '';
$this->registerCssFile('/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css');
$this->registerJsFile('/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/crop_img.js?t=1', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="speakers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo UploadImg::widget(
        [
            'imageName' => $avatar,
            'pathToImg' => '/source/img/avatar/',
            'action' => '/index.php?r=upload/upload-images',
            'form_image_id' => 'avatar',
            'place' => 'avatar',
            'block_height' => 200,
            'field_name' => 'Speakers[avatar]',
        ]
    );?>
    <?= $form->field($model, 'name',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true,'style'=>'margin-top: 26px;'
        ]) ?>

    <?= $form->field($model, 'position',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>
    <input id="resource_id" type="hidden" value="<?=$model->id?>">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

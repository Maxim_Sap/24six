<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.06.2018
 * Time: 11:45
 */

if(!$model->isNewRecord){
    $prefix = 'Update';
}else{
    $prefix = 'Add';
}
$event_id = Yii ::$app -> request -> get('event_id');
$this->title = Yii::t('backend', $prefix.' partner')." to event: ".'<a href="'.Url::toRoute(['events/view', 'id' => $event_model->id]).'">'.$event_model->title.'</a>';
?>
<div class="wrap">
    <div class="space-work-form">
        <h1><?= $this->title ?></h1>
        <div class="form">
            <?php $form = ActiveForm::begin(); ?>

				<?= $form -> field( $model, 'id_partner' ) -> dropDownList(
					$getAllPartner,
					[ 'class'  => 'placeholder form-control',
					  'prompt' => 'Select speaker',
					]
				) ?>
	        <input type="hidden" value="<?= $event_id  ?>" name="PartnerEvents[event_id]">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <br><br><br>
        </div>
    </div>
</div>
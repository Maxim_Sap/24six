<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
    use kartik\select2\Select2;
	use yii\bootstrap\ActiveForm;

	if(!$model->isNewRecord){
		$prefix = 'Update';
	}else{
		$prefix = 'Add';
	}
	$this->title = Yii::t('backend', $prefix.'Lecture')." to event: ".'<a href="'.Url::toRoute(['events/view', 'id' => $event_model->id]).'">'.$event_model->title.'</a>';

	$event_id = Yii ::$app -> request -> get('event_id');
?>
<div class="wrap">
	<div class="space-work-form">
		<h1><?= $this->title ?></h1>
		<div class="form">
			<?php $form = ActiveForm ::begin(); ?>
			<br>
			<?= $form->field($model, 'time_from',['template'=>
															   "<div class=\"form-control\">\n
											{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
										</div>"])
					 ->textInput([
									 'type' => 'time'
								 ]) ?>

			<?= $form->field($model, 'time_to',['template'=>
															 "<div class=\"form-control\">\n
											{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
										</div>"])
					 ->textInput([
									 'type' => 'time'
								 ]) ?>

			<?= $form->field($model, 'subject',['template'=>
															 "<div class=\"form-control\">\n
											{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
										</div>"])
					 ->textInput([
									 'autocomplete'=>'off', 'autofocus' => true
								 ]) ?>

            <?php

            echo $form->field($model, 'speaker_ids')->widget(Select2::className(),[
                'data' => $getAndSetSpeakers,
                'options' => [
                    'placeholder' => 'Please select speakers * ',
                    'multiple' => true,
                    'class' => 'form-control',
                    'id' => 'speakers-select',
                ],
            ])->label(false);
            ?>
			<?= Html ::submitButton( Yii ::t( 'backend', $prefix ), [ 'class' => 'btn btn-success', 'id' => 'add_lecture_data' ] ) ?>
			<?php ActiveForm ::end() ?>
		</div>
	</div>
</div>
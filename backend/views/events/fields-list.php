<?php

	use yii\helpers\Html;
	use yii\grid\GridView;
	use yii\helpers\Url;

	/* @var $this yii\web\View */
	/* @var $searchModel backend\models\search\SpaceWorkSearch */
	/* @var $dataProvider yii\data\ActiveDataProvider */

	$event_id                          = $event_model -> id;
	$this -> title                     = Yii ::t( 'backend', 'Field list' ) . " from Event: " . '<a href="' . Url ::toRoute( [ 'events/view', 'id' => $event_id ] ) . '">' . $event_model -> title . '</a>';
	$this -> params[ 'breadcrumbs' ][] = $this -> title;

?>
<div class="wrap">
	<div class="conference-index">
		<h1><?= $this -> title ?></h1>

		<p>
			<?= Html ::a( Yii ::t( 'backend', 'Add Field' ), [ 'add-fields', 'event_id' => $event_id ], [ 'class' => 'btn btn-success' ] ) ?>
		</p>

		<?= GridView ::widget(
			[
				'dataProvider' => $dataProvider,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],
					'title_field',
					'description_field:html',
//					'source_id',
//					'type_source',
					[
						'class'    => 'yii\grid\ActionColumn',
						'options'  => [ 'style' => 'width:200px' ],
						//'controller' => 'properties',
						'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
						'buttons'  => [
							'update' => function( $url, $data ) use ( $event_id )
							{
								return '<a href="' . Url ::toRoute( [ 'update-fields', 'id' => $data[ 'id' ], 'event_id' => $event_id ] ) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
							},
							'delete' => function( $url, $data ) use ( $event_id )
							{
								return '<a href="' . Url ::toRoute( [ 'delete-field', 'id' => $data[ 'id' ], 'event_id' => $event_id ] ) . '"><span class="glyphicon glyphicon-trash"></span></a>';
							},
						],
					],
				],
			]
		); ?>
	</div>
</div>

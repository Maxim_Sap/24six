<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpaceWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$event_id = $event_model->id;
$this->title = Yii::t('backend', 'Partners list')." from Event: ".'<a href="'.Url::toRoute(['events/view', 'id' => $event_id]).'">'.$event_model->title.'</a>';
$this->params['breadcrumbs'][] = $this->title;
$dir = Yii::getAlias("@logo");

?>
<div class="wrap">
    <div class="conference-index">
        <h1><?= $this->title ?></h1>

        <p>
            <?= Html::a(Yii::t('backend', 'Add partner'), ['add-partner','event_id'=>$event_id], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title_partner',
				[
					'attribute' => 'logo_partner',
					'contentOptions' => function ($model, $key, $index, $column) {
						return ['class' => 'id-img'];
					},
					'content' => function ($data) use($dir) {
						return Html::img(Yii::$app->params['front-url'].$dir."/thumb/".$data['logo_partner'],['width'=>100]);
					}
				],
                'link_partner',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options'=>['style'=>'width:200px'],
                    //'controller' => 'properties',
                    'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
                    'buttons' => [
                        'update' => function ($url,$data)  use ($event_id){
                            return '<a href="'.Url::toRoute(['update-partner', 'id' => $data['id'],'event_id'=>$event_id]).'"><span class="glyphicon glyphicon-pencil"></span></a>';
                        },
                        'delete' => function ($url,$data) use ($event_id){
                            return '<a href="'.Url::toRoute(['delete-partner', 'id' => $data['id'],'event_id'=>$event_id]).'"><span class="glyphicon glyphicon-trash"></span></a>';
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Events */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$dir = Yii::getAlias("@background_image");
$getAttributeOption = Yii::$app->request->get('add_option');
$event_type_mass = [
	3 => 'Weekly',
	1 => 'Single-time'
];
?>
<div class="wrap">
    <div class="events-view ">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(
                Yii::t('backend', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]
            ) ?>
            <?= Html::a(Yii::t('backend', 'Partners list'), ['partners-list', 'event_id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('backend', 'Custom field list'), ['fields-list', 'event_id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('backend', 'List lectures'), ['lectures-list', 'event_id' => $model->id], ['class' => 'btn btn-success']) ?>

        </p>
        <?= DetailView::widget(
            [
                'options' => ['class' => 'get_child table'],
                'model' => $model,
                'attributes' => [
                    'id',
                    'cateEvent.category',
					[
						'attribute' => 'type_event',
						'label' => 'Type',
						'value' => isset($event_type_mass[$model->type_event]) ? $event_type_mass[$model->type_event] : 'wrong type',
					],
                    'title',
                    'description:html',
                    'text:html',
                    'address',
                    'price_ticket_event',
                    [
                        'attribute' => 'image',
                        'value' => ($model->image) ? Html::img(Yii::$app->params['front-url'] . $dir . "/icon/" . $model->image, ['width' => 100]) : 'No image',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'php:d/m/Y H:i:s']
                    ],
                    'time_start_event',
                    'time_end_event',
                    [
                        'attribute' => 'date_event',
                        'format' => ['date', 'php:d/m/Y']
                    ],
                    'phone',
                    'email:email',
                ],
            ]
        ) ?>

    </div>
</div>

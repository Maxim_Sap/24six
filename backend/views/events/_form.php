<?php

use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use common\components\UploadImg\UploadImg;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs(
    "$('#datepicker input').datepicker({                    
        format: 'mm/dd/yyyy',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false 
    });",
    \yii\web\View::POS_READY
);
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css');
$this->registerJsFile('/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$background_image = isset($model->image) ? $model->image : '';
?>

<div class="events-form adminpanel_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_cate_event')->dropDownList($categories,
        ['class' => 'placeholder form-control',
            'prompt' => 'Choose from the list',
        ]) ?>
    <?= $form->field($model, 'id_country')->dropDownList($country,
        ['class' => 'placeholder form-control',
            'prompt' => 'Select Country',
        ]) ?>

    <?= $form->field($model, 'type_event') -> dropDownList(
        [
            3 => 'Weekly',
            1 => 'Single-time',
        ],
        ['id' => 'check_select' ]
    );
    ?>
    <?= $form->field($model, 'title',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>

    <? if (!$model->isNewRecord) {?>
        <div class="event_bg_image">
                <?php echo UploadImg::widget(
                    [
                        'imageName' => $background_image,
                        'pathToImg' => '/source/img/background_image/',
                        'action' => '/index.php?r=upload/upload-images',
                        'form_image_id' => 'background_image',
                        'place' => 'background_image',
                        'block_height' => 400,
                    ]
                );?>
        </div>
    <?php }else{?>
        <p>You can upload and crop image when events was created</p>
    <?php }?>


    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'editorOptions' => [
            //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ]
    ]) ?>
    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => [
            //'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ]
    ]) ?>

    <?= $form->field($model, 'price_ticket_event',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>
    <div id="datepicker">
    <?= $form->field($model, 'date_event',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>
    </div>

    <?= $form->field($model, 'time_start_event',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'type' => 'time'
        ]) ?>

    <?= $form->field($model, 'time_end_event',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'type' => 'time'
        ]) ?>

    <?= $form->field($model, 'address',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>

    <?= $form->field($model, 'max_spaces',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>

    <?= $form->field($model, 'phone',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>

    <?= $form->field($model, 'email',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>
    <input id="resource_id" type="hidden" value="<?=$model->id?>">
    <input id="resource_type" type="hidden" value="event">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    //window.CKEDITOR_BASEPATH="/assets/3f6c234c/"
</script>
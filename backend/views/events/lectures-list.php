<?php

	use yii\helpers\Html;
	use yii\grid\GridView;
	use yii\helpers\Url;

	/* @var $this yii\web\View */
	/* @var $searchModel backend\models\search\SpaceWorkSearch */
	/* @var $dataProvider yii\data\ActiveDataProvider */

	$event_id                          = $event_model -> id;
	$this -> title                     = Yii ::t( 'backend', 'Lecture list' ) . " from Event: " . '<a href="' . Url ::toRoute( [ 'events/view', 'id' => $event_id ] ) . '">' . $event_model -> title . '</a>';
	$this -> params[ 'breadcrumbs' ][] = $this -> title;
    $dir = Yii::getAlias("@avatar");

?>
<div class="wrap">
	<div class="conference-index">
		<h1><?= $this -> title ?></h1>

		<p>
			<?= Html ::a( Yii ::t( 'backend', 'Add Lecture' ), [ 'add-lecture', 'event_id' => $event_id ], [ 'class' => 'btn btn-success' ] ) ?>
			<?= Html ::a( Yii ::t( 'backend', 'Speakers list' ), ['/speakers'], [ 'class' => 'btn btn-success' ] ) ?>
		</p>

		<?= GridView ::widget(
			[
				'dataProvider' => $dataProvider,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],
                    [
                        'label' => 'Speakers',
                        'content' => function ($data) use ($dir) {
                            $speakers = $data->getSpeakersData();
			                if(!empty($speakers));
                            $content = '<div class="speakers-block table_lecture">';
                                foreach($speakers as $speaker){
                                    $content .= '<div class="speaker">'
                                                    .'<div class="avatar" style="background-image: url('.Yii::$app->params['front-url'].$dir."/icon/".$speaker['avatar'].')"></div>'
                                                    .'<div class="name">'.$speaker['name'].'</div>'
                                                .'</div>';
                                }
                            $content .= '</div>';
			                return $content;
                        }
                    ],
					'subject',
					'time_from',
					'time_to',
					[
						'class'    => 'yii\grid\ActionColumn',
						'options'  => [ 'style' => 'width:200px' ],
						//'controller' => 'properties',
						'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
						'buttons'  => [
							'update' => function( $url, $data ) use ( $event_id )
							{
								return '<a href="' . Url ::toRoute( [ 'update-lecture', 'id' => $data[ 'id' ], 'event_id' => $event_id ] ) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
							},
							'delete' => function( $url, $data ) use ( $event_id )
							{
								return '<a href="' . Url ::toRoute( [ 'delete-lecture', 'id' => $data[ 'id' ], 'event_id' => $event_id ] ) . '"><span class="glyphicon glyphicon-trash"></span></a>';
							},
						],
					],
				],
			]
		); ?>
	</div>
</div>

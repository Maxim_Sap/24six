<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Events'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'image',
                'contentOptions' => function ($model, $key, $index, $column) {
                    return ['class' => 'id-img'];
                },
                'content' => function ($data) {
                    $dir = Yii::getAlias("@background_image");
                    return Html::img(Yii::$app->params['front-url'].$dir."/icon/".$data->image,['width'=>100]);
                }
            ],
            [
                'attribute' => 'id_cate_event',
                'label' => 'Category',
                'content' => function ($data) {
                    return $data->cateEvent->category;
                }
            ],
            [
                'attribute' => 'type_event',
                'label' => 'Type',
                'content' => function ($data) {
    	            $mass = [
		                3 => 'Weekly',
						1 => 'Single-time'
		            ];
                    return $mass[$data->type_event];
                }
            ],
            'title',
            [
                'attribute' => 'description',
                'contentOptions' => function ($model, $key, $index, $column) {
                    return ['class' => 'short-desc-column'];
                },
                'content' => function ($data) {
                    return \yii\helpers\StringHelper::truncate(strip_tags($data->description), 200);
                }
            ],
            //'text:ntext',
            //'address',
            //'price_ticket_event',
            //'image',
            //'background_image',
            //'created_at',
            //'time_start_event',
            //'time_end_event',
            //'date_event',
            //'phone',
            //'email:email',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options'=>['style'=>'width:120px'],
                    //'controller' => 'properties',
                    'template' => '{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
                ],
            ],
        ]); ?>
</div>

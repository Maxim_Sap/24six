<?php
	use yii\helpers\Html;
    use mihaildev\ckeditor\CKEditor;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;

	if(!$model->isNewRecord){
		$prefix = 'Update';
	}else{
		$prefix = 'Add';
	}
	$this->title = Yii::t('backend', $prefix.' field')." to event: ".'<a href="'.Url::toRoute(['events/view', 'id' => $event_model->id]).'">'.$event_model->title.'</a>';

	$event_id = Yii ::$app -> request -> get('event_id');
?>
<div class="wrap">
	<div class="space-work-form">
		<h1><?= $this->title ?></h1>
		<div class="form">
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($model, 'title_field',['template'=>
														  "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
					 ->textInput([
									 'maxlength' => true
								 ]) ?>
            <?= $form->field($model, 'description_field')->widget(CKEditor::className(), [
                'editorOptions' => [
                    //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                ]
            ]) ?>
			<input type="hidden" name="CustomFields[source_id]" value="<?= $event_id ?>">
			<input type="hidden" name="CustomFields[type_source]" value="<?= \common\models\Favorites::TYPE_EVENTS ?>">
			<div class="form-group">
				<?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
			</div>

			<?php ActiveForm::end(); ?>
			<br><br><br>
		</div>
	</div>
</div>
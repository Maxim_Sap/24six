<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpaceWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Custom fields list')." from Space: ".'<a href="'.Url::toRoute(['space-work/view', 'id' => $space_model->id]).'">'.$space_model->title.'</a>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap">
    <div class="conference-index">
        <h1><?= $this->title ?></h1>

        <p>
            <?= Html::a(Yii::t('backend', 'Add custom field'), ['add-custom-field','space_id'=>$space_model->id], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title_field',
                'description_field:html',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options'=>['style'=>'width:200px'],
                    //'controller' => 'properties',
                    'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
                    'buttons' => [
                        'update' => function ($url,$data) {
                            return '<a href="'.Url::toRoute(['update-custom-field', 'id' => $data->id,'space_id'=>$data->source_id]).'"><span class="glyphicon glyphicon-pencil"></span></a>';
                        },
                        'delete' => function ($url,$data) {
                            return '<a href="'.Url::toRoute(['delete-custom-field', 'id' => $data->id,'space_id'=>$data->source_id]).'"><span class="glyphicon glyphicon-trash"></span></a>';
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>

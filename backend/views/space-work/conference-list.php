<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpaceWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Conference list')." from Space: ".$space_model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap">
    <div class="conference-index">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('backend', 'Add conference room'), ['add-conf-room','space_id'=>$space_model->id], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                [
                    'attribute' => 'status',
                    'content' => function ($data) {
                        $status_mass = [
                            0 =>'Not Active',
                            1 =>'Active',
                        ];
                        return $status_mass[$data->status];
                    },
                ],
                [
                    'attribute' => 'name',
                    'content' => function ($data) {
                        return $data->name;
                    },
                    'options'=>['style'=>'width:200px;'],
                ],

                [
                    'attribute' => 'desc',
                    'content' => function ($data) {
                        return $data->desc;
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options'=>['style'=>'width:200px'],
                    //'controller' => 'properties',
                    'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
                    'buttons' => [
                        'update' => function ($url,$data) {
                            return '<a href="'.Url::toRoute(['update-conference', 'id' => $data->id,'space_id'=>$data->space_id]).'"><span class="glyphicon glyphicon-pencil"></span></a>';
                        },
                        'delete' => function ($url,$data) {
                            return '<a href="'.Url::toRoute(['delete-conference', 'id' => $data->id,'space_id'=>$data->space_id]).'"><span class="glyphicon glyphicon-trash"></span></a>';
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SpaceWork */

$this->title = Yii::t('backend', 'Update Space Work: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Space Works'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="space-work-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
		'countries' => $countries
    ]) ?>

</div>

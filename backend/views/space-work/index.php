<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpaceWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Space Works');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="space-work-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Add Space'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'image',
                'contentOptions' => function ($model, $key, $index, $column) {
                    return ['class' => 'id-img'];
                },
                'content' => function ($data) {
                    $dir = Yii::getAlias("@background_image");
                    return Html::img(Yii::$app->params['front-url'].$dir."/icon/".$data->image,['width'=>100]);
                }
            ],
            [
                'attribute' => 'id_cate_plase',
                'label' => 'Category',
                'content' => function ($data) {
                    return $data->catePlase->category;
                }
            ],
            [
                'attribute' => 'title',
                'label' => 'Title',
                'content' => function ($data) {
                    return $data->title;
                }
            ],
            [
                'attribute' => 'description',
                'content' => function ($data) {
                    return \yii\helpers\StringHelper::truncate(strip_tags($data->description), 200);
                }
            ],
            [
                'attribute' => 'text',
                'content' => function ($data) {
                    return \yii\helpers\StringHelper::truncate(strip_tags($data->text), 200);
                }
            ],
            //'email:email',
            //'unfixiaed_price',
            //'separate_proce',
            //'address',
            //'phone',
            //'background_image',
            //'image',
            //'square_meter',
            //'working_places',
            //'kitchen',
            //'conference_room',
            //'id_country',
            //'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:110px'],
                //'controller' => 'properties',
                'template' => '{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
            ],
        ],
    ]); ?>
</div>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.06.2018
 * Time: 11:45
 */
$this->title = Yii::t('backend', 'Add conference room')." to space: "." from Space: ".'<a href="'.Url::toRoute(['space-work/view', 'id' => $space_model->id]).'">'.$space_model->title.'</a>';
if($model->isNewRecord){
    $model->working_days = '1,2,3,4,5,6,7';
    $model->working_time_start = '09:00';
    $model->working_time_end= '21:00';
}
?>
<div class="wrap">
    <div class="space-work-form">
        <h1><?= $this->title ?></h1>
        <div class="form">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'status')->dropDownList(
                [
                    0 =>'Not Active',
                    1 =>'Active',
                ]) ?>
            <?= $form->field($model, 'name',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                ->textInput([
                    'maxlength' => true
                ]) ?>

            <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
                'editorOptions' => [
                    //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                ]
            ]) ?>
            <?= $form->field($model, 'price_for_hour',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                ->textInput([
                    'maxlength' => true,'type'=>'number','step'=>'0.10'
                ]) ?>
            <?= $form->field($model, 'working_days',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
				->hiddenInput([
                    'maxlength' => true
                ]) ?>
            <?= $form->field($model, 'working_time_start',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                ->textInput([
                    'maxlength' => true,'type'=>'time'
                ]) ?>
            <?= $form->field($model, 'working_time_end',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                ->textInput([
                    'maxlength' => true,'type'=>'time'
                ]) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <br><br><br>
        </div>
    </div>
</div>
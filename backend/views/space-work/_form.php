<?php

use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use common\components\UploadImg\UploadImg;

/* @var $this yii\web\View */
/* @var $model common\models\SpaceWork */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css');
$this->registerJsFile('/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/crop_img.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$background_image = isset($model->image) ? $model->image : '';
?>

    <div class="space-work-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_cate_plase')->dropDownList($categories,
            ['class' => 'placeholder form-control',
                'prompt' => 'Choose from the list']) ?>

        <?= $form->field($model, 'id_country')->dropDownList($countries,
            ['class' => 'placeholder form-control',
                'prompt' => 'Select City'])->label('Select City') ?>

        <?= $form->field($model, 'title',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                    'maxlength' => true
            ]) ?>

        <? if (!$model->isNewRecord) {?>
            <div class="event_bg_image">
                <?php echo UploadImg::widget(
                    [
                        'imageName' => $background_image,
                        'pathToImg' => '/source/img/background_image/',
                        'action' => '/index.php?r=upload/upload-images',
                        'form_image_id' => 'background_image',
                        'place' => 'background_image',
                        'block_height' => 400,
                    ]
                );?>
            </div>
        <?php }else{?>
            <p>You can upload and crop image when space was created</p>
        <?php }?>

        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'editorOptions' => [
                //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]
        ]) ?>

        <?= $form->field($model, 'text')->widget(CKEditor::className(), [
            'editorOptions' => [
                //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]
        ]) ?>

        <?= $form->field($model, 'address',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true
            ]) ?>

        <?= $form->field($model, 'email',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'email'
            ]) ?>

        <?= $form->field($model, 'phone',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'phone'
            ]) ?>

        <?= $form->field($model, 'square_meter',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

        <?= $form->field($model, 'working_places',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

        <?= $form->field($model, 'kitchen',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

        <?= $form->field($model, 'max_spaces',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

		<?= $form->field($model, 'max_office',['template'=>
												   "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
				 ->textInput([
								 'maxlength' => true, 'type' => 'number'
							 ]) ?>

        <?= $form->field($model, 'unfixiable_price',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

        <?= $form->field($model, 'separate_price',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>

        <?= $form->field($model, 'one_day_price',['template'=>
            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
            ->textInput([
                'maxlength' => true, 'type' => 'number'
            ]) ?>



        <input id="resource_id" type="hidden" value="<?=$model->id?>">
        <input id="resource_type" type="hidden" value="space">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <br><br><br>

    </div>

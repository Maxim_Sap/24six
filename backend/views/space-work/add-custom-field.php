<?php
use yii\helpers\Html;
use yii\helpers\Url;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.06.2018
 * Time: 11:45
 */

if(!$model->isNewRecord){
    $prefix = 'Update';
}else{
    $prefix = 'Add';
}
$this->title = Yii::t('backend', $prefix.' custom field')." to space: ".'<a href="'.Url::toRoute(['space-work/view', 'id' => $space_model->id]).'">'.$space_model->title.'</a>';
?>
<div class="wrap">
    <div class="space-work-form">
        <h1><?= $this->title ?></h1>
        <div class="form">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'title_field',['template'=>
                "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                ->textInput([
                    'maxlength' => true
                ]) ?>

            <?= $form->field($model, 'description_field')->widget(CKEditor::className(), [
                'editorOptions' => [
                    //'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                ]
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <br><br><br>
        </div>
    </div>
</div>
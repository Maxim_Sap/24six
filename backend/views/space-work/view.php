<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SpaceWork */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Space Works'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$getAttributeOption = Yii::$app->request->get('add_option');
$dir = Yii::getAlias("@background_image");
?>
<div class="wrap">
    <div class="space-work-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Conference rooms list'), ['conference-view', 'space_id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('backend', 'Custom fields'), ['custom-fields-list', 'space_id' => $model->id], ['class' => 'btn btn-success']) ?>
            <? //= Html::a(Yii::t('backend', 'Custom fields'), ['add-custom-field', 'space_id' => $model->id], ['class' => 'btn btn-success']) ?>

            <?= Html::a(Yii::t('backend', 'Delete space'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'catePlase.category',
                'title',
                'description:html',
                'text:html',
                'email:email',
                'unfixiable_price',
                'separate_price',
                'address',
                'phone',
                [
                    'attribute' => 'image',
                    'value' => ($model->image) ? Html::img(Yii::$app->params['front-url'] . $dir . "/icon/" . $model->image, ['width' => 100]) : 'No image',
                    'format' => 'html',
                ],
                'square_meter',
                'working_places',
                'kitchen',
                'country.country',
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:d/m/Y']
                ],
            ],
        ]) ?>
        <?php if ($getAttributeOption == 'add_field'): ?>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($attributeModel, 'title_field')->textInput() ?>
            <?= $form->field($attributeModel, 'description_field')->textInput() ?>
            <input id="customfields-source_id" name="CustomFields[source_id]" type="hidden" value="<?= $model->id ?>">
            <input id="customfields-type_source" name="CustomFields[type_source]" type="hidden"
                   value="<?= \common\models\Favorites::TYPE_PLACES ?>">

            <?= Html::submitButton(Yii::t('backend', 'Add'), ['class' => 'btn btn-success', 'id' => 'add_option_field_space']) ?>
            <?php ActiveForm::end() ?>
            <h2>What is inside</h2>
            <div class="custom_field">
                <?php if (!empty($getFieldSpaces)): ?>
                    <?php foreach ($getFieldSpaces as $itemField): ?>
                        <div class="item_field filed_"<?= $itemField->id ?> data-id="<?= $itemField->id ?>">
                            <p><?= $itemField->title_field ?></p>
                            <p><?= $itemField->description_field ?></p>
                            <a id="delete_field"
                               href="<?= Url::toRoute(['space-work/delete-field', 'id' => $itemField->id, 'id_event' => $model->id]) ?>">Delete
                                Field</a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h2>No additions to the space</h2>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SpaceWork */

$this->title = Yii::t('backend', 'Create Space Work');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Space Works'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="space-work-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'countries' => $countries
    ]) ?>

</div>

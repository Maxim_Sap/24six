<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SearchPartners */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Partners');
$this->params['breadcrumbs'][] = $this->title;
$dir = Yii::getAlias("@logo");
?>
<div class="partners-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Add Partners'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'title_partner',
			[
				'attribute' => 'logo_partner',
				'contentOptions' => function ($model, $key, $index, $column) {
					return ['class' => 'id-img'];
				},
				'content' => function ($data) use($dir) {
					return Html::img(Yii::$app->params['front-url'].$dir."/thumb/".$data->logo_partner,['width'=>100]);
				}
			],
            'link_partner',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:110px'],
                //'controller' => 'properties',
                'template' => '{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
            ],
        ],
    ]); ?>
</div>

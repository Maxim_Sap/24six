<?php

use yii\helpers\Html;
use common\components\UploadImg\UploadImg;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */
/* @var $form yii\widgets\ActiveForm */
$logo_partner = isset($model->logo_partner) ? $model->logo_partner : '';
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-partners">
        <div class="form-control">
            <label class="control-label" style="position: relative;">Logo</label>
            <?php echo UploadImg::widget(
                [
                    'imageName' => $logo_partner,
                    'pathToImg' => '/source/img/logo/',
                    'action' => '/index.php?r=upload/upload-images',
                    'form_image_id' => 'logo',
                    //'place' => 'logo',
                    'block_height' => 200,
                    'field_name' => 'Partners[logo_partner]',
                    'buttons' => ['delete'],
                ]
            ); ?>

        </div>
    </div>
    <?= $form->field($model, 'title_partner', ['template' =>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete' => 'off', 'autofocus' => true, 'maxlength' => true, 'style' => 'margin-top: 26px;'
        ]) ?>

    <?= $form->field($model, 'link_partner', ['template' =>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete' => 'off', 'autofocus' => true, 'maxlength' => true
        ]) ?>

    <div class="form-group">
        <input id="resource_id" type="hidden" value="<?= $model->id ?>">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <br><br>

</div>

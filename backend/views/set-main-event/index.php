<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SearchSetMainEvent */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Set Main Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="set-main-event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
	    <?php if($checkCountSetEvent >= 4): ?>
		    Limit reached
	    <?php else: ?>
			<?php $form = ActiveForm ::begin( [ 'action' => '/index.php?r=set-main-event%2Fset-event'] ); ?>
			<?= $form -> field( $eventModel, 'title' ) -> dropDownList(
				$getAllEventTitle,
				[ 'class'  => 'placeholder country_select',
				  'prompt' => 'Select event',
				]
			) ?>
			<?= Html ::submitButton( Yii ::t( 'backend', 'Set Event' ), [ 'class' => 'btn btn-success' ] ) ?>
			<?php ActiveForm::end() ?>
	    <?php endif; ?>
<!--        --><?//= Html::a(Yii::t('backend', 'Create Set Main Event'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'image',
				'contentOptions' => function ($model, $key, $index, $column) {
					return ['class' => 'id-img'];
				},
				'content' => function ($data) {
					$dir = Yii::getAlias("@background_image");
					return Html::img(Yii::$app->params['front-url'].$dir."/icon/".$data -> event -> image,['width'=>100]);
				}
			],
			[
				'label' => 'Title',
				'content' => function ($data) {
					return $data->event->title;
				}
			],
			[
				'label' => 'description',
				'contentOptions' => function ($model, $key, $index, $column) {
					return ['class' => 'short-desc-column'];
				},
				'content' => function ($data) {
					return \yii\helpers\StringHelper::truncate($data -> event -> description, 200);
				}
			],
//            'id_event',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:110px'],
                //'controller' => 'properties',
                'template' => '{delete}&nbsp;&nbsp;&nbsp;{report}',
            ],
        ],
    ]); ?>
</div>

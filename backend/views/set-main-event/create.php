<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SetMainEvent */

$this->title = Yii::t('backend', 'Create Set Main Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Set Main Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="set-main-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h2 class="form_title"><?= Html::encode($this->title) ?></h2>

    <p style="text-align: center">Please fill out the following fields to login:</p>

    <div class="form">
        <div>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?//= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>


                <?= $form->field($model, 'email',['template'=>
                    "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                    ->textInput([
                        'autocomplete'=>'off', 'autofocus' => true
                    ]) ?>



                <?//= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'password',['template'=>
                    "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
                    ->passwordInput([
                        'autocomplete'=>'off'
                    ]) ?>

            <?php //$model->rememberMe = 1;?>
<!--            --><?//= $form->field($model, 'rememberMe',['template'=>
//                    "<div class=\"form-control\">\n
//                                    {input}</div>"])->checkbox(['style'=>'display:none;']) ?>

                <input type="hidden" name="LoginForm[rememberMe]" value="1">
<!--                <input type="checkbox" name="LoginForm[rememberMe]" id="loginform-rememberme" class="custom_checkbox">-->
<!--                <label for="loginform-rememberme">Remeber me</label>-->

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn blue_btn', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
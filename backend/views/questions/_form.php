<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form adminpanel_form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title',['template'=>
		"<div class=\"form-control\">\n
           {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
         </div>"])->textInput(['autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true ])
	?>

	<?= $form->field($model, 'description',['template'=>
		                                        "<div class=\"form-control\">\n
           {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
         </div>"])->textInput(['autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true ])
	?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

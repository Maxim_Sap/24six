<?php

	use yii\helpers\Html;

	/* @var $this yii\web\View */
	/* @var $model common\models\Questions */

	$this->title = Yii::t('backend', 'Update Questions: ' . $model->title, [
		'nameAttribute' => '' . $model->title,
	]);
	$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Questions'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
	$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="questions-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>

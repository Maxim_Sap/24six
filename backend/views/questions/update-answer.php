<?php

	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;

	/* @var $this yii\web\View */
	/* @var $model common\models\Questions */

	$this->title = Yii::t('backend', 'Update Questions: ' . $model->question, [
		'nameAttribute' => '' . $model->question,
	]);
	$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Questions'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="questions-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php $form = ActiveForm::begin(); ?>

		<?= $form -> field( $model, 'user_id' ) -> dropDownList(
			$username,
			[ 'class'  => 'placeholder form-control',
			  'prompt' => 'Users',
			]
		) ?>

		<?= $form->field($model, 'answer',['template'=>
											  "<div class=\"form-control\">\n
	           {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
	         </div>"])->textInput(['autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true ])
		?>

		<?= $form->field($model, 'question',['template'=>
													"<div class=\"form-control\">\n
	           {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
	         </div>"])->textInput(['autocomplete'=>'off', 'autofocus' => true, 'maxlength' => true ])
		?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

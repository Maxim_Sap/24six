<?php

	use yii\helpers\Html;
	use yii\grid\GridView;
	use yii\helpers\Url;

	/* @var $this yii\web\View */
	/* @var $searchModel backend\models\search\SpaceWorkSearch */
	/* @var $dataProvider yii\data\ActiveDataProvider */

	$answer_id = $question_model -> id;
	$this -> title                     = Yii ::t( 'backend', 'List answers' ) . " from Question: ". $question_model-> title . '</a>';
	$this -> params[ 'breadcrumbs' ][] = $this -> title;

?>
<div class="wrap">
	<div class="conference-index">
		<h1><?= $this -> title ?></h1>

		<?= GridView ::widget(
			[
				'dataProvider' => $dataProvider,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],

					[
						'attribute' => 'user_id',
						'label' => 'user',
						'content' => function ($data) {
							return $data->user->username;
						}
					],

					'question',
					'answer',
					[
						'class'    => 'yii\grid\ActionColumn',
						'options'  => [ 'style' => 'width:200px' ],
						//'controller' => 'properties',
						'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
						'buttons'  => [
							'update' => function( $url, $data ) use ( $answer_id )
							{
								return '<a href="' . Url ::toRoute( [ 'update-answer', 'id' => $data[ 'id' ], 'id_question' => $answer_id ] ) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
							},
							'delete' => function( $url, $data ) use ( $answer_id )
							{
								return '<a href="' . Url ::toRoute( [ 'delete-answer', 'id' => $data[ 'id' ], 'id_question' => $answer_id ] ) . '"><span class="glyphicon glyphicon-trash"></span></a>';
							},
						],
					],
				],
			]
		); ?>
	</div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SetMainPlaces */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="set-main-places-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_place')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

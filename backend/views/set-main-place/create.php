<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SetMainPlaces */

$this->title = Yii::t('backend', 'Create Set Main Places');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Set Main Places'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="set-main-places-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

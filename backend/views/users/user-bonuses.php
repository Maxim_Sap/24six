<?php
	use yii\helpers\Html;
	use mihaildev\ckeditor\CKEditor;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;

	$this->title = Yii::t('backend', 'Update bonuses')." to user: ".$user_model -> username. ' '.$user_model -> userProfile -> last_name;
?>
<div class="wrap">
	<div class="space-work-form">
		<h1><?= $this->title ?></h1>
		<div class="form">
			<?php if( !isset( $model ) && empty( $model ) ): ?>
				<h2>This user does not have any bonuses</h2>
			<?php else: ?>
				<?php $form = ActiveForm::begin(['action' => 'index.php?r=users%2Fupdate-user-bonuses']); ?>

				<?= $form->field($model, 'count_events_week',
								 [
									 'template'=>
										 "<div class=\"form-control\">\n
	                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
	                            </div>"])
						 ->textInput(
							 [
								 'maxlength' => true
							 ]
						 )
				?>
				<?= $form->field($model, 'count_time_for_massage',
								 [
									 'template'=>
										 "<div class=\"form-control\">\n
	                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
	                            </div>"])
						 ->textInput(
							 [
								 'maxlength' => true
							 ]
						 )
				?>
				<?= $form->field($model, 'count_time_for_conf_room',
								 [
									 'template'=>
										 "<div class=\"form-control\">\n
	                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
	                            </div>"])
						 ->textInput(
							 [
								 'maxlength' => true
							 ]
						 )
				?>

				<div class="form-group">
					<?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
				</div>
				<input type="hidden" name="UserMainPersonalBonus[user_id]" value="<?= $user_model -> id ?>">
				<input type="hidden" name="UserMainPersonalBonus[id]" value="<?= $model -> id ?>">
				<?php ActiveForm::end(); ?>
			<?php endif; ?>
			<br><br><br>
		</div>
	</div>
</div>
<?php

	use yii\helpers\Html;
	use yii\grid\GridView;
	use yii\helpers\Url;

	/* @var $this yii\web\View */
	/* @var $searchModel backend\models\search\SpaceWorkSearch */
	/* @var $dataProvider yii\data\ActiveDataProvider */
	$bonuses_id = $user_model -> id;
	$this -> title                     = Yii ::t( 'backend', 'Update bonuses' ) . " to user: " . $user_model -> username . ' ' . $user_model -> userProfile -> last_name;
	$this -> params[ 'breadcrumbs' ][] = $this -> title;
	$dir                               = Yii ::getAlias( "@logo" );

?>
<div class="wrap">
	<div class="conference-index">
		<h1><?= $this -> title ?></h1>
		<?= GridView ::widget(
			[
				'dataProvider' => $dataProvider,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],
					'count_events_week',
					'count_time_for_massage',
					'count_time_for_conf_room',
					[
						'class'    => 'yii\grid\ActionColumn',
						'options'  => [ 'style' => 'width:200px' ],
						//'controller' => 'properties',
						'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}&nbsp;&nbsp;&nbsp;{report}',
						'buttons'  => [
							'update' => function( $url, $data ) use ( $bonuses_id )
							{
								return '<a href="' . Url ::toRoute( [ 'find-user-bonuses','id' => $data[ 'id' ], 'user_id' => $bonuses_id ] ) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
							},
							'delete' => function( $url, $data ) use ( $bonuses_id )
							{
								return '<a href="' . Url ::toRoute( [ 'delete-bonuses-user','id' => $data[ 'id' ], 'user_id' => $bonuses_id ] ) . '"><span class="glyphicon glyphicon-trash"></span></a>';
							},
						],
					],
				],
			]
		); ?>
	</div>
</div>

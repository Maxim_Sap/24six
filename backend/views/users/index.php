<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table{
        text-align: center;
    }
</style>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'reset',
            //'token_end_date',
            'email:email',
            [
                'attribute' => 'status',
                'content' => function ($data) {
                    $status_mass = [
                            User::STATUS_DELETED =>'Deleted',
                            User::STATUS_NO_ACTIVE =>'Not Active',
                            User::STATUS_ACTIVE =>'Active',
                    ];
                    return $status_mass[$data->status];
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'last_activity',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:140px'],
                //'label'=>'actions',
                //'controller' => 'properties',
                'template' => '{bonuses}&nbsp;&nbsp;{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}',
                'buttons' => [
                		'bonuses' => function($url,$model,$key) {
    	                    return Html::a('','/index.php?r=users%2Fbonuses-list&user_id='.$model -> id ,['class'=>'bonus','title'=>'user bonuses']);
		                }
                ]
            ],

        ],
    ]); ?>
</div>

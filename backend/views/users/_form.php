<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    "$('#userprofile-birthday').datepicker({                    
        format: 'mm/dd/yyyy',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false
    });
    $('#userprofile-country_id').change(function(){
        var select = $('#userprofile-city_id');
            select.html(null);
    });
    ",
    \yii\web\View::POS_READY
);
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
if(!empty($userProfile->birthday)){
    $userProfile->birthday = date('m/d/Y',strtotime($userProfile->birthday));
}
?>

<div class="user-form adminpanel_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            User::STATUS_NO_ACTIVE =>'Not Active',
            User::STATUS_DELETED =>'Deleted',
            User::STATUS_ACTIVE =>'Active',
        ]) ?>
    <?= $form->field($userProfile, 'user_type')->dropDownList(
        [
            User::USER_DEFAULT=>'User',
            User::USER_ADMIN=>'Admin',
        ]) ?>
    
    <?= $form->field($model, 'email',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true
        ]) ?>

    <?= $form->field($model, 'password',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true, 'placeholder' => '******'
        ]) ?>

    <?= $form->field($model, 'username',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true
        ]) ?>

    <?= $form->field($userProfile, 'last_name',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true
        ]) ?>

    <?= $form->field($userProfile, 'birthday',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true
        ]) ?>

    <?= $form->field($userProfile, 'phone',['template'=>
        "<div class=\"form-control\">\n
                                    {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                                </div>"])
        ->textInput([
            'autocomplete'=>'off', 'autofocus' => true
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

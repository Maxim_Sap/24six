<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = "view profile: ".$model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$status_mass = [
    User::STATUS_DELETED =>'Deleted',
    User::STATUS_NO_ACTIVE =>'Not Active',
    User::STATUS_ACTIVE =>'Active',
];
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value' => $status_mass[$model->status]
            ],
            'email:email',
            'username',
            'userProfile.last_name',
            'userProfile.user_type',
            //'userProfile.birthday',
            [
                'attribute' => 'userProfile.birthday',
                'format' => ['date', 'php:d/m/Y']
            ],
            'userProfile.phone',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'last_activity',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d/m/Y']
            ],
        ],
    ]) ?>

</div>

<?php

namespace console\controllers;


use common\models\User;
use Yii;
use yii\console\Controller;
use yii\db\Query;

/**
 * Class RbacController
 *
 * @package app\commands
 */
class RbacController extends Controller
{
    /**
     * RBAC initialization
     */
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;

        try {
            $authManager->removeAll();

            $roleUser = $authManager->createRole(User::ROLE_NAME_USER);
            $roleUser->description = 'Regular User';
            $authManager->add($roleUser);

            $roleOrganization = $authManager->createRole(User::ROLE_NAME_ORGANIZATION);
            $roleOrganization->description = 'Organization';
            $authManager->add($roleOrganization);

            $roleOrganizationAdmin = $authManager->createRole(User::ROLE_NAME_ORGANIZATION_ADMIN);
            $roleOrganizationAdmin->description = 'Organization Admin';
            $authManager->add($roleOrganizationAdmin);
            $authManager->addChild($roleOrganizationAdmin, $roleOrganization);

            $roleContentManager = $authManager->createRole(User::ROLE_NAME_CONTENT_MANAGER);
            $roleContentManager->description = 'Content Manager';
            $authManager->add($roleContentManager);
            $authManager->addChild($roleContentManager, $roleUser);

            $roleAdmin = $authManager->createRole(User::ROLE_NAME_ADMIN);
            $roleAdmin->description = 'Site administrator';
            $authManager->add($roleAdmin);
            $authManager->addChild($roleAdmin, $roleUser);

            $roleSuperAdmin = $authManager->createRole(User::ROLE_NAME_SUPER_ADMIN);
            $roleSuperAdmin->description = 'Site Super Admin';
            $authManager->add($roleSuperAdmin);
            $authManager->addChild($roleSuperAdmin, $roleAdmin);

            // Trying to find admin in DB and assign role Users::ROLE_NAME_SUPER_ADMIN for it
            $admin = (new Query())
                ->from('user')
                ->where([
                    'email' => Yii::$app->params['superAdminLogin']
                ])
                ->one();

            if (!empty($admin['id'])) {
                $authManager->assign($roleSuperAdmin, $admin['id']);
            }

            echo 'Done!';
        } catch (\Exception $e) {
            $authManager->removeAll();
            echo 'Can\'t init RBAC rules because of error: ' . $e->getMessage() . PHP_EOL;
        }
    }
}

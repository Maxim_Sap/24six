<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'reset'                => $this->string(250)->notNull(),
            'token_end_date'       => $this->integer(11)->notNull(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(5),
            'created_at' => $this->integer()->notNull(),
            'last_activity' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('user', [
            'username' => 'admin',
            'email' => Yii::$app->params['superAdminLogin'],
            'auth_key' => 'uOyPu7AiE_1O3YsIzoJklrGcwiIMZkAp',
            'password_hash' => '$2y$10$gq/rgjKTyEAWC2ZmyJQBqeJtrQ1qvQFqxmxRa1AHalud.QxCkIZtS',
            'password_reset_token' => NULL,
            'status' => 10,
            'created_at' => 1439192838,
            'last_activity' => 1439192838,
            'updated_at' => 1439192838,
        ]);
        $this->insert('user', ['username' => 'Maxim','email' => 'maksym.sapozhnikov@gmail.com','auth_key' => '938SHs0EfNx3u_RzKsb_yq2hbwkDpGAX','password_hash' => '$2y$13$zlG2zmnUQmipF30WlqD8x.76Xn8OZu9d4NbmjZdKTlsH0BtLUIs2y','password_reset_token' => NULL,'status' => 10,'created_at' => 1439192838,'last_activity' => 1439192838,'updated_at' => 1439192838,]);
        $this->insert('user', ['username' => 'Olya','email' => 'olga.fantaz@gmail.com','auth_key' => 'txPitlhAuKJ5Ll37-o_Lk8cs2KKevrVF','password_hash' => '$2y$13$50YIRhmHG6GQAKkGOzgMt.M753f0xlQjah0zWWbttW0oHpO.PUFmq','password_reset_token' => NULL,'status' => 10,'created_at' => 1439192838,'last_activity' => 1439192838,'updated_at' => 1439192838,]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m180402_183902_user_profile
 */
class m180402_183902_user_profile extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%user_profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'user_type' => $this->integer(1)->notNull(),
            'avatar' => $this->string(250),
            'background' => $this->string(250),
            'last_name' => $this->string(50),
            'birthday' => $this->date(),
            'organization_name' => $this->string(150),
            'organization_description' => $this->text(),
            'receive_donation_type' => $this->integer(1)->defaultValue(1),
            'receive_donation_checkbox' => $this->integer(1)->defaultValue(1),
            'routing_number' => $this->string(150),
            'name_on_bank_account' => $this->string(150),
            'bank_name' => $this->string(150),
            'bank_account_type' => $this->integer(1)->defaultValue(1),
            'website' => $this->string(150),
            'school' => $this->string(150),
            'phone' => $this->string(30),
            'phone2' => $this->string(30),
            'country_id' => $this->integer(11)->defaultValue(NULL),
            'city_id' => $this->integer(11)->defaultValue(NULL),
            'address' => $this->string(255),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_profile}}');
    }

}

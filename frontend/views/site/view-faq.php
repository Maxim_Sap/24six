<div class="top_section">
	<div>
		<h1 class="desktop">Have a Questions?</h1>
		<div class="search_field">
			<form action="<?= \yii\helpers\Url::toRoute(['site/view-faq']) ?>" method="get">
				<button class="btn blue_btn right">Search</button>
				<input type="hidden" name="id" value="<?= $question[0]['id_question'] ?>">
				<input name="searchQuestionInGroup" type="text" placeholder="Search a question">
			</form>
		</div>
	</div>
</div>


<div class="wrap">
	<?php  ?>
	<h2><?=  $question[0]['title'] ?></h2>
	<p><?=  $question[0]['description']?></p>
	<div class="accordion">
		<?php foreach($question as $q): ?>
			<div class="accordion-item">
				<a><?= $q['question'] ?></a>
				<div class="content">
					<p><?= $q['answer'] ?></p>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<div class="top_section">
    <div>
        <h1 class="desktop">Have a Questions?</h1>
        <div class="search_field">
            <form action="<?= \yii\helpers\Url::toRoute(['site/faq']) ?>" method="get">
                <button class="btn blue_btn right">Search</button>
                <input name="searchQuestion" type="text" placeholder="Search a question">
            </form>
        </div>
    </div>
</div>


<div class="wrap">
    <h2>What your questions about?</h2>
    <?php if(!empty($faq)): ?>
        <?php foreach ($faq as $f): ?>
            <div class="faq_tile">
                <div class="title"><?= $f['title'] ?></div>
                <div class="text"><?= $f['description']?></div>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/view-faq?id='.$f['id']]) ?>" class="read_more">Read More</a>
            </div>
        <?php endforeach; ?>
    <?php endif ?>
</div>
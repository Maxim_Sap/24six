<?php

	use common\models\UserProfile;

	/* @var $this yii\web\View */

	$this -> title = Yii ::$app -> params[ 'siteName' ] . ' - Homepage';

	$this -> registerCssFile( 'css/slick/slick.css' );
	$this -> registerCssFile( 'css/slick/slick-theme.css' );
	$this -> registerJsFile( 'js/slick/jquery-migrate-1.2.1.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( 'js/slick/slick.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );

	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;

	$formatter = Yii ::$app -> formatter;
	$action_id = Yii ::$app -> controller -> action -> id;
	$get_page  = Yii ::$app -> request -> get( 'homepage-events' );
?>
<div class="top_section">
	<div class="bg">
		<h1>WELCOME TO 24~SIX LAS OLAS<br>
			<small> - a unique coworking hub for young professionals to create, innovate, collaborate and synergize!</small>
		</h1>
		<p>24~Six is a consciously shared, state-of-the-art work environment located in the business mecca of tropical paradise. Nestled in the famous Las Olas district of downtown Fort Lauderdale, along a tranquil river surrounded by a lush naturescape . . .
			<i>your ideal communal workspace awaits!</i></p>
		<a href="/places" class="btn black_btn">Explore Our Space</a>
	</div>
	<div class="form_wrap">
		<?php if( Yii ::$app -> user -> isGuest )
		{
			echo $this -> render( '//layouts/parts/login-form.php' );
		}
		else
		{ ?>
			<div class="form">
				<div class="form_title">Here will be your AD banner</div>
			</div>
		<?php } ?>
	</div>
</div>

<?php if( isset( $partners ) && !empty( $partners ) ) { ?>
	<div class="organizations">
		<?php
			$logo_dir = Yii::getAlias("@logo");
			foreach( $partners as $partner )
			{ ?>
				<a target="_blank" href="<?=$partner['link_partner']?>" title="<?=$partner['title_partner']?>">
					<img src="<?=$logo_dir.'/thumb/'.$partner['logo_partner']?>" alt="<?=$partner['title_partner']?> image" title="<?=$partner['title_partner']?>">
				</a>
			<?php } ?>
	</div>
<?php } ?>

<div class="who_we_are">
	<div class="wrap">
		<h3><span>we are</span> Amazing Coworking Space
			directly in Your Town</h3>
		<p><a
					href="<?= Url ::to(
						[ '/signup' ]
					) ?>"
			>Join</a> our family of entrepreneurs, freelancers, and ingenious minds who benefit from having an extension of their office. 24~Six Las Olas is intended to help you build upon your dreams. Our professional networking mixers and exciting events make for a vibrant social life that no other coworking venue offers!
		</p>
	</div>
</div>

<div class="photoes_slider_wrap">
	<div class="wrap">
		<div class="photo_slider">
			<div><img src="/img/slider_img.jpg" alt=""></div>
			<div><img src="/img/slider_img.jpg" alt=""></div>
			<div><img src="/img/slider_img.jpg" alt=""></div>
			<div><img src="/img/slider_img.jpg" alt=""></div>
		</div>
		<div class="text">
			<h3>24 six - is more <br>
				then just a space for work</h3>
			<p>We are also the first communal workspace to follow the six day workweek model, designed to maximize productivity and support a balanced lifestyle. This is why we ‘rest & recharge’ for 24 hours every week, from sundown on Friday to sundown on Saturday.</p>
			<a href="#" class="btn blue_btn">Explore</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>


<div class="grey_section">
	<div class="wrap">
		<div class="advantages">
			<div class="block_1" style="background: url('/source/img/background_image/origin/<?= $setPlaces[ 0 ][ 'image_place' ] ?>') center / cover no-repeat;">
				<div class="text_wrap">
					<div class="div_1"><?= $setPlaces[ 0 ][ 'title_place' ] ?></div>
					<div class="div_2">each saturday with good company</div>
					<div class="div_3">
						<?= $formatter -> asRelativeTime( $setPlaces[ 0 ][ 'date' ] ) ?>
					</div>
					<a href="<?= Url ::toRoute( [ 'places/view?id=' . $setPlaces[ 0 ][ 'place_id' ] ] ) ?>" class="btn blue_btn">Place Details</a>
				</div>
			</div>
			<div class="block_2_wrap">
				<?php
					$sliedSpaces = array_slice( $setPlaces, 1 );
					foreach( $sliedSpaces as $sliedSpace ):
						?>
						<div class="" style="background: url('/source/img/background_image/origin/<?= $sliedSpace[ 'image_place' ] ?>') center / cover no-repeat;">
							<span><?= $sliedSpace[ 'title_place' ] ?></span>
						</div>
					<?php endforeach; ?>
			</div>
		</div>
		<?php
			$generateSetsPlaces = <<<JS
 $(document).ready(function() {
       var secondSectionBlock = $('.block_2_wrap');
       for(i=2; i <= 4; i++) {
           secondSectionBlock.children().addClass('block_' + i);
           secondSectionBlock.children().first().removeClass('block_3').removeClass('block_4');
           $('.block_2').next().removeClass('block_2').removeClass('block_4');
           secondSectionBlock.children().last().removeClass('block_3');
       }
       $('.testimonials_slider').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 2,
            infinite: true,dots: true,
            adaptiveHeight: true,
            prevArrow:'<span class="slick-prev"></span>',
            nextArrow:'<span class="slick-next"></span>', 
             responsive: [
                {
                  breakpoint: 1023,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                         }
                }] 
        });
        $('.photo_slider').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1,
            infinite: true,
            adaptiveHeight: true,
            prevArrow:'<span class="slick-prev"></span>',
            nextArrow:'<span class="slick-next"></span>'
        });
 });


JS;
			$this -> registerJs( $generateSetsPlaces );
		?>
		<div class="cost">
			<h3>
				<span>HOW MUCH IS IT COST?</span>
				Become a Member
			</h3>
			<p>Lorem ipsum dolor sit amet, id cum lobortis assueverit delicatissimi, cum reque inani in, ne his elitr ridens scripserit. Labores tacimates hendrerit eam et, dico debet docendi et mea, aud.</p>
			<?php if( !empty( $plans ) ): ?>
				<?php foreach( $plans as $plan ): ?>
					<div class="cost_tile">
						<div class="title"><?= $plan[ 'plan' ] -> title ?></div>
						<?php if( $plan[ 'plan' ] -> id == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS ): ?>
							<div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / per day</div>
						<?php else: ?>
							<div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / mounth</div>
						<?php endif; ?>
						<div class="divider"></div>
						<ul>
							<?php $formatter = Yii ::$app -> formatter; ?>
							<?php if( isset( $plan[ 'bonus' ] ) && !empty( $plan[ 'bonus' ] ) ) : ?>
								<?php foreach( $data = [ $plan[ 'bonus' ] ] as $item ) : ?>
									<?php if( $item -> id == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS ): ?>
										<li>includes workspace for one day</li>
									<?php else: ?>
										<li>Workspace for <?= $item -> count_workspaces ?> day</li>
									<?php endif; ?>
									<?php if( !empty( $item -> count_time_for_conf_room ) ): ?>
										<li><?= $item -> count_time_for_conf_room ?> hour private conference room</li>
									<?php endif ?>
									<?php if( !empty( $item -> count_events_week ) ): ?>
										<li><?= $item -> count_events_week ?> Events a week</li>
									<?php endif ?>
									<?php if( !empty( $item -> count_daily_guest_passes ) ): ?>
										<li><?= $item -> count_daily_guest_passes ?> Daily Guest Passes</li>
									<?php endif ?>
									<?php if( !empty( $item -> count_time_for_massage ) ): ?>
										<li><?= $item -> count_time_for_massage ?> 30 min massage sessions in our state of the art massage chairs</li>
									<?php endif ?>
									<?php if( !empty( $item -> priority_registration_for_all_event ) ): ?>
										<li>Priority registration for all events</li>
									<?php endif ?>
									<?php if( !empty( $item -> complimentary_beverag ) ): ?>
										<li>Complimentary beverages</li>
									<?php endif ?>
									<?php if( !empty( $item -> mailbox_with_las_olas ) ): ?>
										<li>Mailbox with Las Olas Blvd Street Address</li>
									<?php endif ?>
									<?php if( !empty( $item -> fax_and_printer_service ) ): ?>
										<li>Fax and printer service</li>
									<?php endif ?>
								<?php endforeach ?>
							<?php endif ?>
						</ul>
						<div class="divider bottom"></div>
						<a href="<?= Url ::to( [ 'account/payments', 'type' => $plan[ 'plan' ] -> id ] ) ?>" class="btn blue_btn">Get Pass</a>
					</div>
				<?php endforeach; ?>
			<?php endif ?>
		</div>
	</div>
</div>


<div class="testimonials_section">
	<div class="wrap">
		<h3>
			<span>TESTIMONIALS</span>
			What People Say
		</h3>
		<div class="testimonials_slider">
			<?php if( !empty( $review ) ): ?>
				<?php foreach( $review as $item ): ?>
					<div>
						<div class="social_img tw"></div>
						<div class="text">“<?= $item[ 'review' ] ?>”</div>
						<div class="author">
							<?= $item[ 'username' ] ?>
							<?= $item[ 'last_name' ] ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif ?>
		</div>
	</div>
</div>


<div class="places_count_section">
	<div>
		<div class="bigger_block" style="background-image: url(/img/mainpage_img_1.jpg)">
			<div class="text"><?= $getCategory[ 2 ][ 'count_place_in_category' ] ?> Working places</div>
		</div>
		<div class="small_block sm_1" style="background-image: url(/img/mainpage_img_2.jpg)">
			<div class="text"><?= $getCategory[ 1 ][ 'count_place_in_category' ] ?> Conference room</div>
		</div>
		<div class="small_block sm_2" style="background-image: url(/img/mainpage_img_3.jpg)">
			<div class="text"><?= $getCategory[ 0 ][ 'count_place_in_category' ] ?> Privat Office</div>
		</div>
		<?php /*if($getCategory): ?>
			<?php foreach($getCategory as $category): ?>
			    <div class="" data-id="" style="background-image: url(/img/<?=$category['background_image']?>)">
				    <div class="text">
						<?=$category['count_place_in_category']?>
						<?=$category['category']?>
				    </div>
			    </div>
			<?php endforeach ?>
		<?php endif */ ?>
	</div>
</div>
<?php
	//	$setClass = <<<JS
	//	$(document).ready(function() {
	//		var boxs = $('.places_count_section div');
	//		boxs.children().first().addClass('bigger_block');
	//		boxs.children().addClass('small_block');
	//		boxs.children().first().removeClass('small_block');
	//		boxs.children().children().removeClass('small_block');
	//		$('.small_block').each(function() {
	//			for(i=1;i <= 2; i++) {
	//			    boxs.children().addClass('sm_' + i);
	//				boxs.children().first().removeClass('sm_' + i);
	//			}
	//		});
	//		$('.small_block').last().removeClass('sm_1');
	//		boxs.children().prev().removeClass('sm_2');
	//		boxs.children().children().removeClass('sm_2');
	//		boxs.children().children().removeClass('sm_1');
	//	});
	//JS;
	//	$this->registerJs($setClass);
	//?>

<div class="our_events">
	<div class="wrap">
		<h3><span>#</span> Check our places</h3>
		<a href="<?= Url ::toRoute( [ 'places/index' ] ) ?>" class="all_events">All Places</a>
		<div class="clearfix"></div>
		<?php if( !empty( $getLastPlaces ) ): ?>
			<?php foreach( $getLastPlaces as $place ): ?>
				<div class="event_wrap left" id="event_1">
					<div class="top_section" style="background: url('/source/img/background_image/thumb/<?= $place -> image ?>') center / cover no-repeat">
					</div>
					<div class="title"><?= $place -> title ?></div>
					<div class="address"><?= $place -> address ?></div>
					<div class="price">Unfixiable Place <?= $place -> unfixiable_price ?></div>
					<div class="price">Separate Offices <?= $place -> separate_price ?></div>
					<a href="<?= Url ::toRoute( [ 'places/view', 'id' => $place -> id ] ) ?>" class="btn blue_btn">Place Details</a>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<div class="clearfix"></div>
	</div>
</div>
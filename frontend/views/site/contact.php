<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

//use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;

$this->title = 'Contact';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="top_section">
    <div>
        <h1><span>#</span> Write to us</h1>
    </div>
</div>

<div class="wrap">
    <div class="contact_info left">
        <h2>Meet Us</h2>
        <p>Lorem ipsum dolor sit amet, id cum lobortis assueverit delicatissimi, cum reque inani in, ne his elitr ridens scripserit. Labores tacimates hendrerit eam et, dico debet docendi et mea, audire suscipit convenire ad vel. An nam habeo facete prodesset, an eos zril aliquid. Decore nullam graece mea an. Quo te munere laudem, sed unum nostro everti ne. Nam et eius partem semper, duo in aperiri adipiscing, aperiam intellegat ea usu.</p>
        <div class="divider"></div>
        <div class="contacts">
            <div class="contacts_title">Contacts</div>
            <div class="phone">+3 8073 546 33 22</div>
            <div class="email">spaceforrent@info.com</div>
            <div class="address">5 west 63rd St. Upper-west side, New York</div>
        </div>

        <div id="map" style="height: 400px;"></div>
        <script type="text/javascript">
            function initMap()
            {
                var uluru = { lat : 40.7709365, lng : -73.98054660000003 };
                var map = new google.maps.Map( document.getElementById( 'map' ), {
                    zoom   : 17,
                    center : uluru,
                } );
                var marker = new google.maps.Marker( {
                    position : uluru,
                    map      : map,
                } );
            }
        </script>
        <?php $this -> registerJsFile( 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBkktOCvkrrOJ7rEHR1hAduahvu7Imzxzg&callback=initMap' ); ?>
    </div>
    <div class="contact_form right">
        <div class="title">Write to us</div>

        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

        <?= $form->field($model, 'name',['template'=>
            "<div class=\"form-control\">\n
            {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
        </div>"])
            ->textInput()->label("Your name") ?>

        <?= $form->field($model, 'email',['template'=>
            "<div class=\"form-control\">\n
            {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
         </div>"])
            ->textInput()->label("Email") ?>
        <?= $form->field($model, 'phone',['template'=>
            "<div class=\"form-control\">\n
            {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
        </div>"])
            ->textInput()->label("Phone") ?>

        <?= $form->field($model, 'body',['template'=>
            "<div class=\"form-control\">\n
            {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
        </div>"])
            ->textarea(['rows' => 6])->label("Your Message") ?>

        <input type="checkbox" id="remember" class="custom_checkbox">
        <label for="remember">I agree with <a href="#" class="red_text"><strong>Terms and Conditions</strong></a></label>

        <?= Html::submitButton('SEND MESSAGE', ['class' => 'btn blue_btn', 'name' => 'contact-button']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>



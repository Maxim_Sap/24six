<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="log_in_page">
    <a href="/" class="logo"><img src="/img/logo.png" alt=""></a>
    <div class="page_divider"></div>
    <div class="form_wrap">
        <div class="wrap clearfix">
            <div class="form_title">Reset password form</div>
            <div class="form">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <div class="form-control">
                    <input type="text" name="PasswordResetRequestForm[email]" id="email" required>
                    <label for="email">Email</label>
                </div>

                <button class="btn blue_btn">Send</button>
                <span class="register_account">Don’t <a href="/signup" class="blue_text"><strong>register account</strong></a>?</span>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php /*?>
<div class="site-request-password-reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out your email. A link to reset password will be sent there.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php */?>
<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('css/slick/slick.css');
$this->registerCssFile('css/slick/slick-theme.css');
$this->registerJsFile('js/slick/jquery-migrate-1.2.1.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('js/slick/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="top_section">
    <div>
        <h1>About Us</h1>
    </div>
</div>
<div class="three_things">
    <div class="wrap">
        <div class="three_things_slider">
            <div class="slide_wrap">
                <div>
                    <div class="num">01</div>
                    <div class="title">Everyday motivation</div>
                    <div class="text">Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. Te decore omnium deserunt sed</div>
                </div>
            </div>
            <div class="slide_wrap">
                <div>
                    <div class="num">02</div>
                    <div class="title">Easy to make a good deeds</div>
                    <div class="text">Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. Te decore omnium deserunt sed</div>
                </div>
            </div>
            <div class="slide_wrap">
                <div>
                    <div class="num">03</div>
                    <div class="title">Worldwide family</div>
                    <div class="text">Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. Te decore omnium deserunt sed</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap">
    <div class="comunity_description">
        <div class="img_wrap right">
            <div>
                <img src="img/about_us.jpg" alt="">
            </div>
        </div>
        <div class="text_wrap right">
            <h2>We are community that have a mission to motivate people do a <strong><span class="blue">good deeds</span></strong> over the world</h2>
            <p>Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. Te decore omnium deserunt sed, ne erat alterum placerat has. Ea sea ullum appareat scripserit, ei errem omnium mea. Eligendi perfecto id nam. Mei virtute molestiae ei. An graecis laboramus quo, quem error menandri ex qui.</p>
            <p>Eu mei utroque vivendo, ut zril liberavisse has. Vim nullam partiendo torquatos te. Tota numquam definitiones ex eum, perpetua democritum at eum. S</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="start_good_deed">
    <div class="wrap">
        <div class="text_wrap left">
            <h3>Want to start do a good deeds and motivate other?</h3>
            <p>Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. alterum placerat has. </p>
        </div>
        <a href="<?=Url::toRoute(['site/contact'])?>" class="btn right white_bnt">Join the community</a>
        <div class="clearfix"></div>
    </div>
</div>

<div class="directors wrap">
    <h2><strong><span class="blue">Directors</span></strong> Board</h2>
    <div class="director_wrap">
        <div class="photo_wrap">
            <div>
                <img src="img/director_1.jpg" alt="">
            </div>
        </div>
        <div class="text_wrap">
            <div class="name">Mr. Bruce J. Schanzer</div>
            <div class="description">
                <p>Mr. Bruce J. Schanzer has been president, chief executive officer and a director of Cedar Realty Trust Inc (CDR) since June 2011. Prior thereto and since 2007, Mr. Schanzer was employed by Goldman Sachs & Co., with his last position being a managing director in their real estate investment banking group. From 2001 to 2007, Mr. Schanzer was employed by Merrill Lynch, with his last position being vice president in their real estate investment banking group. Earlier in his career, Mr. Schanzer practiced real estate law for six years in New York. Mr. Schanzer received a B.A. from Yeshiva College, where he is now a member of its board of trustees, an M.B.A. from the University of Chicago, and a J.D. from Benjamin N. Cardozo School of Law, where he was a member of the Law Review.</p>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="director_wrap">
        <div class="photo_wrap">
            <div>
                <img src="img/director_2.jpg" alt="">
            </div>
        </div>
        <div class="text_wrap">
            <div class="name">Yaron Pinchas</div>
            <div class="description">
                <p>Yaron Pinchas. Born and raised in Israel and received a Bachelor Of Arts in Economics from the University of Haifa, Israel. Yaron served in the IDF for three years. He is the founder and director of a Construction Company in Nigeria, a founder and managing director of a communication company in Nigeria, director and partner in agriculture companies in Central America and the U.S.A.</p>
            </div>
        </div>
    </div>
</div>

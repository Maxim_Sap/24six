<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="log_in_page">
    <a href="/" class="logo"><img src="/img/logo.png" alt="logo"></a>
    <div class="page_divider"></div>
    <div class="form_wrap">
        <div class="wrap clearfix">
            <div class="form_title">Log In</div>
            <div class="form">
                <?php
                $form = ActiveForm::begin([
                    'method' => 'post',
                    'class' => 'form-login',
                    'action' => '/login',
                    'options' => [
                        'autocomplete'=>"off"
                    ],
                ]);?>
                <?//= $form->field($model, 'email')->textInput(['autofocus' => true,'autocomplete'=>'off']) ?>
                <?= $form->field($model, 'email',['template'=>
                    "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                    ->textInput([
                            'autocomplete'=>'off'
                    ]) ?>
                <?= $form->field($model, 'password',['template'=>
                    "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                    ->passwordInput([
                        'autocomplete'=>'off'
                    ]) ?>
                <a href="<?=Url::toRoute(['/login/request-password-reset'])?>" class="right forgot_pass">Forgot password?</a>
                <input type="checkbox" name="LoginForm[remember]" id="remember" class="custom_checkbox"><label for="remember">Remeber me</label>
                <div class="clearfix"></div>
                <button href="#" class="btn blue_btn">log in</button>
                <span class="register_account">Don’t <a href="<?=Url::toRoute(['/signup'])?>" class="blue_text"><strong>register account</strong></a>?</span>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
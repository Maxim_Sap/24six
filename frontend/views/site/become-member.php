<?php
use common\models\UserProfile;
use yii\helpers\Url;
?>
<a href="/" class="logo"><img src="/img/logo.png" alt=""></a>
<div class="page_divider"></div>

<div class="become_member_wrap">
    <div class="wrap">
        <h3>
            Become a Member
        </h3>

        <h4>Memberships at 24~Six Las Olas include:</h4>

        <ul class="membership_includes">
            <li>Plenty of desks, chairs and lounge space in our chic and comfortable Common Area</li>
            <li>Private Conference / Meeting / Think-tank Room to reserve for up to 20 people</li>
            <li>Complimentary coffee, lattes and espressos at our gourmet Sip Station</li>
            <li>Abundance of natural light streaming through our large windows</li>
            <li>Mailbox rental with Las Olas Blvd. street address</li>
            <li>Safe, convenient parking in a covered garage 9am -5pm  (based on availability)</li>
            <li>Gorgeous, inspiring, scenic views</li>
            <li>High speed internet connection</li>
            <li>Fax and printer service</li>
            <li>Clean surroundings</li>
        </ul>

        <h4>Also included in your membership, our facility features:</h4>
        <ul class="facility_features">
            <li>Sprawling Outdoor Aqua Deck & Boat Dock
                <span>Overlooking the beautiful river, work or unwind in total peace while the fresh South Florida air gets your creative juices flowing!</span>
            </li>
            <li>Meditation Retreat Room
                <span>Whenever you need a break, enter our serene and cozy oasis for a relaxing chair massage while breathing in aromatherapy that helps calm your body/mind and invigorate your senses.</span>
            </li>
            <li>Exclusive Networking Events and Social Mixers
                <span>Check out our <a href="/events">calendar</a> for all the dates and details.</span>
            </li>
            <li>Easy access to everything and more!
            <span>Enjoy being only steps from the most exciting and attractive strip of restaurants, bars, clubs, art galleries, and shopping boutiques in the area!  Plus, the ocean is a mere moment away by foot, bicycle, car, skateboard, rickshaw, scooter, segway, or whatever’s clever!  We’re perfectly positioned to meet your needs for business and pleasure.</span></li>
        </ul>

		<?php if(!empty($plans)): ?>
			<?php foreach($plans as $plan): ?>
			    <div class="cost_tile">
				    <div class="title"><?= $plan[ 'plan' ] -> title ?></div>
					<?php if( $plan[ 'plan' ] -> id == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS): ?>
					    <div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / per day</div>
					<?php else: ?>
					    <div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / mounth</div>
					<?php endif; ?>
				    <div class="divider"></div>
				    <ul>
						<?php $formatter = Yii::$app->formatter; ?>
						<?php if(isset($plan['bonus']) && !empty($plan['bonus'])) : ?>
							<?php foreach($data = [$plan['bonus']] as $item) :?>
								<?php if( $item -> id == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS ): ?>
								    <li>includes workspace for one day</li>
								<?php else: ?>
								    <li>Workspace for <?= $item -> count_workspaces ?> day</li>
								<?php endif; ?>
								<?php if( !empty( $item -> count_time_for_conf_room ) ): ?>
								    <li><?= $item -> count_time_for_conf_room ?> hour private conference room</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_events_week ) ): ?>
								    <li><?= $item -> count_events_week ?> Events a week</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_daily_guest_passes ) ): ?>
								    <li><?= $item -> count_daily_guest_passes ?> Daily Guest Passes</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_time_for_massage ) ): ?>
								    <li><?= $item -> count_time_for_massage ?> 30 min massage sessions in our state of the art massage chairs</li>
								<?php endif ?>
								<?php if( !empty( $item -> priority_registration_for_all_event ) ): ?>
								    <li>Priority registration for all events</li>
								<?php endif ?>
								<?php if( !empty( $item -> complimentary_beverag ) ): ?>
								    <li>Complimentary beverages</li>
								<?php endif ?>
								<?php if( !empty( $item -> mailbox_with_las_olas ) ): ?>
								    <li>Mailbox with Las Olas Blvd Street Address</li>
								<?php endif ?>
								<?php if( !empty( $item -> fax_and_printer_service ) ): ?>
								    <li>Fax and printer service</li>
								<?php endif ?>
							<?php endforeach ?>
						<?php endif ?>
				    </ul>
				    <div class="divider bottom"></div>
				    <a href="<?= Url::to(['account/payments', 'type' => $plan[ 'plan' ] -> id]) ?>" class="btn blue_btn">Get Pass</a>
			    </div>
			<?php endforeach; ?>
		<?php endif ?>

        <div class="note">
            <h5>* Please Note:</h5>
            <p>Memberships are month-to-month and allow access to the 24~Six Las Olas location only</p>
            <p>24~Six is <u>closed for work and business related purposes</u> every Friday (beginning 1 hour before sunset) until Sunday morning.  *Members are allowed to come by and relax during that period of time.</p>
            <p>24~Six closes (for setup) 1 hour prior to all events on our <a href="/events">monthly schedule</a>.</p>
            <p>24~Six has the right to terminate any membership and refuse access at any time.</p>
            <p>Prices are subject to change at any time.</p>
        </div>

        <a href="/signup" class="skip">I wan’t to skip choosing membersip plan</a>

    </div>
</div>
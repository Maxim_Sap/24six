<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div id="log_in_page">
        <a href="/" class="logo"><img src="/img/logo.png" alt=""></a>
        <div class="page_divider"></div>
        <div class="form_wrap">
            <div class="wrap clearfix">
                <div class="form_title">Reset password form</div>
                <div class="form">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                    <div class="form-control">
                        <input type="text" name="ResetPasswordForm[password]" id="password" required>
                        <label for="email">New Password</label>
                    </div>

                    <button class="btn blue_btn">Save</button>
                    <span class="register_account">Don’t <a href="/signup"
                                                            class="blue_text"><strong>register account</strong></a>?</span>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php /*?>

<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please choose your new password:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php */ ?>
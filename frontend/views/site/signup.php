<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\User;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(
    "$('#datepicker input').datepicker({                    
        format: 'mm/dd/yyyy',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false 
    });",
    \yii\web\View::POS_READY
);
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css');                        
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

    <div id="sign_up_page">
        <a href="/" class="logo"><img src="/img/logo.png" alt=""></a>
        <div class="page_divider"></div>
        <div class="form_wrap">
            <div class="wrap clearfix">
                <?php $form = ActiveForm::begin([
                    'id' => 'signup-form',
                    'method' => 'post',
                    'class' => 'form-signup',
                    'action' => '/signup',
                ]);?>
                <div class="form_title">Become a Member</div>
                <div class="form">

                    <div>

                        <?= $form->field($model, 'username',['enableAjaxValidation'=>true,'template'=>
                            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->textInput([
                                'autocomplete'=>'off',
                                'required'=>'required',
                            ])
                            ->label('Your Name')?>
                        <?= $form->field($model, 'email',['enableAjaxValidation'=>true,'template'=>
                            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->textInput([
                                'autocomplete'=>'off',
                                'required'=>'required',
                            ])?>
                        <?= $form->field($UserProfile, 'phone',['template'=>
                            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->textInput([
                                'autocomplete'=>'off'
                            ])?>
                        <?= $form->field($model, 'password',['template'=>
                            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->passwordInput([
                                'autocomplete'=>'off',
                                'required'=>'required',
                            ])?>
                        <?= $form->field($model, 'conf_password',['template'=>
                            "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->passwordInput([
                                'autocomplete'=>'off',
                                'required'=>'required',
                            ])?>
                        <?= $form->field($UserProfile, 'birthday',['template'=>
                            "<div class=\"form-control\" id='datepicker' style='position:relative'>\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
                            ->textInput([
                                'autocomplete'=>'off'
                            ])?>

                        <div class="form-group form-control" style="margin-bottom: 0;">
                            <label class="control-label">Sex</label>
                            <div class="select " tabindex="1">
                                <input class="selectopt" name="test" type="radio" id="male" checked>
                                <label for="male" class="option">Male</label>
                                <input class="selectopt" name="test" type="radio" id="female">
                                <label for="female" class="option">Female</label>
                            </div>
                        </div>

                        <input type="checkbox" id="remember" class="custom_checkbox" required>
                        <label for="remember">I accept the <a href="#" class="red_text"><strong>Terms and Conditions</strong></a> of the website</label>
                        <div class="clearfix"></div>
                        <button class="btn blue_btn next_step">Create Account</button>
                        <span class="register_account">Already <a href="<?=Url::toRoute(['/login'])?>" class="blue_text"><strong>have an account</strong></a>?</span>
                    </div>


                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    
    
    
                          


    </div></div>

<?php /*?>
    <div id="sign_up_page">
    <a href="/" class="back right"><img src="/img/close.png" alt=""></a>
    <a href="/" class="logo left"><img src="/img/logo.png" alt=""></a>
    <a href="/" class="logo_2"><img src="/img/logo_2.png" alt=""></a>
    <div class="form_wrap">
        <div class="center_form">
            <div class="wrap clearfix">
                <div class="text_wrap">
                    <div>
                        <h2><span class="white">Welcome to the Biggest Charity Network in the World </span></h2>
                        <p>Lorem ipsum dolor sit amet, ea nam munere reprimique, consul iracundia dissentiet id his. Te decore omnium deserunt sed, ne erat alterum placerat has. Ea sea ullum appareat scripserit, ei errem omnium mea. Eligendi perfecto id nam. Mei virtute molestiae ei.</p>
                    </div>
                </div>
                <div class="form">
                    <?php $form = ActiveForm::begin([
                        'method' => 'post',
                        'class' => 'form-signup',
                        'action' => '/login/signup',
                    ]);?>
                    <div class="steps_line"></div>
                    <div class="steps">
                        <div class="current step_num" data-step="1">1</div>
                        <div class="step_num" data-step="2">2</div>
                    </div>

                    <div class="step step_1 active">
                        <div class="form_title">Profile Information</div>
                        <div class="form-control">
                            <select class="custom-select" placeholder="Choose profile type">
                                <option value="organization">Organization</option>
                                <option value="individual">Individual</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <input type="text" id="name" required>
                            <label for="name">Name</label>
                        </div>
                        <div class="form-control">
                            <input type="text" id="email" required>
                            <label for="email">Email</label>
                        </div>
                        <div class="form-control">
                            <input type="text" id="phone" required>
                            <label for="phone">Phone</label>
                        </div>
                        <div class="form-control">
                            <input type="text" id="date" class="datepicker" required>
                            <label for="date">Birthday</label>
                        </div>
                        <div class="form-control">
                            <input type="password" id="pass" required>
                            <label for="pass">Password</label>
                        </div>
                        <div class="form-control">
                            <input type="password" id="confirm_pass" required>
                            <label for="confirm_pass">Confirm password</label>
                        </div>
                        <div class="clearfix"></div>
                        <a href="#" class="btn blue_btn next_step" data-step="2">NEXT STEP</a>
                        <span class="register_account">Already <a href="#" class="blue_text"><strong>have an account</strong></a>?</span>
                    </div>

                    <div class="step step_2">
                        <div class="form_title">Receive Donation</div>
                        <div class="form-control">
                            <select class="custom-select" placeholder="Receive donation">
                                <option value="bank_account">Bank Account</option>
                                <option value="credit_card">Credit Card</option>
                            </select>
                        </div>

                        <div class="form-control">
                            <input type="text" id="routing_number" required>
                            <label for="routing_number">Routing Number</label>
                        </div>

                        <div class="form-control">
                            <input type="text" id="name_on_bank_account" required>
                            <label for="name_on_bank_account">Name on Bank Account</label>
                        </div>

                        <div class="form-control">
                            <input type="text" id="bank_name" required>
                            <label for="bank_name">Bank Name</label>
                        </div>

                        <div class="form-control">
                            <select class="custom-select" placeholder="Account Type">
                                <option value="acc1">Account Type 1</option>
                                <option value="acc2">Account Type 2</option>
                            </select>
                        </div>

                        <div class="secure_info">All your private information <a href="#" class="blue_text">100% Secure</a></div>

                        <div class="form_divider"></div>

                        <input type="checkbox" id="remember" class="custom_checkbox">
                        <label for="remember">I accept the <a href="#" class="red_text"><strong>Terms and Conditions</strong></a> of the website</label>

                        <div class="form_divider"></div>

                        <input type="checkbox" id="dont_need_don" class="custom_checkbox">
                        <label for="dont_need_don">I don't want to receive donations</label>

                        <div class="form_divider"></div>

                        <div class="clearfix"></div>
                        <a href="#" class="btn blue_btn">CREATE ACCOUNT</a>
                        <span class="register_account">Already <a href="/login" class="blue_text"><strong>have an account</strong></a>?</span>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php */ ?>
<?php /*?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'method' => 'post',
                'class' => 'form-signup',
                'action' => '/login/signup',
            ]);?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email', ['enableAjaxValidation'=>true]) ?>
            <?= $form->field($model, 'user_type')->dropDownList(
                    [
                        User::USER_DEFAULT => 'User',
                        User::USER_ORGANIZATION=> 'Organization',
                    ],
                [
                    'class' => 'placeholder uservacancyform-cat_id',
                    'prompt' => Yii::t('app', 'Choose from the list')
                ])->label('User Type') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php */?>
<?php
	$this -> registerJs(
		"$('#datepicker input').datepicker({                    
        format: 'yyyy-mm-dd',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false 
    }).on('changeDate',function(e) {
        $(this).parent('form').submit();
    });",
		\yii\web\View::POS_READY
	);
	$this -> registerCssFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css' );
	$this -> registerCssFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css' );
	$this -> registerJsFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );

	use yii\helpers\Url;
	use \yii\widgets\ListView;
	use yii\helpers\Html;

?>
<div class="top_section">
	<div>
		<h1><span>#</span> Book Spaces</h1>
	</div>
</div>

<div class="wrap">
	<div class="left_side left">
		<?= $this->render('_parts/_sidebar_places',['categoryToPlaces' => $categoryToPlaces])?>
	</div>

	<div class="events_list left">
		<div class="event_filter">
            <div class="form-control left" id='datepicker'>
                <form action="<?= Url::toRoute(['places/index']) ?>" id="sed-date" method="get" >
                    Select Dates <input type="text" id="get-date" name="get-date" autocomplete="off">
                </form>
            </div>
			<div class="tabs_filter right">
				<?= $this->render('_parts/tulbar_sort_places')?>
			</div>
			<div class="clearfix"></div>
		</div>
		<?= ListView ::widget(
			[
				'dataProvider' => $placesDataProvider,
				'itemView'     => '_parts/_spaces',
				'emptyText'    => 'Data not found',
			]
		);
		?>

		<div class="clearfix"></div>


		<div class="clearfix"></div>
	</div>


	<div class="clearfix"></div>

</div>

<?php

	$setFavorite = <<<JS

$(document).ready(function() {
        
        $('.favourite').click(function() {
            var favourite = $(this);
            var id_places = favourite.attr('data-id');
            $.ajax({
                   url:      '/places/set-favorite',
                   type:     "POST",
                   dataType: 'json',
                   data:     {id_places:id_places},
                   success: function(data) {
                           if(data.status == 'added') {
                               favourite.addClass('active')
                           }
                           if(data.status == 'remove') {
                               favourite.removeClass('active');
                           }
                   }
               }); 
        })
  
})
JS;

	$this->registerJs($setFavorite);

<?php
	use yii\i18n\Formatter;
	use yii\helpers\Html;
	use yii\helpers\Url;

	$formatter = Yii::$app->formatter;
?>
<div class="top_section" style="background-image: url('<?= '/source/img/background_image/thumb/'.$getSinglePlace->image?>')">
	<div>
		<h1><span>#</span> <?=$getSinglePlace->title?></h1>
	</div>
</div>

<div class="wrap">

	<div class="event_info left">
		<div class="title">
			<div class="date left">
				<?=$formatter->asDate($getSinglePlace->created_at,'php:d')?>
				<div class="mounth">
					<?=$formatter->asDate($getSinglePlace->created_at,'php:M')?>
				</div>
			</div>
			<?=$getSinglePlace->title?>
			<div class="address"><?=$getSinglePlace->address?></div>
		</div>
		<div class="additional_place_info">
			<ul>
				<li class="square"><?= $getSinglePlace->square_meter ?> m2</li>
				<li class="sits_count"><?= $getSinglePlace->working_places ?> working places</li>
				<li class="kitchens"><?= $getSinglePlace->kitchen ?> kitchen</li>
				<?php if( !empty( $getSinglePlace -> confRooms ) ): ?>
					<li class="conference_room"><?= count($getSinglePlace -> confRooms) ?> conference rooms</li>
				<?php endif ?>
			</ul>
		</div>
		<div class="description">
			<?=$getSinglePlace->text?>
		</div>
		<?php if(!empty($getCustomField)): ?>
			<div class="divider"></div>
			<div>
				<div class="info_title">What i can find inside?</div>
				<?php foreach($getCustomField as $item):?>
					<div>
						<p><?= $item->title_field ?></p>
						<p><?= $item->description_field ?></p>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif ?>
        <div class="divider"></div>
        <div class="description">
            <h2>How much is it cost</h2>
            <div class="price_wrap">
                <div class="title">Unfixiable Place <span>per person</span></div>
                <div class="price">$<?=$getSinglePlace->unfixiable_price?>/mo</div>
            </div>
            <div class="price_wrap">
                <div class="title">Separate offices <span>per person</span></div>
                <div class="price">$<?=$getSinglePlace->separate_price?>/mo</div>
            </div>
	        <div class="price_wrap">
		        <div class="title">One day <span>per person</span></div>
		        <div class="price">$<?=$getSinglePlace->one_day_price?>/day</div>
	        </div>
        </div>
		<div class="divider"></div>
		<div class="contacts">
			<div class="info_title">Contacts</div>
			<div class="phone"><?=  $getSinglePlace->phone ?></div>
			<div class="email"><?= $getSinglePlace->email?></div>
			<div class="address"><?= $getSinglePlace->address?></div>
		</div>

		<div id="map" style="height: 400px;"></div>
		<script type="text/javascript">
			function initMap()
			{
				var uluru = { lat : 26.1193228, lng : -80.1342668};
				var map = new google.maps.Map( document.getElementById( 'map' ), {
					zoom   : 12,
					center : uluru,
				} );
				var marker = new google.maps.Marker( {
														 position : uluru,
														 map      : map,
													 } );
			}
		</script>
		<?php $this -> registerJsFile( 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBkktOCvkrrOJ7rEHR1hAduahvu7Imzxzg&callback=initMap' ); ?>
	</div>
	<?= \frontend\components\PlacesFormWidget::widget(
			[
					'model' => $getSinglePlace,
			]
	) ?>

	<div class="clearfix"></div>
</div>


<div class="prev_next_pagination">
	<?php if(!empty($postPaginationSpacePrev)): ?>
		<a href="<?= Url::toRoute(['places/view?id='.$postPaginationSpacePrev->id]) ?>" class="post_pagination_section prev" style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/'.$postPaginationSpacePrev->image?>)">
			<p><span>Previews Event</span><?= $postPaginationSpacePrev->title ?></p>
		</a>
	<?php else: ?>
		<a href="<?= Url::toRoute(['places/view?id='.$getLastSpacePost->id]) ?>" class="post_pagination_section prev" style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/'.$getLastSpacePost->image?>)">
			<p><span>Previews Event</span><?= $getLastSpacePost->title ?></p>
		</a>
	<?php endif; ?>
	<?php if(!empty($postPaginationSpaceNext)): ?>
		<a href="<?= Url::toRoute(['places/view?id='.$postPaginationSpaceNext->id]) ?>" class=" post_pagination_section next" style="background-image: url(<?= '../source/img/background_image/origin/'.$postPaginationSpaceNext->image?>)">
			<p><span>Next Event</span><?= $postPaginationSpaceNext->title ?></p>
		</a>
	<?php else: ?>
		<a href="<?= Url::toRoute(['places/view?id='.$getFirstSpacePost->id]) ?>" class="post_pagination_section next" style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/'.$getFirstSpacePost->image?>)">
			<p><span>Next Event</span><?= $getFirstSpacePost->title ?></p>
		</a>
	<?php endif; ?>
</div>
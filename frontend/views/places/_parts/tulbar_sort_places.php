<?php

use  yii\bootstrap\Html;
use yii\helpers\Url;

$action_id = Yii::$app->controller->action->id;
$get_sort = Yii::$app->request->get( 'sort_place' );
?>

<a href="<?= Url::toRoute( ['places/index?sort_place=new-place'] ) ?>" class="<?= $get_sort == 'new-place' ? 'active' : '' ?> ">New first</a>
<?= Html::a( 'Popular first', Url::toRoute( ['places/index?sort_place=popular-places'] ), ['class' => ($get_sort == 'popular-places' ? 'active' : '')] ) ?>
<?php if (!Yii::$app->user->isGuest): ?>
    <?= Html::a( 'Favourites', Url::toRoute( ['places/index?sort_place=my-favorite'] ), ['class' => ($get_sort == 'my-favorite' ? 'active' : '')] ) ?>
<?php endif ?>

<?php
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\helpers\StringHelper;
$formatter = Yii::$app->formatter;
?>
    <div class="event_wrap left" id="places_<?= $model['id'] ?>" style="position: relative;">
        <div class="top_section" style="background: url('/source/img/background_image/thumb/<?= $model['image'] ?>') center / cover no-repeat">
            <?php if (!Yii::$app->user->isGuest): ?>
                <span class="favourite <?= !empty( $model['favorite_place'] ) ? 'active' : '' ?>"
                      data-id="<?= $model['id'] ?>"></span>
            <?php endif ?>
        </div>
        <div class="title"><?= StringHelper::truncate($model['title'] ,70,'...')?></div>
        <div class="description"><?= strip_tags ( StringHelper::truncate($model['description'] ,70,'...'))?></div>
        <div class="date">
            <?= $formatter->asRelativeTime($model['created_at']) ?>
        </div>
        <div class="address"><?= $model['address'] ?></div>
        <a href="<?= Url::toRoute( ['places/view','id'=>$model['id']] ) ?>" class="btn blue_btn">Places Details</a>
    </div>
<?php
//if(!Yii::$app->user->isGuest && Yii::$app->request->get('sort_place') == 'my-favorite') {
//    $deleteFavoriteEvent = <<<JS
//        $(document).ready(function() {
//            $('.favourite').click(function() {
//                var getEvent = $(this)
//                var idFavorite = getEvent.attr('data-id');
//                var eventId = $('#places_' + idFavorite);
//                eventId.remove();
//
//            });
//        })
//JS;
//    $this->registerJs($deleteFavoriteEvent);
//}
//
//?>

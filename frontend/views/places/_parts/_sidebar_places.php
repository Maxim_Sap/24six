<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="search_events">
    <form action="<?= Url::toRoute( ['places/index'] ) ?>" method="get">
        <input name="keysearch" value="" type="text" placeholder="Search the places">
    </form>
</div>
<div class="title">Places type</div>
<div class="category_list">
    <?php if (!empty( $categoryToPlaces )): ?>
        <?= Html::a( 'All Places', [Url::toRoute( ['places/index'] )] ) ?>
        <?php foreach ($categoryToPlaces as $places): ?>
            <div>
                <?= Html::a( $places->category, [Url::toRoute( ['places/index', 'cat_id' => $places->id] )], ['id' => 'changeCategory'] ) ?>
            </div>
        <?php endforeach ?>
    <?php endif ?>
</div>
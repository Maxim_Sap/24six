<?php
use yii\helpers\Url;
use common\models\User;
use yii\helpers\Html;

?>
<header>
    <?php $session = Yii::$app->session; $session->open();?>
    <a href="/" class="logo left"><img src="/img/logo_white.png" alt=""></a>
    <a href="#" class="burger_menu"></a>
    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="profile_menu_wrap right dropdown_menu_link" data-popup="profile_menu_popup">
            <span class="top_arrow"></span>
            <div class="profile_menu" data-animated-item="data-animated-item">
                <div class="name">
					<?= $session->get('username') ?>
					<?= $session->get('lastName') ?>
                </div>
            </div>
        </div>
    <?php }?>
    <div class="main_menu right">
        <ul>
            <li><a href="<?=Url::toRoute(['/'])?>" class="<?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Home</a></li>
            <li><a href="<?=Url::toRoute(['/events'])?>" class="<?=(Yii::$app->controller->id == 'events' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Events</a></li>
            <li><a href="<?=Url::toRoute(['/places'])?>" class="<?=(Yii::$app->controller->id == 'places' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Places</a></li>
            <li><a href="<?=Url::toRoute(['/faq'])?>" class="<?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'faq') ? 'active' : ''?>">FAQ</a></li>
            <li><a href="<?=Url::toRoute(['/contact'])?>" class="<?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'contact') ? 'active' : ''?>">Contacts Us</a></li>
            <?php if (Yii::$app->user->isGuest) { ?>
                <li><a href="<?=Url::toRoute(['/login'])?>" class="<?=(Yii::$app->controller->id == 'login' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Login</a></li>
                <li><a href="<?=Url::toRoute(['/signup'])?>" class="<?=(Yii::$app->controller->id == 'login' && Yii::$app->controller->action->id == 'signup') ? 'active' : ''?>">Sign Up</a></li>
                <style>.main_menu:after{display: none;}</style>
            <?php } ?>
        </ul>
    </div>

    <div class="mobile_menu">
        <div class="mobile_menu_top">
            <a href="/" class="back_to_site">BACK TO WEBSITE</a>
        </div>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <a href="<?=Url::toRoute('/account')?>" class="link active">Edit profile</a>
            <a href="<?=Url::toRoute('/account/my-membership')?>" class="link">My Membership</a>
            <a href="<?=Url::toRoute(['/account/my-booking'])?>" class="link">My Booking</a>
            <a href="<?=Url::toRoute(['/account/profile/order-payment'])?>" class="link">My order payment</a>
            <a href="<?=Url::toRoute(['/account/my-tickets'])?>" class="link">My Tickets</a>
            <a href="<?=Url::toRoute(['/account/favorites-events'])?>" class="link">Favorites Events</a>
            <a href="<?=Url::toRoute(['/account/favorites-places'])?>" class="link">Favorites Place</a>
            <a href="<?=Url::toRoute(['/account/profile/security'])?>" class="link">Security</a>
            <a href="<?=Url::toRoute(['/login/logout'])?>" class="link">Log Out</a>
            <br>
            <br>
            <a href="<?=Url::toRoute(['/'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Home</a>

            <a href="<?=Url::toRoute(['/events'])?>" class="link <?=(Yii::$app->controller->id == 'events' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Events</a>
            <a href="<?=Url::toRoute(['/places'])?>" class="link <?=(Yii::$app->controller->id == 'places' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Places</a>
            <a href="<?=Url::toRoute(['/faq'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'faq') ? 'active' : ''?>">FAQ</a>
            <a href="<?=Url::toRoute(['/contact'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'contact') ? 'active' : ''?>">Contacts Us</a>



        <?php }else{ ?>
            <a href="<?=Url::toRoute(['/'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Home</a>

            <a href="<?=Url::toRoute(['/events'])?>" class="link <?=(Yii::$app->controller->id == 'events' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Events</a>
            <a href="<?=Url::toRoute(['/places'])?>" class="link <?=(Yii::$app->controller->id == 'places' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>">Places</a>
            <a href="<?=Url::toRoute(['/faq'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'faq') ? 'active' : ''?>">FAQ</a>
            <a href="<?=Url::toRoute(['/contact'])?>" class="link <?=(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'contact') ? 'active' : ''?>">Contacts Us</a>

            <a href="<?=Url::toRoute(['/login'])?>" class="link <?=(Yii::$app->controller->id == 'login' && Yii::$app->controller->action->id == 'index') ? 'active' : ''?>" >Login</a>
            <a href="<?=Url::toRoute(['/signup'])?>" class="link <?=(Yii::$app->controller->id == 'login' && Yii::$app->controller->action->id == 'signup') ? 'active' : ''?>" >Sign Up</a>
        <?php } ?>
    </div>
    <div class="mobile_menu_overlay"></div>

    <div class="close_menu_btn"></div>
    <?php $session->close(); ?>
</header>
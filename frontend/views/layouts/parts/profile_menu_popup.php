<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\i18n\Formatter;

$session = Yii::$app->session;
$session->open();
?>
<?php $notification = $session->get( 'dataNotif' ); ?>

    <div class="dropdown_menu_container" id="profile_menu_popup">
        <span class="close_dropdown_menu"></span>
        <div class="dropdown_menu_title">
	        <?= $session->get('username') ?>
			<?= $session->get('lastName') ?>
        </div>
        <div class="links_wrap">
            <a href="<?= Url::toRoute( ['/account'] ) ?>"
               class="<?= (Yii::$app->controller->id == 'profile') ? 'active' : '' ?>">My Account</a>
            <a href="<?= Url::toRoute( ['/account/profile/edit-profile'] ) ?>"
               class="<?= (Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'edit') ? 'active' : '' ?>">Edit Profile</a>
            <a href="<?=Url::toRoute('/account/my-memberships')?>" class="link">My Membership</a>
            <a href="<?=Url::toRoute(['/account/my-booking'])?>" class="link">My Booking</a>
            <a href="<?=Url::toRoute(['/account/profile/order-payment'])?>" class="link">My order payment</a>
            <a href="<?=Url::toRoute(['/account/my-tickets'])?>" class="link">My Tickets</a>
            <a href="<?=Url::toRoute(['/account/favorites-events'])?>" class="link">Favorites Events</a>
            <a href="<?=Url::toRoute(['/account/favorites-places'])?>" class="link">Favorites Place</a>
            <a href="<?=Url::toRoute(['/account/profile/security'])?>" class="link">Security</a>



            <div class="divider"></div>
            <a href="<?= Url::toRoute( ['/logout'] ) ?>">Log Out</a>
        </div>
    </div>
<?php $session->close() ?>
<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<div class="form">
    <div class="form_title">Log In</div>
    <?php
    $model = new \frontend\models\LoginForm();
    $form = ActiveForm::begin([
        'method' => 'post',
        'class' => 'form-login',
        'action' => '/login',
        'options' => [
            'autocomplete'=>"off"
        ],
    ]);?>
    <?//= $form->field($model, 'email')->textInput(['autofocus' => true,'autocomplete'=>'off']) ?>
    <?= $form->field($model, 'email',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->textInput([
            'autocomplete'=>'off'
        ]) ?>
    <?= $form->field($model, 'password',['template'=>
        "<div class=\"form-control\">\n
                                {input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n
                            </div>"])
        ->passwordInput([
            'autocomplete'=>'off'
        ]) ?>
    <a href="<?=Url::toRoute(['/login/request-password-reset'])?>" class="right forgot_pass">Forgot password?</a>
    <input type="checkbox" name="LoginForm[remember]" id="remember" class="custom_checkbox"><label for="remember">Remeber me</label>
    <div class="clearfix"></div>
    <button href="#" class="btn blue_btn">log in</button>
    <span class="register_account">Don’t <a href="<?=Url::toRoute(['/signup'])?>" class="blue_text"><strong>register account</strong></a>?</span>
    <?php ActiveForm::end(); ?>
</div>
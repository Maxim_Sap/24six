<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<footer>
    <div class="bottom_line">© <?=date("Y",time())?> All Rights Reserved</div>
</footer>
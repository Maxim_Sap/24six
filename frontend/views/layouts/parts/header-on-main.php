<?php
use yii\helpers\Url;
use yii\helpers\Html;
$action_id = Yii::$app->controller->action->id;
?>
<div class="header">
    <a href="/" class="logo left"><img src="/img/logo_2.png" alt="Homepage"></a>
    <?php if (Yii::$app->user->isGuest) {
        echo Html::a('Become Member',Url::toRoute(['/become-member']),['class' => 'btn blue_btn right']);
    }else{
        echo Html::a('My Profile',Url::toRoute(['/account']),['class' => 'btn blue_btn right']);
    }?>
	<?= Html::a('Book Space',Url::toRoute(['site/index-places']),['class' => 'btn right '.($action_id == 'index-places' ? 'active':'')])?>
	<?= Html::a('Our Events',Url::toRoute(['site/index-events']),['class' => 'btn right '.(in_array($action_id,['index','index-events']) ? 'active' : '')])?>
</div>

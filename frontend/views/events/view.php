<?php

	use yii\i18n\Formatter;
	use yii\helpers\Html;
	use yii\helpers\Url;

	$formatter = Yii ::$app -> formatter;
?>

<div
		class="top_section"
		style="background-image: url(<?= 'https://' . $_SERVER[ 'HTTP_HOST' ] . '/source/img/background_image/thumb/' . $getSingleEvent -> image ?>)"
>
	<div>
		<h1><span>#</span> <?= $getSingleEvent -> title ?></h1>
	</div>
</div>

<div class="wrap">

	<div class="event_info left">
		<div class="title">
			<div class="date left">
				<?= $formatter -> asDate( $getSingleEvent -> date_event, 'php:d' ) ?>
				<div class="mounth">
					<?= $formatter -> asDate( $getSingleEvent -> date_event, 'php:M' ) ?>
				</div>
			</div>
			<?= $getSingleEvent -> title ?>
			<div class="address"><?= $getSingleEvent -> address ?></div>
		</div>
		<div class="description">
			<?= $getSingleEvent -> text ?>
		</div>
		<div class="divider"></div>
		<div class="contacts">
			<div class="info_title">Contacts</div>
			<div class="phone"><?= $getSingleEvent -> phone ?></div>
			<div class="email"><?= $getSingleEvent -> email ?></div>
			<div class="address"><?= $getSingleEvent -> address ?></div>
		</div>
		<div class="divider"></div>
		<?php if( !empty( $getPartnerEvent ) ): ?>
			<div>
				<div class="info_title">Our Partners</div>
				<?php foreach( $getPartnerEvent as $item ):
					$logo_dir = Yii::getAlias("@logo");?>
					<div class="left partner_wrap">
						<?php /* ?><div style="background-image: url(<?= 'https://'.$_SERVER['HTTP_HOST'].'/source/img/background_image/thumb/'.$item->logo?>)"> <?php */ ?>
						<a href="<?= $item[ 'link_partner' ] ?>" title="<?= $item[ 'title_partner' ] ?>" target="_blank">
							<img
									src="<?= $logo_dir . '/thumb/' . $item[ 'logo_partner' ] ?>"
									alt="<?= $item[ 'title_partner' ] ?> photo"
									title="<?= $item[ 'title_partner' ] ?>"
							>
						</a>
					</div>
				<?php endforeach; ?>
				<br><br>
			</div>
			<div class="divider"></div>
		<?php endif ?>
		<?php if( !empty( $getCustomField ) ): ?>
			<div class="things_inside">
				<div class="info_title">What i can find inside?</div>
				<?php foreach( $getCustomField as $item ): ?>
					<div>
						<div class="thing_title"><?= $item -> title_field ?></div>
						<div class="description"><?= $item -> description_field ?></div>
						<div class="clearfix"></div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="divider"></div>
		<?php endif ?>

		<?php if( !empty( $lecture_mass ) ):
			$avatar_dir = Yii::getAlias("@avatar");?>
			<div>
				<div class="info_title">Program Schedule</div>
				<div>
					<table class="schedule_table">
						<thead>
						<tr>
							<th style="text-align: center">Time</th>
							<th>About</th>
							<th>Speakers</th>
						</tr>
						</thead>
						<?php foreach( $lecture_mass as $lecture ) : ?>
							<tbody>
							<tr>
								<td class="time"><?= date( 'H:i', strtotime( $lecture[ 'time_from' ] ) ) . ' ' ?>
									<span>to</span><?= date( 'H:i', strtotime( $lecture[ 'time_to' ] ) ) ?>
								</td>
								<td class="theme"><?= $lecture[ 'subject' ] ?></td>
								<td>
									<?php foreach( $lecture[ 'speakers' ] as $speaker ) : ?>
										<div class="speaker">
											<div class="avatar" style="background-image: url('<?=$avatar_dir.'/icon/'.$speaker['avatar']?>')"></div>
											<div class="name"><?= $speaker[ 'name' ] ?></div>
											<div class="position"><?= $speaker[ 'position' ] ?></div>
										</div>
									<?php endforeach; ?>
								</td>
							</tr>
							</tbody>
						<?php endforeach; ?>
					</table>

				</div>
			</div>
			<div class="divider"></div>
		<?php endif ?>
		<div id="map" style="height: 400px;"></div>
		<script type="text/javascript">
			function initMap()
			{
				var uluru = { lat : 26.1193228, lng : -80.1342668};
				var map = new google.maps.Map( document.getElementById( 'map' ), {
					zoom   : 12,
					center : uluru,
				} );
				var marker = new google.maps.Marker( {
														 position : uluru,
														 map      : map,
													 } );
			}
		</script>
		<?php $this -> registerJsFile( 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBkktOCvkrrOJ7rEHR1hAduahvu7Imzxzg&callback=initMap' ); ?>
	</div>
	<?php
		if( empty( $countTheTickets[ 0 ][ 'saleTickets' ] ) )
		{
			$totalSpace                            = $getSingleEvent -> max_spaces;
			$countTheTickets[ 0 ][ 'saleTickets' ] = 0;
		}
		else
		{
			$totalSpace = $getSingleEvent -> max_spaces - $countTheTickets[ 0 ][ 'saleTickets' ];
		}
	?>
	<?php if( $getSingleEvent -> date_event < date( 'Y-m-d H:i:s' ) ): ?>
		<div class="buy_ticket_form right">
			<h3>Event is already over</h3>
		</div>
	<?php else: ?>
		<div class="buy_ticket_form right">
			<?php if( $getSingleEvent -> max_spaces == $countTheTickets[ 0 ][ 'saleTickets' ] ): ?>
				<h3 style="color: red">The tickets are sold out</h3>
				<div class="divider"></div>
			<?php else: ?>
				<div class="title">Buy a Ticket</div>
				<div class="form-group">
					<div class="form-control">
						<div class="custom_input_number">
							<input type="number" class="cin_input" step="1" value="0" min="0" max="<?= $totalSpace ?>">
							<div class="cin_btn cin_increment pos_abs">+</div>
							<div class="cin_btn cin_decrement pos_abs">-</div>
						</div>
						<label class="control-label" for="phone">How many tickets you need?</label>
					</div>
				</div>
				<div class="price_count">
					<?php if( !Yii ::$app -> user -> isGuest ): ?>
						<?php if( !empty( $getMyBonus ) ): ?>
							<?php if( $getMyBonus[ 0 ] -> count_events_week != 0 or $getMyBonus[ 0 ] -> count_events_week != NULL ): ?>
								<div class="label left">Your bonus on Free tickets</div>
								<div
										class="value right" id="free_tickets"
										data-id="<?= $getMyBonus[ 0 ] -> count_events_week ?>"
								>
									<?= $getMyBonus[ 0 ] -> count_events_week ?>
								</div>
								<input type="hidden" value="" id="additional_area">
								<div id="elseMainAreaPrice" data-id="0"></div>
								<div class="divider"></div>
							<?php else: ?>
								<div id="elseMainAreaPrice" data-id="1"></div>
							<?php endif ?>
						<?php endif ?>
					<?php endif ?>
					<div class="label left">Number of seats</div>
					<div class="value right" id="countSpaces" data-id="<?= $totalSpace ?>"><?= $totalSpace ?></div>
					<div class="divider"></div>
					<div class="label left">Number of tickets sold</div>
					<div class="value right" id="countSpaces"><?= $countTheTickets[ 0 ][ 'saleTickets' ] ?></div>
					<div class="divider"></div>
					<div class="label left">Price</div>
					<div class="value right" id="mainPrice" data-id="<?= $getSingleEvent -> price_ticket_event ?>">
						$<?= $getSingleEvent[ 'price_ticket_event' ] ?>
					</div>
					<div class="divider"></div>
					<div class="label left">Number of tickets</div>
					<div class="value right" id="countTickets">1</div>
					<div class="divider"></div>
					<div class="label left">Total</div>
					<div class="value right">$<span id="comboPrice">0</span></div>
					<div class="divider"></div>
				</div>
				<form  id="reset_form" action="<?= Url ::toRoute( [ 'account/payments/index-event' ] ) ?>" method="get">
					<input type='hidden' id='type_form' value='1'>
					<?php if( !Yii ::$app -> user -> isGuest ): ?>
						<?php if( !empty( $getMyBonus ) ): ?>
							<?php if( $getMyBonus[ 0 ] -> count_events_week != 0 or $getMyBonus[ 0 ] -> count_events_week != NULL ): ?>
								<input type="hidden" value="0" id="free_tickets_value" name="free_tickets_value">
							<?php endif ?>
						<?php endif ?>
					<?php endif ?>
					<input type="hidden" name="type" value="<?= \common\models\UserProfile::TYPE_PAYMENT_TICKET ?>">
					<input type="hidden" name="id_event" value="<?= $getSingleEvent -> id ?>">
					<input id="count_tickets_get_request" type="hidden" name="count_tickets" value="0">
					<input type="checkbox" id="remember" class="custom_checkbox" required>
					<label for="remember">I agree with <a href="#" class="red_text"><strong>Terms and
								Conditions</strong></a></label>
					<button class="btn blue_btn" id="link_payments">Buy Ticket</button>
				</form>
			<?php endif ?>
		</div>
	<?php endif ?>
	<div class="clearfix"></div>
</div>
<div class="prev_next_pagination">
	<?php if( !empty( $postPaginationEventPrev ) ): ?>
		<a
				href="<?= Url ::toRoute( [ 'events/view?id=' . $postPaginationEventPrev -> id ] ) ?>"
				class="post_pagination_section prev"
				style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/' . $postPaginationEventPrev -> image ?>)"
		>
			<p><span>Previews Event</span><?= $postPaginationEventPrev -> title ?></p>
		</a>
	<?php else: ?>
		<a
				href="<?= Url ::toRoute( [ 'events/view?id=' . $getLastPost -> id ] ) ?>" class="post_pagination_section prev"
				style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/' . $getLastPost -> image ?>)"
		>
			<p><span>Previews Event</span><?= $getLastPost -> title ?></p>
		</a>
	<?php endif; ?>
	<?php if( !empty( $postPaginationEventNext ) ): ?>
		<a
				href="<?= Url ::toRoute( [ 'events/view?id=' . $postPaginationEventNext -> id ] ) ?>"
				class=" post_pagination_section next"
				style="background-image: url(<?= '../source/img/background_image/origin/' . $postPaginationEventNext -> image ?>)"
		>
			<p><span>Next Event</span><?= $postPaginationEventNext -> title ?></p>
		</a>
	<?php else: ?>
		<a
				href="<?= Url ::toRoute( [ 'events/view?id=' . $getFirstPost -> id ] ) ?>" class="post_pagination_section next"
				style="width: 50%; display: inline-block; background-image: url(<?= '../source/img/background_image/origin/' . $getFirstPost -> image ?>)"
		>
			<p><span>Next Event</span><?= $getFirstPost -> title ?></p>
		</a>
	<?php endif; ?>

</div>
<?php

use  yii\bootstrap\Html;
use yii\helpers\Url;

$action_id = Yii::$app->controller->action->id;
$get_sort = Yii::$app->request->get( 'sort_event' );
?>

<a href="<?= Url::toRoute( ['events/index?sort_event=new-event'] ) ?>" class="<?= $get_sort == 'new-event' ? 'active' : '' ?> ">New first</a>
<?= Html::a( 'Upcoming first', Url::toRoute( ['events/index?sort_event=upcoming-events'] ), ['class' => ($get_sort == 'upcoming-events' ? 'active' : '')] ) ?>
<?= Html::a( 'Popular first', Url::toRoute( ['events/index?sort_event=popular-events'] ), ['class' => ($get_sort == 'popular-events' ? 'active' : '')] ) ?>
<?php if (!Yii::$app->user->isGuest): ?>
    <?= Html::a( 'Favourites', Url::toRoute( ['events/index?sort_event=my-favorite'] ), ['class' => ($get_sort == 'my-favorite' ? 'active' : '')] ) ?>
<?php endif ?>

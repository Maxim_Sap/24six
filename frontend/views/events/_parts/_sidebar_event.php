<?php

	use yii\helpers\Url;
	use yii\helpers\Html;

?>
<div class="search_events">
	<form action="<?= Url ::toRoute( [ 'events/index' ] ) ?>" method="get">
		<input name="keysearch" value="" type="text" placeholder="Search the event">
	</form>
</div>
<div class="title">Events type</div>
<div class="category_list">
	<?php if( !empty( $categoryTypeEvents ) ): ?>
		<?= Html ::a( 'All Events', [ Url ::toRoute( [ 'events/index' ] ) ] ) ?>
		<?php foreach( $categoryTypeEvents as $event ): ?>
			<div>
				<?= Html ::a( $event -> category, [ Url ::toRoute( [ 'events/index', 'cat_id' => $event -> id ] ) ], [ 'id' => 'changeCategory' ] ) ?>
			</div>
		<?php endforeach ?>
	<?php endif ?>
</div>
<!--<div class="title">Countries</div>-->
<!--<div class="category_list">-->
<!--	--><?php //if( !empty( $countriesEvent ) ): ?>
<!--		--><?//= Html ::a( 'All countries', [ Url ::toRoute( [ 'events/index' ] ) ] ) ?>
<!--		--><?php //foreach( $countriesEvent as $country ): ?>
<!--			<div>-->
<!--				--><?//= Html ::a( $country -> country, [ Url ::toRoute( [ 'events/index', 'count_id' => $country -> id ] ) ], [ 'id' => 'changeCategory' ] ) ?>
<!--			</div>-->
<!--		--><?php //endforeach ?>
<!--	--><?php //endif ?>
<!--</div>-->
<!--<div class="title">Price Range</div>-->
<!--<div class="category_list">-->
<!---->
<!--</div>-->
$(document).ready(function(){   
    
    $('.score_board_tpl .table tr').click(function(){
        if (window.screen.width <= '1200'){ 
            $('.score_board_left_side').addClass('opened');   
            $('.profile_body').animate({left: "270"}, 300, "linear");                            
            $('.transparent_overlay').fadeIn(300);
        }
        $('.score_board_tpl .table tr').removeClass('active');
        $(this).addClass('active');
        upload_data_to_sideber($(this));
    });
    
    $('.transparent_overlay').click(function(){
        $('.score_board_tpl .table tr').removeClass('active');
        $('.score_board_left_side').removeClass('opened');
        $('.profile_body').animate({left: "0"}, 300, "linear");
        $('.transparent_overlay').fadeOut(300);
    });
    
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

    $(".vert-tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    
    $(".more_details").click(function() {
        $(".detailed").animate({
            height: "toggle"
        });
    });
    
    $(function() {
        $(".switcher").on('click', function(){
            $(this).toggleClass( "switcher_on")
        });
    });
});

function upload_data_to_sideber(self){
    var avatar = self.find('.data-avatar img').attr('src');
    var full_name = self.find('.data-full-name').text();
    var org_name = self.find('.data-orgname').text();
    var country = self.find('.data-country').text();
    var city = self.find('.data-city').text();
    $('.score_board_left_side .avatar').html('<img src="'+avatar+'" alt="'+full_name+'">');
    $('.score_board_left_side .name').text(full_name);
    $('.score_board_left_side .organization_name').text(org_name);
    $('.score_board_left_side .address').text(country+', '+city);


}
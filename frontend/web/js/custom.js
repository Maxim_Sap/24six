

$(function(){
	var ink, d, x, y;
	$("[data-animated-item]").click(function(e){
    if($(this).find(".ink").length === 0){
        $(this).prepend("<span class='ink'></span>");
    }

    ink = $(this).find(".ink");
    ink.removeClass("animate");

    if(!ink.height() && !ink.width()){
        d = Math.max($(this).outerWidth(), $(this).outerHeight());
        ink.css({height: d, width: d});
    }

    x = e.pageX - $(this).offset().left - ink.width()/2;
    y = e.pageY - $(this).offset().top - ink.height()/2;

    ink.css({top: y+'px', left: x+'px'}).addClass("animate");

    ink.one("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
      $(e.currentTarget).remove();
    });

  });
});



/*menu dropdown*/

var dropdown_menu_id;
var current_dropdown_menu;
var dropdown_menu_left;
var clicked_btn;
$(document).ready(function(){
    $('.dropdown_menu_link').click(function(){
        clicked_btn = $(this);
        dropdown_menu_id = $(this).attr('data-popup');
        current_dropdown_menu = $('#' + dropdown_menu_id);

        dropdown_menu_left = $(this).offset().left - ( current_dropdown_menu.outerWidth()/2 ) + ( $(this).outerWidth()/2 );
        current_dropdown_menu.css({'left': dropdown_menu_left });

        current_dropdown_menu.fadeIn(100);
        $(this).find('.top_arrow').fadeIn(200);
        $('.transparent_overlay').fadeIn(300);

        if( ( $(window).outerWidth() - current_dropdown_menu.offset().left - 15) < current_dropdown_menu.outerWidth() ){
            current_dropdown_menu.css({'left' : 'auto' , 'right' : 5 });
        }
    });
    $('.transparent_overlay, .close_dropdown_menu').click(function(){
        if(current_dropdown_menu){
            current_dropdown_menu.fadeOut(100);
            $('.dropdown_menu_link .top_arrow, .transparent_overlay').fadeOut(100);
            $(this).addClass('dropdown_menu_opened').removeClass('dropdown_menu_closed');
        }

    });
});

$(window).resize(function(){
    if ( $('.dropdown_menu_container').is(":visible") ){
        dropdown_menu_left = clicked_btn.offset().left - ( current_dropdown_menu.outerWidth()/2 ) + ( clicked_btn.outerWidth()/2 );
        current_dropdown_menu.css({'left': dropdown_menu_left });

        if( ( $(window).outerWidth() - current_dropdown_menu.offset().left - 15) < current_dropdown_menu.outerWidth() ){
            current_dropdown_menu.css({'left' : 'auto' , 'right' : 5 });
        }
    }
});

/*END menu dropdown*/



$(document).ready(function(){
    $('header .burger_menu').click(function(){
        $('.mobile_menu_overlay, .close_menu_btn, .mobile_menu').addClass('active');
    });
    $('header .close_menu_btn, .back_to_site').click(function(){
        $('.mobile_menu_overlay, .close_menu_btn, .mobile_menu').removeClass('active');
    });
});




/*faq accordion*/
$(document).ready(function(){
    const items = document.querySelectorAll(".accordion a");

    function toggleAccordion(){
      this.classList.toggle('active');
      this.nextElementSibling.classList.toggle('active');
    }

    items.forEach(item => item.addEventListener('click', toggleAccordion));
});






/*custom select*/
$(document).ready(function(){
    $(".custom-select").each(function() {
        var classes = $(this).attr("class"),
            id      = $(this).attr("id"),
            name    = $(this).attr("name");
        var template =  '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function() {
            //template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
            template += '<span onclick="location.href = \'/donate/index?s=' + $(this).attr('value') + '\'" class="custom-option" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
        template += '</div></div>';

        $(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
    });
    $(".custom-option:first-of-type").hover(function() {
        $(this).parents(".custom-options").addClass("option-hover");
    }, function() {
        $(this).parents(".custom-options").removeClass("option-hover");
    });
    $(".custom-select-trigger").on("click", function() {
        $('html').one('click',function() {
            $(".custom-select").removeClass("opened");
        });
        $(this).parents(".custom-select").toggleClass("opened");
        event.stopPropagation();
    });
    $(".custom-option").on("click", function() {
        $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
        $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(".custom-select").removeClass("opened");
        $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
    });
});




/*steps*/
$(document).ready(function(){
    $('.next_step').click(function(){
        var next_step = $(this).attr('data-step');
        if(!signup_check_form(next_step)){
            return false;
        }
        $('.step').removeClass('active');
        $('.step.step_'+next_step).addClass('active');
        $('#sign_up_page .steps div').removeClass('current');
        $('#sign_up_page .steps div[data-step="'+next_step+'"]').addClass('current');
    });
    $('#sign_up_page .steps div').click(function(){
        var next_step = $(this).attr('data-step');
        if(!signup_check_form(next_step)){
            return false;
        }
        $('.step').removeClass('active');
        $('.step.step_'+next_step).addClass('active');
        $('#sign_up_page .steps div').removeClass('current');
        $('#sign_up_page .steps div[data-step="'+next_step+'"]').addClass('current');
    });

    //работа с селектом в форме регистрации
    $("#signup_form_user_type").on("change", function() {
        var curr_type = $(this).val();
        if(curr_type == 1){//individual
            $('#sign_up_page .steps, #sign_up_page .steps_line, #sign_up_page a.next_step').hide();
            $('#sign_up_page .step_1 button.submit').show();
            $('#userprofile-receive_donation_type,#userprofile-bank_account_type,#i_agree').removeAttr('required');
        }else if(curr_type == 2){
            $('#sign_up_page .steps, #sign_up_page .steps_line, #sign_up_page a.next_step').show();
            $('#sign_up_page .step_1 button.submit').hide();
            $('#userprofile-receive_donation_type,#userprofile-bank_account_type,#i_agree').attr('required','required');
        }
    });
});
function signup_check_form(next_step) {
    var signup_form_user_type = $('#signup_form_user_type').val();
    var name = $('#signupform-username').val();
    var email = $('#signupform-email').val();
    var pass = $('#signupform-password').val();
    var confirm_pass = $('#signupform-conf_password').val();
    if(next_step == '2'){
        if(signup_form_user_type == '' || name.trim() == '' || email.trim() == '' || pass.trim() == '' || confirm_pass.trim()== ''){
            $('#signup-form button').eq(0).click();
            return false;
        }
    }
    return true;
}

//custom input number
$( document ).on( 'click', '.custom_input_number .cin_increment', function( e )
{
	let $input = $( this ).siblings( '.cin_input' ),
		val    = parseInt( $input.val() ),
		max    = parseInt( $input.attr( 'max' ) ),
		step   = parseInt( $input.attr( 'step' ) );

	let temp = val + step;
	$input.val( temp <= max ? temp : max );

	countTickets = $( '#countTickets' );
	countTickets.text( $input.val() );

	comboPrice 	= $( '#comboPrice' );
	mainPrice 	= $( '#mainPrice' ).data( 'id' );

	free_tickets = $('#free_tickets');
	free_tickets_value = $('#free_tickets_value');

	additionl_area = $('#additional_area');
	chekFormWhoArea = $('#elseMainAreaPrice');

	link_id_event = $( '#count_tickets_get_request' );

	type_form = $('.wrap').find('#type_form');
	main_tax_price = $('.wrap').find('#main_tax_price');

	reset_form = $('#reset_form');
	if( free_tickets.length ) {
		if(countTickets.text() <= free_tickets.data('id')){
			reset_form.attr('action','/account/payments/update-my-bonuses-payment');
			reset_form.attr('method','post');
		} else {
			reset_form.attr('action','/account/payments/index-event');
			reset_form.attr('method','get');
		}
	}

	if(chekFormWhoArea.data('id') == 0 ) {

		if(parseInt($input.val()) <= free_tickets.data('id')) {
			freeNumber = free_tickets.data('id') - $input.val();
			free_tickets.text(freeNumber);
		} else {
			free_tickets.text(0);
			additioal_price = $input.val() - free_tickets.data('id');
			additionl_area.val(additioal_price);
			totalPrice 	= additionl_area.val() * mainPrice;
			comboPrice.text( totalPrice );

			link_id_event.val( additioal_price );
		}
	} else {
		if( type_form.val() == 2) {
			totalPrice 	= countTickets.text() * mainPrice;
			tax = totalPrice / 100;
			totalPriceWithtax = totalPrice + tax;
			comboPrice.text( totalPriceWithtax );
			main_tax_price.val(totalPriceWithtax);
			link_id_event.val( $input.val() );

		} else {
			totalPrice 	= countTickets.text() * mainPrice;
			comboPrice.text( totalPrice );
			link_id_event.val( $input.val() );
		}
	}

	countSpaces = $('#countSpaces').data('id');
	if($input.val() >= countSpaces) {
		$('.cin_increment').css('opacity',0.2);
	}
	parentInput = $('input');
	if(free_tickets.data('id') != 0) {
		if(parseInt($input.val()) <= free_tickets.data('id')) {
			free_tickets_value.val($input.val());
			comboPrice.text(0);
		}
	}
} );

$( document ).on( 'click', '.custom_input_number .cin_decrement', function( e )
{
	let $input = $( this ).siblings( '.cin_input' ),
		val    = parseInt( $input.val() ),
		min    = parseInt( $input.attr( 'min' ) ),
		step   = parseInt( $input.attr( 'step' ) );

	let temp = val - step;
	$input.val( temp >= min ? temp : min );

	countTickets = $( '#countTickets' );
	countTickets.text( $input.val() );

	comboPrice 	= $( '#comboPrice' );
	mainPrice 	= $( '#mainPrice' ).data( 'id' );
	free_tickets = $('#free_tickets');
	free_tickets_value = $('#free_tickets_value');

	additionl_area = $('#additional_area');
	chekFormWhoArea = $('#elseMainAreaPrice');
	type_form = $('.wrap').find('#type_form');

	link_id_event = $( '#count_tickets_get_request' );


	reset_form = $('#reset_form');
	if( free_tickets.length ) {
		if(countTickets.text() <= free_tickets.data('id')){
			reset_form.attr('action','/account/payments/update-my-bonuses-payment');
			reset_form.attr('method','post');
		} else {
			reset_form.attr('action','/account/payments/index-event');
			reset_form.attr('method','get');
		}
	}
	if(chekFormWhoArea.data('id') == 0 ) {
		if(parseInt($input.val()) <= free_tickets.data('id')) {
			freeNumber = free_tickets.data('id') - $input.val();
			free_tickets.text(freeNumber);
			link_id_event.val(0);
		} else {
			free_tickets.text(0);
			additioal_price = $input.val() - free_tickets.data('id');
			additionl_area.val(additioal_price);
			totalPrice 	= additionl_area.val() * mainPrice;
			comboPrice.text( totalPrice );
			link_id_event.val( additioal_price );
		}
	} else {
		if( type_form.val() == 2) {
			totalPrice 	= countTickets.text() * mainPrice;
			tax = totalPrice / 100;
			totalPriceWithtax = totalPrice + tax;
			comboPrice.text( totalPriceWithtax );
			main_tax_price.val(totalPriceWithtax);
			link_id_event.val( $input.val() );

		} else {
			totalPrice 	= countTickets.text() * mainPrice;
			comboPrice.text( totalPrice );
			link_id_event.val( $input.val() );
		}
	}

	countSpaces = $('#countSpaces').data('id');
	if(parseInt($input.val()) <= countSpaces) {
		$('.cin_increment').css('opacity',1);
	}

	additional_combo_price = $('#additional_combo_price');
	if(free_tickets.data('id') != 0) {
		if(parseInt($input.val()) <= free_tickets.data('id')) {
			free_tickets_value.val($input.val());
			comboPrice.text(0);
		}
	}

} );
$(document).ready(function(  ) {
	type_form = $('.wrap').find('#type_form');
	input = $( '.cin_input' );
	input.change(function() {
		inputVal = $( this );
		countTickets = $( '#countTickets' );
		countTickets.text( inputVal.val() );
		comboPrice = $( '#comboPrice' );
		mainPrice = $( '#mainPrice' ).data( 'id' );
		free_tickets = $('#free_tickets');
		free_tickets_value = $('#free_tickets_value');

		additionl_area = $('#additional_area');
		chekFormWhoArea = $('#elseMainAreaPrice');

		link_id_event = $( '#count_tickets_get_request' );
		reset_form = $('#reset_form');
		if( free_tickets.length ) {
			if(countTickets.text() <= free_tickets.data('id')){
				reset_form.attr('action','/account/payments/update-my-bonuses-payment');
				reset_form.attr('method','post');
			} else {
				reset_form.attr('action','/account/payments/index-event');
				reset_form.attr('method','get');
			}
		}
		if(chekFormWhoArea.data('id') == 0 ) {
			if(parseInt(input.val()) <= free_tickets.data('id')) {
				freeNumber = free_tickets.data('id') - input.val();
				free_tickets.text(freeNumber);
				link_id_event.val(0);
			} else {
				free_tickets.text(0);
				additioal_price = input.val() - free_tickets.data('id');
				additionl_area.val(free_tickets.data('id'));
				totalPrice 	= additionl_area.val() * mainPrice;
				comboPrice.text( totalPrice );
				link_id_event.val( additioal_price );
				free_tickets_value.val(additioal_price);
			}
		} else {
			if( type_form.val() == 2) {
				totalPrice 	= countTickets.text() * mainPrice;
				tax = totalPrice / 100;
				totalPriceWithtax = totalPrice + tax;
				comboPrice.text( totalPriceWithtax );
				main_tax_price.val(totalPriceWithtax);
				link_id_event.val( input.val() );

			} else {
				totalPrice 	= countTickets.text() * mainPrice;
				comboPrice.text( totalPrice );
				link_id_event.val( input.val() );
				free_tickets_value.val( input.val() );
			}
		}

		if(inputVal.val() == input.attr('min')) {
			min_value = input.attr('min');
			free_tickets_value.val(min_value);
			link_id_event.val(0);
			comboPrice.text(min_value);
			countTickets.text(min_value);
		}
		if(parseInt(inputVal.val()) >= input.attr('max') ) {
			$('.cin_increment').css('opacity',0.2);
			input.val(input.attr('max'));
			totalCountFreeTicket = input.attr('max') - free_tickets.data('id');
			link_id_event.val( totalCountFreeTicket );
			countTickets.text(input.attr('max'));
			free_tickets_value.val( free_tickets.data('id'));

			mainTotalPrice = mainPrice * input.attr('max');
			comboPrice.text( mainTotalPrice );
			// alert('You have exceeded the limit of the number of tickets');
		}

		countSpaces = $('#countSpaces').data('id');
		if(countTickets.text() >= countSpaces) {
			$('.cin_increment').css('opacity',0.2);
		} else {
			$('.cin_increment').css('opacity',1);
		}
		if( comboPrice.text() == 0 ) {
			input.val(1);
			comboPrice.text(mainPrice);
			countTickets.text(1);
			link_id_event.val(1);
		}
	} );
});

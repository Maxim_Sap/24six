var CropImageLibrary = (function () {
    'use strict';
    var image_place = false;
    var preview = function(image, selection) {
        if (!selection.width || !selection.height)
            return;
        $(image_place).find('.x1').val(selection.x1);
        $(image_place).find('.y1').val(selection.y1);
        $(image_place).find('.x2').val(selection.x2);
        $(image_place).find('.y2').val(selection.y2);
        $(image_place).find('.width').val(selection.width);
        $(image_place).find('.height').val(selection.height);
        //$('#cropPhoto').hide();
        $(image_place).find('a.crop-apply').show();
        var originalSrc = $(image_place).find('.img_db').attr('data-original-src');
        $(image_place).find('.crop-img').attr({
            //src:originalSrc
        });
    };

    var cropPhoto = function(place) {
        image_place = place;
        //var originalSrc = $(image_place).find('.img_db').attr('data-original-src');
        $('.crop-img-source').imgAreaSelect({remove:true});
        $('.widget_image .crop-img-source').hide();
        $('.widget_image .crop-apply').hide();
        $('.widget_image .close-btn').hide();
        $(image_place).find('.crop-img-source').show();
        $(image_place).find('.crop-apply').show();
        $(image_place).find('.close-btn').show();
        $('.crop_overlay').show();
        //$(image_place).find('.img_db').after("<img class='crop-img' src='"+originalSrc+"' style='position: absolute;top: 0;left: 354px;max-width: 600px;' />");
        /*$(image_place).find('.img_db').attr({
            style:'background-image: url('+originalSrc+');'
        });*/
        var crop_elem = $(image_place).find('.crop-img-source');
        //default like avatar type
        var aspectRatio_val = '1:1';
        var x2_val = 120;
        var y2_val = 120;
        if(image_place.id == 'background_image_box'){
            aspectRatio_val = '3.23:1';
            x2_val = 326;
            y2_val = 100;
        }
        crop_elem.imgAreaSelect({
            aspectRatio: aspectRatio_val,
            x1:0,
            y1:0,
            x2:x2_val,
            y2:y2_val,
            minWidth: x2_val,
            minHeight: y2_val,
            handles: true,
            fadeSpeed: 200,
            onSelectChange: preview,
        });
        $(image_place).find('a.crop-apply').show();
    };

    var applyCrop = function(place) {
        var photoName = $(place).find('a.crop-image').attr('data-image-name');
        var profile_id = $('#userprofile-id').val();
        var img_type = $(place).attr('data-type');
        var x1 = $(place).find('.x1').val();
        var y1 = $(place).find('.y1').val();
        var x2 = $(place).find('.x2').val();
        var y2 = $(place).find('.y2').val();
        var imageCoordinates = [x1, y1, x2, y2];
        var imageSize = [$(place).find('.crop-img-block img').width(), $(place).find('.crop-img-block img').height()];
        $(place).find('a.crop-apply').css('color','#55f3be');
        $.ajax({
            //url: '//photo/crop-photo',
            url: '/upload/crop-photo',
            type: "POST",
            dataType: "json",
            data: {
                'photoName': photoName,
                'imageCoordinates': imageCoordinates,
                'imageSize': imageSize,
                'type':img_type,
                'profile_id':profile_id
            },
            success: function (response) {
                $('.crop_overlay').hide();

                if (response['success'] == true) {
                    if(response['action'] == 'reload'){
                        location.reload(true);
                    }
                    $(image_place+'_preview').css({'background-image': 'url("'+data.thumb_path + '")'});
                    $(image_place).find('.photo_field_name').val(response['new_img']);

                } else {
                    //$('#modalMessage').modal('show');
                    //$('#modalMessage .modal-body span').text(response['message']);
                    alert(response['message']);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }
    var closeCrop = function(place) {
        $(image_place).find('.crop-img-source').hide();
        $(image_place).find('.crop-apply').hide();
        $(image_place).find('.close-btn').hide();
        $(image_place).find('.crop-img-source').imgAreaSelect({remove:true});
        $('.crop_overlay').hide();
    }

    /*$(document).on('click','.imgareaselect-outer',function(){
        $('a.crop-apply').hide();
    });*/

    return {
        applyCrop: applyCrop,
        cropPhoto: cropPhoto,
        preview: preview,
        closeCrop: closeCrop
    }

})(jQuery);
<?php

	namespace frontend\components;

	use common\models\BookingConferenceRoom;
	use common\models\ResourcesEventsPlaces;
	use common\models\SpaceWork;
	use common\models\UserMainPersonalBonus;
	use common\models\UserProfile;
	use yii\base\Widget;
	use Yii;

	class PlacesFormWidget extends Widget
	{

		public $model;
		public $countTheTickets;
		public $form_id;

		public function init()
		{
		}

		public function run()
		{
			$model_id              = $this -> model -> id;
			$my_id                 = [];
			$myBonusConferenceRoom = [];
			try
			{
				if( !Yii ::$app -> user -> isGuest )
				{
					$my_id                 = (int)Yii ::$app -> user -> identity -> getId();
					$myBonusConferenceRoom = UserMainPersonalBonus ::checkMyBonus( $my_id );
				}
			}
			catch( \Exception $e )
			{
				$myBonusConferenceRoom = NULL;
				$my_id                 = NULL;
			}

			return $this -> render(
				'places-form-widget',
				[
					'selectType' => $this -> selectType(),
					'myBonus'    => $myBonusConferenceRoom,
					'model_id'   => $model_id,
				]
			);
		}

		private function selectType()
		{
			$array = [];
			if( $this -> model -> unfixiable_price )
			{
				$array[ UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ] = 'Unfixiable Place';
			}
			if( $this -> model -> separate_price )
			{
				$array[ UserProfile::TYPE_PLACE_SEPARATE_SPACE ] = 'Separate Offices';
			}
			if( $this -> model -> one_day_price )
			{
				$array[ UserProfile::TYPE_PLACE_ONE_DAY ] = 'One Day';
			}

			if( $rooms = $this -> model -> confRooms )
			{
				foreach( $rooms as $room )
				{
					$array[ $room -> id ] = 'Conference Room (' . $room -> price_for_hour . '/hour)';
				}
			}

			return $array;
		}
	}
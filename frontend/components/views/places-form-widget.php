<?php

	use yii\helpers\Html;
	use yii\helpers\Url;

	/** @var $this \yii\web\View */

	$this -> registerCssFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css' );
	$this -> registerCssFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css' );
	$this -> registerJsFile( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );

	$this -> registerCssFile( '/js/tools/select/multiple-select.css' );
	$this -> registerJsFile( '/js/tools/select/multiple-select.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/js/tools/moment/moment.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );

?>

<style>
	.buy_ticket_form .title-space {
		border-bottom  : 1px solid #dbdbdb;
		font           : 600 22px/40px "Open Sans";
		padding-bottom : 10px;
		color          : #262435;
	}

	.buy_ticket_form .control-label {
		margin : 25px 0 10px;
	}

	.buy_ticket_form .price_count {
		margin : 25px 0;
	}

	.buy_ticket_form .form-control {
		margin : 0;
	}

	.sidebar-button {
		display : none;
	}

	.text-center {
		text-align : center;
	}
	#selected-block .time_from option[disabled],
	#selected-block .time_to option[disabled]{
		color : #e6e6e6;
	}
	#selected-block #errors {
		text-align: center;
		padding: 15px 15px 0;
		font-size: 15px;
		color: #ff8686;
	}
</style>
<div class="buy_ticket_form right">
	<div class="title-space">Book this space</div>

	<form id="reset_form" action="<?= Url ::toRoute( [ '/account/payments/index-place' ] ) ?>" method="get">
		<div class="form-group">
			<div class="form-control">
				<div class="custom_input_number">
					<div class="control-label">Type of space</div>
					<?= Html ::dropDownList( 'type_place', NULL, $selectType, [ 'prompt' => 'Select Type', 'id' => 'select-type' ] ) ?>
				</div>

				<?php ################# Dynamic block #################  ?>
				<div id="selected-block"></div>
				<?php ################# ./Dynamic block #################  ?>

			</div>
		</div>

		<div class="sidebar-button">
			<?php if( !Yii ::$app -> user -> isGuest ): ?>
				<?php if( !empty( $myBonus ) ): ?>
					<?php if( $myBonus[ 0 ] -> count_time_for_conf_room != 0 or $myBonus[ 0 ] -> count_time_for_conf_room != NULL ): ?>
						<input type="hidden" value="0" id="free_tickets_value" name="free_tickets_value">
					<?php endif ?>
				<?php endif ?>
			<?php endif ?>
			<input type="hidden" name="id_place" value="<?= $model_id ?>">
			<input id="count_tickets_get_request" type="hidden" name="count_tickets" value="0">
			<input type="checkbox" id="remember" class="custom_checkbox" required>
			<label for="remember">I agree with <a href="#" class="red_text"><strong>Terms and Conditions</strong></a></label>
			<button href="#" class="btn blue_btn">Book Now</button>

		</div>
	</form>
</div>

<?php
	$changeForm = <<<JS
	
	$('#select-type').change(function() {
	  var form = $(this).val(),model = {$model_id};
	  if(!form){
			$('#selected-block').html('');
	  	  	$('.sidebar-button').hide();
	  	  	return false
	  }
	  
	  $.ajax({
	  	type:'post',
	  	url:'/places/set-form',
	  	data:{form:form,model:model},
	  	success: function(data) {
	  	  if(data){
	  	  		$('#selected-block').html(data);
	  	  		$('.sidebar-button').show();
	  	  }else{
	  	  		$('#selected-block').html('');
	  	  		$('.sidebar-button').hide();
	  	  }
	  	}
	  });
	});	
JS;

	$room_hour = <<<JS
// Select date

$('#selected-block').on('focus',"#select-day", function(){
    $(this).datepicker({                    
        format: 'yyyy-mm-dd',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false,
        startDate: new Date() 
    }).on('changeDate',function(e) {
    	$('.wrap').find('#box-count').css('display','block');
    	checkDate = $(this).val();
    	typePlace = $('#type_place').val();
        totalCount = $('.wrap').find('#countSpaces');
        maxSpace = $('.wrap').find('.cin_input');
        $.ajax({
		    type	:'post',
		    url		:'check-date',
		    dataType:'json',
		    data	:{
		                checkDate:checkDate,
		                typePlace:typePlace,
		            },
		    success: function( data ) {
		        result = totalCount.data('id') - data.countSpace;
		        totalCount.attr('data-id',result)
		        totalCount.text(result);
		    } 
        });
        totalCount.text(maxSpace.attr('max'))	
    });
});
$(document).ready(function(  ) {
	$('.wrap').on('change',".time_to, .time_from", function()
	{
		main_parent = $('.wrap');

		time_from = $('.time_from');
		time_to = $('.time_to');

		hour_duration = main_parent.find('#hour_duration');
		hour_price = main_parent.find('#hour_price');
		
		hour_total = main_parent.find('#hour_total');
		
		additionl_area = $('#additional_area');
		chekFormWhoArea = $('#elseMainAreaPrice');
		
		free_tickets = $('#free_tickets');
		free_tickets_value = $('#free_tickets_value');
		
		different = time_to.val() - time_from.val();
		
		link_id_event = $( '#count_tickets_get_request' );
		
		reset_form = $('#reset_form');
		if( free_tickets.length ) {
			if(different <= free_tickets.data('id')){
				reset_form.attr('action','/account/payments/update-bonuses-free-hours');
				reset_form.attr('method','post');
			} else {
				reset_form.attr('action','/account/payments/index-place');
				reset_form.attr('method','get');
			}
		}
		
		if( hour_duration.text == 0 ) {
			e.preventDefault();
		}
		if( different <= 0  ) {
			time_to.val(time_from.val());
			free_tickets.text(free_tickets.data('id'));
			hour_duration.text(0 + ' hour');
			hour_total.text(0);
		} else {
			if(chekFormWhoArea.data('id') == 0 ) {
				if(different > free_tickets.data('id'))  {
					duration_time_free = time_to.val() - time_from.val() - free_tickets.data('id');
					duration_time = time_to.val() - time_from.val();
					hour_duration.text( duration_time + ' hour' );
					
					free_tickets.text(0);
					free_tickets_value.val(free_tickets.data('id'));
					additionl_area.val(free_tickets.data('id'));
					
					link_id_event.val(duration_time_free);
					
					totalPriceForHour = hour_price.data( 'id' ) * duration_time_free;
					hour_total.text('$' + totalPriceForHour + '.00');
				} else {
					link_id_event.val(0);
					duration_time = time_to.val() - time_from.val();
					if( duration_time <= free_tickets.data('id')  ) {
						free_tickets_value.val(duration_time);
						hour_total.text(0);
					}
					
					additionl_area.val(different);
					freeEndHour = free_tickets.data('id') - additionl_area.val();
					free_tickets.text(freeEndHour);
					hour_duration.text(different + ' hour');
					hour_total.text('0.00')
				}
			} else {
				duration_time = time_to.val() - time_from.val();
				hour_duration.text( duration_time + ' hour' );
	
				totalPriceForHour = hour_price.data( 'id' ) * duration_time;
				hour_total.text('$' + totalPriceForHour + '.00');
				
				link_id_event.val(duration_time);
			}	
		}
		

	});
	$('.wrap').on('change',".time_to", function() {
		date = $('.wrap').find('#select-conference-room').val();
		timeStartConf = $('.wrap').find('.time_from').val();
		timeEndConf = $('.wrap').find('.time_to').val();
		conferenceRoom_id = $('#select-type option:selected').val();
		$.ajax({
			    type	:'post',	
			    url		:'check-order-time-conference-room',
			    dataType:'json',
			    data	:{
			    			conferenceRoom_id:conferenceRoom_id,
			    			timeStartConf:timeStartConf,
			    			timeEndConf:timeEndConf,
			    			date:date,
			    		},
			    success: function(data) {
			    	errors = $('#errors')
			    	if( data.errors == '') {
			    		errors.text('');
			    		$('.wrap').find('#disableForm').remove();
			    		$('.blue_btn').click(function(e) {
			    			$('#reset_form').submit();
			    		}); 
			    			
			    	} else {
			    		errors.text('Time ordered ' + data.errors);
			    		errors.after('<div id="disableForm" data-id="1"></div>');
						disableForm = $('.wrap').find('#disableForm');
			    		if(disableForm.data('id') == 1) {
							$('.blue_btn').click(function(e) {
								e.preventDefault();
							})
							free_tickets.text(free_tickets.data('id'));
							hour_duration.text(0 + ' hour');
							hour_total.text(0);
						}
			    	}
			    	
			    } 
    		});
	});
});
$('#selected-block').on('focus',"#select-conference-room", function(){
	
    $(this).datepicker({                    
        format: 'yyyy-mm-dd',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false,
        startDate: new Date() 
    }).on('changeDate',function(e) {
    	
    	checkDate = $(this).val();
    	typePlace = $('#type_place').val();
    	conferenceRoom_id = $('#select-type option:selected').val();
    		$.ajax({
			    type	:'post',	
			    url		:'check-conference-room-time',
			    dataType:'json',
			    data	:{
			    			conferenceRoom_id:conferenceRoom_id,
			    			checkDate:checkDate,
			    			typePlace:typePlace,
			    		},
			    success: function(data) {
			    	$('#show-time').css('display','block');
			      	$('#time_set_one').html(data.html);
			      	$('#time_set_two').html(data.html);
			    } 
    		});
    });
});

JS;

	$place = <<<JS

	$('#selected-block').on('focus',"#select-place", function(){
		selectPlace = $('#select-place');
		selectPlaceDay = $('#select-place-day');
		
		$(this).datepicker({                    
			format: 'yyyy-mm-dd',
			orientation: 'bottom left',
			inline: false,
			sideBySide: false,
			showWeekDays: false,
			startDate: new Date() 
    	}).on('changeDate', function(e){
    		
    		checkDate = $(this).val();
    		typePlace = $('#type_place').val();
    		$.ajax({
			    type	:'post',
			    url		:'check-date',
			    dataType:'json',
			    data	:{
			    			checkDate:checkDate,
			    			typePlace:typePlace,
			    		},
			    success: function() {
			      
			    } 
    		});
    		$(this).datepicker('hide');
    		$(this).unbind('changeDate');
		}).on('hide',function(e) {
		   var val = $(this).val();
		   if(val.length > 10){
		   		return false;
		   }
    		selectPlace.val(val);
      		var days30 = moment(val).add(30, 'days'); 
    		var str = moment(val).format('DD.MM.YYYY') + ' -> '+ days30.format('DD.MM.YYYY')
    		selectPlaceDay.val(val)
    		$(this).val(str);
		});
    });
	
	/*$('#selected-block').on('change',"#select-place-day", function(){
    	var val = $("#select-place-day").val();
      	var days30 = moment(val).add(30, 'days'); 
    	var str = moment(val).format('DD.MM.YYYY') + ' -> '+ days30.format('DD.MM.YYYY')
    	$('#select-place-day').val(str);
	});*/
		
JS;

	$this -> registerJs( $changeForm );
	$this -> registerJs( $room_hour );
	$this -> registerJs( $place );
?>

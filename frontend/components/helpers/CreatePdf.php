<?php

	namespace frontend\components\helpers;

	use kartik\mpdf\Pdf;
	use Yii;
	use yii\helpers\ArrayHelper;
	use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

	class CreatePdf
	{

		public function myTicketToPdf($model){
			$formatter = Yii::$app->formatter;
			$timeStart = new DateTime($model['time_start_event']);
			$timeEnd = new DateTime($model['time_end_event']);
			$intervalTime = $timeStart->diff($timeEnd);
			$optionsArray = array(
				'elementId'=> 'barcode', /* div or canvas id*/
				'value'=> $getDataTicket[ 'code_res' ], /* value for EAN 13 be careful to set right values for each barcode type */
				'type'=>'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
				'settings'=>[
					'barWidth'=>2
				]


			);
			echo BarcodeGenerator::widget($optionsArray);
			$content = '';
			$content .= '<div class="ticket_tile print-version"><div>';
			$content .= '<div class="date">'.date("m/d/Y",strtotime($model[ 'date_event' ])).'</div>';
			$content .= '<div class="title">'.$model[ 'title' ].'</div><div class="time">';
			$content .= $formatter->asDate($model['time_start_event'],'php:H:i').'-'.$formatter->asDate($model['time_end_event'],'php:H:i');
			$content .= '</div><div class="price">$'.$model[ 'price_ticket_event' ].'<div class="text"><div>paid</div><div class="divider"></div>';
			$content .= '<div>'.$intervalTime->h."h ".$intervalTime->i."m".'</div>'.$model[ 'deck_ticket' ].'</div></div><div class="divider"></div>';
			$content .= '<div id="barcode"></div></div></div>';

			$pdf = new Pdf([
							   'mode' => Pdf::MODE_CORE,
							   'format' => Pdf::FORMAT_A4,
							   'orientation' => Pdf::ORIENT_PORTRAIT,
							   'destination' => Pdf::DEST_BROWSER,
							   'marginTop' => 25,
							   'content' => $content,
							   //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
							   'cssInline' => '.kv-heading-1{font-size:18px}',
							   'options' => ['title' => 'My ticket'],
							   'methods' => [
								   'SetFooter' => ['{PAGENO}'],
							   ]
						   ]);
			Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');
			return $pdf->render();
		}

	}
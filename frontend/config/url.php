<?php
	return [
		'/'                                  => 'site/index',
		'signup'                             => 'login/signup',
		'login'                              => 'login/login',
		'request-password-reset'             => 'login/request-password-reset',
		'reset-password'                     => 'login/reset-password',
		'logout'                             => 'login/logout',
		'about'                              => 'site/about',
		'become-member'                      => 'site/become-member',
		'contact'                            => 'site/contact',
		'score-board'                        => 'site/score-board',
		'faq'                                => 'site/faq',
		'homepage-events'                    => 'site/index-events',
		'homepage-places'                    => 'site/index-places',
		'check-payments-plans'               => 'site/check-payments-plans',
		'events/<id:\d+>'                    => 'events/view',
		'places/<id:\d+>'                    => 'places/view',
		'account/favorites-events'           => 'account/profile/favorites-events',
		'account/favorites-places'           => 'account/profile/favorites-places',
		'account/my-booking'                 => 'account/bookings',
		'account/my-tickets'                 => 'account/tickets',
		'account/my-memberships'             => 'account/memberships',
		'account/my-tickets/<id:\d+>'        => 'account/tickets/view',
		'account/my-bookings-place/<id:\d+>' => 'account/bookings/view',
		'account/my-bookings/print/<id:\d+>' => 'account/bookings/print',
		'account/tickets/print/<id:\d+>'     => 'account/tickets/print',

		//crons
		'repeat-event'                       => 'events/repeat-event',
		'update-ticket-status'               => 'events/update-status-ticket',
		//'account/album/<albumID:\d+>'                        => 'account/
	];
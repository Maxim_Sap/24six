<?php
namespace frontend\models;


use yii\base\Model;
use common\models\User;

class PasswordResetRequestForm extends Model
{
    public $email;
    public $reset;

    function rules(){
        return[
            ['email','required'],
            ['email','email'],
            ['email','trim'],
            ['email','checkEmail']
        ];
    }

    function checkEmail($attribute, $params){
        if(!$this->hasErrors()){
            $user = User::findOne(['email' => $this->email]);
            if(!$user){
                $this->addError($attribute, 'E-mail not found');
            }
        }
    }
}
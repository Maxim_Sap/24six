<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "website_faq".
 *
 * @property int $id
 * @property string $Question
 * @property string $Answer
 */
class WebsiteFaq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'website_faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Question', 'Answer'], 'required'],
            [['Question', 'Answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'Question' => Yii::t('frontend', 'Question'),
            'Answer' => Yii::t('frontend', 'Answer'),
        ];
    }
}

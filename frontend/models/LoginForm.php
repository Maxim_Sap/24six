<?php
namespace frontend\models;

use yii;
use yii\base\Model;
use common\models\User;

class LoginForm extends Model
{
    public $user_id;
    public $password;
    public $email;
    public $remember;


    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['user_id'], 'integer'],
            [['email', 'password'], 'trim'],
            [['email', 'password'], 'string'],
            ['email', 'email'],
            ['remember', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
            /*if (!$user or !password_verify($this->password, $user->password)) {
                $this->addError($attribute, 'Incorrect username or password');
            }*/
        }
    }

    public function getUserById()
    {
        return User::findOne(['id' => $this->user_id]);
    }

    public function getUser()
    {

        //var_dump($this->id);
        //return $this->user_id;
        //return Users::findOne(['id' => $this->user_id]);
        return User::findOne(['email' => $this->email]);
    }

    public function attributeLabels()
    {
        return [
            'remember' => \Yii::t('frontend', 'Remember Me'),
            'email'    => 'E-mail',
            'user_id'  => 'Id',
            'password' => \Yii::t('frontend', 'Password'),
        ];
    }
}
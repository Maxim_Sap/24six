<?php
namespace frontend\models;

use common\models\UserProfile;
use common\models\UserSettings;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $user_type;
    public $password;
    public $conf_password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'unique' , 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            //['user_type', 'required'],
            //['user_type', 'integer'],

            ['conf_password', 'compare', 'compareAttribute' => 'password','message' => 'Password and Confirm password don\'t match'],

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return [
                'success' => FALSE,
                'errors'  => $this->errors,
            ];
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->created_at     = time();
            $user->last_activity  = time();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->status = User::STATUS_NO_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if (!$user->save()) {
                throw new \Exception('Can\'t be saved user. Errors: ' . join(', ', $user->getFirstErrors()));
            }

            $this->user_type = User::USER_DEFAULT;
            $user_type = User::ROLE_NAME_USER;
            /*switch ($this->user_type) {
                case User::USER_DEFAULT:
                    $user_type = User::ROLE_NAME_USER;
                    break;
                case User::USER_ORGANIZATION:
                    $user_type = User::ROLE_NAME_ORGANIZATION;
                    break;
                default:
                    $user_type = User::ROLE_NAME_USER;
            }*/
            $userRole = Yii::$app->authManager->getRole($user_type);
            Yii::$app->authManager->assign($userRole, $user->id);
            //Timeline::setNewEvent('newUser', $user->id);
            $transaction->commit();

            //добавляем профиль нового пользователя
            $userProfile          = new UserProfile();
            $UserProfile_post = Yii::$app->request->post('UserProfile');
            $userProfile->attributes = $UserProfile_post;
            $userProfile->user_id = $user->id;
            $userProfile->user_type = $this->user_type;
            $userProfile->birthday = (isset($UserProfile_post['birthday']) && strtotime($UserProfile_post['birthday'])!== false) ? date("Y-m-d",strtotime($UserProfile_post['birthday'])) : null;
            //$userProfile->receive_donation_checkbox = (isset($UserProfile_post['receive_donation_checkbox']) && $UserProfile_post['receive_donation_checkbox'] == true) ? 1 : 0;
            if (!$userProfile->validate() || !$userProfile->save()) {
                //var_dump($userProfile->getFirstErrors());die;
                throw new \Exception('Can\'t be saved user profile. Errors: ' . join(', ', $userProfile->getFirstErrors()));
            }



            return [
                'success' => TRUE,
                'user'    => $user,
            ];

        } catch (\Exception $e) {
            $transaction->rollBack();

            return [
                'success' => FALSE,
                'errors'  => $e->getMessage(),
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();

            return [
                'success' => FALSE,
                'errors'  => $e->getMessage(),
            ];
        }
    }
}

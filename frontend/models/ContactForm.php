<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $body;
//    public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {

        /*
         {
        $ress = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'registration-html', 'text' => 'registration-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['noreplyEmail'] =>Yii::$app->params['siteName']." - robot"])
            ->setTo($user->email)
            ->setSubject('Registration on ' . Yii::$app->params['siteName'])
            ->setCharset('UTF-8')
            ->send();

        return $ress;
    }
         * */


        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->phone)
            ->setTextBody($this->body)
            ->send();
    }
}

<?php
	use yii\helpers\Url;
	use yii\bootstrap\Html;
	use common\models\User;
	use yii\widgets\ListView;
	use yii\widgets\ActiveForm;
	use common\models\UserProfile;
	$formatter = Yii ::$app -> formatter;
?>
<div class="wrap">
	<?= $this->render('../profile/_parts/_side-bar') ?>
	<div class="edit_box left">
        <div class="title">My Membership</div>
		<?php if(!empty($getPlans)): ?>
			<?php foreach($getPlans as $plan): ?>
				<div class="cost_tile <?php if( isset( $checkMyAssnPlan ) ): if( $plan -> id_plan == $checkMyAssnPlan -> payment_type ):?>active<?php endif ?><?php endif ?>">
					<div class="cost_tile_title"><?= $plan[ 'plan' ] -> title ?></div>
					<?php if( $plan[ 'plan' ] -> id == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS): ?>
						<div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / per day</div>
					<?php else: ?>
						<div class="price"><span>$<?= $plan[ 'plan' ] -> price ?></span> / mount</div>
					<?php endif; ?>
                    <span class="active_payment">Active membership</span>
				</div>
			<?php endforeach; ?>
        <div class="clearfix"></div>
		<?php endif ?>
		<?php if( !empty( $myPlans ) && isset( $myPlans ) ): ?>
	        <table class="membership_table">
	            <tr>
	                <th>Membership Type</th>
	                <th>Status</th>
	                <th>Start Date</th>
	                <th>Period end</th>
	                <th>Option</th>
	            </tr>
					<?php foreach( $myPlans as $plan ): ?>
				        <?php
							$date_end = [];
							switch( $plan[ 'payment_type' ] )
							{
								case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP :
									$date_start = strtotime( $plan[ 'date_end' ] ) - 60 * 60 * 24 * 30;
									break;
								case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP :
									$date_start = strtotime( $plan[ 'date_end' ] ) - 60 * 60 * 24 * 30;
									break;
								case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS :
									$date_start = strtotime( $plan[ 'date_end' ] ) - 60 * 60 * 24;
									break;
							}
				        ?>
			            <tr>
			                <td><?= $plan[ 'title' ] ?></td>
				            <?php if( $plan[ 'status' ] == \common\models\ResourcesEventsPlaces::NO_ACTIVE_TICKETS  ): ?>
					            <td><span class="inactive">Inactive</span></td>
				            <?php else: ?>
					            <td><span class="active">Active</span></td>
				            <?php endif ?>
			                <td><strong><?= $formatter -> asDate( $date_start , 'php:d M Y' ) ?></strong></td>
			                <td><strong><?= $formatter -> asDate( $plan[ 'date_end' ], 'php:d M Y' ) ?></strong></td>
			                <td>
				                <?php if( $plan[ 'status' ] != \common\models\ResourcesEventsPlaces::NO_ACTIVE_TICKETS): ?>
					                <a href="<?= Url::to( [ '/account/memberships/cancel-plans' , 'date' => $plan[ 'date_end' ] ,'type' => $plan[ 'payment_type' ] ] )?>" class="cancel">Cancel</a>
					                <a href="<?= Url::to(['/account/payments', 'type' => $plan[ 'payment_type' ] , 'continue_plan' => UserProfile::TYPE_CONTINUE_PACKAGE_PLAN, 'main_date' => $plan[ 'date_end' ] ] ) ?>" class="extend">Extend</a>
				                <?php else: ?>
					                Package is closed
				                <?php endif; ?>
			                </td>
			            </tr>
			        <?php endforeach; ?>
	        </table>
		<?php else: ?>
			<h3>You not have a plans</h3>
		<?php endif; ?>
		<div class="bonus">
			<div class="title">Your bonus</div>
			<?php if( !empty($getMyBonus) ): ?>
				<?php foreach( $getMyBonus as $myBonus ): ?>
					<li><?= $myBonus -> count_events_week ?> Events a week</li>
					<li><?= $myBonus -> count_time_for_massage ?> 30 min massage sessions in our state of the art massage chairs</li>
					<li><?= $myBonus -> count_time_for_conf_room ?> hour private conference room</li>
				<?php endforeach; ?>
			<?php else: ?>
				<h3>You not have bonuses</h3>
			<?php endif; ?>
		</div>
		<?php
			$alert = <<<JS
			$(document).ready(function() {
			  	$('.cancel').click(function() {
			  	  	if( confirm('Do you really want to close the package') ){
			  	  		return true;
			  	  	} else {
			  	  		return false;
			  	  	}
			  	});
			})
JS;
			$this -> registerJS($alert);

		?>

	</div>
	<div class="clearfix"></div>
</div>
<?php
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Html;
	use common\models\UserProfile;
	use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
	$formatter = Yii::$app->formatter;
	$my_js = <<<JS
 $(document).ready(function() {
 	window.print();
 });
JS;
	$this->registerJs($my_js);
	$getData = Yii ::$app -> request -> get();
//dd($getDataTicket);
	?>
<?php if( $getData[ 'type_place' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
	<?php foreach( $getDataTicket as $item ):
		$price_date = "$".$item[ 'price_for_hour' ];
		$timeStartAttr = new DateTime( $item[ 'time_start' ][ 'time_start' ] );
		$timeEndAttr   = new DateTime( $item[ 'time_end' ][ 'time_end' ] );
		$intervalTime  = $timeStartAttr -> diff( $timeEndAttr );
		$data          = [
			'main'         => $item,
			'title'        => UserProfile::CONF_ROM,
			'price'        => $item[ 'price_for_hour' ],
			'date'         => $item[ 'date' ],
			'intervalTime' => $intervalTime -> h,
		];
		$data['main']['type_place'] = 'cr';
		?>
	<?php endforeach;
	$date_val = $item[ 'date' ];
	$time_val = substr($item[ 'time_start' ][ 'time_start' ],0,-3)." - ".substr($item[ 'time_end' ][ 'time_end' ],0,-3);
	$code_res = $item[ 'code_res_room' ];
else: ?>
<?php
	switch( $getDataTicket[ 'type_place' ] )
	{
		case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
			$price_date = $getDataTicket[ 'unfixiable_price' ];
			$type_place = UserProfile::WORK_PLACE;
			$data = [
				'main'  => $getDataTicket,
				'title' => $getDataTicket[ 'title' ],
				'desk'  => '30 days',
				'price' => $getDataTicket[ 'unfixiable_price' ],
			];
			break;
		case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
			$price_date = $getDataTicket[ 'separate_price' ];
			$type_place = UserProfile::PRIVATE_OFFICE;
			$data = [
				'main'  => $getDataTicket,
				'title' => $getDataTicket[ 'title' ],
				'desk'  => '30 days',
				'price' => $getDataTicket[ 'separate_price' ],
			];
			break;
		case UserProfile::TYPE_PLACE_ONE_DAY :
			$price_date = $getDataTicket[ 'one_day_price' ];
			$type_place = UserProfile::WORK_PLACE;
			$data = [
				'main'  => $getDataTicket,
				'title' => $getDataTicket[ 'title' ],
				'desk'  => 'One day',
				'price' => $getDataTicket[ 'one_day_price' ],
			];
			break;
	}
	$date_val = $formatter -> asDate( $getDataTicket[ 'date' ], 'php:d.m.Y' )." - ". $formatter -> asDate( $getDataTicket[ 'date_end' ], 'php:d.m.Y' );
	$time_val = '';
	$code_res = $getDataTicket[ 'code_res' ];?>
<?php endif;
	$this->title = "Ticket to - ".$data[ 'title' ];
?>
<style>
	#barcode{
		position: absolute;
		bottom: 4px;
		left: calc(50% - 95px);
	}
</style>
<div class="wrap">
	<div class="ticket_tile print-version">
		<div>
			<div class="date"><?=$date_val?></div>
			<div class="title"><?= $data[ 'title' ] ?></div>
			<div class="time"><?=$time_val?></div>
			<div class="price">
				<?//= $price_date."$"?>
				<?= isset( $data[ 'intervalTime' ] ) ? $data[ 'price' ] * $data[ 'intervalTime' ] : $data[ 'price' ] ?>$
				<div class="text">
					<div>paid</div>
					<div class="divider"></div>
					<div>
						<?php
							if
							(
								$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_ONE_DAY or
								$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
								$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE
							) : ?>
								<?= $data[ 'desk' ] ?>
							<?php else: ?>
								<?= $data[ 'intervalTime' ]; ?>
								hours
							<?php endif ?>
					</div>
				</div>
			</div>
<!--			<div style="position: relative;top: 37px;">--><?//= $getDataTicket[ 'deck_ticket' ] ?><!--</div>-->
			<div class="divider"></div>
			<div id="barcode"></div>
		</div>
	</div>
	<?php
		$optionsArray = array(
			'elementId'=> 'barcode', /* div or canvas id*/
			'value'=> $code_res, /* value for EAN 13 be careful to set right values for each barcode type */
			'type'=>'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
			'settings'=>[
				'barWidth'=>2
			]


		);
		echo BarcodeGenerator::widget($optionsArray);
	?>

</div>
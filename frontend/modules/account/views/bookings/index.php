<?php
	$formatter = Yii ::$app -> formatter;

	use common\models\UserProfile;

?>

<div class="wrap">
	<?= $this -> render( '/profile/_parts/_side-bar' ) ?>

	<div class="edit_box left">
		<div class="title">My Booking</div>
		<?php if( !empty( $dataTickets ) ): ?>
			<?php foreach( $dataTickets as $item ): ?>
				<?php
				switch( $item[ 'type_place' ] )
				{
					case UserProfile::TYPE_PLACE_ONE_DAY :
						$data = [
							'main'  => $item,
							'title' => $item[ 'title' ],
							'desk'  => 'One day',
							'price' => $item[ 'one_day_price' ],
							'date'  => $item[ 'date_order' ],
						];
						break;
					case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
						$data = [
							'main'  => $item,
							'title' => $item[ 'title' ],
							'desk'  => '30 days',
							'price' => $item[ 'unfixiable_price' ],
							'date'  => $item[ 'date_order' ],
						];
						break;
					case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
						$data = [
							'main'  => $item,
							'title' => $item[ 'title' ],
							'desk'  => '30 days',
							'price' => $item[ 'separate_price' ],
							'date'  => $item[ 'date_order' ],
						];
						break;
					case UserProfile::TYPE_PLACE_CONF_ROM :
						$timeStartAttr = new DateTime( $item[ 'time_start' ][ 'time_start' ] );
						$timeEndAttr   = new DateTime( $item[ 'time_end' ][ 'time_end' ] );
						$intervalTime  = $timeStartAttr -> diff( $timeEndAttr );
						$data          = [
							'main'         => $item,
							'title'        => UserProfile::CONF_ROM,
							'price'        => $item[ 'price_for_hour' ],
							'date'         => $item[ 'date' ],
							'intervalTime' => $intervalTime -> h,
						];
						break;

				} ?>
				<div class="booking_tile <?php if( $data[ 'main' ][ 'status' ] == \common\models\ResourcesEventsPlaces::NO_ACTIVE_TICKETS ): ?>inactive <?php endif; ?>">
					<div>
						<div class="date">
							<?= $formatter -> asDate( $data[ 'date' ], 'php:d.m.Y' ) ?>
						</div>
						<div class="title"><?= $data[ 'title' ] ?></div>
						<div class="time">
							<?php
								if
								(
									$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
									$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
									$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_ONE_DAY
								) : ?>

								<?php else: ?>
									<?= $formatter -> asDate( $item[ 'time_start' ][ 'time_start' ], 'php:H:i' ) ?>
									-
									<?= $formatter -> asDate( $item[ 'time_end' ][ 'time_end' ], 'php:H:i' ) ?>
								<?php endif; ?>
						</div>
						<div class="price">
							<?= isset( $data[ 'intervalTime' ] ) ? $data[ 'price' ] * $data[ 'intervalTime' ] : $data[ 'price' ] ?>$
							<div class="text">
								<div>paid</div>
								<div class="divider"></div>
								<div>
									<?php
										if
										(
											$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_ONE_DAY or
											$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
											$data[ 'main' ][ 'type_place' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE
										) : ?>
											<?= $data[ 'desk' ] ?>
										<?php else: ?>
											<?= $data[ 'intervalTime' ]; ?>
											hours
										<?php endif ?>
								</div>
							</div>
						</div>
						<div class="divider"></div>
						<?php if( $item[ 'type_place' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
							<a href="<?= \yii\helpers\Url ::toRoute( [ 'bookings/view/','id' => $item[ 'id_ticket' ] , 'type_place' => $item[ 'type_place' ] ] ) ?>">Ticket Details</a>
						<?php else: ?>
							<a href="<?= \yii\helpers\Url ::toRoute( [ 'bookings/view/','id' => $item[ 'id_ticket' ],  'type_place' => $item[ 'type_place' ]  ] ) ?>">Ticket Details</a>
						<?php endif ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<h2>The list is empty</h2>
		<?php endif; ?>
	</div>

</div>
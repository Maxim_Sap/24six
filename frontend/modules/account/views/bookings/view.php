<?php

	use yii\bootstrap\ActiveForm;
	use yii\helpers\Html;
	use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
	use yii\helpers\Url;
	use common\models\UserProfile;

	$formatter = Yii ::$app -> formatter;
	$getData = Yii ::$app -> request -> get();
?>
<div class="wrap">
	<?= $this -> render( '/profile/_parts/_side-bar' ) ?>
	<div class="edit_box left">
		<div class="title">Ticket details<a class="print" target="_blank" title="print ticket" href="<?= Url::toRoute( [ '/account/my-bookings/print/'.$getData['id'],'type_place'=>$getData[ 'type_place' ]] ) ?>"></a></div>
		<?php if( $getData[ 'type_place' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
			<?php foreach( $getDataTicket as $item ):
				$code_res = $item[ 'code_res_room' ];?>
				<div><strong>Place: </strong> <a href="<?= Url ::toRoute( [ '/places/' . $item[ 'id_place' ] ] ) ?>"><?= $item[ 'title' ] ?></a></div>
				<div><strong>Name conference room : </strong> <?= $item[ 'name' ] ?></div>
<!--				<div><strong>Working Days: </strong> --><?//= $item[ 'working_days' ] ?><!--</div>-->
				<div><strong>Price for hour: </strong> $<?= $item[ 'price_for_hour' ] ?></div>
				<div><strong>Time start: </strong> <?= $formatter -> asTIme($item[ 'time_start' ][ 'time_start' ],'php:H:i') ?></div>
				<div><strong>Time end: </strong> <?= $formatter -> asTime($item[ 'time_end' ][ 'time_end' ],'php:H:i') ?></div>
				<div><strong>Type  </strong> <?= UserProfile::CONF_ROM ?></div>
				<div><strong>Date: </strong> <?= $formatter -> asDate( $item[ 'date' ], 'php:d.m.Y' ) ?></div>
				<div><strong>Location: </strong> <?= $item[ 'address' ] ?></div>
				<div><strong>Ticket S/N: </strong> <?= $code_res ?></div>
				<div id="barcode"></div>
			<?php endforeach; ?>
		<?php else: ?>
			<?php
			$code_res = $getDataTicket[ 'code_res' ];
			switch( $getDataTicket[ 'type_place' ] )
			{
				case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
					$price_date = $getDataTicket[ 'unfixiable_price' ];
					$type_place = UserProfile::WORK_PLACE;
					break;
				case UserProfile::TYPE_PLACE_SEPARATE_SPACE :
					$price_date = $getDataTicket[ 'separate_price' ];
					$type_place = UserProfile::PRIVATE_OFFICE;
					break;
				case UserProfile::TYPE_PLACE_ONE_DAY :
					$price_date = $getDataTicket[ 'one_day_price' ];
					$type_place = UserProfile::WORK_PLACE;
					break;
			}
			?>

			<div><strong>Place: </strong> <a href="<?= Url ::toRoute( [ '/events/' . $getDataTicket[ 'id_place' ] ] ) ?>"><?= $getDataTicket[ 'title' ] ?></a></div>
			<div><strong>Email: </strong> <?= $getDataTicket[ 'email' ] ?></div>
			<div><strong>Phone: </strong> <?= $getDataTicket[ 'phone' ] ?></div>
			<div><strong>Price: </strong> $<?= $price_date ?></div>
			<div><strong>Type  </strong> <?= $type_place ?></div>
			<div><strong>Date start: </strong> <?= $formatter -> asDate( $getDataTicket[ 'date' ], 'php:d.m.Y' ) ?></div>
			<div><strong>Date end: </strong> <?= $formatter -> asDate( $getDataTicket[ 'date_end' ], 'php:d.m.Y' ) ?></div>
			<div><strong>Location: </strong> <?= $getDataTicket[ 'address' ] ?></div>
			<?php if( !empty( $getDataTicket[ 'deck_ticket' ] ) ) { ?>
				<div><strong>Ticket detail: </strong> <?= $getDataTicket[ 'deck_ticket' ] ?></div>
			<?php } ?>
			<div><strong>Ticket S/N: </strong> <?= substr($code_res,0,12) ?></div>
			<div id="barcode"></div>
			<br>
			<?php $form = ActiveForm ::begin( [ 'action' => '/account/bookings/desk-tickets']); ?>
				<div class="content">
					<?= $form -> field( $model, 'deck_ticket', [ 'template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>" ] ) -> textarea() -> label( 'Add Name / Surname to ticket' ) ?>
					<input type="hidden" name="ResourcesEventsPlaces[id]" value="<?= $getDataTicket[ 'id_ticket' ] ?>">
					<input type="hidden" name="ResourcesEventsPlaces[type_place]" value="<?= $getDataTicket[ 'type_place' ] ?>">
				</div>
				<?= Html ::submitButton( 'Save data', [ 'class' => 'btn blue_btn' ] ) ?>
			<?php ActiveForm ::end() ?>
		<?php endif; ?>

		<?php
			$optionsArray = [
				'elementId' => 'barcode', /* div or canvas id*/
				'value'     => substr($code_res,0,12), /* value for EAN 13 be careful to set right values for each barcode type */
				'type'      => 'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
				'settings'  => [
					'barWidth' => 2,
				],

			];
			echo BarcodeGenerator ::widget( $optionsArray );
		?>
	</div>

</div>
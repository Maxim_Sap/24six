<?php
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Html;
	use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
	use yii\helpers\Url;
	$formatter = Yii::$app->formatter;
?>
<div class="wrap">
	<?= $this->render('/profile/_parts/_side-bar') ?>
	<div class="edit_box left">
		<div class="title">Ticket details<a class="print" target="_blank" title="print ticket" href="<?= Url::toRoute( [ '/account/tickets/print/'.$model->id ] ) ?>"></a></div>
		<div><strong>Event: </strong> <a href="<?= Url::toRoute( [ '/events/'.$getDataTicket[ 'id_event' ] ] ) ?>"><?= $getDataTicket[ 'title' ] ?></a></div>
		<div><strong>Date event: </strong> <?= date('m/d/Y',strtotime($getDataTicket[ 'date_event' ])) ?></div>
		<div><strong>Time start: </strong> <?=$formatter->asDate($getDataTicket['time_start_event'],'php:H:i')?></div>
		<div><strong>Time end: </strong> <?=$formatter->asDate($getDataTicket['time_end_event'],'php:H:i')?></div>
		<div><strong>Ticket price: </strong> $<?= $getDataTicket[ 'price_ticket_event' ] ?></div>
		<div><strong>Location: </strong> <?= $getDataTicket[ 'address' ] ?></div>
		<?php if(!empty($getDataTicket[ 'deck_ticket' ])){?>
			<div><strong>Ticket detail: </strong> <?= $getDataTicket[ 'deck_ticket' ] ?></div>
		<?php } ?>
		<div><strong>Ticket S/N: </strong> <?= substr($getDataTicket[ 'code_res' ],0,12) ?></div>
		<div id="barcode"></div>

		<br>
		<?php $form = ActiveForm::begin( ); ?>
			<div class="content">
				<?= $form->field($model, 'deck_ticket', ['template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textarea()->label('Add Name / Surname to ticket') ?>
				<input type="hidden" name="ResourcesEventsPlaces[id]" value="<?= $getDataTicket['id_ticket'] ?>">
			</div>
			<?= Html::submitButton( 'Save data', ['class' => 'btn blue_btn']) ?>
		<?php ActiveForm::end() ?>

		<?php
			$optionsArray = array(
				'elementId'=> 'barcode', /* div or canvas id*/
				'value'=> substr($getDataTicket[ 'code_res' ],0,12), /* value for EAN 13 be careful to set right values for each barcode type */
				'type'=>'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
				'settings'=>[
					'barWidth'=>2
				]


			);
			echo BarcodeGenerator::widget($optionsArray);
		?>
	</div>

</div>
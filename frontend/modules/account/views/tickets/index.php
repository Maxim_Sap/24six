<?php
	$formatter = Yii::$app->formatter;
?>
<div class="wrap">
    <?= $this->render('/profile/_parts/_side-bar') ?>
    <div class="edit_box left">
        <div class="title">My Tickets</div>
	    <?php if(!empty($myTickets)): ?>
		    <?php foreach($myTickets as $myTicket): ?>
		        <div class="ticket_tile<?php if( $myTicket[ 'status' ] == \common\models\ResourcesEventsPlaces::NO_ACTIVE_TICKETS ):?> inactive<?php endif ?>">
		            <div>
		                <div class="date"><?= $formatter -> asDate( $myTicket['date'], 'php:d.m.Y' ) ?></div>
		                <div class="title"><?= $myTicket['title']?></div>
		                <div class="time">
							<?=$formatter->asDate($myTicket['time_start_event'],'php:H:i')?>
			                -
							<?=$formatter->asDate($myTicket['time_end_event'],'php:H:i')?>
		                </div>
		                <div class="price">
		                    <?= $myTicket['price_ticket_event'] ?>$
		                    <div class="text">
		                        <div>paid</div>
		                        <div class="divider"></div>
		                        <div>
									<?php
										$timeStart = new DateTime($myTicket['time_start_event']);
										$timeEnd = new DateTime($myTicket['time_end_event']);
										$intervalTime = $timeStart->diff($timeEnd);
									?>
									<?=$intervalTime->h."h ".$intervalTime->i."m"?>
		                        </div>
		                    </div>
		                </div>
		                <div class="divider"></div>
		                <a href="<?= \yii\helpers\Url::toRoute(['tickets/view/','id' => $myTicket['id_ticket']]) ?>">Ticket Details</a>
		            </div>
		        </div>
	        <?php endforeach; ?>
		<?php else: ?>
		    <h2>You didn't buy tickets</h2>
	    <?php endif ?>
    </div>

</div>
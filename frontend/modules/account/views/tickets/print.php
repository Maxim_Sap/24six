<?php
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Html;
	use barcode\barcode\BarcodeGenerator as BarcodeGenerator;
	$formatter = Yii::$app->formatter;
	$my_js = <<<JS
 $(document).ready(function() {
 	window.print();
 });
JS;
	$this->registerJs($my_js);
	$this->title = "Ticket to - ".$getDataTicket[ 'title' ];
	?>
<style>
	#barcode{
		position: absolute;
		bottom: 4px;
		left: calc(50% - 95px);
	}
</style>
<div class="wrap">
	<div class="ticket_tile print-version">
		<div>
			<div class="date"><?= date('m/d/Y',strtotime($getDataTicket[ 'date_event' ])) ?></div>
			<div class="title"><?= $getDataTicket[ 'title' ] ?></div>
			<div class="time">
				<?=$formatter->asDate($getDataTicket['time_start_event'],'php:H:i')?>
				-
				<?=$formatter->asDate($getDataTicket['time_end_event'],'php:H:i')?>
			</div>
			<div class="price">
				$<?= $getDataTicket[ 'price_ticket_event' ] ?>
				<div class="text">
					<div>paid</div>
					<div class="divider"></div>
					<div>
						<?php
							$timeStart = new DateTime($getDataTicket['time_start_event']);
							$timeEnd = new DateTime($getDataTicket['time_end_event']);
							$intervalTime = $timeStart->diff($timeEnd);
						?>
						<?=$intervalTime->h."h ".$intervalTime->i."m"?>
					</div>
				</div>
			</div>
			<div style="position: relative;top: 37px;"><?= $getDataTicket[ 'deck_ticket' ] ?></div>
			<div class="divider"></div>
			<div id="barcode"></div>
		</div>
	</div>
	<?php
		$optionsArray = array(
			'elementId'=> 'barcode', /* div or canvas id*/
			'value'=> $getDataTicket[ 'code_res' ], /* value for EAN 13 be careful to set right values for each barcode type */
			'type'=>'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
			'settings'=>[
				'barWidth'=>2
			]


		);
		echo BarcodeGenerator::widget($optionsArray);
	?>

</div>
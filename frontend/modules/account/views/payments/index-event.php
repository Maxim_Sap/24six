<?php

	use common\models\UserProfile;
	use yii\helpers\StringHelper;
	use yii\helpers\Url;
	use common\models\SpaceWork;

	$this -> registerCssFile( '/lib/css/bootstrap-formhelpers-min.css' );
	$this -> registerJsFile( '//js.stripe.com/v2/' );
	$this -> registerJsFile( '/lib/js/payment-stripe.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap-formhelpers-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrapValidator-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$getData   = Yii ::$app -> request -> get();
	$formatter = Yii ::$app -> formatter;
	try
	{
		$getData[ 'id_event' ];
	}
	catch( \Exception $e )
	{

	}
?>
<style>
	.amount-value.form-control {
		width : 50%;
	}

	.has-feedback .form-control {
		padding : 6px 13px 6px 11px !important;
	}

	div.payment-info {
		text-align       : center;
		padding          : 18px;
		background-color : #b6f6bc;
	}
</style>
<div>
	<div class="page_divider"></div>
	<div class="form_wrap">
		<div class="wrap clearfix">
			<div class="payment_cost_tile">
				<div>You selected :</div>
					<?php switch( $type )
					{
						case UserProfile::TYPE_PAYMENT_TICKET:
							$plan = UserProfile ::payment_tickets( $getData[ 'id_event' ], UserProfile::TYPE_PAYMENT_TICKET );
							break;
					}
					?>
				<div>
					<ul>
						<div class="event_wrap left" id="event_<?= $plan[ 'id' ] ?>" style="position: relative;">
							<div class="top_section" style="height:159px; background: url(/source/img/background_image/origin/<?= $plan[ 'image' ] ?>) center / cover no-repeat"></div>
							<div class="title"><?= StringHelper ::truncate( $plan[ 'title' ], 40, '...' ) ?></div>
							<div class="description"><?= StringHelper ::truncate( strip_tags( $plan[ 'description' ] ), 70, '...' ) ?></div>
							<div class="date">
								<?= $formatter -> asDate( $plan[ 'date_event' ], 'php:M d' ) ?>
								<?= ' at ' ?>
								<?= $formatter -> asTime( $plan[ 'time_start_event' ], 'php:H.i' ) ?>
								<?= '-' ?>
								<?= $formatter -> asTime( $plan[ 'time_end_event' ], 'php:H.i' ) ?>
							</div>
							<div class="address"><?= $plan[ 'address' ] ?></div>

							<div class="price">Price for ticket <?= $plan[ 'price_ticket_event' ] ?>$</div>


							<a href="<?= Url ::toRoute( [ '/events/' . $plan[ 'id' ] ] ) ?>" class="btn blue_btn">Event Details</a>
						</div>
					</ul>
				</div>
			</div>

			<?php if( isset( $getData[ 'free_tickets_value' ] ) && !empty( $getData[ 'free_tickets_value' ] ) ): ?>

				You selected <?= $getData[ 'count_tickets' ] + $getData[ 'free_tickets_value' ] ?> tickets on event <a href="<?= Url ::toRoute( [ '/events/' . $plan[ 'id' ] ] ) ?>"><?= $plan[ 'title' ] ?></a>
				<div>Using bonuses <?= $getData[ 'free_tickets_value' ] ?></div>

				<?php if( $getData[ 'count_tickets' ] != 0 ): ?>
					<div>Must pay for the <?= $getData[ 'count_tickets' ] ?> ticket <?= $plan[ 'price_ticket_event' ] * $getData[ 'count_tickets' ] ?>$</div>
				<?php endif ?>

			<?php else: ?>
			<div>You selected <?= $getData[ 'count_tickets' ] ?> tickets on event <a href="<?= Url ::toRoute( [ '/events/' . $plan[ 'id' ] ] ) ?>"><?= $plan[ 'title' ] ?></a></div>
			<?php endif; ?>

			<div class="payment_form">
				<div class="form_title">Secure Payment Form</div>
				<div class="form">
					<form action="<?= $stripe_conf[ 'url-payment-event' ] ?>" method="POST" id="payment-form" class="form-horizontal">
						<input type="hidden" id="publish_key" value="<?= $stripe_conf[ 'publish_key' ]; ?>" />
						<noscript>
							<div class="bs-callout bs-callout-danger">
								<h4>JavaScript is not enabled!</h4>
								<p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript
									and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a>
									for more informations.</p>
							</div>
						</noscript>
						<div class="alert alert-danger" id="a_x200" style="display: none;"><strong>Error!</strong>
							<span class="payment-errors"></span>
						</div>
						<span class="payment-success"></span>
						<div class="sub_title">Card Details</div>
						<!-- Card Holder Name -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" name="cardholdername" maxlength="70" class="card-holder-name form-control" />
								<label class="control-label" for="textinput">Card Holder's Name</label>
							</div>
						</div>

						<!-- Card Number -->

						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cardnumber" maxlength="19" class="card-number form-control">
								<label class="control-label" for="textinput">Card Number</label>
							</div>
						</div>
						<div>test card number: 4242424242424242</div>

						<!-- Expiry-->
						<div class="form-group">
							<div class="form-control">
								<div class="form-inline">
									<select
											name="select2" data-stripe="exp-month"
											class="card-expiry-month stripe-sensitive required form-control"
									>
										<option value="01" selected="selected">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<span> / </span>
									<select
											name="select2" data-stripe="exp-year"
											class="card-expiry-year stripe-sensitive required form-control"
									>
									</select>
								</div>
								<label class="control-label" for="textinput">Card Expiry Date</label>
							</div>
						</div>

						<!-- CVV -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cvv" maxlength="4" class="card-cvc form-control">
								<label class="control-label" for="textinput">CVV/CVV2</label>
							</div>
						</div>

						<div class="form-group">
							<div class="form-control">
								<input type="text" id="amount-value" value="<?= $plan[ 'price_ticket_event' ] * $getData[ 'count_tickets' ] ?>.00" class="amount-value form-control" disabled>
								<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>
							</div>
						</div>

						<!-- Important notice -->
						<div class="panel panel-success">

						</div>

						<!-- Submit -->
						<div class="control-group">
							<div class="controls">
								<input type="hidden" name="type" value="<?= $type ?>">
								<input type="hidden" name="date_event" value="<?= $plan[ 'date_event' ] ?>">
								<input type="hidden" name="id_event" value="<?= $getData[ 'id_event' ] ?>">
								<input type="hidden" name="count_tickets" value="<?= $getData[ 'count_tickets' ] ?>">
								<?php if( isset( $getData[ 'free_tickets_value' ] ) && !empty( $getData[ 'free_tickets_value' ] ) ): ?>
									<input name="count_free_tickets" type="hidden" value="<?= $getData[ 'free_tickets_value' ] ?>">
								<?php endif ?>
								<button class="btn blue_btn" type="submit">Pay Now</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



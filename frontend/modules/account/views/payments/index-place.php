<?php

	use common\models\UserProfile;
	use yii\helpers\StringHelper;
	use yii\helpers\Url;
	use common\models\SpaceWork;

	$this -> registerCssFile( '/lib/css/bootstrap-formhelpers-min.css' );
	$this -> registerJsFile( '//js.stripe.com/v2/' );
	$this -> registerJsFile( '/lib/js/payment-stripe.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap-formhelpers-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrapValidator-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$getData   = Yii ::$app -> request -> get();
	$formatter = Yii ::$app -> formatter;
	try
	{
		$getData[ 'id_event' ];
	}
	catch( \Exception $e )
	{

	}
?>
<style>
	.amount-value.form-control {
		width : 50%;
	}

	.has-feedback .form-control {
		padding : 6px 13px 6px 11px !important;
	}

	div.payment-info {
		text-align       : center;
		padding          : 18px;
		background-color : #b6f6bc;
	}
</style>
<div>
	<div class="page_divider"></div>
	<div class="form_wrap">
		<div class="wrap clearfix">
			<div class="payment_cost_tile">
				<div>You selected :</div>
				<div class="title">
					<?php switch( $type )
					{
						case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
							$plan = UserProfile ::payment_tickets( $getData[ 'id_place' ], UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE );
							break;
						case UserProfile::TYPE_PLACE_SEPARATE_SPACE:
							$plan = UserProfile ::payment_tickets( $getData[ 'id_place' ], UserProfile::TYPE_PLACE_SEPARATE_SPACE );
							break;
						case UserProfile::TYPE_PLACE_ONE_DAY:
							$plan = UserProfile ::payment_tickets( $getData[ 'id_place' ], UserProfile::TYPE_PLACE_ONE_DAY );
							break;
						case UserProfile::TYPE_PLACE_CONF_ROM:
							$plan = UserProfile ::payment_tickets( $getData[ 'id_place' ], UserProfile::TYPE_PLACE_CONF_ROM );
							break;
						default:
							$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PLACE_ONE_DAY );
					}
						if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or $getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE )
						{
							echo 'Your plan on a mount in place ' . '</br>' . $plan[ 'title' ];
						}
						else if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY )
						{
							echo 'Your ticket(s) in place ' . '</br>' . $plan[ 'title' ];
						}
						else if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM )
						{
							echo 'Your conference room in place: ' . '</br>' . $plan[ 'title' ];
						}
					?>
				</div>
				<div class="divider"></div>
				<div>
					<ul>
						<?php if
						(
							$getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
							$getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
							$getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY or
							$getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM
						): ?>
							<div class="event_wrap left" id="event_<?= $plan[ 'id' ] ?>" style="position: relative;">
								<div class="top_section" style="height:159px; background: url(/source/img/background_image/origin/<?= $plan[ 'image' ] ?>) center / cover no-repeat">
								</div>
								<div class="title"><?= StringHelper ::truncate( $plan[ 'title' ], 40, '...' ) ?></div>
								<div class="description"><?= StringHelper ::truncate( strip_tags( $plan[ 'description' ] ), 70, '...' ) ?></div>
								<div class="address"><?= $plan[ 'address' ] ?></div>

								<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ): ?>
									<div class="price">Unfixiable Price <?= $plan[ 'unfixiable_price' ] ?>$</div>
								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE ): ?>
									<div class="price">Separate Price <?= $plan[ 'separate_price' ] ?>$</div>
								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY ): ?>
									<div class="price">One Day Price <?= $plan[ 'one_day_price' ] ?>$</div>
								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
									<div class="price">Price for hour <?= $getData[ 'price_hours_conf_rom' ]?>$</div>
								<?php endif; ?>

								<a href="<?= Url ::toRoute( [ '/places/' . $plan[ 'id' ] ] ) ?>" class="btn blue_btn">Place Details</a>

							</div>
						<?php endif; ?>
					</ul>
				</div>
			</div>

			<?php if
			(
				$getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
				$getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
				$getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY or
				$getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM
			): ?>

				<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ): ?>
					<?php if( isset( $getData[ 'count_tickets' ] ) && !empty( $getData[ 'count_tickets' ] ) ): ?>
						<div>On date with <?= $getData[ 'mount-date' ] ?></div>
						<div>Price with the place <?= $plan[ 'unfixiable_price' ] ?>$</div>
					<?php endif ?>
				<?php endif ?>

				<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE ): ?>
					<?php if( isset( $getData[ 'count_tickets' ] ) && !empty( $getData[ 'count_tickets' ] ) ): ?>
						<div>On date with <?= $getData[ 'mount-date' ] ?></div>
						<div>Price with the place <?= $plan[ 'separate_price' ] ?>$</div>
					<?php endif ?>
				<?php endif ?>

				<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY ): ?>
					<?php if( isset( $getData[ 'count_tickets' ] ) && !empty( $getData[ 'count_tickets' ] ) ): ?>
						<div>On date <?= $getData[ 'date' ] ?></div>
						<div>Price with the place <?= $plan[ 'one_day_price' ] ?>$</div>
					<?php endif ?>
				<?php endif ?>

				<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>

					<?php if( isset( $getData[ 'free_tickets_value' ] ) && !empty( $getData[ 'free_tickets_value' ] ) ): ?>
						<?php if( empty( $getData[ 'count_tickets' ] ) && $getData[ 'count_tickets' ] == 0): ?>
							<?php $different = $getData[ 'to' ] - $getData[ 'from' ] ?>
							<div>You use free <?= $getData[ 'free_tickets_value' ] ?> hour(s) in conference room</div>
							<div>on date <?= $getData[ 'date' ] ?></div>
							<div>form <?= $getData[ 'from' ] ?>:00 on <?= $getData[ 'to' ] ?>:00 (<?= $different ?> hours)</div>
						<?php else: ?>
							<?php $different = $getData[ 'to' ] - $getData[ 'from' ] ?>
							<div>on date <?= $getData[ 'date' ] ?></div>
							<div>form <?= $getData[ 'from' ] ?>:00 on <?= $getData[ 'to' ] ?>:00 (<?= $different ?> hours)</div>
							<?php if( isset( $getData[ 'free_tickets_value' ] ) && !empty( $getData[ 'free_tickets_value' ] ) ): ?>
								<div> with your bonuses on free hours in conference room: <?= $getData[ 'free_tickets_value' ] ?> hours</div>
							<?php endif ?>
						<?php endif ?>
					<?php else: ?>
						<div>You ordered the time a conference room</div>
						<?php $different = $getData[ 'to' ] - $getData[ 'from' ] ?>
						<div>on date <?= $getData[ 'date' ] ?></div>
						<div>form <?= $getData[ 'from' ] ?>:00 on <?= $getData[ 'to' ] ?>:00 (<?= $different ?> hours)</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<div class="payment_form">
				<div class="form_title">Secure Payment Form</div>
				<div class="form">
					<form action="<?= $stripe_conf[ 'url-payment-place' ]; ?>" method="POST" id="payment-form" class="form-horizontal">
						<input type="hidden" id="publish_key" value="<?= $stripe_conf[ 'publish_key' ]; ?>" />
						<noscript>
							<div class="bs-callout bs-callout-danger">
								<h4>JavaScript is not enabled!</h4>
								<p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript
									and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a>
									for more informations.</p>
							</div>
						</noscript>
						<div class="alert alert-danger" id="a_x200" style="display: none;"><strong>Error!</strong>
							<span class="payment-errors"></span>
						</div>
						<span class="payment-success"></span>
						<div class="sub_title">Card Details</div>
						<!-- Card Holder Name -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" name="cardholdername" maxlength="70" class="card-holder-name form-control" />
								<label class="control-label" for="textinput">Card Holder's Name</label>
							</div>
						</div>

						<!-- Card Number -->

						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cardnumber" maxlength="19" class="card-number form-control">
								<label class="control-label" for="textinput">Card Number</label>
							</div>
						</div>
						<div>test card number: 4242424242424242</div>

						<!-- Expiry-->
						<div class="form-group">
							<div class="form-control">
								<div class="form-inline">
									<select
											name="select2" data-stripe="exp-month"
											class="card-expiry-month stripe-sensitive required form-control"
									>
										<option value="01" selected="selected">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<span> / </span>
									<select
											name="select2" data-stripe="exp-year"
											class="card-expiry-year stripe-sensitive required form-control"
									>
									</select>
								</div>
								<label class="control-label" for="textinput">Card Expiry Date</label>
							</div>
						</div>

						<!-- CVV -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cvv" maxlength="4" class="card-cvc form-control">
								<label class="control-label" for="textinput">CVV/CVV2</label>
							</div>
						</div>

						<div class="form-group">
							<div class="form-control">


								<?php if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ): ?>

									<input type="text" id="amount-value" value="<?= $getData[ 'main_tax_price' ] ?>" class="amount-value form-control" disabled>
									<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>

								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE ): ?>

									<input type="text" id="amount-value" value="<?= $getData[ 'main_tax_price' ] ?>" class="amount-value form-control" disabled>
									<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>

								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_ONE_DAY ): ?>

									<input type="text" id="amount-value" value="<?= $getData[ 'main_tax_price' ] ?>" class="amount-value form-control" disabled>
									<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>

								<?php elseif( $getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>

									<input type="text" id="amount-value" value="<?= $getData[ 'price_hours_conf_rom' ] * $getData[ 'count_tickets' ]; ?>.00" class="amount-value form-control" disabled>

									<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>

								<?php endif; ?>
							</div>
						</div>

						<!-- Important notice -->
						<div class="panel panel-success">

						</div>

						<!-- Submit -->
						<div class="control-group">
							<div class="controls">
								<input type="hidden" name="type" value="<?= $type ?>">
								<input type="hidden" name="id_place" value="<?= $getData[ 'id_place' ] ?>">
								<?php if( $getData['type'] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
									<?php if( isset( $getData[ 'free_tickets_value' ] ) && !empty( $getData[ 'free_tickets_value' ] ) ): ?>
										<input type="hidden" name="free_hours_in_room" value="<?= $getData[ 'free_tickets_value' ] ?>">
									<?php endif ?>
									<input type="hidden" name="count_tickets" value="<?= $getData[ 'count_tickets' ] ?>">
									<input type="hidden" name="conf_room_id" value="<?= $getData[ 'type_place' ] ?>">
									<input type="hidden" name="from" value="<?= $getData[ 'from' ] ?>">
									<input type="hidden" name="to" value="<?= $getData[ 'to' ] ?>">
									<input type="hidden" name="date" value="<?= $getData[ 'date' ] ?>">
								<?php else: ?>
									<input type="hidden" name="count_tickets" value="<?= $getData[ 'count_tickets' ] ?>">
									<input type="hidden" name="date" value="<?= $getData[ 'date' ] ?>">
								<?php endif; ?>
								<button class="btn blue_btn" type="submit">Pay Now</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



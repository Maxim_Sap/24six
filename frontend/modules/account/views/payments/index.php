<?php

	use common\models\UserProfile;
	use yii\helpers\StringHelper;
	use yii\helpers\Url;
	use common\models\SpaceWork;

	$this -> registerCssFile( '/lib/css/bootstrap-formhelpers-min.css' );
	$this -> registerJsFile( '//js.stripe.com/v2/' );
	$this -> registerJsFile( '/lib/js/payment-stripe.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap.min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrap-formhelpers-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$this -> registerJsFile( '/lib/js/bootstrapValidator-min.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
	$getData   = Yii ::$app -> request -> get();
	$formatter = Yii ::$app -> formatter;
	try
	{
		$getData[ 'id_event' ];
	}
	catch( \Exception $e )
	{

	}
?>
<style>
	.amount-value.form-control {
		width : 50%;
	}

	.has-feedback .form-control {
		padding : 6px 13px 6px 11px !important;
	}

	div.payment-info {
		text-align       : center;
		padding          : 18px;
		background-color : #b6f6bc;
	}
</style>
<div>
	<div class="page_divider"></div>
	<div class="form_wrap">
		<div class="wrap clearfix">
			<div class="payment_cost_tile">
				<div>You selected :</div>
				<div class="title">
					<?php switch( $type )
					{
						case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
							$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
							break;
						case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
							$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP );
							break;
						case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
							$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP );
							break;
						default:
							$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
					}
						echo 'Your select plan' . '</br>' . $plan[0]['plan'][ 'title' ];
					?>
				</div>
				<div class="divider"></div>
				<div>Plan details:</div>
				<div>
					<ul>
						<?php if( isset( $plan[0][ 'plan' ] ) && !empty( $plan[0][ 'plan' ] ) ): ?>
							<?php foreach( $data = [ $plan[0][ 'bonus' ] ] as $item ): ?>
								<?php if( $getData[ 'type' ] == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS ): ?>
									<li>includes workspace for one day</li>
								<?php else: ?>
									<li>Workspace for <?= $item -> count_workspaces ?> day</li>
								<?php endif; ?>
								<?php if( !empty( $item -> count_time_for_conf_room ) ): ?>
									<li><?= $item -> count_time_for_conf_room ?> hour private conference room</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_events_week ) ): ?>
									<li><?= $item -> count_events_week ?> Events a week</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_daily_guest_passes ) ): ?>
									<li><?= $item -> count_daily_guest_passes ?> Daily Guest Passes</li>
								<?php endif ?>
								<?php if( !empty( $item -> count_time_for_massage ) ): ?>
									<li><?= $item -> count_time_for_massage ?> 30 min massage sessions in our state of the art massage chairs</li>
								<?php endif ?>
								<?php if( !empty( $item -> priority_registration_for_all_event ) ): ?>
									<li>Priority registration for all events</li>
								<?php endif ?>
								<?php if( !empty( $item -> complimentary_beverag ) ): ?>
									<li>Complimentary beverages</li>
								<?php endif ?>
								<?php if( !empty( $item -> mailbox_with_las_olas ) ): ?>
									<li>Mailbox with Las Olas Blvd Street Address</li>
								<?php endif ?>
								<?php if( !empty( $item -> fax_and_printer_service ) ): ?>
									<li>Fax and printer service</li>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>

			<?php if( isset( $getData[ 'continue_plan' ] ) && !empty( $getData[ 'continue_plan' ] ) ): ?>
				<div>You are going to extend the plan for another month</div>
				<div>

					<?php switch( $getData[ 'type' ] )
					{
						case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
							$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
							break;
						case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
							$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP );
							break;
						case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
							$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP );
							break;
					}
						echo $old_plan[ 0 ][ 'plan' ] -> title;
						$continue_date = strtotime($user_assn_paym -> date_end) + 60 * 60 * 24 * 30;
					?>
					<br><span>before </span><span><?= $formatter->asDate($continue_date ,'php:Y-m-d')?></span>
				</div>
			<?php else: ?>
				<?php if( !empty( $user_assn_paym ) ) { ?>
					<div>But you have current Membership plan:</div>
					<div>

						<?php switch( $user_assn_paym -> payment_type )
						{
							case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
								$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
								break;
							case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
								$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP );
								break;
							case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
								$old_plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP );
								break;
						}
							echo $old_plan[ 0 ][ 'plan' ] -> title; ?>
						<br><span>Date end:</span><span><?= $user_assn_paym -> date_end ?></span>
						<div>so current plan will be remove by ny new membership plan</div>
					</div>
				<?php } ?>
			<?php endif;?>
			<div class="payment_form">
				<div class="form_title">Secure Payment Form</div>
				<div class="form">
					<form action="<?= $stripe_conf[ 'url-payment-plan' ] ?>" method="POST" id="payment-form" class="form-horizontal">
						<input type="hidden" id="publish_key" value="<?= $stripe_conf[ 'publish_key' ]; ?>" />
						<noscript>
							<div class="bs-callout bs-callout-danger">
								<h4>JavaScript is not enabled!</h4>
								<p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript
									and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a>
									for more informations.</p>
							</div>
						</noscript>
						<div class="alert alert-danger" id="a_x200" style="display: none;"><strong>Error!</strong>
							<span class="payment-errors"></span>
						</div>
						<span class="payment-success"></span>
						<div class="sub_title">Card Details</div>
						<!-- Card Holder Name -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" name="cardholdername" maxlength="70" class="card-holder-name form-control" />
								<label class="control-label" for="textinput">Card Holder's Name</label>
							</div>
						</div>

						<!-- Card Number -->

						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cardnumber" maxlength="19" class="card-number form-control">
								<label class="control-label" for="textinput">Card Number</label>
							</div>
						</div>
						<div>test card number: 4242424242424242</div>

						<!-- Expiry-->
						<div class="form-group">
							<div class="form-control">
								<div class="form-inline">
									<select
											name="select2" data-stripe="exp-month"
											class="card-expiry-month stripe-sensitive required form-control"
									>
										<option value="01" selected="selected">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<span> / </span>
									<select
											name="select2" data-stripe="exp-year"
											class="card-expiry-year stripe-sensitive required form-control"
									>
									</select>
								</div>
								<label class="control-label" for="textinput">Card Expiry Date</label>
							</div>
						</div>

						<!-- CVV -->
						<div class="form-group">
							<div class="form-control">
								<input type="text" id="cvv" maxlength="4" class="card-cvc form-control">
								<label class="control-label" for="textinput">CVV/CVV2</label>
							</div>
						</div>

						<div class="form-group">
							<div class="form-control">
								<input type="text" id="amount-value" value="<?= $plan[0][ 'plan' ] -> price ?>.00" class="amount-value form-control" disabled>
								<label class="col-sm-4 control-label" for="textinput">SUMM (USD)</label>
							</div>
						</div>

						<!-- Important notice -->
						<div class="panel panel-success">

						</div>

						<!-- Submit -->
						<div class="control-group">
							<div class="controls">
								<input type="hidden" name="type" value="<?= $type ?>">
								<?php if( isset( $getData[ 'continue_plan' ] ) && !empty( $getData[ 'continue_plan' ] ) ): ?>
									<input type="hidden" value="<?= $getData[ 'continue_plan' ] ?>" name="continue_plan">
									<input type="hidden" value="<?= $continue_date ?>" name="date_end">
								<?php endif; ?>
								<?php if( isset( $user_assn_paym ) && !empty( $user_assn_paym ) ): ?>
									<input type="hidden" name="date_end" value="<?= $user_assn_paym -> date_end ?>">
									<input type="hidden" name="my_assn_type" value="2">
								<?php else: ?>
									<input type="hidden" name="my_assn_type" value="1">
								<?php endif ?>
								<button class="btn blue_btn" type="submit">Pay Now</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



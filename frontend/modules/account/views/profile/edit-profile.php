<?php


use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\User;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/** @var $userProfile \common\models\UserProfile */
/** @var $user \common\models\User */

$this->registerJs(
    "$('#datepicker input').datepicker({                    
        format: 'mm/dd/yyyy',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false
    });
    $('#userprofile-country_id').change(function(){
        var select = $('#userprofile-city_id');
            select.html(null);
    });
    ",
    \yii\web\View::POS_READY
);
if (!empty($userProfile->birthday)) {
    $userProfile->birthday = date('m/d/Y', strtotime($userProfile->birthday));
}
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="wrap">
    <?= $this->render('/profile/_parts/_side-bar') ?>

    <div class="edit_box left">
        <div class="title">Personal details</div>
        <div class="form_wrap">
        <?php $form = ActiveForm::begin(['action' => '/account/profile/edit-profile']); ?>
	        <input type="hidden" id="userprofile-id" value="<?= $model->id ?>"/>
	        <br/><br/>
	        <div class="content">
                <?= $form->field($model, 'username', ['template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textInput() ?>
                <?= $form->field($userProfile, 'last_name', ['template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textInput() ?>
                <?= $form->field($userProfile, 'phone', ['template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textInput() ?>
                <?= $form->field($model, 'email', ['template' => "<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textInput() ?>
                <?= $form->field($userProfile, 'birthday', ['template' => "<div id='datepicker' class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}</div>"])->textInput() ?>
            </div>

            <?= Html::submitButton( 'Update profile', ['class' => 'btn blue_btn', 'name' => 'personal_details', 'value' => 'personal_details'] ) ?>
        <?php ActiveForm::end() ?>
        </div>
    </div>


</div>
<div class="clearfix"></div>

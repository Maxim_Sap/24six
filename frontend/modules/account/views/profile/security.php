<?php
	use yii\helpers\Url;
	use yii\bootstrap\Html;
	use common\models\User;
	use yii\widgets\ListView;
	use yii\bootstrap\ActiveForm;
?>
<div class="wrap">
	<?= $this->render('_parts/_side-bar') ?>
	<div class="edit_box left">
        <div class="title">Change Password</div>
        <div class="form_wrap">
		<?php $from = ActiveForm::begin(['action' => '/account/profile/change-password']) ?>
			<br/><br/>
			<div class="content">
				<?= $from->field($model, 'password', ['template' =>"<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n</div>"])->passwordInput(['autocomplete' => 'off',])->label('Confirm current password') ?>
				<?= $from->field($model, 'new_password', ['template' =>"<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n</div>"])->passwordInput(['autocomplete' => 'off',])->label('New password') ?>
				<?= $from->field($model, 'confirm_new_password', ['template' =>"<div class=\"form-control\">\n{input}{beginLabel}{labelTitle}{endLabel}\n{error}\n{hint}\n</div>"])->passwordInput(['autocomplete' => 'off',])->label('Confirm new password') ?>
			</div>

			<?= Html::submitButton( 'Change Password', ['class' => 'btn blue_btn', 'name' => 'change_password', 'value' => 'change_password'] ) ?>
		<?php ActiveForm::end() ?>
        </div>
	</div>
</div>

	<div class="clearfix"></div>
</div>
<?php

	use yii\helpers\Url;
	use yii\bootstrap\Html;
	use common\models\User;
	use yii\widgets\ListView;
	use yii\widgets\ActiveForm;
	use common\models\UserProfile;
	use common\models\PaymentHistory;

	$formatter = Yii ::$app -> formatter;

?>
<div class="wrap">
	<?= $this -> render( '_parts/_side-bar' ) ?>
	<div class="edit_box left">
        <div class="title">My order payment</div>
		<?php if( !empty( $queryHistoryPayment ) ): ?>
			<table>
			<thead>
			<tr>
				<th style="text-align: center">Title</th>
				<th>Count</th>
				<th>Amount</th>
				<th>Date</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach( $queryHistoryPayment as $payment ): ?>
				<?php
				$typePayment = [];
				switch( $payment[ 'paym_type' ] )
				{
					case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
						$typePayment = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
						break;
					case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
						$typePayment = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP );
						break;
					case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
						$typePayment = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP );
						break;
					case UserProfile::TYPE_PAYMENT_FREE_TICKET:
						$typePayment = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_FREE_TICKET );
						break;
					case UserProfile::TYPE_PAYMENT_TICKET:
						$typePayment = UserProfile ::payment_tickets();
						break;

					case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
						$typePayment = $payment;
						break;
					case UserProfile::TYPE_PLACE_SEPARATE_SPACE:
						$typePayment = $payment;
						break;
					case UserProfile::TYPE_PLACE_ONE_DAY:
						$typePayment = $payment;
						break;
					case UserProfile::TYPE_PLACE_CONF_ROM:
						$typePayment = $payment;
						break;
				} ?>
						<?php if( $payment[ 'paym_type' ] == UserProfile::TYPE_PAYMENT_TICKET ): ?>
							<tr>
								<td>
									<a href="<?= Url ::toRoute( [ '/events/' . $payment[ 'id_event' ] ] ) ?>">
										<?= $payment[ 'event_title' ] ?>
									</a>
								</td>
								<td>
									Was purchased <span><?= $payment[ 'count_tickets' ] ?></span> tickets
								</td>
								<td><?= $payment[ 'amount' ] ?>$</td>
								<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y  H:i:s' ) ?></td>
							</tr>

						<?php elseif( $payment[ 'paym_type' ] == UserProfile::TYPE_PAYMENT_FREE_TICKET ): ?>

							<tr>
								<td>
									<a href="<?= Url ::toRoute( [ '/events/' . $payment[ 'id_event' ] ] ) ?>">
										<?= $payment[ 'event_title' ] ?>
									</a>
								</td>
								<td>
									You bought <span><?= $payment[ 'count_tickets' ] ?></span> tickets for bonuses on event
								</td>
								<td>bonus(es)</td>
								<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y  H:i:s' ) ?></td>
							</tr>

						<?php else: ?>

							<?php
							if
							(
								$payment[ 'paym_type' ] == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
								$payment[ 'paym_type' ] == UserProfile::TYPE_PLACE_SEPARATE_SPACE
							): ?>
								<tr>
									<td>
										<a href="<?= Url ::toRoute( [ '/places/' . $payment[ 'id_spaces' ] ] ) ?>"><?= $payment[ 'spaces_title' ] ?></a>
									</td>
									<td>
										<?php if( $payment[ 'count_tickets' ] == 1 ): ?>
											Was purchased package for place
										<?php else: ?>
											Was purchased <?= $payment[ 'count_tickets' ] ?> package for place
										<?php endif; ?>
									</td>
									<td><?= $payment[ 'amount' ] ?>$</td>
									<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y  H:i:s' ) ?></td>
								</tr>

							<?php elseif( $payment[ 'paym_type' ] == UserProfile::TYPE_PLACE_ONE_DAY ): ?>
								<tr>
									<td>
										<a href="<?= Url ::toRoute( [ '/places/' . $payment[ 'id_spaces' ] ] ) ?>"><?= $payment[ 'spaces_title' ] ?></a>
									</td>
									<td>
										Was purchased ticket <?= $payment[ 'count_tickets' ] ?> for place
									</td>
									<td><?= $payment[ 'amount' ] ?>$</td>
									<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y H:i:s' ) ?></td>
								</tr>
							<?php elseif( $payment[ 'paym_type' ] == UserProfile::TYPE_PLACE_CONF_ROM ): ?>
								<tr>
									<td>
										<a href="<?= Url ::toRoute( [ '/places/' . $payment[ 'id_spaces' ] ] ) ?>"><?= $payment[ 'spaces_title' ] ?></a>
									</td>
									<td>
										<?php if( $payment[ 'count_tickets' ] != 0 ): ?>
											You order conference room on <?= $payment[ 'count_tickets' ] ?> hour
										<?php else: ?>
											You order conference room on the bonus
										<?php endif ?>
									</td>
									<td>
										<?php if( $payment[ 'amount' ] != 0 ): ?>
											<?= $payment[ 'amount' ] ?>$
										<?php else: ?>
											<?= $payment[ 'count_tickets' ] ?> bonus(es)
										<?php endif; ?>
									</td>
									<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y  H:i:s' ) ?></td>
								</tr>
							<?php else: ?>
								<tr>
									<td>
										Membership plans
									</td>
									<td>
										<div>Was purchased <?= $typePayment[ 0 ][ 'plan' ] -> title ?></div>
									</td>
									<td><?= $payment[ 'amount' ] ?>$</td>
									<td><?= $formatter -> asDate( $payment[ 'created_at' ], 'php:d.m.Y  H:i:s' ) ?></td>
								</tr>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<h4>No information</h4>
		<?php endif; ?>
	</div>
	<div class="clearfix"></div>
</div>
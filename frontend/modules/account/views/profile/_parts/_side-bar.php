<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
$name = Yii::$app->user->identity->username;
$last_name = Yii::$app->user->identity->userProfile->last_name;
?>
<div class="profile_main_menu left">
    <div class="name"><?=trim($name." ".$last_name)?></div>
    <div class="divider"></div>
    <a href="<?=Url::toRoute('/account')?>" class="link<?= Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'edit-profile' ? ' active' : '' ?>">Edit profile</a>
    <a href="<?=Url::toRoute('/account/memberships')?>" class="link<?= Yii::$app->controller->id == 'memberships' && Yii::$app->controller->action->id == 'index' ? ' active' : '' ?>">My Membership</a>
    <a href="<?=Url::toRoute(['/account/my-booking'])?>" class="link<?= Yii::$app->controller->id == 'bookings' && Yii::$app->controller->action->id == 'index' ? ' active' : '' ?>">My Booking</a>
    <a href="<?=Url::toRoute(['/account/profile/order-payment'])?>" class="link<?= Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'order-payment' ? ' active' : '' ?>">My order payment</a>
    <a href="<?=Url::toRoute(['/account/my-tickets'])?>" class="link<?= Yii::$app->controller->id == 'tickets' && Yii::$app->controller->action->id == 'index' ? ' active' : '' ?>">My Tickets</a>
    <a href="<?=Url::toRoute(['/account/favorites-events'])?>" class="link<?= Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'favorites-events' ? ' active' : '' ?>">Favorites Events</a>
    <a href="<?=Url::toRoute(['/account/favorites-places'])?>" class="link<?= Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'favorites-places' ? ' active' : '' ?>">Favorites Place</a>
    <a href="<?=Url::toRoute(['/account/profile/security'])?>" class="link<?= Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'security' ? ' active' : '' ?>">Security</a>
    <a href="<?=Url::toRoute(['/login/logout'])?>" class="link">Log Out</a>
</div>

<?php

	use yii\helpers\Url;
	use yii\i18n\Formatter;
	use yii\helpers\StringHelper;

	$formatter = Yii::$app->formatter;
?>
	<div class="event_wrap left" id="event_<?= $model['id'] ?>" style="position: relative;">
		<div class="top_section" style="background: url(/source/img/background_image/origin/<?= $model['image'] ?>) center / cover no-repeat">
			<span class="favourite <?= !empty( $model['id_favorites'] ) ? 'active' : '' ?>" data-id="<?= $model['id'] ?>"></span>
		</div>
		<div class="title"><?= StringHelper::truncate( $model['title'], 40, '...' ) ?></div>
		<div class="description"><?= StringHelper::truncate( strip_tags($model['description']), 70, '...' ) ?></div>
		<div class="date">
			<?php if(Yii::$app->controller->action->id == 'favorites-events'): ?>
				<?= $formatter->asDate( $model['date_event'], 'php:M d' ) ?>
				<?= ' at ' ?>
				<?= $formatter->asTime( $model['time_start_event'], 'php:H.i' ) ?>
				<?= '-' ?>
				<?= $formatter->asTime( $model['time_end_event'], 'php:H.i' ) ?>
				<input id="type_favorite" type="hidden" value="<?= \common\models\Favorites::TYPE_EVENTS ?>">
			<?php else: ?>
				<?= $formatter->asRelativeTime($model['created_at']) ?>
				<input id="type_favorite" type="hidden" value="<?= \common\models\Favorites::TYPE_PLACES ?>">
			<?php endif ?>
		</div>
		<div class="address"><?= $model['address'] ?></div>
		<?php if(Yii::$app->controller->action->id == 'favorites-events'): ?>
			<div class="price"><?= $model['price_ticket_event'] ?></div>
		<?php endif ?>
        <?php if( Yii::$app->controller->action->id == 'favorites-events'): ?>
		    <a href="<?= Url::toRoute( ['/events/view','id'=> $model['id']] ) ?>" class="btn blue_btn">Event Details</a>
        <?php else: ?>
            <a href="<?= Url::toRoute( ['/places/view','id'=> $model['id']] ) ?>" class="btn blue_btn">Place Details</a>
        <?php endif ?>
	</div>
<?php
	//    if(!Yii::$app->user->isGuest && Yii::$app->request->get('sort_event') == 'my-favorite') {
	//        $deleteFavoriteEvent = <<<JS
	//        $(document).ready(function() {
	//            $('.favourite').click(function() {
	//                var getEvent = $(this)
	//                var idFavorite = getEvent.attr('data-id');
	//                var eventId = $('#event_' + idFavorite);
	//                eventId.remove();
	//
	//            });
	//        })
	//JS;
	//        $this->registerJs($deleteFavoriteEvent);
	//    }
	//
	//?>

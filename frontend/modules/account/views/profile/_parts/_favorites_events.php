<?php

	use yii\helpers\Url;
	use yii\i18n\Formatter;
	use yii\helpers\StringHelper;

	$formatter = Yii::$app->formatter;
?>
<?php //dd($model) ?>

	<div class="event_wrap left" id="event_<?= $model['id'] ?>" style="position: relative;">
		<div class="top_section" style="background: url(/img/<?= $model['image'] ?>) center / cover no-repeat">
			<?php if (!Yii::$app->user->isGuest): ?>
				<span class="favourite <?= !empty( $model['favorite_event'] ) ? 'active' : '' ?>"
					  data-id="<?= $model['id'] ?>"></span>
			<?php endif ?>
		</div>
		<div class="title"><?= StringHelper::truncate( $model['title'], 40, '...' ) ?></div>
		<div class="description"><?= StringHelper::truncate( $model['description'], 70, '...' ) ?></div>
		<div class="date">
			<?= $formatter->asDate( $model['date_event'], 'php:M d' ) ?>
			<?= ' at ' ?>
			<?= $formatter->asTime( $model['time_start_event'], 'php:H.i' ) ?>
			<?= '-' ?>
			<?= $formatter->asTime( $model['time_end_event'], 'php:H.i' ) ?>
		</div>
		<div class="address"><?= $model['address'] ?></div>
		<div class="price"><?= $model['price_ticket_event'] ?></div>
		<a href="<?= Url::toRoute( ['/events/view','id'=> $model['id']] ) ?>" class="btn blue_btn">Event Details</a>
	</div>
<?php
	//    if(!Yii::$app->user->isGuest && Yii::$app->request->get('sort_event') == 'my-favorite') {
	//        $deleteFavoriteEvent = <<<JS
	//        $(document).ready(function() {
	//            $('.favourite').click(function() {
	//                var getEvent = $(this)
	//                var idFavorite = getEvent.attr('data-id');
	//                var eventId = $('#event_' + idFavorite);
	//                eventId.remove();
	//
	//            });
	//        })
	//JS;
	//        $this->registerJs($deleteFavoriteEvent);
	//    }
	//
	//?>
<?php
	$decodeData =
	$setFavorite = <<<JS

$(document).ready(function() {
        $('.favourite').click(function() {
            var favourite = $(this);
            var id_event = favourite.attr('data-id');
            $.ajax({
                   url:      '/events/set-favorite',
                   type:     "POST",
                   dataType: 'json',
                   data:     {id_event:id_event},
                   success: function(data) {
                           if(data.status == 'added') {
                               favourite.addClass('active')
                           }
                           if(data.status == 'remove') {
                               favourite.removeClass('active')
                           }
                   }
               }); 
        })
  
})
JS;

	$this->registerJs( $setFavorite );
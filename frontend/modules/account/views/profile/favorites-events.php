<?php


	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use common\models\User;
	use kartik\select2\Select2;
	use dosamigos\ckeditor\CKEditor;
	use yii\helpers\ArrayHelper;
	use yii\web\JsExpression;
	use yii\widgets\ListView;

	/** @var $userProfile \common\models\UserProfile */
	/** @var $user \common\models\User */

	$this->registerJs(
		"$('#datepicker input').datepicker({                    
        format: 'mm/dd/yyyy',
        orientation: 'bottom left',
        inline: false,
        sideBySide: false,
        showWeekDays: false
    });
    $('#userprofile-country_id').change(function(){
        var select = $('#userprofile-city_id');
            select.html(null);
    });
    ",
		\yii\web\View::POS_READY
	);
	if (!empty($userProfile->birthday)) {
		$userProfile->birthday = date('m/d/Y', strtotime($userProfile->birthday));
	}
	$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css');
	$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css');
	$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="wrap">
	<?= $this->render('_parts/_side-bar') ?>


	<div class="edit_box left">
		<div class="title">My Favorites events</div>
        <?= ListView ::widget(
            [
                'dataProvider' => $queryMyFavorite,
                'itemView'     => '_parts/_favorites',
                'emptyText'    => 'Data not found',
                'options' => [
                    'class' => ''
                ]
            ]
        );
        ?>
	</div>


</div>
<div class="clearfix"></div>

<?php
	$decodeData =
	$setFavorite = <<<JS

$(document).ready(function() {
        $('.favourite').click(function() {
            var favourite = $(this);
            var typeFavorite = $('#type_favorite').val();
            var id_post = favourite.attr('data-id');
            $.ajax({
                   url:      '/account/profile/set-favorite',
                   type:     "POST",
                   dataType: 'json',
                   data:     {
                   				id_post:id_post,
                   				typeFavorite:typeFavorite
                   			},
                   success: function(data) {
                           if(data.status == 'added') {
                               favourite.addClass('active')
                           }
                           if(data.status == 'remove') {
                               favourite.removeClass('active')
                           }
                   }
               }); 
        })
  
})
JS;

	$this->registerJs( $setFavorite );

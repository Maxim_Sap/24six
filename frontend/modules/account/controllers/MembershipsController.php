<?php

	namespace frontend\modules\account\controllers;

	use Codeception\Lib\Notification;
	use common\models\BonusPlans;
	use common\models\PaymentHistory;
	use common\models\Plans;
	use common\models\PlansWithBonus;
	use common\models\ResourcesEventsPlaces;
	use common\models\User;
	use common\models\UserAssnPayments;
	use common\models\UserBonus;
	use common\models\UserMainPersonalBonus;
	use common\models\UserProfile;
	use Yii;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use frontend\modules\account\controllers\AccountController;
	use yii\web\BadRequestHttpException;
	use yii\helpers\Json;
	use yii\filters\AccessControl;
	use yii\helpers\Url;
	use common\models\UserSettings;
	use yii\filters\VerbFilter;

	class MembershipsController extends AccountController
	{

		public function beforeAction( $action )
		{
			$my_id = Yii ::$app -> user -> identity -> getId();
			$this -> deleteAllBonuses( $my_id );
			return parent ::beforeAction( $action );
		}

		public function actionIndex()
		{
			$my_id      = Yii ::$app -> user -> identity -> getId();
			$getPlans   = PlansWithBonus ::find() -> with( 'plan' ) -> with( 'bonus' ) -> all();
			$getMyBonus = UserMainPersonalBonus ::getMyBonus( $my_id );

			$myPlans = UserAssnPayments ::getMyAssnPlans( $my_id );

			$checkMyAssnPlan = UserAssnPayments ::checkMyPlans( $my_id );

			$this -> deleteAllBonuses( $my_id );

			return $this -> render(
				'index',
				[
					'checkMyAssnPlan' => $checkMyAssnPlan[ 'row' ],
					'getPlans'        => $getPlans,
					'myPlans'         => $myPlans,
					'getMyBonus'      => $getMyBonus,
				]
			);
		}

		public function actionCancelPlans( $date, $type = NULL )
		{
			$model = UserAssnPayments ::find()
									  -> where( [ 'user_Id' => Yii ::$app -> user -> identity -> getId() ] )
									  -> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
									  -> andWhere( [ 'date_end' => $date ] )
									  -> one();

			$model -> scenario = UserAssnPayments::SCENARIO_UPDATE_STATUS_ASSN_PLANS;
			$model -> status   = ResourcesEventsPlaces::NO_ACTIVE_TICKETS;
			if( $model -> save() )
			{
				if( $type != UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS && $model -> status == ResourcesEventsPlaces::NO_ACTIVE_TICKETS )
				{
					$deleteBonuses                             = UserMainPersonalBonus ::find() -> where( [ 'date_end' => $date ] ) -> one();
					$deleteBonuses -> count_events_week        = 0;
					$deleteBonuses -> count_time_for_massage   = 0;
					$deleteBonuses -> count_time_for_conf_room = 0;
					if( !$deleteBonuses -> save() )
					{
						dd( $deleteBonuses -> errors );
					}
				}
				Yii ::$app -> session -> setFlash( 'message', 'Plans was canceled' );
				return $this -> redirect( '/account/my-memberships' );
			}
			else
			{
				var_dump( $model -> errors );
			}
		}


		function deleteAllBonuses( $user_id )
		{
			$checkMyAssnPlan = UserAssnPayments ::checkMyPlans( $user_id );
			if( empty( $checkMyAssnPlan[ 'row' ] ) )
			{
				if( $checkMyAssnPlan[ 'type' ] == UserProfile::DELETE_ALL )
				{
					$modelDelete = UserMainPersonalBonus ::find() -> where( [ 'user_id' => $user_id ] ) -> all();
					foreach( $modelDelete as $item )
					{
						$model = UserMainPersonalBonus ::findOne( $item -> id );

						$model -> count_events_week        = 0;
						$model -> count_time_for_massage   = 0;
						$model -> count_time_for_conf_room = 0;
						$model -> save();
					}
				}
			}
		}
	}

<?php

	namespace frontend\modules\account\controllers;

	use Codeception\Lib\Notification;
	use common\models\Comments;
	use common\models\Events;
	use common\models\Favorites;
	use common\models\Followings;
	use common\models\Friends;
	use common\models\GeoCountries;
	use common\models\PaymentHistory;
	use common\models\PlansWithBonus;
	use common\models\PopularEvents;
	use common\models\PopularPlaces;
	use common\models\Questions;
	use common\models\SpaceWork;
	use common\models\User;
	use common\models\UserBonus;
	use common\models\UserPosts;
	use common\models\UserProfile;
	use common\models\GeoCities;
	use common\models\Notofications;
	use common\models\LikesPosts;
	use Yii;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use frontend\modules\account\controllers\AccountController;
	use yii\web\BadRequestHttpException;
	use yii\helpers\Json;
	use yii\filters\AccessControl;
	use yii\helpers\Url;
	use common\models\UserSettings;
	use yii\filters\VerbFilter;

	class ProfileController extends AccountController
	{

		public function beforeAction( $action )
		{
			$arrayOfActionsWithDisabledCSRFValidation = [
				'test',
			];
			if( in_array( $action -> id, $arrayOfActionsWithDisabledCSRFValidation ) )
			{
				$this -> enableCsrfValidation = FALSE;
			}
			$session = Yii ::$app -> session;
			$session -> open();
			if( $session -> isActive )
			{
				if( !Yii ::$app -> user -> isGuest )
				{
					$session -> set( 'username', Yii ::$app -> user -> identity -> username );
					$session -> set( 'lastName', Yii ::$app -> user -> identity -> userProfile -> last_name );
					$session -> set( 'userId', Yii ::$app -> user -> identity -> userProfile -> user_id );
					$session -> set( 'liveTime', Yii ::$app -> user -> identity -> userProfile -> created_at );
				}
				else
				{
					$this -> redirect( 'login/login' );
				}
			}
			$session -> close();
			return parent ::beforeAction( $action );
		}

		public function actionIndex()
		{
			return $this -> render( 'index' );
		}

		public function actionEditProfile()
		{

			$model       = Yii ::$app -> user -> identity;
			$userProfile = $model -> userProfile;
			if( Yii ::$app -> request -> post( 'personal_details' ) && Yii ::$app -> request -> post() )
			{
				$model -> scenario = 'update';
				$model_save        = FALSE;
				$profile_save      = FALSE;
				if( $model -> load( Yii ::$app -> request -> post() ) && $model -> save() )
				{
					$model_save = TRUE;
				}
				$userProfileData = Yii ::$app -> request -> post( 'UserProfile' );
				if( !empty( $userProfileData[ 'birthday' ] ) )
				{
					$userProfileData[ 'birthday' ] = date( 'Y-m-d', strtotime( $userProfileData[ 'birthday' ] ) );
				}
				$userProfile -> attributes = $userProfileData;

				//            $userProfile->avatar = $this->UploadImages( $userProfile->avatar, 'avatar' );
				//            $userProfile->logo = $this->UploadImages( $userProfile->logo, 'logo' );
				//$ress = $this->UploadImages($userProfile->background_image,'background_image');
				//            $userProfile->background_image = $this->UploadImages( $userProfile->background_image, 'background_image' );

				if( $userProfile -> save() )
				{
					$profile_save = TRUE;
				}
				else
				{
					dd( $userProfile -> errors );
				}
				if( $model_save && $profile_save )
				{
					Yii ::$app -> session -> setFlash( 'message', 'Your contact information updated successfully' );
					return $this -> refresh();
				}
			}

			$this -> view -> registerCssFile( '/plagins/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css' );
			$this -> view -> registerJsFile( '/plagins/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
			$this -> view -> registerJsFile( '/js/crop_img.js', [ 'depends' => [ \yii\web\JqueryAsset ::className() ] ] );
			//if($userProfile->load($userProfileData,'UserProfile')  && $userProfile->save() ) {

			//}

			return $this -> render(
				'edit-profile',
				[
					'model'       => $model,
					'userProfile' => $userProfile,
				]
			);
		}

		public function actionDeleteAccount()
		{
			$model = User ::findOne( Yii ::$app -> user -> identity -> getId() );
			if( Yii ::$app -> request -> post( 'delete_account' ) )
			{
				if( $dataDelete = Yii ::$app -> request -> post() )
				{
					if( $model -> load( $dataDelete, 'delete_account' ) )
					{
						$model -> scenario = 'update';
						$model -> status   = User::STATUS_DELETED;
						if( $model -> save() )
						{
							Yii ::$app -> user -> logout();
							Yii ::$app -> session -> setFlash( 'message', 'Delete account completed' );
							return $this -> redirect( '/' );
						}
					}
				}
			}
		}

		public function actionChangePassword()
		{
			$model = User ::findOne( Yii ::$app -> user -> identity -> getId() );

			//ловим форму change_password
			if( Yii ::$app -> request -> post( 'change_password' ) && Yii ::$app -> request -> post() )
			{
				$data_pass = Yii ::$app -> request -> post();

				//если оригинальный пароль не пуст
				if( !empty( $data_pass[ 'User' ][ 'password' ] ) )
				{

					// валидируем оригинальный пароль
					if( $model -> validatePassword( $data_pass[ 'User' ][ 'password' ] ) )
					{

						// Сравниваем данные с форм изменения пароля
						// если не пусты
						if( !empty( $data_pass[ 'User' ][ 'new_password' ] ) && !empty( $data_pass[ 'User' ][ 'confirm_new_password' ] ) )
						{
							//если равны
							if( $data_pass[ 'User' ][ 'new_password' ] == $data_pass[ 'User' ][ 'confirm_new_password' ] )
							{

								//генерируем хеш
								$model -> password_hash = Yii ::$app -> security -> generatePasswordHash( $data_pass[ 'User' ][ 'new_password' ] );

								//сохраняем новый пароль

								$model -> scenario = User::SCENARIO_EDIT_PASSW;
								if( $model -> load( $data_pass ) && $model -> save() )
								{
									Yii ::$app -> session -> setFlash( 'message', 'Password changed' );
									return $this -> redirect( '/account' );
								}
							}
							else
							{
								// если пароли не совпадают
								Yii ::$app -> session -> setFlash( 'message', 'Passwords do not match' );
								return $this -> redirect( '/account/profile/security' );
							}
						}
					}
					else
					{
						// если валидация вернула false
						Yii ::$app -> session -> setFlash( 'message', 'Incorrect password' );
						return $this -> redirect( '/account/profile/security' );
					}
				}
			}
		}

		public function actionOrderPayment()
		{
			$my_id               = Yii ::$app -> user -> identity -> getId();
			$queryHistoryPayment = PaymentHistory ::getHistoryPayment( $my_id );
//			dd($queryHistoryPayment);
			return $this -> render(
				'order-payment',
				[
					'queryHistoryPayment' => $queryHistoryPayment,
				]
			);
		}

		public function actionSecurity()
		{
			$model             = Yii ::$app -> user -> identity;
			$model -> scenario = User::SCENARIO_EDIT_PASSW;
			return $this -> render(
				'security',
				[
					'model' => $model,
				]
			);
		}

		public function actionFavoritesEvents()
		{
			$my_favorite     = Favorites ::checkMyFavorite( Yii ::$app -> user -> identity -> getId(), Events ::tableName() );
			$queryMyFavorite = new ActiveDataProvider(
				[
					'query'      => $my_favorite,
					'pagination' => [
						'pageSize' => 6,
					],
				]
			);
			return $this -> render(
				'favorites-events',
				[
					'queryMyFavorite' => $queryMyFavorite,
				]
			);
		}

		public function actionFavoritesPlaces()
		{
			$my_favorite     = Favorites ::checkMyFavorite( Yii ::$app -> user -> identity -> getId(), SpaceWork ::tableName() );
			$queryMyFavorite = new ActiveDataProvider(
				[
					'query'      => $my_favorite,
					'pagination' => [
						'pageSize' => 6,
					],
				]
			);
			return $this -> render(
				'favorites-places',
				[
					'queryMyFavorite' => $queryMyFavorite,
				]
			);
		}

		public function actionSetFavorite()
		{
			$data = [];
			if( Yii ::$app -> request -> isAjax && $setFavorite = Yii ::$app -> request -> post() )
			{
				$my_id = (int)Yii ::$app -> user -> identity -> getId();
				if( $setFavorite[ 'typeFavorite' ] == Favorites::TYPE_PLACES )
				{
					$data = $this -> checkSetFavorite( $my_id, $setFavorite[ 'id_post' ], $setFavorite[ 'typeFavorite' ] );
				}
				if( $setFavorite[ 'typeFavorite' ] == Favorites::TYPE_EVENTS )
				{
					$data = $this -> checkSetFavorite( $my_id, $setFavorite[ 'id_post' ], $setFavorite[ 'typeFavorite' ] );
				}
			}
			return $data;
		}

		public function checkSetFavorite( $user_id, $id_favorite, $type_favorite )
		{
			$status = 'added';
			$result = FALSE;

			if( $type_favorite == Favorites::TYPE_EVENTS )
			{
				$row = PopularEvents ::find()
									 -> where( [ 'user_id' => $user_id ] )
									 -> andWhere( [ 'id_event' => $id_favorite ] )
									 -> one();
			}
			if( $type_favorite == Favorites::TYPE_PLACES )
			{
				$row = PopularPlaces ::find()
									 -> where( [ 'user_id' => $user_id ] )
									 -> andWhere( [ 'id_place' => $id_favorite ] )
									 -> one();
			}
			if( $row == NULL )
			{
				if( $type_favorite == Favorites::TYPE_PLACES )
				{
					$favorite             = new PopularPlaces();
					$favorite -> user_id  = $user_id;
					$favorite -> id_place = $id_favorite;
				}
				if( $type_favorite == Favorites::TYPE_EVENTS )
				{
					$favorite             = new PopularEvents();
					$favorite -> user_id  = $user_id;
					$favorite -> id_event = $id_favorite;
				}
				if( $favorite -> save() )
				{
					Favorites ::setFavorite( $user_id, $id_favorite, $type_favorite );
				}
			}
			else
			{
				if( $row -> delete() )
				{
					$status = 'remove';
					Favorites ::removeFavorite( $user_id, $id_favorite, $type_favorite );
				}
			}
			$returnJson = json_encode(
				[
					'success' => $result,
					'status'  => $status,
				]
			);
			return $returnJson;
		}
	}

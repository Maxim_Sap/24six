<?php

namespace frontend\modules\account\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class AccountController extends Controller
{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action){
                    Yii::$app->session->setFlash('message', 'Please login.');
                    $this->redirect('/login');
                },
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['user','organization','organizationAdmin'],
                    ],
                    /*[
                        'controllers' => ['account/profile'],
                        'actions'=>['index','create','edit','delete','load-image'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    */
                ],
            ],
        ];
    }

    function UploadImages($imageDb,$img_type = 'avatar'){
        switch ($img_type){
            case 'avatar':
                $new_image = \Yii::$app->request->post('avatar');
                $dir        = Yii::getAlias('@avatar');
                break;
            case 'logo':
                $new_image = \Yii::$app->request->post('logo');
                $dir        = Yii::getAlias('@logo');
                break;
            case 'background_image':
                $new_image = \Yii::$app->request->post('background_image');
                $dir        = Yii::getAlias('@background_image');
                break;
        }

        $origin_path = $dir."origin/";
        $thumb_path = $dir."thumb/";
        $icon_path = $dir."icon/";
        //return 1;
        if($new_image == $imageDb){
            return $new_image;
        }
        if (!empty($new_image)) {
            // Сохранение картинки
            $name          = pathinfo($new_image);
            $new_image = $name['filename'] . '.' . $name['extension'];
            // Если картинка новая
            if (!$imageDb and !empty($new_image)) {
                if (!file_exists($origin_path)) {
                    mkdir($origin_path, 0777, TRUE);
                    mkdir($thumb_path, 0777, TRUE);
                    mkdir($icon_path, 0777, TRUE);
                }
            }
            // Если картинку заменили
            if ($imageDb != $new_image) {
                // Удаляем старую
                @unlink($origin_path . $imageDb);
                @unlink($thumb_path . $imageDb);
                @unlink($icon_path . $imageDb);
            }
            @rename(Yii::getAlias('@temp_image') . $new_image, $origin_path . $new_image);

            $file = Yii::getAlias($origin_path . $new_image);
            if (!file_exists($file)) {
                return '';
            }
            $image = Yii::$app->image->load($file);
            switch ($img_type){
                case 'avatar':
                    $image->resize(200, 200, \yii\image\drivers\Image::WIDTH);
                    $image->save($thumb_path . $new_image);
                    $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                    $image->save($icon_path . $new_image);
                    break;
                case 'logo':
                    $image->resize(160, 160, \yii\image\drivers\Image::WIDTH);
                    $image->save($thumb_path . $new_image);
                    $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                    $image->save($icon_path . $new_image);
                    break;
                case 'background_image':
                    $image->resize(1110, 340, \yii\image\drivers\Image::HEIGHT);
                    $image->save($thumb_path . $new_image);
                    break;
            }
        }elseif (!empty($imageDb) and empty($new_image)) {
            // Если картинка была, но ее удалили
            @unlink($origin_path. $imageDb);
            @unlink($thumb_path. $imageDb);
            @unlink($icon_path. $imageDb);
        }

        return $new_image;
    }


}

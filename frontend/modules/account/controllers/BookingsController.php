<?php

	namespace frontend\modules\account\controllers;

	use Codeception\Lib\Notification;
	use common\models\BookingConferenceRoom;
	use common\models\CategoriesToPlase;
	use common\models\ConfRooms;
	use common\models\Favorites;
	use common\models\Plans;
	use common\models\User;
	use common\models\UserProfile;
	use Yii;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use frontend\modules\account\controllers\AccountController;
	use yii\web\BadRequestHttpException;
	use yii\helpers\Json;
	use yii\filters\AccessControl;
	use yii\helpers\Url;
	use common\models\UserSettings;
	use yii\filters\VerbFilter;
	use common\models\ResourcesEventsPlaces;

	class BookingsController extends AccountController
	{

		public function beforeAction( $action )
		{

			return parent ::beforeAction( $action );
		}

		public function actionIndex()
		{
			$my_id                = Yii ::$app -> user -> identity -> getId();
			$myTicketsForSpace    = ResourcesEventsPlaces ::getBookingTickets( $my_id, Favorites::TYPE_PLACES );
			$myTicketsForConfRoom = BookingConferenceRoom ::getTicketsForConfRoom( $my_id );
			$dataTickets          = array_merge( $myTicketsForConfRoom, $myTicketsForSpace );
			$plans                = Plans ::find() -> all();
			return $this -> render(
				'index',
				[
					'dataTickets' => $dataTickets,
					'plans'       => $plans,
				]
			);
		}

		public function actionView( $type_place )
		{

			$getId     = Yii ::$app -> request -> get( 'id' );
			$my_id     = Yii ::$app -> user -> identity -> getId();
			$model = [];
			if(
				$type_place == UserProfile::TYPE_PLACE_ONE_DAY or
				$type_place == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
				$type_place == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE
			)
			{
				$model = ResourcesEventsPlaces ::find()
											   -> where( [ 'id' => $getId ] )
											   -> andWhere( [ 'id_res_type' => Favorites::TYPE_PLACES ] )
											   -> one();

				$data_mass = ResourcesEventsPlaces ::getDataTicketPlace( $getId );
			}
			else
			{
				$data_mass = BookingConferenceRoom::getOneTicketConfRoom( $getId, $my_id );
			}

			return $this -> render(
				'view',
				[
					'getDataTicket' => $data_mass,
					'model'         => $model,
				]
			);
		}

		public function actionPrint( $type_place )
		{

			$getId     = Yii ::$app -> request -> get( 'id' );
			$my_id     = Yii ::$app -> user -> identity -> getId();
			$model = [];
			if(
				$type_place == UserProfile::TYPE_PLACE_ONE_DAY or
				$type_place == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
				$type_place == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE
			)
			{
				$model = ResourcesEventsPlaces ::find()
											   -> where( [ 'id' => $getId ] )
											   -> andWhere( [ 'id_res_type' => Favorites::TYPE_PLACES ] )
											   -> one();

				$data_mass = ResourcesEventsPlaces ::getDataTicketPlace( $getId );
			}
			else
			{
				$data_mass = BookingConferenceRoom::getOneTicketConfRoom( $getId, $my_id );
			}

			$this->layout = '/ticket-print';

			return $this -> render(
				'print',
				[
					'getDataTicket' => $data_mass,
					'model'         => $model,
				]
			);
		}

		public function actionDeskTickets(  )
		{
			$post = Yii ::$app -> request -> post();
			$model = ResourcesEventsPlaces::findOne( $post[ 'ResourcesEventsPlaces' ][ 'id' ] );
			$model -> deck_ticket = $post[ 'ResourcesEventsPlaces' ][ 'deck_ticket' ];
			if( $model -> save() )
			{
				Yii ::$app -> session -> setFlash( 'message' , 'Data entered');
				return $this -> redirect( [ 'bookings/view/','id' => $post[ 'ResourcesEventsPlaces' ][ 'id' ], 'type_place' => $post[ 'ResourcesEventsPlaces' ][ 'type_place' ]  ] );
			}
			else
			{
				dd( $model -> errors );
			}
		}

	}

<?php

	namespace frontend\modules\account\controllers;

	use Codeception\Lib\Notification;
	use common\models\Events;
	use common\models\Favorites;
	use frontend\components\helpers\CreatePdf;
	use kartik\mpdf\Pdf;
	use common\models\User;
	use Yii;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use frontend\modules\account\controllers\AccountController;
	use yii\web\BadRequestHttpException;
	use yii\helpers\Json;
	use yii\filters\AccessControl;
	use yii\helpers\Url;
	use yii\filters\VerbFilter;
	use common\models\ResourcesEventsPlaces;

	class TicketsController extends AccountController
	{

		public function beforeAction( $action )
		{

			return parent ::beforeAction( $action );
		}

		public function actionIndex()
		{
			$my_id     = Yii ::$app -> user -> identity -> getId();
			$myTickets = ResourcesEventsPlaces ::getTickets( $my_id, Favorites::TYPE_EVENTS );
			return $this -> render(
				'index',
				[
					'myTickets' => $myTickets,
				]
			);
		}

		public function actionView()
		{
			$getId    = Yii ::$app -> request -> get( 'id' );
			$rowCheck = ResourcesEventsPlaces :: find() -> where( [ 'id' => $getId ] ) -> one();
			if( empty( $rowCheck ) && !isset( $rowCheck ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'There is no such ticket' );
				return $this -> redirect( '/account/my-tickets' );
			}
			else
			{
				$model = ResourcesEventsPlaces ::findOne( $getId );
				if( $data_ticket = Yii ::$app -> request -> post() )
				{
					if( $model -> load( $data_ticket ) )
					{
						$model -> scenario    = ResourcesEventsPlaces::SCENARIO_SET_DECK_TICKET;
						$model -> deck_ticket = $data_ticket[ 'ResourcesEventsPlaces' ][ 'deck_ticket' ];
						if( $model -> save() )
						{
							Yii ::$app -> session -> setFlash( 'message', 'Data for ticket was save' );
							return $this -> redirect( '/account/my-tickets/' . $getId );
						}
					}
					else
					{
						Yii ::$app -> session -> setFlash( 'message', 'The field is not filled' );
						return $this -> redirect( '/account/my-tickets/' . $getId );
					}
				}
				$getDataTicket = ResourcesEventsPlaces ::getDataTicketEvent( $getId );
			}
			return $this -> render(
				'view',
				[
					'getDataTicket' => $getDataTicket,
					'model'         => $model,
				]
			);
		}

		public function actionPrint($id)
		{
			$rowCheck = ResourcesEventsPlaces :: find() -> where( [ 'id' => $id ] ) -> one();
			if( empty( $rowCheck ) && !isset( $rowCheck ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'There is no such ticket' );

				return $this -> redirect( '/account/my-tickets' );
			}else{

				$model = ResourcesEventsPlaces ::findOne( $id );
				if( $data_ticket = Yii ::$app -> request -> post() )
				{

					Yii ::$app -> session -> setFlash( 'message', 'The field is not filled' );
					return $this -> redirect( '/account/print/' . $id );

				}
				$getDataTicket = ResourcesEventsPlaces ::getDataTicketEvent( $id );

			}

			$this->layout = '/ticket-print';

			return $this -> render(
				'print',
				[
					'getDataTicket' => $getDataTicket,
				]
			);
		}

	}

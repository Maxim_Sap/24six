<?php

	namespace frontend\modules\account\controllers;

	//	use Codeception\Events;
	use common\models\BookingConferenceRoom;
	use common\models\Events;
	use common\models\BonusPlans;
	use common\models\Favorites;
	use common\models\PaymentHistory;
	use common\models\UserAssnPayments;
	use common\models\UserBonus;
	use common\models\UserMainPersonalBonus;
	use Yii;
	use Codeception\Lib\Notification;
	use common\models\User;
	use common\models\UserProfile;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use frontend\modules\account\controllers\AccountController;
	use yii\web\BadRequestHttpException;
	use yii\helpers\Json;
	use yii\filters\AccessControl;
	use yii\helpers\Url;
	use yii\filters\VerbFilter;
	use Stripe\Stripe;
	use Stripe\Charge;
	use common\models\ResourcesEventsPlaces;

	class PaymentsController extends AccountController
	{

		public function beforeAction( $action )
		{
			$arrayOfActionsWithDisabledCSRFValidation = [
				'add-funds-by-stripe',
				'add-funds-by-stripe-event',
				'add-funds-by-stripe-place',
				'update-my-bonuses-payment',
				'update-bonuses-free-hours',
			];
			if( in_array( $action -> id, $arrayOfActionsWithDisabledCSRFValidation ) )
			{
				$this -> enableCsrfValidation = FALSE;
			}
			return parent ::beforeAction( $action );
		}

		//		###########!##################
		public function actionIndex( $type )
		{
			if( !in_array(
				$type,
				[
					UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS,
					UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP,
					UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP,
				]
			)
			)
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( '/' );
			}
			$stripe_conf = Yii ::$app -> params[ 'stripe' ];

			//check user current paym plan
			$user_id        = Yii ::$app -> user -> identity -> id;
			$user_assn_paym = UserAssnPayments ::find()
											   -> where( [ 'user_Id' => $user_id ] )
											   -> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
											   -> orderBy( 'date_end ASC' )
											   -> one();
			return $this -> render(
				'index',
				[
					'stripe_conf'    => $stripe_conf,
					'type'           => $type,
					'user_assn_paym' => $user_assn_paym,
				]
			);
		}

		//		###########!##################
		public function actionIndexPlace( $type )
		{
			$getData                = Yii :: $app -> request -> get();
			if( empty( $getData[ 'date' ] ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'You did not enter any data' );
				return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
			}
			$user_model             = Yii :: $app -> user -> identity;
			$checkMyBonus           = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			$checkCountSpaceInPlace = \common\models\SpaceWork ::findOne( $getData[ 'id_place' ] );
			if(
					UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
					UserProfile::TYPE_PLACE_SEPARATE_SPACE or
					UserProfile::TYPE_PLACE_ONE_DAY
			)
			{
				if( $getData[ 'count_tickets' ] <= 0 )
				{
					Yii ::$app -> session -> setFlash( 'message', 'You did not enter any data' );
					return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
				}
			}
			if( !in_array(
				$type,
				[
					UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE,
					UserProfile::TYPE_PLACE_SEPARATE_SPACE,
					UserProfile::TYPE_PLACE_CONF_ROM,
					UserProfile::TYPE_PLACE_ONE_DAY,
				]
			)
			)
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( '/' );
			}

			if( $getData[ 'type' ] == UserProfile::TYPE_PLACE_CONF_ROM )
			{
				if( !isset( $getData[ 'from' ] ) && !isset( $getData[ 'to' ] ) )
				{
					Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
					return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
				}
				else
				{
					if( $getData[ 'from' ] == $getData[ 'to' ] )
					{
						Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
						return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
					}
				}
				if( isset( $getData[ 'free_tickets_value' ] ) )
				{
					if( $getData[ 'free_tickets_value' ] <= 0 )
					{
						if( $getData[ 'count_tickets' ] <= 0 )
						{
							Yii ::$app -> session -> setFlash( 'message', 'You did not specify the number of tickets' );
							return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
						}
					}
					else
					{

						if( $getData[ 'free_tickets_value' ] > $checkMyBonus[ 0 ] -> count_time_for_conf_room )
						{
							Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
							return $this -> redirect( '/' );
						}
						//				if( $getData[ 'free_tickets_value' ] <= 0 )
						//				{
						//					Yii ::$app -> session -> setFlash( 'message', 'You did not specify the number of tickets' );
						//					return $this -> redirect( '/places/' . $getData[ 'id_place' ] );
						//				}
					}
				}

			}

			if( $getData[ 'count_tickets' ] > $checkCountSpaceInPlace -> working_places )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
				return $this -> redirect( '/' );
			}
			$stripe_conf = Yii ::$app -> params[ 'stripe' ];

			return $this -> render(
				'index-place',
				[
					'stripe_conf' => $stripe_conf,
					'type'        => $type,
				]
			);
		}

		//		###########!##################
		public function actionIndexEvent( $type )
		{
			$getData                = Yii ::$app -> request -> get();
			$user_model             = Yii :: $app -> user -> identity;
			$checkMyBonus           = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			$checkCountSpaceInEvent = \common\models\Events ::findOne( $getData[ 'id_event' ] );

			if( !in_array( $type, [ UserProfile::TYPE_PAYMENT_TICKET, ] ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( '/' );
			}
			if( !isset( $getData[ 'count_free_tickets' ] ) )
			{
				if( $getData[ 'count_tickets' ] <= 0 )
				{
					Yii ::$app -> session -> setFlash( 'message', 'You did not specify the number of tickets' );
					return $this -> redirect( '/events/' . $getData[ 'id_event' ] );
				}
			}
			else
			{
				if( $getData[ 'count_free_tickets' ] > $checkMyBonus[ 0 ] -> count_events_week )
				{
					Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
					return $this -> redirect( '/' );
				}
				//				if( $getData[ 'count_free_tickets' ] <= 0 )
				//				{
				//					Yii ::$app -> session -> setFlash( 'message', 'You did not specify the number of tickets' );
				//					return $this -> redirect( '/events/' . $getData[ 'id_place' ] );
				//				}
			}
			if( $getData[ 'count_tickets' ] > $checkCountSpaceInEvent -> max_spaces )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
				return $this -> redirect( '/' );
			}
			$stripe_conf = Yii ::$app -> params[ 'stripe' ];
			return $this -> render(
				'index-event',
				[
					'stripe_conf' => $stripe_conf,
					'type'        => $type,
				]
			);
		}

		//		###########!##################
		public function actionAddFundsByStripe()
		{
			$post         = Yii ::$app -> request -> post();
			$user_model   = Yii ::$app -> user -> identity;
			$payment_type = isset( $post[ 'type' ] ) && in_array(
				$post[ 'type' ],
				[
					(int)UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS,
					(int)UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP,
					(int)UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP,
				]
			) ? $post[ 'type' ] : NULL;

			if( !isset( $post[ 'stripeToken' ] ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Stripe Token' );
				return $this -> redirect( '/' );
			}

			if( !$payment_type )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( Yii ::$app -> request -> referrer );
			}

			$stripe_conf = Yii ::$app -> params[ 'stripe' ];
			Stripe ::setApiKey( $stripe_conf[ 'api_key' ] );

			switch( $payment_type )
			{
				case UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS:
					$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
					break;
				case UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP:
					$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP );
					break;
				case UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP:
					$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP );
					break;
				default:
					$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
			}
			try
			{
				$mass_plan =
					[
						'price' => $plan[ 0 ][ 'plan' ] -> price,
						'title' => $plan[ 0 ][ 'plan' ] -> title,
					];

				$data = [
					'price'       => str_replace([',','.'],'',$mass_plan[ 'price' ]),
					'stripeToken' => $post[ 'stripeToken' ],
					'user_id'     => $user_model -> id,
					'email'       => $user_model -> email,
					'title'       => $mass_plan[ 'title' ],
				];
				//dd($data);

				//#########!###########
				$this -> payData( $data );
				//#########!###########

				if
				(
					$payment_type == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS or
					$payment_type == UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP or
					$payment_type == UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP
				)
				{
					if( isset( $post[ 'continue_plan' ] ) && !empty( $post[ 'continue_plan' ] ) )
					{
						if( $post[ 'type' ] == UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS )
						{
							UserAssnPayments ::addContinuePlan( $user_model -> id, $payment_type );
							PaymentHistory ::addFunds( $user_model -> id, $payment_type, $mass_plan[ 'price' ] );
						}
						else
						{
							UserAssnPayments ::addContinuePlan( $user_model -> id, $payment_type );
							UserMainPersonalBonus ::addBonus( $user_model -> id, $payment_type,$post[ 'date_end' ] );
							PaymentHistory ::addFunds( $user_model -> id, $payment_type, $mass_plan[ 'price' ] );
						}
					}
					else
					{
						if( $payment_type == UserProfile::TYPE_PAYMENT_GOLD_MEMBERSHIP )
						{
							UserMainPersonalBonus ::addBonus( $user_model -> id, $payment_type );
						}
						if( $payment_type == UserProfile::TYPE_PAYMENT_SILVER_MEMBERSHIP )
						{
							UserMainPersonalBonus ::addBonus( $user_model -> id, $payment_type );
						}
						UserAssnPayments ::addPayPlan(
							$user_model -> id,
							$payment_type,
							isset( $post[ 'date_end' ] ) ? $post[ 'date_end' ] : NULL,
							$post[ 'my_assn_type' ]
						);
						PaymentHistory ::addFunds( $user_model -> id, $payment_type, $mass_plan[ 'price' ] );
						UserBonus ::addBonusForUser( $user_model -> id, $payment_type );
					}
				}

				$message = "Your payment was successful. Your bought plan: " . $mass_plan[ 'title' ];
			}
			catch( BadRequestHttpException $e )
			{
				$message = $e -> getMessage();
			}

			Yii ::$app -> session -> setFlash( 'message', $message );
			return $this -> redirect( '/account/memberships' );
		}





		public function actionUpdateMyBonusesPayment()
		{

			$post         = Yii ::$app -> request -> post();
			$user_model   = Yii ::$app -> user -> identity;
			$checkMyBonus           = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			if( $post[ 'free_tickets_value' ] > $checkMyBonus[0] -> count_events_week )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
				return $this -> redirect( '/' );
			}
			try
			{
				UserMainPersonalBonus ::updateMyBonus( $user_model -> id, $post[ 'free_tickets_value' ] );
				ResourcesEventsPlaces ::addResource( $user_model -> id, $post[ 'id_event' ], Favorites::TYPE_EVENTS, $post[ 'free_tickets_value' ] );
				PaymentHistory ::addFunds( $user_model -> id, UserProfile::TYPE_PAYMENT_FREE_TICKET, 0, $post[ 'free_tickets_value' ], $post[ 'id_event' ] );
			}
			catch( BadRequestHttpException $e )
			{
				$message = $e -> getMessage();
			}
			$message = "Your used the bonus tickets.";
			Yii ::$app -> session -> setFlash( 'message', $message );
			return $this -> redirect( '/account/tickets' );
		}


		public function actionUpdateBonusesFreeHours()
		{
			$post         = Yii ::$app -> request -> post();
			$user_model   = Yii ::$app -> user -> identity;
			$checkMyBonus           = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			if( $post[ 'free_tickets_value' ] == 0 && $post[ 'to' ] == 0 && $post[ 'from' ] == 0 )
			{
				Yii ::$app -> session -> setFlash( 'message', 'You have not entered any data' );
				return $this -> redirect( '/' );
			}
			if( $post[ 'free_tickets_value' ] > $checkMyBonus[0] -> count_time_for_conf_room )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Data' );
				return $this -> redirect( '/' );
			}

			$time_array = [
				'from'      => $post[ 'from' ],
				'to'        => $post[ 'to' ],
				'different' => $post[ 'free_tickets_value' ],
			];

			UserMainPersonalBonus ::updateMyBonusConferenceRoom( $user_model -> id, $post[ 'free_tickets_value' ] );
			BookingConferenceRoom ::addFundsConfRoom( $post[ 'conf_room_id' ], $user_model -> id, $post[ 'date' ], $time_array );
			PaymentHistory ::addFunds(
				$user_model -> id,
				$post[ 'type' ],
				0,
				$post[ 'free_tickets_value' ],
				$post[ 'id_place' ]
			);
			$message = "You used the bonus time for a conference room";
			Yii ::$app -> session -> setFlash( 'message', $message );
			return $this -> redirect( '/account/bookings' );
		}


		//		###########!##################
		public function actionAddFundsByStripeEvent()
		{
			$post         = Yii ::$app -> request -> post();
			$user_model   = Yii ::$app -> user -> identity;
			$checkMyBonus = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			$payment_type = isset( $post[ 'type' ] ) && in_array( $post[ 'type' ], [ (int)UserProfile::TYPE_PAYMENT_TICKET ] ) ? $post[ 'type' ] : NULL;

			if( !isset( $post[ 'stripeToken' ] ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Stripe Token' );
				return $this -> redirect( '/' );
			}

			if( !$payment_type )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( Yii ::$app -> request -> referrer );
			}

			$stripe_conf = Yii ::$app -> params[ 'stripe' ];
			Stripe ::setApiKey( $stripe_conf[ 'api_key' ] );

			switch( $payment_type )
			{
				case UserProfile::TYPE_PAYMENT_TICKET:
					$plan = UserProfile ::payment_tickets( $post[ 'id_event' ], UserProfile::TYPE_PAYMENT_TICKET );
					break;
				default:
					$plan = UserProfile ::getPlansForFront( UserProfile::TYPE_PAYMENT_DAILY_GUEST_PASS );
			}
			try
			{
				$data = [
					'price'       => str_replace([',','.'],'',$plan[ 'price_ticket_event' ]),
					'stripeToken' => $post[ 'stripeToken' ],
					'user_id'     => $user_model -> id,
					'email'       => $user_model -> email,
					'title'       => $plan[ 'title' ],
				];

				//#########!###########
				$this -> payData( $data );
				//#########!###########
				if( isset( $post[ 'count_free_tickets' ] ) )
				{
					UserMainPersonalBonus ::updateMyBonus( $user_model -> id, $post[ 'count_free_tickets' ] );
					// Для бесплатных билетов
					ResourcesEventsPlaces ::addResource(
						$user_model -> id,
						$post[ 'id_event' ],
						Favorites::TYPE_EVENTS,
						$post[ 'count_free_tickets' ],
						'',
						$post[ 'date_event' ]
					);
					PaymentHistory ::addFunds( $user_model -> id, UserProfile::TYPE_PAYMENT_FREE_TICKET, 0, $post[ 'count_free_tickets' ], $post[ 'id_event' ] );
					//Для обычных билетов с оплатой
					if( !empty( $post[ 'count_tickets' ] ) )
					{
						ResourcesEventsPlaces ::addResource(
							$user_model -> id,
							$post[ 'id_event' ],
							Favorites::TYPE_EVENTS,
							$post[ 'count_tickets' ],
							'',
							$post[ 'date_event' ]
						);
						PaymentHistory ::addFunds( $user_model -> id, $payment_type, $plan[ 'price_ticket_event' ], $post[ 'count_tickets' ], $post[ 'id_event' ] );
					}
				}
				else
				{
					ResourcesEventsPlaces ::addResource(
						$user_model -> id,
						$post[ 'id_event' ],
						Favorites::TYPE_EVENTS,
						$post[ 'count_tickets' ],
						'',
						$post[ 'date_event' ]
					);
					PaymentHistory ::addFunds( $user_model -> id, $payment_type, $plan[ 'price_ticket_event' ], $post[ 'count_tickets' ], $post[ 'id_event' ] );
				}

				$message = "Your payment was successful";
			}
			catch( BadRequestHttpException $e )
			{
				$message = $e -> getMessage();
			}

			Yii ::$app -> session -> setFlash( 'message', $message );
			return $this -> redirect( '/account/tickets' );
		}



		//		###########!##################
		public function actionAddFundsByStripePlace()
		{
			$post         = Yii ::$app -> request -> post();
			$user_model   = Yii ::$app -> user -> identity;
			$payment_type = isset( $post[ 'type' ] ) && in_array(
				$post[ 'type' ],
				[
					strip_tags( (string)UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE ),
					strip_tags( (string)UserProfile::TYPE_PLACE_SEPARATE_SPACE ),
					strip_tags( (string)UserProfile::TYPE_PLACE_CONF_ROM ),
					strip_tags( (string)UserProfile::TYPE_PLACE_ONE_DAY ),
				]
			) ? $post[ 'type' ] : NULL;

			$checkMyBonus           = UserMainPersonalBonus ::checkMyBonus( $user_model -> id );
			$checkCountSpaceInPlace = \common\models\SpaceWork ::findOne( $post[ 'id_place' ] );

			if( !isset( $post[ 'stripeToken' ] ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong Stripe Token' );
				return $this -> redirect( '/' );
			}

			if( !$payment_type )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Wrong plan type' );
				return $this -> redirect( Yii ::$app -> request -> referrer );
			}

			$stripe_conf = Yii ::$app -> params[ 'stripe' ];
			Stripe ::setApiKey( $stripe_conf[ 'api_key' ] );

			switch( $payment_type )
			{
				case UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE :
					$plan  = UserProfile ::payment_tickets( $post[ 'id_place' ], UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE );
					$price = $plan[ 'unfixiable_price' ];
					break;
				case UserProfile::TYPE_PLACE_SEPARATE_SPACE:
					$plan  = UserProfile ::payment_tickets( $post[ 'id_place' ], UserProfile::TYPE_PLACE_SEPARATE_SPACE );
					$price = $plan[ 'separate_price' ];
					break;
				case UserProfile::TYPE_PLACE_CONF_ROM :
					$plan  = UserProfile ::payment_tickets( $post[ 'id_place' ], UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE );
					$price = $plan[ 'one_day_price' ];
					break;
				case UserProfile::TYPE_PLACE_ONE_DAY:
					$plan  = UserProfile ::payment_tickets( $post[ 'id_place' ], UserProfile::TYPE_PLACE_ONE_DAY );
					$price = $plan[ 'one_day_price' ];
					break;
			}
			try
			{
				$mass_plan =
					[
						'price' => $price,
						'title' => $plan[ 'title' ],
					];

				$data = [
					'price'       => str_replace([',','.'],'',$mass_plan[ 'price' ]),
					'stripeToken' => $post[ 'stripeToken' ],
					'user_id'     => $user_model -> id,
					'email'       => $user_model -> email,
					'title'       => $mass_plan[ 'title' ],
				];

				//#########!###########
				$this -> payData( $data );
				//#########!###########

				if
				(
					$payment_type == UserProfile::TYPE_PLACE_UNFIXIABLE_SPACE or
					$payment_type == UserProfile::TYPE_PLACE_SEPARATE_SPACE or
					$payment_type == UserProfile::TYPE_PLACE_ONE_DAY
				)
				{
					ResourcesEventsPlaces ::addResource
					(
						$user_model -> id,
						$post[ 'id_place' ],
						Favorites::TYPE_PLACES,
						$post[ 'count_tickets' ],
						$post[ 'type' ],
						$post[ 'date' ]
					);
					PaymentHistory ::addFunds(
						$user_model -> id,
						$payment_type,
						$mass_plan[ 'price' ],
						$post[ 'count_tickets' ],
						$post[ 'id_place' ]
					);
				}
				else if( UserProfile::TYPE_PLACE_CONF_ROM )
				{
					if( isset( $post[ 'free_hours_in_room' ] ) )
					{
						$total = $post[ 'count_tickets' ] + $post[ 'free_hours_in_room' ];
					}
					else
					{
						$total = $post[ 'count_tickets' ];
					}
					$time_array = [
						'from'      => $post[ 'from' ],
						'to'        => $post[ 'to' ],
						'different' => $total,
					];
					if( !empty( $post[ 'free_hours_in_room' ] ) && isset( $post[ 'free_hours_in_room' ] ) )
					{
						UserMainPersonalBonus ::updateMyBonusConferenceRoom( $user_model -> id, $post[ 'free_hours_in_room' ] );
					}
					BookingConferenceRoom ::addFundsConfRoom( $post[ 'conf_room_id' ], $user_model -> id, $post[ 'date' ], $time_array );
					PaymentHistory ::addFunds(
						$user_model -> id,
						$payment_type,
						(int)$post[ 'amount' ],
						$total,
						$post[ 'id_place' ]
					);
				}
				$message = "Your payment was successful";
			}
			catch( BadRequestHttpException $e )
			{
				$message = $e -> getMessage();
			}

			Yii ::$app -> session -> setFlash( 'message', $message );
			return $this -> redirect( '/account/bookings' );
		}

		protected function payData( $data )
		{
			return Charge ::create(
				[
					"amount"      => str_replace([',','.'],'',$data[ 'price' ]), /*. "00",*/// in cent
					"currency"    => 'usd',
					"card"        => $data[ 'stripeToken' ],
					"metadata"    => [
						"user_id"   => $data[ 'user_id' ],
						'email'     => $data[ 'email' ],
						'plan_type' => $data[ 'title' ],
					],
					"description" => 'User ' . $data[ 'email' ] . " bought plan: " . $data[ 'title' ],
				]
			);
		}
	}

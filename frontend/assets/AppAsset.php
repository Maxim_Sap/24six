<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        '//fonts.googleapis.com/css?family=Oswald:600',
        'css/normalize.css',
        'css/header.css',
        'css/style.css',
    ];
    public $js = [
        'js/custom.js',
		//'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => [],
            'js' => [],
        ];
    }
}

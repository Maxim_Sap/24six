<?php

namespace frontend\controllers;


use common\models\UserProfile;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Yii;
use yii\web\Controller;
use frontend\models\LoginForm;
use frontend\models\SignupForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


class LoginController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    $this->goBack();
                },
                'rules'        => [
                    [
                        'allow'   => TRUE,
                        'actions' => [
                            'login',
                            'signup',
                            'check-email-link',
                            'request-password-reset',
                            'reset-password',
                        ],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow'   => TRUE,
                        'actions' => ['logout'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSignup()
    {

        $model = new SignupForm();
        $UserProfile = new UserProfile();
        $this->layout = "withiout_header_footer";
        if (\Yii::$app->request->isAjax && $model->load($_POST)) {  // если получаем AJAX и POST запрос
            \Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model); // выполняем валидацию формы
        }

        if (Yii::$app->request->post('SignupForm')) {

            $model->attributes = Yii::$app->request->post('SignupForm');
            /*$model->validate();
            $model->signup();
            var_dump($model->errors);die;*/
            if ($model->validate() and $result = $model->signup()) {
                if ($result['success']) {
                    if (!User::isPasswordResetTokenValid($result['user']->password_reset_token)) {
                        $result['user']->generatePasswordResetToken();
                        if (!$result['user']->save()) {
                            Yii::$app->session->setFlash('message', Yii::t('frontend', 'Error saving user'));

                            return $this->goHome();
                        }
                    }
                } else {
                    //$model->validate();
                    Yii::$app->session->setFlash('message', Yii::t('frontend', 'Validation error'));

                    return $this->goHome();
                }

                $emailSendSuccessfully = $this->SendConfirmLinkToEmail($result['user']);
                if ($emailSendSuccessfully) {
                    Yii::$app->session->setFlash('message', Yii::t('frontend', 'To complete the registration you need to click on the link sent to the specified mail'));

                    return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('message', Yii::t('frontend', 'Error sending mail'));

                    return $this->goHome();
                }
            }else{
//                var_dump($model->errors);
				Yii::$app->session->setFlash('message', Yii::t('frontend', 'This email already exists'));
				return $this->redirect('/signup');
            }
            Yii::$app->session->setFlash('message', Yii::t('frontend', 'Form submission error'));

            return $this->goHome();
        }


        $js_script = "$(document).ready(function(){
		    $('.datepicker').datepicker({
		        changeMonth: true,
		        changeYear: true
		    });
		});";
        //$this->view->registerJs($js_script, Yii\web\View::POS_READY);
        return $this->render('/site/signup', [
            'model' => $model,
            'UserProfile' => $UserProfile,
        ]);

    }

    public function SendConfirmLinkToEmail($user)
    {
        $ress = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'registration-html', 'text' => 'registration-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['noreplyEmail'] =>Yii::$app->params['siteName']." - robot"])
            ->setTo($user->email)
            ->setSubject('Registration on ' . Yii::$app->params['siteName'])
            ->setCharset('UTF-8')
            ->send();

        return $ress;
    }

    public function actionCheckEmailLink()
    {
        $token = Yii::$app->request->get('token');
        try {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamExceptio(Yii::t('frontend', 'Password reset token cannot be blank.'));
            }

            if (!$user = User::findByPasswordResetToken($token)) {
                throw new InvalidParamException(Yii::t('frontend', 'Wrong password reset token.'));
            }
        } catch (InvalidParamException $e) {
            Yii::$app->session->setFlash('message', $e->getMessage());

            return $this->goHome();
        }

        $user->status = User::STATUS_ACTIVE;
        $user->removePasswordResetToken();
        $user->auth_key       = \Yii::$app->security->generateRandomString();
        $user->last_activity  = time();
        $user->token_end_date = time() + Yii::$app->params['token_time'];

        if (!$user->save()) {
            //dd($user->errors);
            Yii::$app->session->setFlash('message', Yii::t('frontend', 'User profile error'));

            return $this->goHome();
        }
        Yii::$app->user->login($user, 0);
        $role = Yii::$app->authManager->getRolesByUser($user->id);

        if (isset($role['admin'])) {
            return $this->redirect('/admin');
        } elseif (isset($role[User::ROLE_NAME_USER])) {
            return $this->redirect('/account');
        } elseif (isset($role[User::ROLE_NAME_ORGANIZATION])) {
            //return $this->redirect('/organization-account');
            return $this->redirect('/account');
        } elseif (isset($role[User::ROLE_NAME_ORGANIZATION_ADMIN])) {
            //return $this->redirect('/organization-account');
            return $this->redirect('/account');
        } elseif (isset($role[User::ROLE_NAME_CONTENT_MANAGER])) {
            return $this->redirect('/admin');
        } elseif (isset($role[User::ROLE_NAME_ADMIN])) {
            return $this->redirect('/admin');
        } elseif (isset($role[User::ROLE_NAME_SUPER_ADMIN])) {
            return $this->redirect('/admin');
        } else {
            Yii::$app->session->setFlash('message', Yii::t('frontend', 'Something wrong with user role'));

            return $this->goHome();
        }

    }

    function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = "withiout_header_footer";
        $model = new LoginForm();
        if ($post = Yii::$app->request->post('LoginForm')) {
            $model->attributes = $post;
            $model->remember = isset($post['remember']) ? true : false;
            if ($model->validate()) {
                $user = $model->getUser();
                switch ($user->status) {
                    case User::STATUS_NO_ACTIVE :
                        Yii::$app->session->setFlash('message', Yii::t('frontend', 'Your account not active yet'));

                        return $this->goHome();
                        break;
                    case User::STATUS_DELETED :
                        Yii::$app->session->setFlash('message', Yii::t('frontend', 'Your account was deleted. Please contact to site support.'));

                        return $this->goHome();
                        break;
                }

                if ($model->remember) { // checkbox remember

                    $user->auth_key = \Yii::$app->security->generateRandomString();
                    $user->save();
                }

                Yii::$app->user->login($model->getUser(), $model->remember ? 3600 * 24 * 30 : 0);
                // redirect
                $role = Yii::$app->authManager->getRolesByUser($model->getUser()->id);
                if (isset($role['admin'])) {
                    return $this->redirect('/admin');
                } elseif (isset($role[User::ROLE_NAME_USER])) {
                    return $this->redirect('/account');
                } elseif (isset($role[User::ROLE_NAME_ORGANIZATION])) {
                    //return $this->redirect('/organization-account');
                    return $this->redirect('/account');
                } elseif (isset($role[User::ROLE_NAME_ORGANIZATION_ADMIN])) {
                    //return $this->redirect('/organization-account');
                    return $this->redirect('/account');
                } elseif (isset($role[User::ROLE_NAME_CONTENT_MANAGER])) {
                    return $this->redirect('/admin');
                } elseif (isset($role[User::ROLE_NAME_ADMIN])) {
                    return $this->redirect('/admin');
                } elseif (isset($role[User::ROLE_NAME_SUPER_ADMIN])) {
                    return $this->redirect('/admin');
                } else {
                    Yii::$app->session->setFlash('message', Yii::t('frontend', 'Something wrong with user role'));

                    return $this->goHome();
                }
            } else {
                //var_dump($model->errors);die;
                Yii::$app->session->setFlash('message', 'Login or password wrong.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('message', Yii::t('frontend', 'Login or password wrong.'));
        }

        return $this->render('/site/login', [
            'model' => $model,
        ]);

    }

    function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();

            return $this->goHome();
        }
    }

    function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        $this->layout = "withiout_header_footer";
        if (\Yii::$app->request->post()) {
            $model->attributes = \Yii::$app->request->post('PasswordResetRequestForm');
            if ($model->validate()) {
                $user        = User::findOne(['email' => $model->email]);
                //$user->reset = \Yii::$app->security->generateRandomString();
                $user->generatePasswordResetToken();
                if ($user->save()) {
                    //$link = 'https://' . $_SERVER['SERVER_NAME'] . '/login/reset-password/?mail=' . $model->email . '&str=' . $user->reset;
                    Yii::$app->session->setFlash('message', Yii::t('frontend', 'Request has been sent. Check your mail.'));
                    Yii::$app->mailer->compose(
                        ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                        ['user' => $user]
                    )
                        ->setTo($model->email)
                        ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->params['siteName'] . ' - robot'])
                        ->setSubject(Yii::$app->params['siteName'] . ' - '.Yii::t('frontend', 'Request for password recovery'))
                        ->send();
                }
            }else{
                Yii::$app->session->setFlash('message', 'email not found');
                return $this->refresh();
            }
        }

        return $this->render('/site/requestPasswordResetToken', compact('model'));
    }

    function actionResetPassword($token)
    {

        $this->layout = "withiout_header_footer";
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('message', 'New password saved. Please login');

            return $this->goHome();
        }

        return $this->render('/site/resetPassword', [
            'model' => $model,
        ]);



        $model = new ChangeForm();

        if (\Yii::$app->request->post()) {
            $model->attributes = \Yii::$app->request->post('ChangeForm');
            if ($model->validate()) {
                $user = Users::findOne(['email' => $model->email]);
                if ($user) {
                    $user->password = password_hash($model->password, PASSWORD_BCRYPT);
                    $user->reset    = NULL;
                    $mess           = ($user->save()) ? Yii::t('frontend', 'Password successfully changed') : Yii::t('frontend', 'Failed to change password');
                    Yii::$app->session->setFlash('message', $mess);

                    return $this->redirect('/');
                }
            }
            throw new HttpException(404, 'Error');
        }

        if (\Yii::$app->request->get()) {
            if (!$mail or !$str) {
                return $this->goHome();
            }
            $user = Users::findOne(['email' => $mail]);
            if ($user and $user->reset === $str) {
                $model->email = $mail;

                return $this->render('change', compact('model'));
            } else {
                throw new HttpException(404, 'Error');
            }
        }
        throw new HttpException(404, 'Error');
    }

}

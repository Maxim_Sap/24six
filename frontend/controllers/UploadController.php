<?php

namespace frontend\controllers;

use Yii;
use common\models\UserProfile;
use yii\web\Response;
use yii\web\Controller;
use yii\web\BadRequestHttpException;

//use app\common\helpers\Translator;
//use yii\validators\InlineValidator;

class UploadController extends Controller
{

    public function actionIndex()
    {

        throw new BadRequestHttpException('404 Error');
    }

    public function actionUploadPhoto()
    {

        $model = new \common\components\UploadImg\ModelUploadImg();
        if (Yii::$app->request->isAjax) {
            $model->setPath('source/temp-image/');
            $model->image = \yii\web\UploadedFile::getInstanceByName('image');

            return $model->upload();
        }
    }

    public function actionCropPhoto()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $photoName                  = Yii::$app->request->post('photoName');
        $profileId                  = Yii::$app->request->post('profile_id');
        if (!isset($photoName) || trim($photoName) == '' || !isset($profileId) || !is_numeric($profileId)) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $imageCoordinates = Yii::$app->request->post('imageCoordinates');

        if (!isset($imageCoordinates) || !is_array($imageCoordinates) || !array_filter($imageCoordinates, 'is_numeric') || count($imageCoordinates) != 4) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $imageSize = Yii::$app->request->post('imageSize');
        if (!isset($imageSize) || !is_array($imageSize) || !array_filter($imageSize, 'is_numeric') || count($imageSize) != 2) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $cropType = Yii::$app->request->post('type');

        $cropTypes = ['avatar','background_image','logo'];
        if (!in_array($cropType, $cropTypes)) {
            return [
                'success' => FALSE,
                'message' => 'Failed to update the object for unknown reason.',
            ];
        }

        $owner = FALSE;
        $profile_query = false;
        switch ($cropType) {
            case 'avatar':
                //доп проверка
                $profile_img_name = null;
                $profile_query = UserProfile::findOne(['user_id'=>$profileId]);
                if(!empty($profile_query)){
                    $profile_img_name = $profile_query->avatar;
                }
                if ($photoName == $profile_img_name) {
                    $owner = TRUE;
                };
                break;
            case 'logo':
                //доп проверка
                $profile_img_name = null;
                $profile_query = UserProfile::findOne(['user_id'=>$profileId]);
                if(!empty($profile_query)){
                    $profile_img_name = $profile_query->logo;
                }
                if ($photoName == $profile_img_name) {
                    $owner = TRUE;
                };
                break;
            case 'background_image':
                //доп проверка
                $profile_img_name = null;
                $profile_query = UserProfile::findOne(['user_id'=>$profileId]);
                if(!empty($profile_query)){
                    $profile_img_name = $profile_query->background_image;
                }
                if ($photoName == $profile_img_name) {
                    $owner = TRUE;
                };
                break;
            case 'gallery':
                break;
        }

        if (!$owner) {
            return [
                'success' => FALSE,
                'message' => 'Access Denied',
            ];
        }

        $result = $this->cropImage($cropType, $photoName, $imageCoordinates, $imageSize);
        if ($result && $profile_query) {
            switch ($cropType) {
                case 'avatar':
                    $profile_query->avatar = $result;
                    break;
                case 'logo':
                    $profile_query->logo = $result;
                    break;
                case 'background_image':
                    $profile_query->background_image = $result;
                    break;
                case 'gallery':
                    break;
            }

            if (!$profile_query->save()) {
                return [
                    'success' => FALSE,
                    'message' => $profile_query->errors,
                ];
            }

            return [
                'success' => TRUE,
                'message' => 'image was crop',
            ];

        }

        return [
            'success' => FALSE,
            'message' => 'Error crop',
        ];
    }

    private function cropImage($cropType, $photoName, $imageCoordinates, $imageSize)
    {
        list($x1, $y1, $x2, $y2) = $imageCoordinates;

        $originalPath = Yii::getAlias('@app/web/source/img/' . $cropType . "/origin/" . $photoName);
        $width        = $imageSize[0]; //image width on front end
        $height       = $imageSize[1]; //image width on front end

        $newWidth  = $x2 - $x1;
        $newHeight = $y2 - $y1;

        $widthRatio  = $newWidth / $width;
        $heightRatio = $newHeight / $height;

        $result = $this->cropOneImage($cropType, $photoName, $originalPath, $width, $height, $widthRatio, $heightRatio, $x1, $y1);

        return $result;

    }

    private function cropOneImage($cropType, $photoName, $file, $width, $height, $widthRatio, $heightRatio, $x1, $y1)
    {
        $originalSize   = getimagesize($file);
        $originalWidth  = $originalSize[0];
        $originalHeight = $originalSize[1];
        $xRatio         = $width / $originalWidth;
        $yRatio         = $height / $originalHeight;
        $newWidth       = round($originalWidth * $widthRatio);
        $newHeight      = round($originalHeight * $heightRatio);
        $newX           = round($x1 / $xRatio);
        $newY           = round($y1 / $yRatio);
        $image          = Yii::$app->image->load($file);

        $extension = pathinfo($file);
        $extension = $extension['extension'];

        $image->crop($newWidth, $newHeight, $newX, $newY);

        // delete old photos
        $thumb = str_replace('/origin/', '/thumb/', $file);
        @unlink($thumb);

        $icon = str_replace('/origin/', '/icon/', $file);
        @unlink($icon);

        $newName    = md5($file . time());
        $originPath = 'source/img/' . $cropType . '/origin/';
        $thumbPath  = 'source/img/' . $cropType . '/thumb/';
        $iconPath   = 'source/img/' . $cropType . '/icon/';

        $originName = $originPath . $newName . '.' . $extension;
        $thumbName  = $thumbPath . $newName . '.' . $extension;
        $iconName   = $iconPath . $newName . '.' . $extension;


        switch ($cropType) {
            case 'avatar':
                $image->resize(200, 200, \yii\image\drivers\Image::WIDTH);
                $image->save($thumbName);
                $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                $image->save($iconName);
                break;
            case 'logo':
                $image->resize(160, 160, \yii\image\drivers\Image::WIDTH);
                $image->save($thumbName);
                $image->resize(64, 64, \yii\image\drivers\Image::WIDTH);
                $image->save($iconName);
                break;
            case 'background_image':
                $image->resize(1110, 340, \yii\image\drivers\Image::HEIGHT);
                $image->save($thumbName);
                break;
            case 'gallery':
                break;
        }
        @rename($file, $originName);

        return $newName . '.' . $extension;
    }


}

<?php
namespace frontend\controllers;

use common\models\BonusPlans;
use common\models\CategoriesTypeEvent;
use common\models\Events;
use common\models\Partners;
use common\models\Plans;
use common\models\PlansWithBonus;
use common\models\SpaceWork;
use common\models\UserAssnPayments;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\WebsiteFaq;
use common\models\AnswerForQuestion;
use common\models\User;
use common\models\UserProfile;
use common\models\Questions;
use common\models\CustomerFeedback;
use yii\data\ActiveDataProvider;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $js_script = "";
        $this->view->registerJs($js_script, Yii\web\View::POS_READY);
        $review = $review = CustomerFeedback::getFeedbackCustomers(CustomerFeedback::TYPE_EVENT_FEEDBACK);
        $allFeedbackCustomers = CustomerFeedback::getFeedbackCustomers();
        $setEvents = Events::getSetsEvents();
        return $this->render('index-events',[
            'setEvents' => $setEvents,
            'allFeedbackCustomers' => $allFeedbackCustomers,
            'review' => $review,
            'getLastEvents' => Events::getLastEvents(),
			'plans' => UserProfile ::getPlansForFront(),
        ]);
    }

    public function actionIndexEvents()
    {

        $js_script = "";
		$this->view->registerJs($js_script, Yii\web\View::POS_READY);

		$review = $review = CustomerFeedback::getFeedbackCustomers(CustomerFeedback::TYPE_EVENT_FEEDBACK);
//		$getCategory = $this->getCategory(Events::tableName());
        $setEvents = Events::getSetsEvents();
        return $this->render('index-events',[
            'setEvents' => $setEvents,
        	'review' => $review,
			'getLastEvents' => Events::getLastEvents(),
			'plans' => UserProfile ::getPlansForFront(),
		]);
    }

    public function actionIndexPlaces()
    {

        $js_script = "$('.vertical_slider').slick({
        centerMode: true,
        centerPadding: '0',
        /*slidesToShow: 1,*/
        adaptiveHeight: true,
        vertical: true,
        verticalSwiping: true,
        prevArrow:'<span class=\"slick-prev\"></span>',
        nextArrow:'<span class=\"slick-next\"></span>'
    });";
        $this->view->registerJs($js_script, Yii\web\View::POS_READY);

		$review = CustomerFeedback::getFeedbackCustomers(CustomerFeedback::TYPE_PLACE_FEEDBACK);
		$getCategory = $this->getCategory(SpaceWork::tableName());
		$setPlaces = SpaceWork::getSetsPlaces();
		$partners = Partners::find()->asArray()->all();

        return $this->render('index-places',[
            'setPlaces' => $setPlaces,
        	'getCategory' => $getCategory,
        	'review' => $review,
            'getLastPlaces' => SpaceWork::getLastPlaces(),
			'plans' => UserProfile ::getPlansForFront(),
			'partners' => $partners,
		]);
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */

	public function actionFaq()
	{
		$searchQuestion = Yii::$app->request->get('searchQuestion');
		$faq = (new Query())
			->from(Questions::tableName()." quest");
		if(!empty($searchQuestion)) {
			$faq->leftJoin(AnswerForQuestion::tableName()." ans_for_que",'ans_for_que.id_question = quest.id')
				->select('ans_for_que.id_question as id_question, quest.title, quest.description, quest.id')
				->groupBy(['id_question','quest.id'])
				->andWhere([
					'or',
					['like','ans_for_que.question',$searchQuestion],
					['like','ans_for_que.answer',$searchQuestion],
				]);
		}

		return $this->render('faq',[
			'faq' => $faq->all()
		]);
	}

    public function actionBecomeMember(){
        $this->layout = "withiout_footer";

	    return $this->render('become-member',[
			'plans' => UserProfile ::getPlansForFront(),
        ]);
    }

    public function actionViewFaq()
	{
		$getIdQuestion = Yii::$app->request->get( 'id' );
		$searchQuestionInGroup = Yii::$app->request->get('searchQuestionInGroup');
		$question = (new Query())
			->from(Questions::tableName()." question")
			->select('
				question.title,
				question.description,
				question.id as id_question,
				
				ans_for_que.id as id_ans_que,
				ans_for_que.question,
				ans_for_que.answer
			')
			->leftJoin(AnswerForQuestion::tableName()." ans_for_que",'question.id = ans_for_que.id_question')
			->where(['question.id' => $getIdQuestion]);
			if(!empty($searchQuestionInGroup)) {
				$question->where(['question.id' => $getIdQuestion])
					->andWhere([
					'or',
					['like','ans_for_que.question',$searchQuestionInGroup],
					['like','ans_for_que.answer',$searchQuestionInGroup],
				]);
			};
		return $this->render('view-faq',[
			'question' => $question->all(),
		]);
	}

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        //$this->layout = "withiout_header_footer";
        $this->layout = "withiout_footer";
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('message', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('message', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            //var_dump($model->errors);die;
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $js_script = "$('.three_things_slider').slick({
		arrows: false,
		slidesPerRow: 3,
		slidesToShow: 3,
		аccessibility: false,
		infinite: false,
		edgeFriction: 0,

		responsive: [{
			breakpoint: 1024,
			settings: {
				variableWidth: true,
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		}]
  });";
        $this->view->registerJs($js_script, Yii\web\View::POS_READY);

        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if(yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function getCategory($tableName)
	{
		$requestCategory = [];
		$query = new Query();
		if($tableName) {
			switch($tableName) {
				case SpaceWork::tableName() :
					$query = SpaceWork::getPlaceInCategory($tableName);
					break;
			}
			$requestCategory = $query->all();
		}
		return $requestCategory;
	}

	public function actionCheckPaymentsPlans(){

        return UserAssnPayments::checkCurrentPlans();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    /*public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }*/

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    /*public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }*/
}

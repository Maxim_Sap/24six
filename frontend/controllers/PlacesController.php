<?php

	namespace frontend\controllers;

	use common\models\BookingConferenceRoom;
	use common\models\ConfRooms;
	use common\models\CustomFields;
	use common\models\Events;
	use common\models\PopularPlaces;
	use common\models\ResourcesEventsPlaces;
	use common\models\UserProfile;
	use Yii;
	use yii\db\ActiveRecord;
	use yii\web\Controller;
	use common\models\User;
	use yii\filters\AccessControl;
	use yii\web\HttpException;
	use yii\web\Response;
	use common\models\CategoriesToPlase;
	use yii\base\InvalidParamException;
	use yii\web\BadRequestHttpException;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use yii\widgets\Pjax;
	use yii\db\Expression;
	use common\models\Favorites;
	use common\models\SpaceWork;

	class PlacesController extends Controller
	{

		public function actionIndex()
		{
			$my_id              = [];
			$sort_place         = Yii ::$app -> request -> get( 'sort_place' );
			$get_date           = Yii ::$app -> request -> get( 'get-date' );
			$categoryToPlace_id = Yii ::$app -> request -> get( 'cat_id' );
			$categoryToPlace_id = is_numeric( $categoryToPlace_id ) ? $categoryToPlace_id : FALSE;

			$keySearchTypePlace = Yii ::$app -> request -> get( 'keysearch' );
			$keySearchTypePlace = $keySearchTypePlace ? strip_tags( $keySearchTypePlace ) : FALSE;
			try
			{
				if( !Yii ::$app -> user -> isGuest )
				{
					$my_id = (int)Yii ::$app -> user -> identity -> getId();
				}
			}
			catch( \Exception $e )
			{
				$my_id = NULL;
			}

			// выборка всех категорий событий
			$categoryToPlaces = CategoriesToPlase ::find() -> all();

			//Выборка всех мест
			/**
			 * $my_id идентефикатор залогиненого пользователя
			 * $categoryTypeEvent_id передаем идентефикатор категории для выборки события по конкретной категории
			 * $keySearchTypeEvent поиск по названию,
			 * $get_date поиск места по конкретной дате - в конкретной дате
			 * $sort_event параметр для сортировки, здесь передаются гет параметры такие как
			 * new-place
			 * popular-place
			 * my-favorite
			 */
			$places = SpaceWork ::getPlaces(
				$my_id,
				$categoryToPlace_id,
				$keySearchTypePlace,
				$get_date,
				$sort_place
			);

			$queryPlaces = new ActiveDataProvider(
				[
					'query'      => $places,
					'pagination' => [
						'pageSize' => 6,
					],
				]
			);

			return $this -> render(
				'index',
				[
					'placesDataProvider' => $queryPlaces,
					'categoryToPlaces'   => $categoryToPlaces,
				]
			);
		}

		public function actionSetFavorite()
		{
			$status = 'added';
			$result = FALSE;
			if( Yii ::$app -> request -> isAjax && $setFavorite = Yii ::$app -> request -> post() )
			{
				$my_id = (int)Yii ::$app -> user -> identity -> getId();
				$row   = PopularPlaces ::find()
									   -> where( [ 'user_id' => $my_id ] )
									   -> andWhere( [ 'id_place' => $setFavorite[ 'id_places' ] ] )
									   -> one();
				if( $row == NULL )
				{
					$favorite             = new PopularPlaces();
					$favorite -> user_id  = $my_id;
					$favorite -> id_place = $setFavorite[ 'id_places' ];
					if( $favorite -> save() )
					{
						$status = 'added';
						Favorites ::setFavorite( $my_id, $setFavorite[ 'id_places' ], Favorites::TYPE_PLACES );
					}
				}
				else
				{
					if( $row -> delete() )
					{
						$status = 'remove';
						Favorites ::removeFavorite( $my_id, $setFavorite[ 'id_places' ], Favorites::TYPE_PLACES );
					}
				}
				$result = TRUE;
			}

			return json_encode(
				[
					'success' => $result,
					'status'  => $status,
				]
			);
		}

		public function actionView()
		{
			$getIdPlaces = Yii ::$app -> request -> get( 'id' );

			$postPaginationSpacePrev = SpaceWork ::getNextOrPrevSpacePost( $getIdPlaces, '<' );
			$postPaginationSpaceNext = SpaceWork ::getNextOrPrevSpacePost( $getIdPlaces, '>' );

			$getFirstSpacePost = SpaceWork ::getLastOrFirstEventPost( $getIdPlaces, 'first' );
			$getLastSpacePost  = SpaceWork ::getLastOrFirstEventPost( $getIdPlaces, 'last' );

			$getCustomField  = CustomFields ::find() -> where( [ 'source_id' => $getIdPlaces ] )
											-> andWhere( [ 'type_source' => Favorites::TYPE_PLACES ] )
											-> all();
			$getSinglePlaces = SpaceWork ::find()
										 -> where( [ 'id' => $getIdPlaces ] )
										 -> with( 'confRooms' )
										 -> one();

			return $this -> render(
				'view',
				[
					'getSinglePlace'          => $getSinglePlaces,
					'getCustomField'          => $getCustomField,
					'postPaginationSpacePrev' => $postPaginationSpacePrev,
					'postPaginationSpaceNext' => $postPaginationSpaceNext,
					'getFirstSpacePost'       => $getFirstSpacePost,
					'getLastSpacePost'        => $getLastSpacePost,
				]
			);
		}

		/* ####### ! ####### */
		public function actionSetForm()
		{
			if( Yii ::$app -> request -> isAjax )
			{
				$model             = new SpaceWork();
				$model -> form_id  = Yii ::$app -> request -> post( 'form' );
				$model -> place_id = Yii ::$app -> request -> post( 'model' );
				return $model -> getForm();
			}
		}

		public function actionCheckDate()
		{
			$post = Yii ::$app -> request -> post();
			if( Yii ::$app -> request -> isAjax )
			{
				if( $post[ 'typePlace' ] == UserProfile::TYPE_PLACE_ONE_DAY )
				{
					$row = ResourcesEventsPlaces ::find()
												 -> select( 'date, date_end' )
												 -> where( [ 'date' => $post[ 'checkDate' ] ] )
												 -> andWhere( [ 'type_place' => UserProfile::TYPE_PLACE_ONE_DAY ] )
												 -> andWhere( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
												 -> count();

					if( !empty( $row ) && isset( $row ) )
					{
						return json_encode(
							[
								'countSpace' => $row,
							]
						);
					}
				}
			}
		}

		public function actionCheckConferenceRoomTime()
		{
			$post = Yii ::$app -> request -> post();
			if( Yii ::$app -> request -> isAjax )
			{
				$time_start = ConfRooms ::find()
										-> select( 'working_time_start' )
										-> where( [ 'id' => $post[ 'conferenceRoom_id' ] ] )
										-> orderBy( [ 'working_time_start' => SORT_ASC ] )
										-> one();

				$time_end = ConfRooms ::find()
									  -> select( 'working_time_end' )
									  -> where( [ 'id' => $post[ 'conferenceRoom_id' ] ] )
									  -> orderBy( [ 'working_time_end' => SORT_DESC ] )
									  -> one();


				$time_start_mark = Yii ::$app -> formatter -> asTime( $time_start -> working_time_start, 'php:G' );
				$time_end_mark = Yii ::$app -> formatter -> asTime( $time_end -> working_time_end, 'php:G' );

				$checkTimeStart = ( new BookingConferenceRoom() ) -> getAllTimeMyOrderInConferenceRoom( $post[ 'conferenceRoom_id' ], $post[ 'checkDate' ] );

				$options = '';
				for( $i = $time_start_mark; $i <= $time_end_mark; $i++ )
				{
					$disabled = ( in_array( $i,$checkTimeStart)) ? 'disabled' : '';
					$options .= "<option ".$disabled." value='".$i."'>".$i.":00</option>";
				}
					return json_encode(
						[
							'html' => $options,
						]
					);
			}
		}

		public function actionCheckOrderTimeConferenceRoom()
		{
			$post = Yii ::$app -> request -> post();
			if( Yii ::$app -> request -> isAjax )
			{
				$array_time = [];
				for( $i = $post[ 'timeStartConf' ]; $i <= $post[ 'timeEndConf']; $i++ )
				{
					$conferenceTime = BookingConferenceRoom::convertToTime( $i );
					$array_time[] .= $conferenceTime;
				}
				$checkTimeStart = ( new BookingConferenceRoom() ) -> getOrderMyConferenceRoom( $post[ 'conferenceRoom_id' ],$post['date'],$array_time );
				return  json_encode(['errors' => $checkTimeStart]);
			}
		}

	}
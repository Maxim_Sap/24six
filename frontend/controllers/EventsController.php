<?php

	namespace frontend\controllers;

	use common\models\BookingConferenceRoom;
	use common\models\CategoriesTypeEvent;
	use common\models\Countryes;
	use common\models\CustomFields;
	use common\models\Events;
	use common\models\Lectures;
	use common\models\LecturesAssnSpeakers;
	use common\models\PartnerEvents;
	use common\models\Partners;
	use common\models\PopularEvents;
	use common\models\ResourcesEventsPlaces;
	use common\models\Speakers;
	use common\models\UserAssnPayments;
	use common\models\UserBonus;
	use common\models\UserMainPersonalBonus;
	use common\models\UserProfile;
	use Yii;
	use yii\db\ActiveRecord;
	use yii\web\Controller;
	use common\models\User;
	use yii\filters\AccessControl;
	use yii\web\HttpException;
	use yii\web\Response;
	use yii\base\InvalidParamException;
	use yii\web\BadRequestHttpException;
	use yii\data\ActiveDataProvider;
	use yii\db\Query;
	use yii\db\Expression;
	use common\models\Favorites;

	class EventsController extends Controller
	{

		public function actionIndex()
		{
			$my_id                = [];
			$sort_event           = Yii ::$app -> request -> get( 'sort_event' );
			$get_date             = Yii ::$app -> request -> get( 'get-date' );
			$categoryTypeEvent_id = Yii ::$app -> request -> get( 'cat_id' );
			$categoryTypeEvent_id = is_numeric( $categoryTypeEvent_id ) ? $categoryTypeEvent_id : FALSE;

			$keySearchTypeEvent = Yii ::$app -> request -> get( 'keysearch' );
			$keySearchTypeEvent = $keySearchTypeEvent ? strip_tags( $keySearchTypeEvent ) : FALSE;

			try
			{
				if( !Yii ::$app -> user -> isGuest )
				{
					$my_id = (int)Yii ::$app -> user -> identity -> getId();
				}
			}
			catch( \Exception $e )
			{
				$my_id = NULL;
			}
			// выборка всех категорий событий
			$categoryTypeEvent = CategoriesTypeEvent ::find() -> all();
			$countriesEvent    = Countryes ::find() -> all();

			//Выборка всех собитый
			/**
			 * $my_id идентефикатор залогиненого пользователя
			 * $categoryTypeEvent_id передаем идентефикатор категории для выборки события по конкретной категории
			 * $keySearchTypeEvent поиск по названию,
			 * $get_date поиск события по конкретной дате - в конкретной дате
			 * $sort_event параметр для сортировки, здесь передаются гет параметры такие как
			 * new-event
			 * upcoming-events
			 * popular-events
			 * my-favorite
			 */
			$events = Events ::getEvents(
				$my_id,
				$categoryTypeEvent_id,
				$keySearchTypeEvent,
				$get_date,
				$sort_event
			);

			$queryEvents = new ActiveDataProvider(
				[
					'query'      => $events,
					'pagination' => [
						'pageSize' => 6,
					],
				]
			);
			return $this -> render(
				'index',
				[
					'eventsDataProvider' => $queryEvents,
					'categoryTypeEvents' => $categoryTypeEvent,
					'countriesEvent'     => $countriesEvent,
				]
			);
		}

		public function actionSetFavorite()
		{

			$status          = 'added';
			$result          = FALSE;
			$checkMyFavorite = [];
			if( Yii ::$app -> request -> isAjax && $setFavorite = Yii ::$app -> request -> post() )
			{
				$my_id = (int)Yii ::$app -> user -> identity -> getId();
				$row   = PopularEvents ::find()
									   -> where( [ 'user_id' => $my_id ] )
									   -> andWhere( [ 'id_event' => $setFavorite[ 'id_event' ] ] )
									   -> one();
				if( $row == NULL )
				{
					$favorite             = new PopularEvents();
					$favorite -> user_id  = $my_id;
					$favorite -> id_event = $setFavorite[ 'id_event' ];

					if( $favorite -> save() )
					{
						$status = 'added';
						Favorites ::setFavorite( $my_id, $setFavorite[ 'id_event' ], Favorites::TYPE_EVENTS );
					}
				}
				else
				{
					if( $row -> delete() )
					{
						$status = 'remove';
						Favorites ::removeFavorite( $my_id, $setFavorite[ 'id_event' ], Favorites::TYPE_EVENTS );
						$queryMyFavorite = Favorites ::checkMyFavorite( $my_id, Events ::tableName() );
						$checkMyFavorite = $queryMyFavorite -> all();
					}
				}
				$result = TRUE;
			}

			return json_encode(
				[
					'checkMyFavorite' => $checkMyFavorite,
					'success'         => $result,
					'status'          => $status,
				]
			);
		}

		public function actionView()
		{
			$my_id       = [];
			$getMyBonus  = [];
			$getIdEvents = Yii ::$app -> request -> get( 'id' );
			try
			{
				if( !Yii ::$app -> user -> isGuest )
				{
					$my_id      = Yii ::$app -> user -> identity -> getId();
					$getMyBonus = UserMainPersonalBonus ::getMyBonus( $my_id );
				}
			}
			catch( \Exception $e )
			{
				$my_id      = NULL;
				$getMyBonus = NULL;
			}
			$checkEvent = Events ::findOne( $getIdEvents );
			if( empty( $checkEvent ) && !isset( $checkEvent ) )
			{
				Yii ::$app -> session -> setFlash( 'message', 'Event not found' );
				return Yii ::$app -> response -> redirect( '/events' );
			}
			$postPaginationEventPrev = Events ::getNextOrPrevEventPost( $getIdEvents, '<' );
			$postPaginationEventNext = Events ::getNextOrPrevEventPost( $getIdEvents, '>' );

			$getFirstPost = Events ::getLastOrFirstEventPost( $getIdEvents, 'first' );
			$getLastPost  = Events ::getLastOrFirstEventPost( $getIdEvents, 'last' );

			$countTheTickets = Events ::checkTickets( $getIdEvents );

			$getLecture   = LecturesAssnSpeakers ::getLecturesAndSpeakers( $getIdEvents );
			$lecture_mass = [];
			if( !empty( $getLecture ) )
			{
				foreach( $getLecture as $lecture )
				{
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'lecture_id' ] = $lecture[ 'id_event' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'time_from' ]  = $lecture[ 'time_from' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'time_to' ]    = $lecture[ 'time_to' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'subject' ]    = $lecture[ 'subject' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'name' ]       = $lecture[ 'name' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'avatar' ]     = $lecture[ 'avatar' ];
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'position' ]   = $lecture[ 'position' ];
					unset( $lecture[ 'time_to' ] );
					unset( $lecture[ 'time_from' ] );
					unset( $lecture[ 'id_event' ] );
					unset( $lecture[ 'subject' ] );
					$lecture_mass[ $lecture[ 'lecture_id' ] ][ 'speakers' ][] = $lecture;
				}
			}
			$getCustomField = CustomFields ::find() -> where( [ 'source_id' => $getIdEvents ] )
										   -> andWhere( [ 'type_source' => Favorites::TYPE_EVENTS ] )
										   -> all();

			$getPartnerEvent = PartnerEvents ::getPartnerEvents( $getIdEvents );

			if( !Yii ::$app -> user -> isGuest )
			{
				$getMyBonus = UserMainPersonalBonus ::getMyBonus( $my_id );
			}
			$getSingleEvent = Events ::find() -> where( [ 'id' => $getIdEvents ] ) -> one();
			return $this -> render(
				'view',
				[
					'getPartnerEvent'         => $getPartnerEvent,
					'lecture_mass'            => $lecture_mass,
					'getCustomField'          => $getCustomField,
					'countTheTickets'         => $countTheTickets,
					'getSingleEvent'          => $getSingleEvent,
					'getMyBonus'              => $getMyBonus,
					'postPaginationEventPrev' => $postPaginationEventPrev,
					'postPaginationEventNext' => $postPaginationEventNext,
					'getFirstPost'            => $getFirstPost,
					'getLastPost'             => $getLastPost,
				]
			);
		}

		public static function actionRepeatEvent()
		{
			$getEventForRepeat = Events ::find() -> where( [ 'type_event' => Favorites::TYPE_REPEAT_EVENT ] ) -> all();
			foreach( $getEventForRepeat as $item )
			{
				if( $item -> date_event < date( "Y-m-d H:i:s", time() ) )
				{
					$item -> scenario   = Events::SCENARIO_UPDATE_DATE;
					$item -> date_event = date( "Y-m-d H:i:s", time() + 14 * 60 * 24 * 30 );
					if( !$item -> save() )
					{
						dd( $item -> errors );
					}
				}
			}
		}

		public function actionUpdateStatusTicket()
		{


			// cron for bonuses user
			UserMainPersonalBonus ::deleteAll( [ '<', 'date_end', date( 'Y-m-d: H:i:s', strtotime('00:00:00' ) ) ] );

			//cron for status plan users
			$userAssPayment = UserAssnPayments ::find()
											   -> where( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
											   -> all();
			foreach( $userAssPayment as $item )
			{
				if( $item -> date_end == date( "Y-m-d H:i:s", strtotime('00:00:00' ) ) )
				{
					$userAssPaymentModel             = UserAssnPayments ::findOne( $item -> id );
					$userAssPaymentModel -> scenario = UserAssnPayments::SCENARIO_UPDATE_STATUS_ASSN_PLANS;
					$userAssPaymentModel -> status   = ResourcesEventsPlaces::NO_ACTIVE_TICKETS;
					if( !$userAssPaymentModel -> save() )
					{
						var_dump( $userAssPaymentModel -> errors );
						die();
					}
				}
			}

			// cron for tickets in place
			$myTicketsConfRoom = BookingConferenceRoom ::find()
											   -> where( [ 'status' => ResourcesEventsPlaces::ACTIVE_TICKETS ] )
											   -> all();
			foreach( $data = $myTicketsConfRoom as $myTicketConfRoom )
			{
				if( $myTicketConfRoom -> date < date( 'Y-m-d H:i:s', strtotime('00:00:00' ) ) )
				{
					$model             = BookingConferenceRoom ::findOne( $myTicketConfRoom -> id );
					$model -> scenario = BookingConferenceRoom::SCENARIO_UPDATE_STATUS_PLACE_FOR_TICKETS;
					$model -> status   = ResourcesEventsPlaces::NO_ACTIVE_TICKETS;
					if( !$model -> save() )
					{
						dd( $model -> errors );
					}
				}
			}

			// cron for ticket one day place
			$myTicketsPlaces = ResourcesEventsPlaces ::getTickets( '', Favorites::TYPE_PLACES );
			foreach( $myTicketsPlaces as $myTicketPlace )
			{
				if( $myTicketPlace[ 'date_end' ] < date( 'Y-m-d H:i:s',strtotime('00:00:00' ) ) )
				{
					$model             = ResourcesEventsPlaces ::findOne( $myTicketPlace[ 'id_ticket' ] );
					$model -> scenario = ResourcesEventsPlaces::SCENARIO_UPDATE_STATUS;
					$model -> status   = ResourcesEventsPlaces::NO_ACTIVE_TICKETS;
					if( !$model -> save() )
					{
						dd( $model -> errors );
					}
				}
			}

			// cron for ticket event
			$myTickets = ResourcesEventsPlaces ::getTickets( '', Favorites::TYPE_EVENTS );
			foreach( $myTickets as $myTicket )
			{
				if( $myTicket[ 'date' ] < date( 'Y-m-d H:i:s',strtotime('00:00:00' ) ) )
				{
					$model             = ResourcesEventsPlaces ::findOne( $myTicket[ 'id_ticket' ] );
					$model -> scenario = ResourcesEventsPlaces::SCENARIO_UPDATE_STATUS;
					$model -> status   = ResourcesEventsPlaces::NO_ACTIVE_TICKETS;
					if( !$model -> save() )
					{
						dd( $model -> errors );
					}
				}
			}
		}
	}